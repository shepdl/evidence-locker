# --- !Ups
ALTER TABLE `lti_tool_consumers` ADD COLUMN `is_active` TINYINT NOT NULL AFTER `name`;


# --- !Downs
ALTER TABLE `lti_tool_consumers` DROP COLUMN `is_active`;

