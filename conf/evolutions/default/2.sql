# --- !Ups
DROP TABLE IF EXISTS `documents`;

CREATE TABLE IF NOT EXISTS `documents` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `assignment_id` BIGINT NOT NULL,
  `system_filename` TEXT,
  `original_filename` TEXT,
  `file_type` VARCHAR(10),
  `uploader_id` BIGINT NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`),

  CONSTRAINT `document_uploader_id`
    FOREIGN KEY (`uploader_id`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `document_assignment_id`
    FOREIGN KEY (`assignment_id`)
    REFERENCES `assignments` (`id`)
    ON DELETE CASCADE
    on UPDATE NO ACTION

) ENGINE=InnoDB;

INSERT INTO `documents` (
    assignment_id, system_filename, original_filename, file_type, uploader_id,
    created_at, updated_at
) SELECT 
    id, filename, filename, 'HTML', instructor_id, created_at, updated_at
FROM `assignments` 
;


ALTER TABLE `assignments` 
  DROP COLUMN `filename`
;


# --- !Downs
UPDATE `assignments`, `documents`
  SET `assignments`.`filename` = `documents`.`system_filename` 
    WHERE `assignments`.`id` = `documents`.`assignment_id`;

DROP TABLE `documents`;

