# --- !Ups

CREATE TABLE IF NOT EXISTS `lti_launch_requests` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `reported_tool_consumer_guid` VARCHAR(100) NOT NULL,
  `provided_consumer_key` VARCHAR(50),
  `found_tool_consumer_id` BIGINT,
  `provided_context_id` VARCHAR(255) NOT NULL,
  `found_context_id` BIGINT,
  `provided_user_guid` VARCHAR(100) NOT NULL,
  `found_user_id` BIGINT,
  `attempted_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE  = InnoDB;

# --- !Downs
DROP TABLE IF EXISTS `lti_launch_requests`