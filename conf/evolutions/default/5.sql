# --- !Ups
ALTER TABLE `assignments` CHANGE `rubric_type` `rubric_type` ENUM('numeric', 'textual', 'both');

# --- !Downs
ALTER TABLE `assignments` CHANGE `rubric_type` `rubric_type` ENUM('numeric', 'textual');
