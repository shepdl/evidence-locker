# --- !Ups
ALTER TABLE `lti_tool_consumers`
  ADD COLUMN `api_key` VARCHAR(255) NULL AFTER `shared_secret`,
  ADD COLUMN `lms_type` VARCHAR(255) NULL AFTER `shared_secret`
;

# --- !Downs
ALTER TABLE `lti_tool_consumers`
  DROP COLUMN `api_key`,
  DROP COLUMN `lms_type`
;

