# --- !Ups
ALTER TABLE `annotations`
  ADD COLUMN `page_no` VARCHAR(10) AFTER `end_offset`
;

# --- !Downs
ALTER TABLE `annotations`
  DROP COLUMN `page_no`
;

