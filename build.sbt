import play.sbt.PlayImport.PlayKeys.playRunHooks

name := """Evidence Locker"""

version := "0.2.003-debugging"

scalaVersion := "2.11.8"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

libraryDependencies ++= Seq(
  cache,
  ws,
  "ch.qos.logback" % "logback-classic" % "1.1.+",
  "com.typesafe.play" %% "play-mailer" % "5.0.0"
)

libraryDependencies ++= Seq(
  "javax.inject" % "javax.inject" % "1",
  "com.google.inject" % "guice" % "4.0"
)

libraryDependencies ++= Seq(
  jdbc,
  "mysql" % "mysql-connector-java" % "6.0.4",
  "com.typesafe.play" %% "anorm" % "2.5.2",
  evolutions
)

// Testing
libraryDependencies += specs2 % Test
libraryDependencies += "org.specs2" %% "specs2-matcher-extra" % "3.8.4" % Test


libraryDependencies += "org.imsglobal" % "basiclti-util" % "1.1.2"
libraryDependencies += "javax.servlet" % "servlet-api" % "2.5"



libraryDependencies ++= Seq(
  "de.svenkubiak" % "jBCrypt" % "0.4.1"
)

libraryDependencies ++= Seq(
  "org.hamcrest" % "hamcrest-all" % "1.3" % "test"
)


// run gulp
playRunHooks += RunSubProcess("gulp")

javaOptions in Test += "-Dconfig.file=conf/test.conf"

routesGenerator := InjectedRoutesGenerator
