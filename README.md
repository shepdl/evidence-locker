Evidence Locker
=================================

Evidence Locker is an [LTI](https://www.imsglobal.org/activity/learning-tools-interoperability)-enabled learning tool for teaching literary analysis. An instructor provides students with a text and some themes to search for, and students select passages that contain these themes and write explications for these passages. Students may then review each others' work. 

This project is currently in development at UCLA's [Center for Digital Humanities](https://cdh.ucla.edu).


Developing
===

To build this project, you will need SBT (and hence Java) and Gulp (and hence Node.js).

Install Java, SBT, and Node.js by following the instructions given for your system. To install Gulp,
simply type ```npm install -g gulp``` and ```npm install -g bower``` once Node.js is installed on the
command line. After that, run ```npm install``` and ```bower install``` to download all the dependencies.

The build process is mostly controlled by SBT. To run, start SBT in the directory of this project (the project
this file is in):

```
evidence-locker $> sbt
```

Then, in SBT, you should see some status info, and then a command prompt:

```
[Evidence Locker] $
```

* To run a development server, use the ```run``` command.
* To compile the Scala code in the project, use ```compile```.
* To build a bundle to deploy to the server, use ```dist```. This will build a zip file that has all the
compiled JAR files, plus all the web assets (JavaScript, HTML, and CSS). This file will show up in the
```target/universal``` directory. Upload this file to a web server, decompress it, and then run the
shell script in one of the subdirectories, or add it to the server boot process (ask your sysadmin how
to do this for your brand of Linux).