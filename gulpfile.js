var gulp = require("gulp");
var ngAnnotate = require('gulp-ng-annotate');
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
var ngHtml2Js = require('gulp-ng-html2js');
var minifyHtml = require('gulp-minify-html');
var plumber = require('gulp-plumber');
var gutil = require('gulp-util');
var webpack = require('webpack-stream');
var WebpackDevServer = require('webpack-dev-server');
var replace = require('gulp-replace');
var karma = require('karma');
var sass = require('gulp-sass');

var path = {
  js: ['./app/assets/**/*.js'],
  template: ['./app/assets/**/*.tpl.html'],
  watch: ['app/assets/**/*.*']
};

var requireConfig = {
  baseUrl: __dirname + '/app/assets/scripts/scripts/',
};

gulp.task('js:webpack', ['template:compileMinify'], function (x) {
  return gulp.src(requireConfig.baseUrl + 'main.js')
    .pipe(webpack(require('./webpack.config.js')))
    .pipe(gulp.dest('./public/javascripts/'));
});

gulp.task('template:compileMinify', function () {
  return gulp.src('./app/assets/scripts/scripts/**/*.html')
    .pipe(ngHtml2Js({
      moduleName: 'el.tpl',
      prefix: '/'
    }))
    .pipe(concat('eltpl.js'))
    .pipe(gulp.dest('./app/assets/scripts/scripts/views/'))
});

gulp.task('sass', function () {
    return gulp.src('./app/assets/scripts/styles/main.scss')
            .pipe(sass().on('error', sass.logError))
            .pipe(gulp.dest('./public/stylesheets/'))
    ;
});

gulp.task('webpack-dev-server', function (callback) {

    var compiler = webpack(require('./webpack.config.js'));

    new WebpackDevServer(compiler, {
    }).listen(8080, 'localhost', function (err) {
        if (err) {
            throw new gutil.PluginError('webpack-dev-server', err);
        }
        gutil.log('[webpack-dev-server]', 'http://localhost:8080/webpack-dev-server/index.html');
    });
});

gulp.task('default', ['js:webpack'], function () {
    gulp.watch(path.watch, ['js:webpack']);
});

gulp.task('test-watch', ['test'], function (done) {
    gulp.watch(['app/assets/scripts/**/*.js', 'app/assets/scripts/**/*.html', 'test/**/*.js'], ['test']);
});

gulp.task('test', ['template:compileMinify'], function (done) {
    new karma.Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done).start();
});
