package reviews

import annotations.Explication
import assignments.RubricPrompt
import edu.ucla.cdh.EvidenceLocker.authentication.User
import edu.ucla.cdh.EvidenceLocker.common.TimestampsProperties

/**
  * Created by dave on 8/29/16.
  */
case class PeerReview(id:Long, reviewer:User, explication:Explication, responses:List[PromptResponse]) {
  def addResponse(responses:List[PromptResponse]):PeerReview = PeerReview(id, reviewer, explication, responses)
}


case object PeerReviewProperties extends TimestampsProperties {
  val table = "peer_reviews"
  val id = s"$table.id"

  val isComplete = s"$table.is_complete"
  val explicationId = s"$table.explication_id"
  val reviewerId = s"$table.reviewer_id"

}


case class PromptResponse(id:Long, text:String, prompt:RubricPrompt, score:Int)


case object PromptResponseProperties extends TimestampsProperties {
  val table = "prompt_responses"
  val id = s"$table.id"

  val text = s"$table.text"
  val score = s"$table.score"

  val reviewId = s"$table.peer_review_id"
  val rubricId = s"$table.prompt_id"

}
