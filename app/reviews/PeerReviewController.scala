package reviews

import java.util.Date
import javax.inject.Named

import akka.actor.ActorRef
import annotations._
import assignments._
import authentication.{AssignmentService, AuthenticationService}
import com.google.inject.Inject
import common.JsonErrorReporting
import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import play.api.Logger
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json._
import play.api.mvc.{Action, Controller}
import reviews.ReviewEmailer.SendEmail

/**
  * Created by dave on 8/29/16.
  */
trait PeerReviewController extends JsonErrorReporting with I18nSupport with RendersExplicationsAsJson with RendersThemesAsJson {
  this:Controller =>

  val messagesApi:MessagesApi
  val logger = Logger("PeerReviewController")

  val userRepo:UserRepository
  val explicationRepo:ExplicationRepository
  val assignmentRepo:AssignmentRepository
  val annotationRepo:AnnotationRepository
  val themeRepo:ThemeRepository
  val peerReviewRepo:PeerReviewRepository

  val authenticationService:AuthenticationService
  val assignmentService:AssignmentService

  val reviewEmailer:ActorRef

  def promptResponse2Json(response:PromptResponse) = Json.obj(
    "id" -> response.id,
    if (response.text.nonEmpty) "text" -> response.text
    else "score" -> response.score,

    "rubricId" -> response.prompt.id,
    "rubricText" -> response.prompt.description,
    "rubricMin" -> response.prompt.min,
    "rubricMax" -> response.prompt.max
  )

  def review2Json(review:PeerReview):JsObject = Json.obj (
    "id" -> review.id,
    "explication" -> explicationToJson(review.explication),
    "theme" -> theme2Json(themeRepo.getForAnnotation(review.explication.annotation.id).get),
    "responses" -> review.responses.map(promptResponse2Json),
    "reviewer" -> Json.obj(
      "userRef" -> s"/users/${review.reviewer.id}",
      "name" -> review.reviewer.providedName
    )
  )

  val ASSIGNMENT_COMPLETE = "assignment_complete"
  val PEER_REVIEW_SAVED = "peer_review_saved"
  val FIELDS_MISSING = "fields_missing"
  val UPDATED = "updated"

  def createLock(assignmentId:Long) = Action(parse.urlFormEncoded) { implicit request =>
    request.session.get("userId") match {
      case None => {
        logger.warn("Attempted to get Peer Review without being logged in")
        Forbidden(jsonError("User must be logged in"))
      }
      case Some(userId) => userRepo.getById(userId.toLong) match {
        case None => {
          logger.warn(s"User ${userId} not found")
          Forbidden(jsonError("User not found"))
        }
        case Some(user) => {
          assignmentRepo.getById(assignmentId) match {
            case None => {
              logger.warn("Assignment not found")
              NotFound(jsonError("Assignment not found"))
            }
            case Some(assignment) => {
              peerReviewRepo.createLock(user, assignment) match {
                case Right(review) => {
                  val reviewJson = review2Json(review)

                  val explicationsDone = explicationRepo.getByUserAndAssignment(user, assignment, user) match {
                    case Left(ex) => -1
                    case Right(explications) => explications.size
                  }
                  Ok(reviewJson ++ Json.obj(
                    "reviewsLeft" -> (assignment.numberRequired - explicationsDone),
                    "criteriaType" -> assignment.rubricType,
                    "quote" -> review.explication.annotation.quote
                  ))
                }
                case Left(message:UserHasNoPeerReviewsAvailable) => {
                  Ok(Json obj("message" -> "no_peer_reviews"))
                }
                case Left(message:UserIsFinishedWithAssignment) => {
                  Ok(Json obj(
                    "message" -> ASSIGNMENT_COMPLETE
                    )
                  )
                }
              }
            }
          }
        }
      }
    }
  }

  def createLockForAnnotation(annotationId:Long) = Action(parse.urlFormEncoded) { implicit request =>
    request.session.get("userId") match {
      case None => {
        logger.warn("Attempted to get Peer Review without being logged in")
        Forbidden(jsonError("User must be logged in"))
      }
      case Some(userId) => userRepo.getById(userId.toLong) match {
        case None => {
          logger.warn(s"User ${userId} not found")
          Forbidden(jsonError("User not found"))
        }
        case Some(user) => {
          explicationRepo.getByAnnotationId(user, annotationId) match {
            case Left(ex:AnnotationDoesNotExist) => {
              logger.warn(s"Annotation ${annotationId} not found")
              NotFound(jsonError(s"Annotation ${annotationId} not found"))
            }
            case Left(ex:UserHasNoAccessToAnnotation) => {
              logger.warn(s"User ${user.id} not allowed to view annotation $annotationId")
              Forbidden(jsonError(s"Not allowed to view annotation ${annotationId}"))
            }
            case Right(explication) => {
              explicationRepo.assignmentForExplication(explication) match {
                case Right(assignment) => {
                  peerReviewRepo.createLock(user, assignment) match {
                    case Right(review) => Ok(review2Json(review))
                    case Left(message:UserHasNoPeerReviewsAvailable) => {
                      Ok(Json obj("message" -> "no_peer_reviews"))
                    }
                    case Left(message:UserIsFinishedWithAssignment) => {
                      Ok(Json obj(
                        "message" -> ASSIGNMENT_COMPLETE
                        )
                      )
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  def listByAnnotation(annotationId:Long) = authenticationService.ChecksForUserAction { implicit request =>
    val user = request.user
    explicationRepo.getByAnnotationId(user, annotationId) match {
      case Left(error) => {
        logger.error(error.toString)
        InternalServerError(jsonError(s"Sorry; an internal error occurred"))
      }
      case Right(explication) => {
        peerReviewRepo.listCompletedForExplication(user, explication) match {
          case Left(error) => {
            logger.error(error.toString)
            InternalServerError(jsonError(s"Sorry; an internal error occurred"))
          }
          case Right(reviews) => {
            Ok(Json.obj(
              "reviews" -> reviews.map(review2Json)
            ))
          }
        }
      }
    }
  }

  def getById(reviewId:Long) = Action { implicit request =>
    NotFound("Not implemented")
  }

  def getByUserInAssignment(assignmentId:Long, studentId:Long) = (
    authenticationService.ChecksForUserAction andThen
    assignmentService.ChecksForAccessToAssignment(assignmentId) andThen
    assignmentService.ChecksThatUserIsOwnerOrInstructor(studentId)
    ) { implicit request => {
      val user = request.user
      val assignment = request.assignment
      userRepo.getById(studentId) match {
        case None => NotFound(jsonError(s"Student ${studentId} not found"))
        case Some(student) => {
          peerReviewRepo.getReviewsCompletedByUserForAssignment(student, assignment) match {
            case Left(_) => InternalServerError(jsonError("An internal error occurred"))
            case Right(peerReviews) => Ok(Json.obj(
              "student" -> Json.obj(
                "id" -> student.id,
                "name" -> student.providedName
              ),
              "peerReviews" -> peerReviews.map(review2Json)
            ))
          }
        }
      }
    }
  }

  import play.api.libs.functional.syntax._

  implicit val promptResponseReads: Reads[PromptResponseData] = (
    (JsPath \ "id").read[Long] and (JsPath \ "text").readNullable[String]
      and (JsPath \ "score").readNullable[Int]
    )(PromptResponseData.apply _)

  implicit val peerReviewReads: Reads[PeerReviewData] = (
    (JsPath \ "id").read[Long] and (JsPath \ "responses").read[List[PromptResponseData]]
  )(PeerReviewData.apply _)


  def update(reviewId:Long) = Action(parse.json) { implicit request =>
    request.session.get("userId") match {
      case None => {
        logger.warn("Attempted to get Peer Review without being logged in")
        Forbidden(jsonError("User must be logged in"))
      }
      case Some(userId) => userRepo.getById(userId.toLong) match {
        case None => {
          logger.warn(s"User ${userId} not found")
          Forbidden(jsonError("User not found"))
        }
        case Some(user) => {
          peerReviewRepo.getById(user, reviewId) match {
            case Left(ex:UserHasNoAccessToReview) => Forbidden(jsonError("User cannot view object"))
            case Right(peerReview) => {
              request.body.validate[PeerReviewData].fold(
                errors => {
                  BadRequest(Json.obj("status" -> "KO", "message" -> JsError.toJson(errors)))
                },
                data => {
                  val promptResponseData = data.responses.foldLeft(Map[Long,PromptResponseData]()) {
                    case (acc, response) => acc.updated(response.id, response)
                  }
                  val now = new Date
                  val updatedPeerReview = peerReview.addResponse(
                    peerReview.responses.map(originalResponse => {
                      val correspondingResponse = promptResponseData(originalResponse.id)
                      PromptResponse(
                        originalResponse.id,
                        correspondingResponse.text.getOrElse(""),
                        originalResponse.prompt,
                        correspondingResponse.score.getOrElse(-1)
                      )
                    })
                  )
                  peerReviewRepo.update(user, updatedPeerReview) match {
                    case Right(review) => {
                      reviewEmailer ! SendEmail(updatedPeerReview)
                      Ok(Json obj("message" -> PEER_REVIEW_SAVED))
                    }
                    case _ => {
                      BadRequest(jsonError("Something went wrong with the update"))
                    }
                  }
                }
              )
            }
          }
        }
      }
    }
  }

  def delete(reviewId:Long) = Action { implicit request =>
    NotFound("Not implemented")
  }

}

class DefaultPeerReviewController @Inject() (
  override val messagesApi:MessagesApi,
  @Named("review-emailer") override val reviewEmailer: ActorRef,
  override val userRepo:UserRepository,
  override val explicationRepo:ExplicationRepository,
  override val assignmentRepo:AssignmentRepository,
  override val themeRepo:ThemeRepository,
  override val annotationRepo:AnnotationRepository,
  override val peerReviewRepo:PeerReviewRepository,


  override val authenticationService:AuthenticationService,
  override val assignmentService:AssignmentService
) extends PeerReviewController with Controller


case class PromptResponseData(id:Long, text:Option[String], score:Option[Int])
case class PeerReviewData(id:Long, responses:List[PromptResponseData])

