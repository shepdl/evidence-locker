package reviews

import java.util.Date
import javax.inject.Inject

import annotations._
import anorm._
import assignments._
import common.ELError
import edu.ucla.cdh.EvidenceLocker.authentication.{User, UserProperties, UserRepository}
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import play.api.Logger

/**
  * Created by dave on 8/29/16.
  */

object PeerReviewRepository {
  case object CleanUp
}

class PeerReviewRepository @Inject() (
  dbs:DatabaseService, userRepo:UserRepository, explicationRepo:ExplicationRepository
) {

  val db = dbs.db
  val logger = Logger("PeerReviewRepository")

  import SqlParser._


  def createLock(user:User, assignment:Assignment):Either[ELError,PeerReview] = db.withTransaction(implicit c => {
    val incompleteReviewsByStudent = SQL(
      s"""
         |SELECT ${PeerReviewProperties.id}
         |FROM  ${ThemeProperties.table}, ${AnnotationProperties.table},
         |      ${ExplicationProperties.table}, ${PeerReviewProperties.table}
         |WHERE ${PeerReviewProperties.reviewerId} = {${PeerReviewProperties.reviewerId}}
         |  AND ${PeerReviewProperties.explicationId} = ${ExplicationProperties.id}
         |  AND ${ExplicationProperties.annotationId} = ${AnnotationProperties.id}
         |  AND ${AnnotationProperties.themeId} = ${ThemeProperties.id}
         |  AND ${ThemeProperties.assignmentId} = {${ThemeProperties.assignmentId}}
         |  AND ${PeerReviewProperties.isComplete} = 0
       """.stripMargin).on(
      PeerReviewProperties.reviewerId -> user.id,
      ThemeProperties.assignmentId -> assignment.id
    ).executeQuery()
      .as(long(PeerReviewProperties.id) *).map({case peerReviewId => peerReviewId})

    if (incompleteReviewsByStudent.nonEmpty) {
      getById(user, incompleteReviewsByStudent.head)
    } else {
      val explicationsCompletedByUser = SQL(
        s"""
           |SELECT COUNT(*) AS completeReviews
           |FROM  ${ThemeProperties.table}, ${AnnotationProperties.table},
           |      ${ExplicationProperties.table}, ${PeerReviewProperties.table}
           |WHERE ${PeerReviewProperties.reviewerId} = {${PeerReviewProperties.reviewerId}}
           |  AND ${PeerReviewProperties.explicationId} = ${ExplicationProperties.id}
           |  AND ${ExplicationProperties.annotationId} = ${AnnotationProperties.id}
           |  AND ${AnnotationProperties.themeId} = ${ThemeProperties.id}
           |  AND ${ThemeProperties.assignmentId} = {${ThemeProperties.assignmentId}}
           |  AND ${PeerReviewProperties.isComplete} = 1
         """.stripMargin
      ).on(
        PeerReviewProperties.reviewerId -> user.id,
        ThemeProperties.assignmentId -> assignment.id
      ).executeQuery()
        .as(long("completeReviews").single)
      if (explicationsCompletedByUser == assignment.numberRequired) {
        c.rollback()
        Left(new UserIsFinishedWithAssignment(assignment))
      } else {
        // NOTE: please don't rely on this query to return a precise number of reviews
        // for each explication. It returns 1 if there are 0 or 1 reviews. This generally
        // won't be a problem in the scenario I imagine, so we're going to just live with
        // that for the moment.
        val explicationsThatNeedReviews = SQL(
          s"""
              | SELECT all_explications.explication_id AS explication_id FROM (
              |     SELECT ${ExplicationProperties.id} AS explication_id
              |     FROM ${ExplicationProperties.table}
              |     INNER JOIN ${AnnotationProperties.table} ON ${AnnotationProperties.id} = ${ExplicationProperties.annotationId}
              |     INNER JOIN ${ThemeProperties.table} ON ${AnnotationProperties.themeId} = ${ThemeProperties.id}
              |     WHERE ${ThemeProperties.assignmentId} = {${ThemeProperties.assignmentId}}
              |       AND ${ExplicationProperties.ownerId} != {${ExplicationProperties.ownerId}}
              |       AND ${ExplicationProperties.submitted} = 1
              |   ) AS all_explications
              |   LEFT JOIN (
              |     SELECT ${PeerReviewProperties.explicationId} AS explication_id, COUNT(*) AS peer_reviews_complete
              |     FROM ${PeerReviewProperties.table}
              |     INNER JOIN ${ExplicationProperties.table} ON ${ExplicationProperties.id} = ${PeerReviewProperties.explicationId}
              |     INNER JOIN ${AnnotationProperties.table} ON ${AnnotationProperties.id} = ${ExplicationProperties.annotationId}
              |     INNER JOIN ${ThemeProperties.table} ON ${AnnotationProperties.themeId} = ${ThemeProperties.id}
              |     WHERE ${ThemeProperties.assignmentId} = {${ThemeProperties.assignmentId}}
              |   ) AS explications_that_need_reviews
              |   ON explications_that_need_reviews.explication_id = all_explications.explication_id
              | WHERE
              |   (
              |     explications_that_need_reviews.peer_reviews_complete < {${AssignmentProperties.numberRequired}}
              |     OR explications_that_need_reviews.peer_reviews_complete IS NULL
              |   )
              | AND
              |   explications_that_need_reviews.explication_id NOT IN (
              |     SELECT ${PeerReviewProperties.explicationId}
              |     FROM ${PeerReviewProperties.table}
              |     INNER JOIN ${ExplicationProperties.table} ON ${ExplicationProperties.id} = ${PeerReviewProperties.explicationId}
              |     INNER JOIN ${AnnotationProperties.table} ON ${AnnotationProperties.id} = ${ExplicationProperties.annotationId}
              |     INNER JOIN ${ThemeProperties.table} ON ${AnnotationProperties.themeId} = ${ThemeProperties.id}
              |     WHERE ${ThemeProperties.assignmentId} = {${ThemeProperties.assignmentId}}
              |     AND ${PeerReviewProperties.reviewerId} = {${PeerReviewProperties.reviewerId}}
              |   )
              |
           """.stripMargin
        ).on(
          ThemeProperties.assignmentId -> assignment.id,
          AnnotationProperties.ownerId -> user.id,
          PeerReviewProperties.reviewerId -> user.id,
          ExplicationProperties.ownerId -> user.id,
          AssignmentProperties.numberRequired -> assignment.numberRequired
        ).executeQuery()
          .as(
            long("explication_id") *
          ).map({case prId => (prId)})

        if (explicationsThatNeedReviews.isEmpty) {
          Left(new UserHasNoPeerReviewsAvailable(user, assignment))
        } else {
          val explicationId = explicationsThatNeedReviews.head
          val explication = explicationRepo.getById(user, explicationId).right.get
          val reviewId = SQL(
            s"""
               |INSERT INTO ${PeerReviewProperties.table} (
               |  ${PeerReviewProperties.reviewerId}, ${PeerReviewProperties.explicationId},
               |  ${PeerReviewProperties.isComplete}
               |) VALUES (
               |  {${PeerReviewProperties.reviewerId}}, {${PeerReviewProperties.explicationId}},
               |  0
               |)
             """.stripMargin)
            .on(
              PeerReviewProperties.reviewerId -> user.id,
              PeerReviewProperties.explicationId -> explicationId
            ).executeInsert().get

          val promptResponses = assignment.reviewRubric.map(criterion => {
            val promptResponseId = SQL(
              s"""
                 |INSERT INTO ${PromptResponseProperties.table} (
                 |  ${PromptResponseProperties.reviewId}, ${PromptResponseProperties.rubricId}
                 |) VALUES (
                 |  {${PromptResponseProperties.reviewId}}, {${PromptResponseProperties.rubricId}}
                 |)
               """.stripMargin).on(
              PromptResponseProperties.reviewId -> reviewId,
              PromptResponseProperties.rubricId -> criterion.id
            ).executeInsert().get
            PromptResponse(promptResponseId, "", criterion, 0)
          })
          Right(PeerReview(
            reviewId, user, explication, promptResponses
          ))
        }
      }
    }

  })

  def userCanUpdateReview(user:User, review:PeerReview):Boolean = db.withConnection(implicit c => {
    SQL(
      s"""
         |SELECT ${PeerReviewProperties.reviewerId}
         |FROM ${PeerReviewProperties.table}
         |WHERE ${PeerReviewProperties.id} = {${PeerReviewProperties.id}}
       """.stripMargin).on(
      PeerReviewProperties.id -> review.id
    ).executeQuery().as(long(PeerReviewProperties.reviewerId).single) == user.id
  })

  def update(user:User, review:PeerReview):Either[ELError,PeerReview] = db.withTransaction(implicit c => {
    userCanUpdateReview(user, review) match {
      case false => Left(new UserHasNoAccessToReview(user, review))
      case true => {
        SQL(
          s"""
             |UPDATE ${PeerReviewProperties.table}
             |SET
             |  ${PeerReviewProperties.isComplete} = {${PeerReviewProperties.isComplete}},
             |  ${PeerReviewProperties.updatedAt} = NOW()
             |WHERE ${PeerReviewProperties.id} = {${PeerReviewProperties.id}}
           """.stripMargin).on(
          PeerReviewProperties.isComplete -> true,
          PeerReviewProperties.id -> review.id
        ).executeUpdate()
        review.responses.foreach(promptResponse => {
          SQL(
            s"""
               |UPDATE ${PromptResponseProperties.table}
               |SET ${PromptResponseProperties.text} = {${PromptResponseProperties.text}},
               |    ${PromptResponseProperties.score} = {${PromptResponseProperties.score}}
               |WHERE ${PromptResponseProperties.id} = {${PromptResponseProperties.id}}
             """.stripMargin
          ).on(
            PromptResponseProperties.text -> promptResponse.text,
            PromptResponseProperties.score -> promptResponse.score,
            PromptResponseProperties.id -> promptResponse.id
          ).executeUpdate();
        })
        Right(review)
      }
    }
  })

  def getById(user:User, id:Long):Either[ELError,PeerReview] = db.withConnection(implicit c => {
    val peerReview = SQL(
      s"""
         |SELECT ${PeerReviewProperties.id}, ${PeerReviewProperties.isComplete},
         |       ${PeerReviewProperties.explicationId},
         |
         |       ${UserProperties.id}, ${UserProperties.remoteId}, ${UserProperties.name},
         |       ${UserProperties.email}, ${UserProperties.lastLogin}
         |FROM  ${PeerReviewProperties.table}, ${UserProperties.table}
         |WHERE ${PeerReviewProperties.id} = {${PeerReviewProperties.id}}
         |  AND ${PeerReviewProperties.reviewerId} = ${UserProperties.id}
       """.stripMargin).on(
      PeerReviewProperties.id -> id
    ).executeQuery().as(
      long(PeerReviewProperties.id) ~ bool(PeerReviewProperties.isComplete)
        ~ long(PeerReviewProperties.explicationId) ~ long(UserProperties.id)
        ~ str(UserProperties.remoteId) ~ str(UserProperties.name) ~ str(UserProperties.email)
        ~ get[Option[Date]](UserProperties.lastLogin) *)
      .map({
        case prId ~ isComplete ~ explicationId ~ reviewerId ~ remoteId ~ userName ~ email
            ~ lastLogin =>
          explicationRepo.getById(user, explicationId) match {
            case Left(ex) => Left(ex)
            case Right(explication) =>  {
              val promptResponses = SQL(
                s"""
                   |SELECT ${PromptResponseProperties.id}, ${PromptResponseProperties.text},
                   |       ${PromptResponseProperties.score},
                   |
                   |       ${RubricPromptProperties.id}, ${RubricPromptProperties.description},
                   |       ${RubricPromptProperties.min}, ${RubricPromptProperties.max}
                   |
                   |FROM ${PromptResponseProperties.table}, ${RubricPromptProperties.table}
                   |WHERE ${PromptResponseProperties.reviewId} = {${PromptResponseProperties.reviewId}}
                   |  AND ${PromptResponseProperties.rubricId} = ${RubricPromptProperties.id}
                   |ORDER BY ${RubricPromptProperties.order}
                 """.stripMargin
              ).on(
                PromptResponseProperties.reviewId -> prId
              ).as(
                long(PromptResponseProperties.id) ~ get[Option[String]](PromptResponseProperties.text) ~ get[Option[Int]](PromptResponseProperties.score)
                  ~ long(RubricPromptProperties.id) ~ str(RubricPromptProperties.description)
                  ~ int(RubricPromptProperties.min) ~ int(RubricPromptProperties.max) *
              ).map({
                case prId~text~score~rpId~description~min~max => PromptResponse(
                  prId, text.getOrElse(""), RubricPrompt(rpId, description, min, max), score.getOrElse(-1)
                )
              })
              Right(PeerReview(
                prId, User(reviewerId, remoteId, userName, email, lastLogin.orNull), explication, promptResponses
              ))
            }
          }
      })
    peerReview.head
  })

  def getByUserAndAssignment(user:User, assignment: Assignment, owner:User):Either[ELError,List[PeerReview]] = db.withConnection(implicit c =>
    Right(SQL(
      s"""
         |SELECT ${PeerReviewProperties.id}
         |FROM ${PeerReviewProperties.table}, ${AnnotationProperties.table},
         |     ${ThemeProperties.table}, ${ExplicationProperties.table}
         |WHERE ${PeerReviewProperties.reviewerId} = {${PeerReviewProperties.reviewerId}}
         |  AND ${ThemeProperties.assignmentId} = {${ThemeProperties.assignmentId}}
         |  AND ${ThemeProperties.id} = ${AnnotationProperties.themeId}
         |  AND ${AnnotationProperties.id} = ${ExplicationProperties.annotationId}
         |  AND ${PeerReviewProperties.explicationId} = ${ExplicationProperties.id}
       """.stripMargin
    ).on(
      PeerReviewProperties.reviewerId -> owner.id,
      ThemeProperties.assignmentId -> assignment.id
    ).executeQuery().as(
      long(PeerReviewProperties.id) *
    ).map({ case reviewId => getById(user, reviewId)})
      .filter(_.isRight).map(_.right.get)
  )
  )

  def listCompletedForExplication(user:User, explication:Explication):Either[ELError,List[PeerReview]] = if (!explication.isSaved) {
    Right(List())
  } else {
    // TODO: Check role in context
    db.withConnection(implicit c => {
      Right(
        SQL(
          s"""
             |SELECT ${PeerReviewProperties.id}
             |FROM ${PeerReviewProperties.table}
             |WHERE ${PeerReviewProperties.explicationId} = {${PeerReviewProperties.explicationId}}
             |  AND ${PeerReviewProperties.isComplete} = 1
           """.stripMargin).on(
          PeerReviewProperties.explicationId -> explication.id
        ).executeQuery().as(long(PeerReviewProperties.id) *).map({
          case peerReviewId => getById(user, peerReviewId)
        }).filter(_.isRight).map(_.right.get)
      )
    })
  }

  def getReviewsCompletedByUserForAssignment(student:User, assignment:Assignment):Either[ELError, List[PeerReview]] = Right(db.withConnection(implicit c => {
    SQL(
        s"""
           |SELECT ${PeerReviewProperties.id}
           |FROM   ${PeerReviewProperties.table}, ${ExplicationProperties.table},
           |       ${AnnotationProperties.table}, ${ThemeProperties.table}
           |WHERE  ${PeerReviewProperties.reviewerId} = {${PeerReviewProperties.reviewerId}}
           |  AND  ${PeerReviewProperties.isComplete} = 1
           |  AND  ${PeerReviewProperties.explicationId} = ${ExplicationProperties.id}
           |  AND  ${ExplicationProperties.annotationId} = ${AnnotationProperties.id}
           |  AND  ${AnnotationProperties.themeId} = ${ThemeProperties.id}
           |  AND  ${ThemeProperties.assignmentId} = {${ThemeProperties.assignmentId}}
         """.stripMargin).on(
        PeerReviewProperties.reviewerId -> student.id,
        ThemeProperties.assignmentId -> assignment.id
      ).executeQuery().as(long(PeerReviewProperties.id) *).map(getById(student, _).right.get)
  }))

  def delete(user:User, id:Long):Either[ELError,Boolean] = db.withTransaction(implicit c => {
    // TODO: check role in context
    SQL(
      s"""
         |SELECT ${PeerReviewProperties.reviewerId} = {${PeerReviewProperties.reviewerId}} AS is_reviewer
         |FROM ${PeerReviewProperties.table}
         |WHERE ${PeerReviewProperties.id} = {${PeerReviewProperties.id}}
       """.stripMargin)
      .on(
        PeerReviewProperties.reviewerId -> user.id,
        PeerReviewProperties.id -> id
      ).executeQuery().as(bool("is_reviewer").single) match {
      case false => {
        logger.error(s"User ${user.id} (${user.providedName}) tried to update review ${id}")
        val review = getById(user, id).right.get
        Left(new UserHasNoAccessToReview(user, review))
      }
      case true => {
        SQL(
          s"""
             |DELETE FROM ${PeerReviewProperties.table}
             |WHERE ${PeerReviewProperties.id} = {${PeerReviewProperties.id}}
       """.stripMargin).on(
          PeerReviewProperties.id -> id
        ).executeUpdate()
        SQL(
          s"""
             |DELETE FROM ${PromptResponseProperties.table}
             |WHERE ${PromptResponseProperties.reviewId} = {${PromptResponseProperties.reviewId}}
       """.stripMargin
        ).on(
          PromptResponseProperties.reviewId -> id
        ).executeUpdate()
        Right(true)
      }
    }
  })

}

case class PeerReviewDoesNotExist(peerReviewId:Long) extends Throwable with ELError
class UserIsFinishedWithAssignment(assignment: Assignment) extends Throwable with ELError
class UserHasNoPeerReviewsAvailable(user:User, assignment: Assignment) extends Throwable with ELError
class UserHasNoAccessToReview(student:User, review:PeerReview) extends Throwable with ELError
case class ValueOutOfRange(response:PromptResponse) extends Exception with ELError

case class WrongValueType(rubric:RubricPrompt, response:PromptResponse, expectedType:String) extends Exception with ELError {
  val TEXTUAL = "TEXTUAL"
  val NUMERIC = "NUMERIC"
}
