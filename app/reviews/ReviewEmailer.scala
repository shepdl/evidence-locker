package reviews

import java.io.{File, PrintWriter}
import javax.inject.Inject

import akka.actor.Actor.Receive
import akka.actor.{Actor, Props}
import assignments.{AssignmentRepository, ThemeRepository}
import lti.LMSContextRepo
import play.api.{Configuration, Logger}
import play.api.libs.mailer.{Email, MailerClient}

/**
  * Created by dave on 2/3/17.
  */
object ReviewEmailer {
  def props = Props[ReviewEmailer]

  case class SendEmail(peerReview: PeerReview)
}


class ReviewEmailer @Inject()(
  val mailerClient:MailerClient,
  val peerReviewRepo:PeerReviewRepository,
  val themeRepo:ThemeRepository,
  val assignmentRepo:AssignmentRepository,
  val lmsContextRepo:LMSContextRepo
 ) extends Actor {

  import ReviewEmailer._

  val logger = Logger("Review Emailer Logger")

  {
    val outDir = new File("./emails-sent/")
    outDir.mkdirs()
  }

  override def receive: Receive = {

    case SendEmail(peerReview:PeerReview) => {
      try {
        val reviewedStudent = peerReview.explication.annotation.owner
        val theme = themeRepo.getForAnnotation(peerReview.explication.annotation.id).get
        val assignment = themeRepo.assignmentForTheme(theme).right.get
        val context = lmsContextRepo.getContextForAssignment(reviewedStudent, assignment).get
        val email = Email(
          "Review Completed",
          "Evidence Locker <application-no-reply@locker.cdh.ucla.edu>",
          Seq(reviewedStudent.email),
          bodyHtml = Some(views.html.email.reviewCompleted(
            reviewedStudent.providedName,
            assignment,
            context,
            peerReview.explication,
            peerReview
          ).body)
        )
        mailerClient.send(email)
        val outFile = new PrintWriter(new File(s"./emails-sent/peer-review-${peerReview.id}.html"))
        outFile.write(email.bodyHtml.get)
        outFile.close()
      } catch {
        case ex:Throwable => {
          logger.error(s"Exception while sending review email for peer review ${peerReview.id}: ${ex.getMessage}")
        }
      }
    }

    case ex => {
      logger.error(s"Unrecognized message: ${ex}")
    }
  }

}
