Functions of the Administration Section
===

# Logging In

This needs the following components:
## Login form
## Backend logging check system
## Error page
## Dashboard (successful login)
## Application template

# Add new LTI Tool Consumers

## List of existing Tool Consumers with link to create a new one
## Form for creating a Tool Consumer
## Create method in tool consumer controller
## Save method for creating a tool consumer in repository
## Response


# List existing Tool Consumers

This needs the following components:
## Method to load existing tool consumers, with pagination
## Template for a tool consumer


# Update Tool Consumer (change name, domain, and generate new shared secret)

This needs the following components
## Edit link for an existing tool consumer (in list of Tool Consumers)
## Form for updating a Tool Consumer
## Update method in TC Controller
## Save method for creating a tool consumer in repository
## Response


# Remove Tool Consumer

This needs the following components
## Delete link for an existing tool consumer
## Delete method in TC Controller
## Delete method in TC Repository
## Response
## Reload list


# View of single Tool Consumer

This needs the following components
## Get info for an existing TC
## Template for TC
## Method to load Contexts for TC
## Template for Context
## Method to load Assignments for Context
## Template for Assignments
## Method to load Users for TC
## Template for User

# Menu
* Tool Consumers
* Administrators

