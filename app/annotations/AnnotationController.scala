package annotations

import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.Date
import javax.inject.Inject

import assignments.{AssignmentDoesNotExist, RendersThemesAsJson, ThemeRepository, UserHasNoAccessToAssignment}
import common.JsonErrorReporting
import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import play.api.Logger
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._

/**
  * Created by dave on 8/12/16.
  */
trait AnnotationController extends JsonErrorReporting with I18nSupport with RendersAnnotationsAsJson {

  this:Controller =>

  val annotationRepo:AnnotationRepository
  val themeRepo:ThemeRepository
  val userRepo:UserRepository

  val messagesApi:MessagesApi
  val logger = Logger("AnnotationController")

  val dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

  val domain = "http://evidencelocker.cdh.ucla.edu/"

  def getById(annotationId:Long) = Action { implicit request =>
    request.session.get("userId") match {
      case None => {
        logger.warn("Attempted to view annotation without being logged in")
        Forbidden(jsonError("User must be logged in"))
      }
      case Some(userId) => userRepo.getById(userId.toLong) match {
        case None => {
          logger.warn(s"User ${userId} not found")
          Forbidden(jsonError(s"User $userId not found"))
        }
        case Some(user) => {
          annotationRepo.getById(user, annotationId) match {
            case Left(ex:UserHasNoAccessToAssignment) => {
              Forbidden(jsonError(s"User $userId has no access to assignment"))
            }
            case Left(ex:AssignmentDoesNotExist) => {
              NotFound(jsonError(s"Assignment $annotationId does not exist"))
            }
            case Left(ex:AnnotationDoesNotExist) => {
              NotFound(jsonError(s"Annotation ${annotationId} does not exist"))
            }
            case Right(annotation:Annotation) => {
              Ok(annotationToJson(annotation))
            }
          }
        }
      }
    }
  }

  def getForTheme(themeId:Long) = Action { implicit request =>
    request.session.get("userId") match {
      case None => {
        logger.warn("Attempted to view annotation without being logged in")
        Forbidden(jsonError("User must be logged in"))
      }
      case Some(userId) => userRepo.getById(userId.toLong) match {
        case None => {
          logger.warn(s"User ${userId} not found")
          Forbidden(jsonError(s"User $userId not found"))
        }
        case Some(user) => {
          themeRepo.getById(user, themeId) match {
            case None => NotFound(jsonError("Theme not found"))
            case Some(theme) => annotationRepo.getForTheme(user, theme) match {
              case Left(ex:UserHasNoAccessToAssignment) => Forbidden(jsonError("User has no access to assignment"))
              case Right(annotations) => {
                Ok(
                  Json.obj(
                    "id" -> theme.id,
                    "description" -> theme.description,
                    "annotations" -> annotations.map(annotationToJson(_))
                  )
                )
              }
            }
          }
        }
      }
    }
  }

  import play.api.data._
  import play.api.data.Forms._

  implicit val annotationForm:Form[AnnotationData] = Form(
    mapping(
      "quote" -> nonEmptyText,
      "start" -> nonEmptyText,
      "end" -> nonEmptyText,
      "startOffset" -> number,
      "endOffset" -> number,
      "pageNo" -> nonEmptyText
    )(AnnotationData.apply)(AnnotationData.unapply)
  )

  def create(themeId:Long) = Action(parse.urlFormEncoded) { implicit request =>
    request.session.get("userId") match {
      case None => {
        logger.warn("Attempted to view annotation without being logged in")
        Forbidden(jsonError("User must be logged in"))
      }
      case Some(userId) => userRepo.getById(userId.toLong) match {
        case None => {
          logger.warn(s"User ${userId} not found")
          Forbidden(jsonError(s"User $userId not found"))
        }
        case Some(user) => {
          annotationForm.bindFromRequest.fold(
            formWithErrors => BadRequest(formWithErrors.errorsAsJson),
            annotationData => {
              themeRepo.getById(user, themeId) match {
                case None => NotFound(jsonError(s"Theme ${themeId} not found, or user has no access"))
                case Some(theme) => {
                  val now = new Date
                  val annotation = Annotation(
                    -1, annotationData.quote,
                    annotationData.start,
                    annotationData.end,
                    annotationData.startOffset,
                    annotationData.endOffset,
                    annotationData.pageNo,
                    user
                  )
                  annotationRepo.create(user, theme, annotation) match {
                    case Right(annotation) => Created(annotationToJson(annotation))
                  }
                }
              }
            }
          )
        }
      }
    }
  }

  def delete(annotationId:Long) = Action { implicit request =>
    request.session.get("userId") match {
      case None => {
        logger.warn("Attempted to view annotation without being logged in")
        Forbidden(jsonError("User must be logged in"))
      }
      case Some(userId) => userRepo.getById(userId.toLong) match {
        case None => {
          logger.warn(s"User ${userId} not found")
          Forbidden(jsonError(s"User $userId not found"))
        }
        case Some(user) => {
          annotationRepo.delete(user, annotationId) match {
            case Left(ex:AnnotationDoesNotExist) => NotFound(
              jsonError(s"Annotation $annotationId not found")
            )
            case Left(ex:UserHasNoAccessToAssignment) => Forbidden(
              jsonError(s"User has no access to the annotation's assignment")
            )
            case Left(ex:UserHasNoAccessToAnnotation) => Forbidden(
              jsonError(s"User has no access to the annotation")
            )
            case Right(status) => if (status) {
              Ok(Json obj("message" -> s"Annotation $annotationId deleted"))
            } else {
              Forbidden(jsonError("User cannot access assignment"))
            }
          }
        }
      }
    }
  }

}

case class AnnotationData(
  quote: String, start:String, end:String, startOffset:Int, endOffset:Int, pageNo:String
)

trait RendersAnnotationsAsJson {

  val domain:String
  val dateFormatter:SimpleDateFormat

  def annotationToJson(annotation: Annotation): JsObject = Json obj(
    "id" -> annotation.id,
    "text" -> "",
    "quote" -> annotation.quote,
//    "created" -> dateFormatter.format(annotation.createdAt),
//    "updated" -> dateFormatter.format(annotation.updatedAt),
    "uri" -> s"$domain/annotations/${annotation.id}",
    "ranges" -> Json.arr(
      Json.obj(
        "start" -> annotation.start,
        "end" -> annotation.end,
        "startOffset" -> annotation.startOffset,
        "endOffset" -> annotation.endOffset
      )
    ),
    "pageNo" -> annotation.pageNo,
    "user" -> s"$domain/users/${annotation.owner.id}",
    "consumer" -> domain,
    "permissions" -> Json.obj (
      "read" -> Json.arr("group:__world__"),
      "admin" -> Json.arr(),
      "update" -> Json.arr(s"$domain/users/${annotation.owner.id}"),
      "delete" -> Json.arr(s"$domain/users/${annotation.owner.id}")
    )
  )

}

case class RangeData(start:String, end:String, startOffset:Int, endOffset:Int)

class DefaultAnnotationController @Inject() (
    override val themeRepo:ThemeRepository,
    override val messagesApi: MessagesApi,
    override val userRepo: UserRepository,
    override val annotationRepo:AnnotationRepository
  ) extends AnnotationController with Controller
