package annotations

import java.util.Date

import edu.ucla.cdh.EvidenceLocker.authentication.User
import edu.ucla.cdh.EvidenceLocker.common.{NodeType, TimestampsProperties}

/**
  * Created by dave on 8/25/16.
  */
case class Explication(id:Long, text:String, submitted:Boolean, annotation:Annotation) extends NodeType

case object Explication {

  def from(text:String, submitted:Boolean, annotation:Annotation, owner:User) = {
    val now = new Date
    this(-1, text, submitted, annotation)
  }

  def from(text:String, annotation:Annotation, owner:User) = {
    val now = new Date
    val submitted = false
    this(-1, text, submitted, annotation)
  }

}

case object ExplicationProperties extends TimestampsProperties {
  val table = "explications"
  val id = s"$table.id"

  val text = s"$table.text"
  val submitted = s"$table.submitted"
  val annotationId = s"$table.annotation_id"
  val ownerId = s"$table.owner_id"

}
