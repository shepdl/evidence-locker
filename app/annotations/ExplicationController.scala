package annotations

import javax.inject.Inject

import authentication.{AssignmentService, AuthenticationService}
import common.JsonErrorReporting
import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import play.api.Logger
import play.api.data.Form
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._

/**
  * Created by dave on 8/25/16.
  */
trait ExplicationController extends JsonErrorReporting with I18nSupport with RendersExplicationsAsJson {
  this:Controller =>

  val messagesApi:MessagesApi
  val logger = Logger("ExplicationController")

  val userRepo:UserRepository
  val annotationRepo:AnnotationRepository
  val explicationRepo:ExplicationRepository

  val authenticationService:AuthenticationService
  val assignmentService:AssignmentService

  import play.api.libs.json._

  def create(annotationId:Long) = Action(parse.json) { implicit request =>
    request.session.get("userId") match {
      case None => {
        logger.warn("Attempted to create explication without being logged in")
        Forbidden(jsonError("User must be logged in"))
      }
      case Some(userId) => userRepo.getById(userId.toLong) match {
        case None => {
          logger.warn(s"User ${userId} not found")
          Forbidden(jsonError(s"User $userId not found"))
        }
        case Some(user) => {
          annotationRepo.getById(user, annotationId) match {
            case Left(ex:UserHasNoAccessToAnnotation) => {
              val message = "User has no access to this annotation"
              logger.warn(message)
              Forbidden(jsonError(message))
            }
            case Left(ex:AnnotationDoesNotExist) => {
              val message = "Annotation not found"
              logger.warn(message)
              NotFound(jsonError(message))
            }
            case Right(annotation) => {
              val text = (request.body \ "text").as[String]
              val submitted = (request.body \ "submitted").asOpt[Boolean].getOrElse(false)
//              val explication = Explication.from(text, submitted, annotation, user)
              explicationRepo.getByAnnotationId(user, annotationId) match {
                case Right(explication) => {
                  val realExplication = if (!explication.isSaved) {
                    Explication.from(text, submitted, annotation, user)
                  } else {
                    Explication(
                      explication.id,
                      text,
                      submitted,
                      annotation
                    )
                  }
                  explicationRepo.save(user, annotation, realExplication) match {
                    case Left(ex: UserHasNoAccessToAnnotation) => {
                      val message = s"User ${user.providedName} (#${user.id}) tried to explicate annotation ${annotation.id} but is not allowed to do that"
                      logger.warn(message)
                      Forbidden(jsonError(message))
                    }
                    case Right(createdExplication) => {
                      Created(explicationToJson(createdExplication))
                    }
                  }
                }
                case Left(ex: UserHasNoAccessToAnnotation) => {
                  Forbidden(jsonError("User has no access to this annotation"))
                }
              }
            }
          }
        }
      }
    }
  }

  def getByAnnotationId(annotationId:Long) = Action { implicit request =>
    request.session.get("userId") match {
      case None => {
        logger.warn("Attempted to view explication without being logged in")
        Forbidden(jsonError("User must be logged in"))
      }
      case Some(userId) => userRepo.getById(userId.toLong) match {
        case None => {
          logger.warn(s"User $userId not found")
          Forbidden(jsonError(s"User $userId not found"))
        }
        case Some(user) => {
          annotationRepo.getById(user, annotationId) match {
            case Left(ex:UserHasNoAccessToAnnotation) => {
              val message = "User has no access to this annotation"
              logger.warn(message)
              Forbidden(jsonError(message))
            }
            case Left(ex:AnnotationDoesNotExist) => {
              val message = "Annotation not found"
              logger.warn(message)
              NotFound(jsonError(message))
            }
            case Right(annotation) => {
              explicationRepo.getByAnnotationId(user, annotation.id) match {
                case Left(ex:UserHasNoAccessToAnnotation) => {
                  val message = s"User ${user.providedName} (#${user.id}) tried to explicate annotation ${annotation.id} but is not allowed to do that"
                  logger.warn(message)
                  Forbidden(jsonError(message))
                }
                case Right(explication) => {
                  Ok(explicationToJson(explication))
                }
              }
            }
          }
        }
      }
    }
  }

  def getExplicationsByUserInAssignment(assignmentId:Long, userId:Long) = (
    authenticationService.ChecksForUserAction andThen assignmentService.ChecksForAccessToAssignment(assignmentId)
    andThen assignmentService.ChecksThatUserIsInstructor
  ) { implicit request =>
    val user = request.user
    val assignment = request.assignment
    userRepo.getById(userId) match {
      case None => NotFound(jsonError(s"User ${userId} not found"))
      case Some(student) => explicationRepo.getByUserAndAssignment(user, assignment, student) match {
        case Left(errors) => InternalServerError(jsonError("An internal error occurred"))
        case Right(explications) => {
          Ok(
            Json.obj(
              "student" -> Json.obj(
                "id" -> student.id,
                "name" -> student.providedName
              ),
              "explications" -> explications.map(explicationToJson)
            )
          )
        }
      }
    }
  }
}

trait RendersExplicationsAsJson {

  import play.api.libs.json._

  def explicationToJson(explication:Explication): JsObject = Json obj (
    "id" -> explication.id,
    "text" -> explication.text,
    "ownerRef" -> explication.annotation.owner.id,
    "annotationRef" -> s"/annotations/${explication.annotation.id}",
    "annotation" -> Json.obj(
      "id" -> explication.annotation.id,
      "quote" -> explication.annotation.quote
    )
  )

}

class DefaultExplicationController @Inject() (
   override val messagesApi: MessagesApi,
   override val userRepo: UserRepository,
   override val annotationRepo: AnnotationRepository,
   override val explicationRepo:ExplicationRepository,
   override val authenticationService: AuthenticationService,
   override val assignmentService: AssignmentService
 ) extends ExplicationController with Controller

