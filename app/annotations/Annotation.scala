package annotations

import java.util.Date

import edu.ucla.cdh.EvidenceLocker.authentication.User
import edu.ucla.cdh.EvidenceLocker.common.TimestampsProperties

/**
  * Created by dave on 8/12/16.
  */
case class Annotation(id:Long, quote:String, start:String, end:String,
                      startOffset:Int, endOffset:Int, pageNo:String, owner:User
           )

case object Annotation {
  def from(quote:String, start:String, end:String,
            startOffset:Int, endOffset:Int, pageNo:String, owner:User
  ) = {
    val now = new Date
    this(-1, quote, start, end, startOffset, endOffset, pageNo, owner)
  }
}

case object AnnotationProperties extends TimestampsProperties {
  val table = "annotations"
  val id = s"$table.id"

  val text = s"$table.text"
  val quote = s"$table.quote"

  val start = s"$table.start"
  val end = s"$table.end"
  val startOffset = s"$table.start_offset"
  val endOffset = s"$table.end_offset"
  val pageNo = s"$table.page_no"

  val ownerId = s"$table.owner_id"
  val themeId = s"$table.theme_id"

}
