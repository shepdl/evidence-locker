package annotations

import java.sql.SQLIntegrityConstraintViolationException
import java.util.Date
import javax.inject.Inject

import anorm._
import assignments._
import common.ELError
import edu.ucla.cdh.EvidenceLocker.authentication.{User, UserProperties, UserRepository}
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import lti.{LMSContextMembershipProperties, LMSContextRepo}
import play.api.Logger

/**
  * Created by dave on 8/12/16.
  */
class AnnotationRepository @Inject() (
  dbs:DatabaseService, userRepo:UserRepository, assignmentRepo:AssignmentRepository,
  override val lmsContextRepo:LMSContextRepo, themeRepo:ThemeRepository
) extends ChecksAccessToAssignment {

  val db = dbs.db

  val logger = Logger("AnnotationRepository")

  import SqlParser._


  def getById(user:User, id:Long):Either[ELError,Annotation] = db.withConnection(implicit c => {
    SQL(
      s"""
         |SELECT ${AnnotationProperties.id}, ${AnnotationProperties.quote},
         |       ${AnnotationProperties.start}, ${AnnotationProperties.end},
         |       ${AnnotationProperties.startOffset}, ${AnnotationProperties.endOffset},
         |       ${AnnotationProperties.pageNo},
         |
         |       ${UserProperties.id}, ${UserProperties.remoteId}, ${UserProperties.name},
         |       ${UserProperties.email}, ${UserProperties.lastLogin}
         |
         |FROM ${AnnotationProperties.table}, ${UserProperties.table}
         |WHERE ${AnnotationProperties.id} = {${AnnotationProperties.id}}
         |  AND ${UserProperties.id} = ${AnnotationProperties.ownerId}
       """.stripMargin).on(
      AnnotationProperties.id -> id,
      AnnotationProperties.ownerId -> user.id
    ).executeQuery().as(
      long(AnnotationProperties.id) ~ str(AnnotationProperties.quote) ~ str(AnnotationProperties.start)
        ~ str(AnnotationProperties.end) ~ int(AnnotationProperties.startOffset) ~ int(AnnotationProperties.endOffset)
        ~ str(AnnotationProperties.pageNo)
        ~ long(UserProperties.id) ~ str(UserProperties.remoteId) ~ str(UserProperties.name)
        ~ str(UserProperties.email) ~ get[Option[Date]](UserProperties.lastLogin) *
    ).map({
      case annotationId ~ quote ~ start ~ end ~ startOffset ~ endOffset ~ pageNo ~ userId ~ remoteId ~ userName ~ email ~ lastLogin => Annotation(
        id, quote, start, end, startOffset, endOffset, pageNo, User(userId, remoteId, userName, email, lastLogin.orNull)
      )
    }).headOption match {
      case None => Left(new AnnotationDoesNotExist(id))
        // TODO: Commented out check because the query is too slow
      case Some(annotation) => if (userCanAccessAnnotation(user, annotation)) {
        Right(annotation)
      } else {
        Left(new UserHasNoAccessToAnnotation(annotation))
      }
    }
  })

  def userCanCreateAnnotationForTheme(user:User, theme: Theme) = theme.owner == user || {
    db.withConnection(implicit c => {
      SQL(
        s"""
           |SELECT ${LMSContextMembershipProperties.roleId}
           |FROM ${ThemeProperties.table}, ${AssignmentProperties.table},
           |     ${LMSContextMembershipProperties.table}
           |WHERE ${ThemeProperties.id} = {${ThemeProperties.id}}
           |  AND ${ThemeProperties.assignmentId} = ${AssignmentProperties.id}
           |  AND ${AssignmentProperties.contextId} = ${LMSContextMembershipProperties.contextId}
           |  AND ${LMSContextMembershipProperties.userId} = ${ThemeProperties.ownerId}
           |  AND ${LMSContextMembershipProperties.roleId} = ${userRepo.roles.instructor.id}
         """.stripMargin).on(
        ThemeProperties.id -> theme.id,
        LMSContextMembershipProperties.userId -> user.id
      ).executeQuery().as(long(LMSContextMembershipProperties.roleId) *).nonEmpty
    })
  }

  def userCanAccessAnnotation(user:User, annotation:Annotation) = {
    db.withConnection(implicit c => {
      SQL(
        s"""
           |SELECT ${LMSContextMembershipProperties.roleId}
           |FROM ${ThemeProperties.table}, ${AssignmentProperties.table},
           |     ${LMSContextMembershipProperties.table}, ${AnnotationProperties.table}
           |WHERE ${AnnotationProperties.id} = {${AnnotationProperties.id}}
           |  AND ${AnnotationProperties.themeId} = ${ThemeProperties.id}
           |  AND ${ThemeProperties.assignmentId} = ${AssignmentProperties.id}
           |  AND ${AssignmentProperties.contextId} = ${LMSContextMembershipProperties.contextId}
           |  AND ${LMSContextMembershipProperties.userId} = {${LMSContextMembershipProperties.userId}}
         """.stripMargin).on(
        AnnotationProperties.id -> annotation.id,
        LMSContextMembershipProperties.userId -> user.id
      ).executeQuery().as(long(LMSContextMembershipProperties.roleId) *).nonEmpty
    })
  }

  def userCanUpdateAnnotation(user: User, annotation: Annotation) = annotation.owner == user

  def themeForAnnotation(user:User, annotation:Annotation):Either[ELError,Theme] = {
    db.withConnection(implicit c => {
      val themeId = SQL(
        s"""
           |SELECT ${AnnotationProperties.themeId}
           |FROM ${AnnotationProperties.table}
           |WHERE ${AnnotationProperties.id} = {${AnnotationProperties.id}}
         """.stripMargin).on(
        AnnotationProperties.id -> annotation.id
      ).executeQuery().as(
        long(AnnotationProperties.themeId).single
      )
      themeRepo.getById(user, themeId) match {
        case None => Left(new ThemeDoesNotExistException(themeId))
        case Some(theme) => Right(theme)
      }
    })
  }

  def create(user:User, theme:Theme, annotation: Annotation):Either[ELError,Annotation] = db.withTransaction(implicit c => {
    if (userCanCreateAnnotationForTheme(user, theme)) {
      try {
        val newId = SQL(s"""
           |INSERT INTO ${AnnotationProperties.table} (
           |  ${AnnotationProperties.quote}, ${AnnotationProperties.text},
           |  ${AnnotationProperties.start}, ${AnnotationProperties.end},
           |  ${AnnotationProperties.startOffset}, ${AnnotationProperties.endOffset},
           |  ${AnnotationProperties.pageNo},
           |  ${AnnotationProperties.ownerId}, ${AnnotationProperties.themeId}
           |) VALUES (
           |  {${AnnotationProperties.quote}}, {${AnnotationProperties.text}},
           |  {${AnnotationProperties.start}}, {${AnnotationProperties.end}},
           |  {${AnnotationProperties.startOffset}}, {${AnnotationProperties.endOffset}},
           |  {${AnnotationProperties.pageNo}},
           |  {${AnnotationProperties.ownerId}}, {${AnnotationProperties.themeId}}
           |)
         """.stripMargin).on(
        AnnotationProperties.quote -> annotation.quote,
        AnnotationProperties.text -> "",
        AnnotationProperties.start -> annotation.start,
        AnnotationProperties.end -> annotation.end,
        AnnotationProperties.startOffset -> annotation.startOffset,
        AnnotationProperties.endOffset -> annotation.endOffset,
        AnnotationProperties.pageNo -> annotation.pageNo,
        AnnotationProperties.ownerId -> user.id,
        AnnotationProperties.themeId ->
          theme.
            id
      ).executeInsert().get
      Right(Annotation(
        newId, annotation.quote, annotation.start, annotation.end, annotation.startOffset, annotation.endOffset, annotation.pageNo, user
      ))
        } catch {
        case ex:SQLIntegrityConstraintViolationException => {
          Left(new ThemeDoesNotExistException(theme.id))
        }
      }
    } else {
      Left(UserCannotUpdateTheme(user, theme))
    }
  })

  def getForTheme(user:User, theme:Theme):Either[ELError,List[Annotation]] = db.withConnection(implicit c => {
    themeRepo.assignmentForTheme(theme) match {
      case Left(ex: AssignmentDoesNotExist) => Left(ex)
      case Right(assignment) => userCanAccessAssignment(user, assignment) match {
        case Right(canAccess) => if (canAccess) {
          val annotationIds = SQL(s"""
               |SELECT ${AnnotationProperties.id}
               |FROM ${AnnotationProperties.table}
               |WHERE ${AnnotationProperties.themeId} = {${AnnotationProperties.themeId}}
             """.stripMargin).on(
            AnnotationProperties.themeId -> theme.id
          ).executeQuery().as(
            long(AnnotationProperties.id) *
          )
          val annotations = annotationIds.map(getById(user, _)).filter(_ match {
            case Left(err) => false
            case Right(ann) => true
          })
          Right(annotations.map(_.right.get))
        } else {
          Left(UserHasNoAccessToAssignment(user, assignment))
        }
      }
    }
  })

  def getForThemeFilteredByUser(user:User, theme:Theme, owner:User):Either[ELError,List[Annotation]] = db.withConnection(implicit c => {
    themeRepo.assignmentForTheme(theme) match {
      case Left(ex: AssignmentDoesNotExist) => Left(ex)
      case Right(assignment) => userCanAccessAssignment(user, assignment) match {
        case Right(canAccess) => if (canAccess) {
          val annotationIds = SQL(
            s"""
             |SELECT ${AnnotationProperties.id}
             |FROM ${AnnotationProperties.table}
             |WHERE ${AnnotationProperties.themeId} = {${AnnotationProperties.themeId}}
             |  AND ${AnnotationProperties.ownerId} = {${AnnotationProperties.ownerId}}
             """.stripMargin).on(
            AnnotationProperties.themeId -> theme.id,
            AnnotationProperties.ownerId -> owner.id
          ).executeQuery().as(
            long(AnnotationProperties.id) *
          )
          val annotations = annotationIds.map(getById(user, _)).filter(_ match {
            case Left(err) => false
            case Right(ann) => true
          })
          Right(annotations.map(_.right.get))
        } else {
          Left(
            UserHasNoAccessToAssignment(user, assignment))
        }
      }
    }
  })

  def update(user:User, annotation: Annotation):Either[ELError,Annotation] = ???

  def delete(user:User, id:Long):Either[ELError,Boolean] = db.withTransaction(implicit c =>
    getById(user, id) match {
      case Left(ex) => Left(ex)
      case Right(annotation) => if (userCanUpdateAnnotation(user, annotation)) {
        SQL(
          s"""
             |DELETE FROM ${AnnotationProperties.table}
             |WHERE ${AnnotationProperties.id} = {${AnnotationProperties.id}}
           """.stripMargin).on(
          AnnotationProperties.id -> id
        ).executeUpdate()
        Right(true)
      } else {
        Right(false)
      }
    }
  )

}


class AnnotationDoesNotExist(annotationId:Long) extends Throwable with ELError
class UserHasNoAccessToAnnotation(annotation:Annotation) extends Throwable with ELError
class AnnotationHasNoTheme(annotationId:Long) extends Throwable with ELError

