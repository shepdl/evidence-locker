package annotations

import java.sql.SQLIntegrityConstraintViolationException
import javax.inject.Inject

import anorm._
import assignments._
import common.ELError
import edu.ucla.cdh.EvidenceLocker.authentication.{User, UserRepository}
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import lti.LMSContextRepo
import play.api.Logger

/**
  * Created by dave on 8/25/16.
  */
class ExplicationRepository @Inject() (
    dbs:DatabaseService, userRepo:UserRepository, override val lmsContextRepo:LMSContextRepo,
    annotationRepo: AnnotationRepository, assignmentRepo:AssignmentRepository
  ) extends ChecksAccessToAssignment {

  val db = dbs.db
  val logger = Logger("ExplicationRepository")

  import SqlParser._

  def getById(user:User, id:Long):Either[ELError,Explication] = db.withConnection(implicit c => {
    val explicationDataList = SQL(
      s"""
         |SELECT ${ExplicationProperties.id}, ${ExplicationProperties.text},
         |       ${ExplicationProperties.submitted}, ${ExplicationProperties.annotationId}
         |FROM  ${ExplicationProperties.table}
         |WHERE ${ExplicationProperties.id} = {${ExplicationProperties.id}}
       """.stripMargin).on(
      ExplicationProperties.id -> id
    ).executeQuery().as(
      long(ExplicationProperties.id) ~ str(ExplicationProperties.text) ~ bool(ExplicationProperties.submitted)
        ~ long(ExplicationProperties.annotationId) *
    ).map({
      case id ~ text ~ submitted ~ annotationId => (id, text, submitted, annotationId)
    })
    if (explicationDataList.isEmpty) {
      Left(new ExplicationDoesNotExist(id))
    } else {
      val explicationData = explicationDataList.head
      annotationRepo.getById(user, explicationData._4) match {
        case Left(ex) => {
          Left(ex)
        }
        case Right(annotation) => Right(Explication(
          explicationData._1, explicationData._2, explicationData._3, annotation
        ))
      }
    }
  })

  def assignmentForExplication(explication: Explication):Either[ELError,Assignment] = db.withConnection(implicit c => {
    val assignmentId = SQL(
      s"""
         |SELECT ${AssignmentProperties.id}
         |FROM   ${ExplicationProperties.table}, ${AnnotationProperties.table},
         |       ${ThemeProperties.table}, ${AssignmentProperties.table}
         |WHERE ${ExplicationProperties.id} = {${ExplicationProperties.id}}
         |  AND ${ExplicationProperties.annotationId} = ${AnnotationProperties.id}
         |  AND ${AnnotationProperties.themeId} = ${ThemeProperties.id}
         |  AND ${ThemeProperties.assignmentId} = ${AssignmentProperties.id}
       """.stripMargin).on(
      ExplicationProperties.id -> explication.id
    ).executeQuery().as(long(AssignmentProperties.id).single)
    assignmentRepo.getById(assignmentId) match {
      case None => {
        logger.error(s"Somehow, explication ${explication.id} points to a non-existent assignment ($assignmentId)")
        Left(AssignmentDoesNotExist(assignmentId))
      }
      case Some(assignment) => Right(assignment)
    }
  })

  def getByAnnotationId(user:User, annotationId:Long):Either[ELError,Explication] = db.withConnection(implicit c => {
    val possibleExplicationId = SQL(
      s"""
         |SELECT ${ExplicationProperties.id}
         |FROM ${ExplicationProperties.table}
         |WHERE ${ExplicationProperties.annotationId} = {${ExplicationProperties.annotationId}}
       """.stripMargin).on(
      ExplicationProperties.annotationId -> annotationId
    ).executeQuery().as(long(ExplicationProperties.id) *)
    if (possibleExplicationId.isEmpty) {
      Right(Explication.from(
        "", annotationRepo.getById(user, annotationId).right.get, user
      ))
    } else {
      getById(user, possibleExplicationId.head)
    }
  })

  def getCompletedByUserAndAssignment(user:User, assignment: Assignment, owner:User):Either[ELError,List[Explication]] = db.withConnection(implicit c => {
    val explicationIds = SQL(
      s"""
         |SELECT ${ExplicationProperties.id}
         |FROM ${AssignmentProperties.table}, ${ThemeProperties.table}, ${AnnotationProperties.table},
         |     ${ExplicationProperties.table}
         |WHERE ${AssignmentProperties.id} = {${AssignmentProperties.id}}
         |  AND ${AssignmentProperties.id} = ${ThemeProperties.assignmentId}
         |  AND ${ThemeProperties.id} = ${AnnotationProperties.themeId}
         |  AND ${AnnotationProperties.ownerId} = {${AnnotationProperties.ownerId}}
         |  AND ${AnnotationProperties.id} = ${ExplicationProperties.annotationId}
         |  AND ${ExplicationProperties.submitted} = 1
       """.stripMargin
    ).on(
      AssignmentProperties.id -> assignment.id,
      AnnotationProperties.ownerId -> owner.id
    ).executeQuery().as(
      long(ExplicationProperties.id) *
    ).map({
      case explicationId => explicationId
    })

    if (explicationIds.isEmpty) {
      Right(List())
    } else {
      Right(explicationIds.map(getById(user, _)).filter(_.isRight).map(_.right.get))
    }
  })

  def getByUserAndAssignment(user:User, assignment: Assignment, owner:User):Either[ELError,List[Explication]] = db.withConnection(implicit c => {
    val explicationIds = SQL(
      s"""
         |SELECT ${ExplicationProperties.id}
         |FROM ${AssignmentProperties.table}, ${ThemeProperties.table}, ${AnnotationProperties.table},
         |     ${ExplicationProperties.table}
         |WHERE ${AssignmentProperties.id} = {${AssignmentProperties.id}}
         |  AND ${AssignmentProperties.id} = ${ThemeProperties.assignmentId}
         |  AND ${ThemeProperties.id} = ${AnnotationProperties.themeId}
         |  AND ${AnnotationProperties.ownerId} = {${AnnotationProperties.ownerId}}
         |  AND ${AnnotationProperties.id} = ${ExplicationProperties.annotationId}
       """.stripMargin
    ).on(
      AssignmentProperties.id -> assignment.id,
      AnnotationProperties.ownerId -> owner.id
    ).executeQuery().as(
      long(ExplicationProperties.id) *
    ).map({
      case explicationId => explicationId
    })

    if (explicationIds.isEmpty) {
      Right(List())
    } else {
      Right(explicationIds.map(getById(user, _)).filter(_.isRight).map(_.right.get))
    }
  })

  def userCanExplicateAnnotation(user:User, annotation:Annotation) = user == annotation.owner

  def save(user:User, annotation: Annotation, explication: Explication):Either[ELError,Explication] = db.withTransaction(implicit c => {
    if (userCanExplicateAnnotation(user, annotation)) {
      if (!explication.isSaved) {
        try {
          val explicationId = SQL(s"""
             |INSERT INTO ${ExplicationProperties.table} (
             |  ${ExplicationProperties.text}, ${ExplicationProperties.submitted},
             |  ${ExplicationProperties.annotationId}, ${ExplicationProperties.ownerId}
             |) VALUES (
             |  {${ExplicationProperties.text}}, {${ExplicationProperties.submitted}},
             |  {${ExplicationProperties.annotationId}}, {${ExplicationProperties.ownerId}}
             |)
           """.stripMargin).on(
              ExplicationProperties.text -> explication.text,
              ExplicationProperties.submitted -> explication.submitted,
              ExplicationProperties.annotationId -> annotation.id,
              ExplicationProperties.ownerId -> user.id
            ).executeInsert().get
            Right(Explication(explicationId, explication.text, explication.submitted, annotation))
          } catch {
          case ex:SQLIntegrityConstraintViolationException => {
            Left(new AnnotationDoesNotExist(annotation.id))
          }
        }
      } else {
        try {
          SQL(s"""
             |UPDATE ${ExplicationProperties.table}
             |SET
             |    ${ExplicationProperties.text} = {${ExplicationProperties.text}},
             |    ${ExplicationProperties.submitted} = {${ExplicationProperties.submitted}},
             |    ${ExplicationProperties.annotationId} = {${ExplicationProperties.annotationId}}
             |WHERE ${ExplicationProperties.id} = {${ExplicationProperties.id}}
           """.stripMargin).on(
          ExplicationProperties.text -> explication.text,
          ExplicationProperties.submitted -> explication.submitted,
          ExplicationProperties.annotationId -> annotation.id,
          ExplicationProperties.id -> explication.id
        ).executeUpdate()
        Right(explication)
          } catch {
            case ex:SQLIntegrityConstraintViolationException => {
              Left(new AnnotationDoesNotExist(annotation.id))
            }
        }
      }
    } else {
      Left(new UserHasNoAccessToAnnotation(annotation))
    }
  })

}

class ExplicationDoesNotExist(explicationId:Long) extends Throwable with ELError
class AnnotationHasNoExplication(annotationId:Long) extends Throwable with ELError
