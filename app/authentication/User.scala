package edu.ucla.cdh.EvidenceLocker.authentication

import java.util.Date


case class User(
    id:Long,
    remoteId:String,
    providedName:String,
    email:String,
    lastLogin:Date
  )


case object UserProperties {
  val table = "users"

  val id = s"$table.id"
  val remoteId = s"$table.remote_id"
  val name = s"$table.provided_name"
  val email = s"$table.email"
  val isAdmin = s"$table.is_admin"
  val password = s"$table.password"
  val salt = s"$table.salt"
  val createdAt = s"$table.created_at"

  val toolConsumerId = s"$table.tool_consumer_id"

  val lastLogin = s"$table.last_login"

}
