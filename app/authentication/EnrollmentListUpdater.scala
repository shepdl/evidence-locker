package authentication

import javax.inject.Inject

import akka.actor.{Actor, Props}
import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import lti.{LMSContext, LMSContextRepo, ToolConsumer, ToolConsumerRepository}
import play.api.Logger
import play.api.libs.json.{JsObject, Json}
import play.api.libs.ws.{WSClient, WSResponse}

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success, Try}

object EnrollmentListUpdater {

  def props = Props[EnrollmentListUpdater]

  case class UpdateContext(context:LMSContext)
}

class EnrollmentListUpdater @Inject() (
      toolConsumerRepo:ToolConsumerRepository, lmsContextRepo:LMSContextRepo, userRepo:UserRepository,
      implicit val wsContext:ExecutionContext, ws:WSClient
  ) extends Actor {

  import EnrollmentListUpdater._

  val logger = Logger("Enrollment List Updater Actor")

  def parseMoodleResponse(toolConsumer: ToolConsumer, context:LMSContext)(response:Try[WSResponse]) = response match {
    case Failure(exception) => logger.error(exception.getMessage)
    case Success(response) => {
      val parsedBody = Json.parse(response.body)
      parsedBody.as[List[JsObject]].map({
        userJson => {
          val userRemoteId = s"${toolConsumer.url}-${(userJson \ "id").as[Long]}"
          val name = (userJson \ "fullname").as[String]
          val email = (userJson \ "email").as[String]
          val inRoles = (userJson \ "roles").as[List[JsObject]].map(json => (json \ "shortname").as[String].trim.toLowerCase)
          val validatedRoles = Set(if (inRoles.contains("student") || inRoles.contains("learner")) {
            Some(userRepo.roles.student)
          } else {
            None
          }) ++ Set(if (inRoles.contains("instructor")) {
            Some(userRepo.roles.instructor)
          } else {
            None
          }).filter(_.isDefined)
          userRepo.getByRemoteId(userRemoteId) match {
            case None => {
              val user = userRepo.create(userRemoteId, name, email).get
              lmsContextRepo.setRolesInContextForUser(user, context, validatedRoles.map(_.get))
            }
            case Some(user) => {
              lmsContextRepo.setRolesInContextForUser(user, context, validatedRoles.map(_.get))
              None
            }
          }
        }
      })
    }
  }

  override def receive: Receive = {
    case UpdateContext(context:LMSContext) => {
      toolConsumerRepo.getToolConsumerForContext(context) match {
        case None => logger.error(s"Serious error: Could not find tool consumer for ${context.instanceName} (with local ID ${context.id})")
        case Some(toolConsumer) => {
          toolConsumer.apiKey match {
            case None => {
              logger.error(s"Attempted to get users for ${context.id} for LMS ${toolConsumer.id}, but API key was not supplied")
            }
            case Some(apiKey) =>  toolConsumer.lmsType.toLowerCase match {
              case "moodle" => {
                val realRemoteId = context.remoteId.split("-", 2).last
                ws.url(s"https://${toolConsumer.url}/webservice/rest/server.php")
                  .withQueryString(
                    "wstoken" -> apiKey,
                    "wsfunction" -> "core_enrol_get_enrolled_users",
                    "moodlewsrestformat" -> "json",
                    "courseid" -> realRemoteId
                  ).get().onComplete(
                  parseMoodleResponse(toolConsumer, context)
                )
              }
              case _ => logger.error(s"Unrecognized LMS type: ${toolConsumer.lmsType}")
            }
          }
        }
      }
    }
  }
}
