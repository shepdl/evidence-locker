package authentication

import play.api.mvc._
import common.JsonErrorReporting
import edu.ucla.cdh.EvidenceLocker.authentication.{User, UserRepository}
import lti.{LMSContextRepo, UserIsNotInstructor}

/**
  * Created by dave on 10/6/16.
  */
trait ContextController extends JsonErrorReporting {

  this: Controller =>

  val lmsContextRepo:LMSContextRepo
  val userRepo:UserRepository
  val authenticationService:AuthenticationService

  import play.api.libs.json._

  def user2Json(user:User):JsObject = Json.obj(
    "id" -> user.id,
    "remoteId" -> user.remoteId,
    "name" -> user.providedName
  )

  def getStudentsInContext(contextId:Long) = authenticationService.ChecksForUserAction { implicit request =>
    val user = request.user
    lmsContextRepo.getById(contextId) match {
      case None => NotFound(jsonError("Context not found"))
      case Some(context) => {
        lmsContextRepo.getUsersWithRoleInContext(user, context, userRepo.roles.student) match {
          case Left(ex:UserIsNotInstructor) => Forbidden(jsonError("User is not instructor"))
          case Right(students) => Ok(students.map(user2Json).head)
        }
      }
    }
  }

}
