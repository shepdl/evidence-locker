package edu.ucla.cdh.EvidenceLocker.authentication


case class Role(id:Long, name:String)

case object RoleProperties {
  val table = "roles"
  val id = s"$table.id"
  val name = s"$table.name"

}



