package authentication

import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import play.api.data.Form
import play.api.i18n.{I18nSupport, MessagesApi}
import com.google.inject.{Inject, Singleton}
import play.api.Logger
import play.api.mvc._

/**
  * Created by dave on 4/11/17.
  */
trait AdminApplicationController extends I18nSupport {

  this:Controller=>

  val authService:AuthenticationService
  val userRepo:UserRepository
  val messagesApi: MessagesApi

  val logger = Logger("Admin Application Controller")


  def loginPage = Action {
    Ok(views.html.administrator.login(None))
  }

  import play.api.data.Forms._

  case class UserData(username:String, password:String)

  val loginForm:Form[UserData] = Form(
    mapping(
      "username" -> nonEmptyText,
      "password" -> nonEmptyText
    )(UserData.apply)(UserData.unapply)
  )

  def checkLogin = Action(parse.urlFormEncoded) { implicit request =>

    loginForm.bindFromRequest.fold(
      formWithErrors => BadRequest(formWithErrors.errorsAsJson),
      loginData => {
        userRepo.loginWithEmailAndPassword(loginData.username, loginData.password) match {
          case None => {
            Forbidden(views.html.administrator.login(Some("Incorrect login")))
          }
          case Some(user) => TemporaryRedirect("/administrator").withSession("userId" -> user.id.toString)
        }
      }
    )
  }

  def administrator = (authService.ChecksForUserAction andThen authService.ChecksThatUserIsAdministrator) {
    Ok(views.html.administrator.application())
  }

}


@Singleton
class DefaultAdminApplicationController @Inject() (
   override val authService: AuthenticationService,
   override val userRepo:UserRepository,
   override val messagesApi:MessagesApi
 ) extends AdminApplicationController with Controller
