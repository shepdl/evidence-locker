package edu.ucla.cdh.EvidenceLocker.authentication

import java.io.{File, PrintWriter}
import java.util.Date
import javax.inject.Inject

import com.google.inject.Singleton
import assignments.{Assignment, AssignmentRepository, RubricPrompt, Theme}
import authentication.AuthenticationService
import common.JsonErrorReporting
import lti.{LMSContextRepo, ToolConsumerRepository}
import play.api.data.Form
import play.api.libs.Files.TemporaryFile
import play.api.data.Form
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.{JsObject, Json}
import play.api.mvc.{Action, Controller}

trait UsersController extends JsonErrorReporting with I18nSupport {

  this: Controller =>

  val userRepo:UserRepository
  val lmsContextRepo:LMSContextRepo
  val toolConsumerRepo:ToolConsumerRepository
  val assignmentRepo:AssignmentRepository
  val authService:AuthenticationService

  val messagesApi: MessagesApi

  def user2Json(user:User):JsObject = Json.obj(
    "id" -> user.id,
    "name" -> user.providedName
  )

  def whoAmI = Action { implicit request =>
    request.session("userId") match {
      case "" => Forbidden(jsonError("You are not logged in"))
      case userId => {
        userRepo.getById(userId.toLong) match {
          case Some(user) => Ok(user2Json(user))
          case None => NotFound(jsonError(s"User not found"))
        }
      }
    }
  }

  import play.api.data.Forms._

  case class UserData(username:String, password:String)

  val loginForm:Form[UserData] = Form(
    mapping(
      "username" -> nonEmptyText,
      "password" -> nonEmptyText
    )(UserData.apply)(UserData.unapply)
  )

  def checkLogin = Action(parse.urlFormEncoded) { implicit request =>

    loginForm.bindFromRequest.fold(
      formWithErrors => BadRequest(formWithErrors.errorsAsJson),
      loginData => {
        userRepo.loginWithEmailAndPassword(loginData.username, loginData.password) match {
          case None => Forbidden(Json.obj(
            "error" -> "Username and data did not match"
          ))
          case Some(user) => Ok(user2Json(user))
        }
      }
    )
  }

  case class ChangePasswordData(password:String)

  val changePasswordForm:Form[ChangePasswordData] = Form(
    mapping(
      "password" -> nonEmptyText
    )(ChangePasswordData.apply)(ChangePasswordData.unapply)
  )

  def changePassword = (authService.ChecksForUserAction andThen authService.ChecksThatUserIsAdministrator)(parse.urlFormEncoded) { implicit request =>

    changePasswordForm.bindFromRequest.fold(
      formWithErrors => BadRequest(formWithErrors.errorsAsJson),
      passwordData => {
        userRepo.setPassword(request.user, passwordData.password)
        Ok(Json.obj(
          "status" -> "Password changed"
        ))
      }
    )

  }

}

@Singleton
class DefaultUserController @Inject() (
      override val userRepo:UserRepository,
      override val lmsContextRepo:LMSContextRepo,
      override val toolConsumerRepo:ToolConsumerRepository,
      override val assignmentRepo:AssignmentRepository,
      override val messagesApi: MessagesApi,
      override val authService: AuthenticationService
  ) extends UsersController with Controller
