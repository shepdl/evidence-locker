package edu.ucla.cdh.EvidenceLocker.authentication

import java.util.Date
import javax.inject.Inject

import anorm._
import common.HandlesMySQLBooleans
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import org.mindrot.jbcrypt.BCrypt
import play.api.Logger


class UserRepository @Inject() (val dbs:DatabaseService) extends HandlesMySQLBooleans {

  val db = dbs.db

  import SqlParser._

  val logger:Logger = Logger("UserRepository")

  private def roleForName(roleName:String) = {
    db.withConnection(implicit c => {
      val id = SQL("SELECT id FROM roles WHERE name = {name}").on("name" -> roleName).executeQuery()
        .as(SqlParser.long("id") *).head
      Role(id, roleName)
    })
  }

  case object roles {
    val student = roleForName("learner")
    val instructor = roleForName("instructor")
    private val rolesById = Map(
      student.id -> student,
      instructor.id -> instructor
    )
    def byId(id:Long) = rolesById(id)
  }

  val userParser = long(UserProperties.id) ~ str(UserProperties.remoteId) ~ str(UserProperties.name) ~
    str(UserProperties.email) ~ bool(UserProperties.isAdmin) ~ get[Option[Date]](UserProperties.lastLogin)

  def getById(id:Long):Option[User] = {
    db.withConnection(implicit c => {
      val foundUsers = SQL(
        s"""
           |SELECT ${UserProperties.id}, ${UserProperties.remoteId}, ${UserProperties.name},
           |    ${UserProperties.email}, ${UserProperties.isAdmin}, ${UserProperties.lastLogin}
           |FROM ${UserProperties.table}
           |WHERE ${UserProperties.id} = {${UserProperties.id}}""".stripMargin)
        .on(UserProperties.id -> id).executeQuery().as(userParser.*).map({
        case id~remote~name~email~isAdmin~lastLogin => User(id, remote, name, email, lastLogin.orNull)
      })
      foundUsers.headOption
    })
  }

  def getByRemoteId(remoteId:String):Option[User] = {
    db.withConnection(implicit c => {
      val foundUsers = SQL(s"""
               |SELECT ${UserProperties.id}, ${UserProperties.remoteId}, ${UserProperties.name},
               |    ${UserProperties.email}, ${UserProperties.isAdmin}, ${UserProperties.lastLogin}
               |FROM ${UserProperties.table}
               |WHERE ${UserProperties.remoteId} = {${UserProperties.remoteId}}""".stripMargin
        ).on(UserProperties.remoteId -> remoteId).executeQuery().as(userParser *).map({
        case id~remote~name~email~isAdmin~lastLogin => User(id, remote, name, email, lastLogin.orNull)
      })
      foundUsers.headOption
    })
  }

  private def hashPassword(password:String, salt:String) = BCrypt.hashpw(password, salt)

  def createAdminUser(name:String, email:String, password:String):Option[User] = {
    val salt = BCrypt.gensalt(12)
    val now = new Date
    val id = db.withTransaction(implicit c =>
      SQL(
        s"""
           |INSERT INTO ${UserProperties.table} (
           |  ${UserProperties.remoteId},
           |  ${UserProperties.name},
           |  ${UserProperties.email},
           |  ${UserProperties.password},
           |  ${UserProperties.salt},
           |  ${UserProperties.isAdmin}
           |) VALUES (
           |  "local-user",
           |  {${UserProperties.name}},
           |  {${UserProperties.email}},
           |  {${UserProperties.password}},
           |  {${UserProperties.salt}},
           |  {${UserProperties.isAdmin}}
           |)
         |""".stripMargin).on(
        UserProperties.name -> name,
        UserProperties.email -> email,
        UserProperties.password -> hashPassword(password, salt),
        UserProperties.salt -> salt,
        UserProperties.isAdmin -> false
      ).executeInsert()
    ).get
    Some(User(id, "local-user", name, email, now))
  }

  def create(remoteId:String, name:String, email:String):Option[User] = {
    val id = db.withTransaction(implicit c =>
      SQL(s"""
           |INSERT INTO ${UserProperties.table} (
           |  ${UserProperties.remoteId}, ${UserProperties.name},
           |  ${UserProperties.email}, ${UserProperties.isAdmin}
           |) VALUES (
           |  {${UserProperties.remoteId}},
           |  {${UserProperties.name}},
           |  {${UserProperties.email}},
           |  {${UserProperties.isAdmin}}
           |)
         """.stripMargin).on(
          UserProperties.remoteId -> remoteId,
          UserProperties.name -> name,
          UserProperties.email -> email,
          UserProperties.isAdmin -> false
        ).executeInsert()
      ).get
    Some(User(id, remoteId, name, email, null))
  }

  def setPassword(user:User, password:String) = {
    db.withTransaction(implicit c => {
      val salt = BCrypt.gensalt(12)
      val hashedPassword = hashPassword(password, salt)
      SQL(
        s"""
           |UPDATE ${UserProperties.table}
           |   SET ${UserProperties.password} = {${UserProperties.password}},
           |       ${UserProperties.salt} = {${UserProperties.salt}}
           | WHERE ${UserProperties.id} = {${UserProperties.id}}
       """.stripMargin
      ).on(
        UserProperties.password -> hashedPassword,
        UserProperties.salt -> salt,
        UserProperties.id -> user.id
      ).executeUpdate()
    })
  }

  def createBulk(users:List[User]) = db.withTransaction(implicit c =>
    BatchSql(s"""
                |INSERT INTO ${UserProperties.table} (
                |  ${UserProperties.remoteId}, ${UserProperties.name},
                |  ${UserProperties.email}, ${UserProperties.isAdmin}
                |) VALUES (
                |  {${UserProperties.remoteId}},
                |  {${UserProperties.name}},
                |  {${UserProperties.email}},
                |  {${UserProperties.isAdmin}}
                |)
         """.stripMargin, users.map(user => Seq[NamedParameter](
        UserProperties.remoteId -> user.remoteId,
        UserProperties.name -> user.providedName,
        UserProperties.email -> user.email,
        UserProperties.isAdmin -> false
      )
    )
    ).execute()
  )

  def isAdmin(user:User):Boolean = {
    db.withConnection(implicit c => {
        val isAdmin = SQL(
          s"""
             |SELECT ${UserProperties.isAdmin}
             |FROM ${UserProperties.table}
             |WHERE ${UserProperties.id} = {${UserProperties.id}}
           """.stripMargin).on(
          UserProperties.id -> user.id
        ).executeQuery()
          .as(int(UserProperties.isAdmin).single)
        isAdmin == 1
      }
    )
  }


  val failedLogin = User(-1, null, null, null, null)

  def loginWithEmailAndPassword(email:String, passwordAttempt:String):Option[User] = {
    db.withTransaction(implicit c => {
      val foundUser = SQL(
        s"""
           |SELECT ${UserProperties.id}, ${UserProperties.remoteId}, ${UserProperties.name},
           |       ${UserProperties.email}, ${UserProperties.password}, ${UserProperties.salt}
           |FROM ${UserProperties.table}
           |WHERE ${UserProperties.email} = {${UserProperties.email}}
         """.stripMargin
      ).on(UserProperties.email -> email)
        .executeQuery()
        .as(
          long(UserProperties.id) ~ str(UserProperties.remoteId) ~ str(UserProperties.name)
            ~ str(UserProperties.email) ~ str(UserProperties.password) ~ str(UserProperties.salt) *
        ).map({
        case id ~ remoteId ~ providedName ~ email ~ hashedPassword ~ salt  => {
          if (hashedPassword == hashPassword(passwordAttempt, salt)) {
            User(id, remoteId, providedName, email, null)
          } else {
            failedLogin
          }
        }
      })

      foundUser match {
        case List() => None
        case user::List() => {
          if (user == failedLogin) {
            None
          } else {
            updateLastLoginTime(user)
            Some(user)
          }
        }
        case List(user,_) => {
          logger.error("Somehow, two users with the same email and password were discovered")
          Some(user)
        }
      }
    })
  }

  def updateLastLoginTime(user:User) = {
    db.withConnection(implicit c => {
      SQL(
        s"""
           |UPDATE ${UserProperties.table}
           |SET    ${UserProperties.lastLogin} = NOW()
           |WHERE  ${UserProperties.id} = {${UserProperties.id}}
         """.stripMargin)
        .on(UserProperties.id -> user.id)
        .executeUpdate()
    })
  }

}

