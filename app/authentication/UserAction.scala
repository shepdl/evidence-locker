package authentication

import javax.inject.Inject

import assignments.{Assignment, AssignmentRepository}
import common.JsonErrorReporting
import play.api.mvc._
import edu.ucla.cdh.EvidenceLocker.authentication.{Role, User, UserRepository}
import lti.{LMSContext, LMSContextRepo}
import play.api.Logger

import scala.concurrent.Future

/**
  * Created by dave on 8/12/16.
  */
class UserRequest[A](val user:User, request:Request[A]) extends WrappedRequest[A](request)


class AuthenticationService @Inject() (userRepo: UserRepository) {

  val logger = Logger("AuthenticationService")

  def ChecksForUserAction = new ActionBuilder[UserRequest] with ActionRefiner[Request, UserRequest] with Results with JsonErrorReporting {

    val userIdKey = "userId"

    def refine[A](request: Request[A]):Future[Either[Result,UserRequest[A]]] = Future.successful {
      request.session.get(userIdKey) match {
        case None => {
          logger.warn("User not logged in")
          Left(Forbidden(jsonError("User must be logged in")))
        }
        case Some(userId) => userRepo.getById(userId.toLong) match {
          case None => Left(Forbidden(jsonError(s"User ${userId} not found")))
          case Some(user) => Right(new UserRequest(user, request))
        }
      }
    }

  }

  def ChecksThatUserIsAdministrator = new ActionFilter[UserRequest] with Results with JsonErrorReporting {
    override def filter[A](request: UserRequest[A]): Future[Option[Result]] = Future.successful {
      if (!userRepo.isAdmin(request.user)) {
        Some(Forbidden(jsonError(s"You must be an administrator to view this page")))
      } else {
        None
      }
    }
  }

}

class AssignmentRequest[A](val user:User, val context:LMSContext, val assignment: Assignment, request:Request[A]) extends WrappedRequest[A](request)

class AssignmentService @Inject() (lmsContextRepo: LMSContextRepo, userRepo:UserRepository, assignmentRepo:AssignmentRepository) {

  val logger = Logger("AssignmentService")

  def ChecksForAccessToAssignment(assignmentId:Long) = new ActionRefiner[UserRequest, AssignmentRequest] with Results with JsonErrorReporting {

    def refine[A](request: UserRequest[A]):Future[Either[Result,AssignmentRequest[A]]] = Future.successful {
      assignmentRepo.getById(assignmentId) match {
        case None => {
          logger.error(s"Assignment ${assignmentId} not found")
          Left(Forbidden(jsonError(s"Assignment ${assignmentId} not found")))
        }
        case Some(assignment) => {
          val user = request.user
          lmsContextRepo.getContextForAssignment(user, assignment) match {
            case None => {
              logger.error(s"Somehow, assignment ${assignment.id} is in the database, but it has no assigned context")
              Left(InternalServerError(jsonError(s"Sorry, an internal server error occurred")))
            }
            case Some(context) => {
              if (assignmentRepo.canViewAssignment(user, assignment)) {
                Right(new AssignmentRequest(user, context, assignment, request))
              } else {
                Left(Forbidden(jsonError(s"You are not allowed to view this class")))
              }
            }
          }
        }
      }
    }

  }

  def ChecksThatUserIsOwnerOrInstructor(ownerId:Long) = new ActionFilter[AssignmentRequest] with Results with JsonErrorReporting {
    def filter[A](request: AssignmentRequest[A]) = Future.successful({
      val user = request.user
      if (user.id == ownerId || lmsContextRepo.getRolesInContextForUser(user, request.context).roles.contains(userRepo.roles.instructor)) {
        None
      } else {
        Some(Forbidden(jsonError(s"You must be the author of this resource, or the instructor, to access it.")))
      }
    })
  }

  def ChecksThatUserIsInstructor = new ActionFilter[AssignmentRequest] with Results with JsonErrorReporting {
    def filter[A](request: AssignmentRequest[A]) = Future.successful {
      if (lmsContextRepo.getRolesInContextForUser(request.user, request.context).roles.contains(userRepo.roles.instructor)) {
        None
      } else {
        Some(Forbidden(jsonError(s"You must be an instructor to view this screen")))
      }
    }
  }

}
