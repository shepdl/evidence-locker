package assignments

import edu.ucla.cdh.EvidenceLocker.authentication.User
import reviews.PeerReview

/**
  * Created by dave on 11/28/16.
  */
class StudentPeerReviewSummary(val reviewer:User, val peerReviews:List[PeerReview])
