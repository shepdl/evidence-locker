package assignments

import java.io.File
import java.util.Date

import edu.ucla.cdh.EvidenceLocker.authentication.User
import edu.ucla.cdh.EvidenceLocker.common.{NodeType, TimestampsProperties}

/**
  * Created by dave on 6/30/16.
  */
case class AssignmentDocument(
  id:Long, file:File, originalFilename:String,
  fileType:String
) {

  val HTML = "HTML"
  val PDF = "PDF"

}


object NoDocumentYet extends AssignmentDocument(-1, null, null, null)
object NoDocumentUploaded extends AssignmentDocument(-2, null, null, null)
object InvalidDocumentType extends AssignmentDocument(-3, null, null, null)


object AssignmentDocumentProperties extends TimestampsProperties {
  val table = "documents"
  val id = s"$table.id"

  val assignmentId = s"$table.assignment_id"
  val systemFilename = s"$table.system_filename"
  val originalFilename = s"$table.original_filename"
  val fileType = s"$table.file_type"
  val uploaderId = s"$table.uploader_id"
}

case class Assignment(
             id:Long, remoteId:String,
             document:AssignmentDocument,
             title:String, description:String,
             openDate:Date, closeDate:Date,
             instructor:User,
             peerReviewEnabled:Boolean, numberRequired:Int,
             rubricType:String,
             reviewRubric:List[RubricPrompt],
             themes:List[Theme]
           ) {

  val TEXTUAL_RUBRIC = "textual"
  val NUMERIC_RUBRIC = "numeric"
  val BOTH_RUBRIC = "both"
  def hasTextualRubric = rubricType == TEXTUAL_RUBRIC || rubricType == BOTH_RUBRIC
  def hasNumericRubric = rubricType == NUMERIC_RUBRIC || rubricType == BOTH_RUBRIC
}


case object AssignmentProperties extends TimestampsProperties {
  val table = "assignments"
  val id = s"$table.id"

  val title = s"$table.title"
  val remoteId = s"$table.remote_id"
  val description = s"$table.description"
  val openDate = s"$table.open_date"
  val closeDate = s"$table.close_date"
  val peerReviewEnabled = s"$table.peer_review_enabled"
  val rubricType = s"$table.rubric_type"
  val numberRequired = s"$table.number_required"
  val reviewSchema = s"$table.review_schema"
  val instructorId = s"$table.instructor_id"
  val contextId = s"$table.lti_context_id"

}


case class RubricPrompt (id:Long, description:String, min:Int, max:Int) extends NodeType

case object RubricPromptProperties extends TimestampsProperties {
  val table = "rubric_prompts"
  val id = s"$table.id"

  val description = s"$table.description"
  val min = s"$table.min"
  val max = s"$table.max"
  val order = s"$table.presentation_order"

  val assignmentId = s"$table.assignment_id"

}


case class Theme (id:Long, description:String, parent:Theme, children:List[Theme], owner:User, order:Int) extends NodeType

case object ThemeProperties extends TimestampsProperties {
  val table = "themes"
  val id = s"$table.id"

  val description = s"$table.description"
  val assignmentId = s"$table.assignment_id"
  val parentId = s"$table.parent_id"
  val ownerId = s"$table.owner_id"
  val order = s"$table.presentation_order"

}
