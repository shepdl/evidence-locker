package assignments

import java.text.SimpleDateFormat
import java.util.Date
import javax.inject.Inject

import annotations.ExplicationRepository
import authentication.{AssignmentService, AuthenticationService}
import com.google.inject.Singleton
import common.JsonErrorReporting
import edu.ucla.cdh.EvidenceLocker.authentication.{User, UserRepository}
import lti.LMSContextRepo
import org.joda.time.format.{DateTimeParser, DateTimeParserBucket}
import play.api.Logger
import play.api.mvc.{Action, Controller}
import reviews.PeerReviewRepository

/**
  * Created by dave on 6/29/16.
  */
trait AssignmentController extends JsonErrorReporting {

  this: Controller =>

  val authenticationService:AuthenticationService
  val assignmentService:AssignmentService

  val userRepo:UserRepository
  val lmsContextRepo:LMSContextRepo
  val assignmentRepo:AssignmentRepository
  val explicationRepo:ExplicationRepository
  val peerReviewRepo:PeerReviewRepository

  val logger = Logger("AssignmentController")

  import play.api.libs.json._


  def assignment2JSObject(assignment:Assignment):JsObject = Json.obj(
    "ref" -> s"/assignments/${assignment.id}",
    "title" -> assignment.title,
    "description" -> assignment.description,
    "filename" -> assignmentRepo.fileURL(assignment),
    "document" -> Json.obj(
      "filename" -> assignment.document.originalFilename,
      "url" -> assignmentRepo.fileURL(assignment),
      "type" -> assignment.document.fileType
    ),
    "openDate" -> dtParser.format(assignment.openDate),
    "closeDate" -> dtParser.format(assignment.closeDate),
    "instructor" -> Json.obj(
      "name" -> assignment.instructor.providedName,
      "ref" -> s"/users/${assignment.instructor.id}"
    ),
    "peerReviewEnabled" -> assignment.peerReviewEnabled,
    "numberRequired" -> assignment.numberRequired,
    "rubricType" -> assignment.rubricType,
    "reviewRubric" -> assignment.reviewRubric.map(rr => Json.obj(
      "id" -> rr.id,
      "title" -> rr.description,
      "min" -> rr.min,
      "max" -> rr.max
    )),
    "themes" -> assignment.themes.map(t => Json.obj(
      "id" -> t.id,
      "description" -> t.description,
      "children" -> Json.arr(),
      "ownerRef" -> s"/users/${t.owner.id}"
    ))
  )

  def getById(assignmentId:Long) = (
    authenticationService.ChecksForUserAction andThen assignmentService.ChecksForAccessToAssignment(assignmentId)
    ) { implicit request =>
    val user = request.user
    val assignment = request.assignment
    val explicationCount = explicationRepo.getCompletedByUserAndAssignment(user, assignment, user) match {
      case Right(explications) => explications.size
      case Left(_) => -1
    }
    val peerReviewCount = peerReviewRepo.getByUserAndAssignment(user, assignment, user) match {
      case Right(peerReviews) => peerReviews.size
      case Left(_) => -1
    }
    val context = lmsContextRepo.getContextForAssignment(user, assignment).get
    val updatedAssignment = if (lmsContextRepo.getRolesInContextForUser(user, context).roles.contains(userRepo.roles.instructor)) {
      val students = lmsContextRepo.getUsersWithRoleInContext(user, context, userRepo.roles.student).right.get
      val explications = students.map(
        student => new StudentExplicationSummary(
          student, explicationRepo.getCompletedByUserAndAssignment(user, assignment, student).right.get
        )
      )
      val peerReviews = students.map(
        student => new StudentPeerReviewSummary(
          student,
          peerReviewRepo.getReviewsCompletedByUserForAssignment(student, assignment).right.get
        )
      )
      assignment2JSObject(assignment) ++ Json.obj(
        "totalStudents" -> students.size,
        "status" -> Json.obj(
          "explications" -> explications.map(
             studentExplicationSummary => {
              val student = studentExplicationSummary.student
              val explications = studentExplicationSummary.explications
              Json.obj(
                "student" -> Json.obj(
                  "id" -> student.id,
                  "name" -> student.providedName
                ),
                "explicationCount" -> explications.size
              )
            }
          ),
          "peerReviews" -> peerReviews.map(
            peerReviewSummary => {
              val student = peerReviewSummary.reviewer
              val peerReviews = peerReviewSummary.peerReviews
              Json.obj(
                "student" -> Json.obj(
                  "id" -> student.id,
                  "name" -> student.providedName
                ),
                "peerReviewsCompleted" -> peerReviews.size
              )
            }
          )
        )
      )
    } else {
      assignment2JSObject(assignment) ++ Json.obj(
        "status" -> Json.obj(
          "completedExplications" -> explicationCount,
          "completedReviews" -> peerReviewCount
        )
      )
    }
    Ok(updatedAssignment)
  }

  def getFile(assignmentId:Long) = (
    authenticationService.ChecksForUserAction andThen assignmentService.ChecksForAccessToAssignment(assignmentId)
    ) { implicit request =>
    Ok.sendFile(request.assignment.document.file)
  }

  val dtParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z")

  val allowedFileTypes = Set(
    "html", "htm", "pdf", "txt"
  )

  def update(assignmentId:Long) = (
      authenticationService.ChecksForUserAction andThen assignmentService.ChecksForAccessToAssignment(assignmentId)
    )(parse.multipartFormData) { implicit request => {
      val oldAssignment = request.assignment
      val user = request.user
      request.body.dataParts.get("assignment") match {
        case None => BadRequest(jsonError("Assignment JSON missing"))
        case Some(assignmentJsonRaw) => {
          val jsAssignment = Json.parse(assignmentJsonRaw(0))
          val file = if (request.body.files.nonEmpty) {
            request.body.files(0).ref.file
            val filename = request.body.files(0).filename
            val fileType = filename.split("\\.").reverse.head.toLowerCase()
            if (allowedFileTypes.contains(fileType)) {
              AssignmentDocument(
                -1,
                request.body.files(0).ref.file,
                request.body.files(0).filename,
                fileType
              )
            } else {
              InvalidDocumentType
            }
          } else {
            NoDocumentUploaded
          }

          if (file == InvalidDocumentType) {
            BadRequest(jsonError(s"Invalid file type"))
          } else {
            val newAssignment = Assignment(
              assignmentId,
              oldAssignment.remoteId,
              file,
              (jsAssignment \ "title").as[String],
              (jsAssignment \ "description").as[String],
              dtParser.parse((jsAssignment \ "openDate").as[String]),
              dtParser.parse((jsAssignment \ "closeDate").as[String]),
              oldAssignment.instructor,
              (jsAssignment \ "peerReviewEnabled").as[Boolean],
              (jsAssignment \ "numberRequired").as[Int],
              (jsAssignment \ "rubricType").as[String],
              (jsAssignment \ "reviewRubric").as[List[JsObject]].map(rr => {
                RubricPrompt(
                  (rr \ "id").getOrElse(JsNumber(-1)).as[Long],
                  (rr \ "description").as[String],
                  (rr \ "min").getOrElse(JsNumber(0)).as[Int],
                  (rr \ "max").getOrElse(JsNumber(0)).as[Int]
                )
              }),
              (jsAssignment \ "themes").as[List[JsObject]].zipWithIndex.map({
                case (rr, index) => {
                  Theme(
                    (rr \ "id").getOrElse(JsNumber(-1)).as[Long],
                    (rr \ "description").as[String],
                    null,
                    List(),
                    user,
                    index
                  )
                }
              })
            )
            assignmentRepo.updateAssignment(user, newAssignment) match {
              case Left(AssignmentDoesNotExist(assignmentId)) => {
                logger.warn(s"Assignment $assignmentId not found when requested by ${user.id}")
                NotFound(jsonError(s"Assignment ${assignmentId} not found"))
              }
              case Left(UserCannotUpdateAssignment(user, assignment)) => {
                // It's hard to imagine how this case could occur except through hacking
                logger.warn(s"User ${user.providedName} (${user.id}) is not allowed to update assignment '${assignment.title}' (${assignment.id})")
                Forbidden(
                  jsonError(s"User ${user.providedName} (${user.id}) is not allowed to update assignment '${assignment.title}' (${assignment.id})")
                )
              }
              case Left(x) => {
                logger.error(
                  s"""${user.providedName} (id #${user.id}) trying to update assignment
                   | #${newAssignment.id} ("${newAssignment.title}") caused an unknown
                   | error.
                 """.stripMargin)
              InternalServerError(jsonError("An unknown error occurred"))
            }
            case Right(assignment) =>
              Ok(
                assignment2JSObject(assignment)
              )
            }
          }
        }
      }
    }
  }


}

@Singleton
class DefaultAssignmentController @Inject() (override val userRepo:UserRepository,
      val assignmentRepo:AssignmentRepository,
      override val explicationRepo:ExplicationRepository,
      override val peerReviewRepo:PeerReviewRepository,
      override val lmsContextRepo: LMSContextRepo,
      override val authenticationService: AuthenticationService,
      override val assignmentService: AssignmentService
  ) extends AssignmentController with Controller


