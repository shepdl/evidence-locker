package assignments

import java.io.File
import java.util.Date
import javax.inject.Inject

import anorm._
import com.google.inject.Singleton
import edu.ucla.cdh.EvidenceLocker.authentication.{User, UserProperties, UserRepository}
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import lti.{LMSContext, LMSContextMembershipProperties}
import play.api.{Configuration, Logger}

/**
  * Created by dave on 6/30/16.
  */
@Singleton
class AssignmentRepository @Inject() (config:Configuration, dbs:DatabaseService, userRepo:UserRepository) {

  val db = dbs.db

  val logger = Logger("AssignmentRepository")

  val mediaFile = config.getString("evidence_locker.text_upload_dir", None).getOrElse({
    "/tmp/evidence-locker/"
  })

  val mediaDirectory = new File(mediaFile)

  if (!mediaDirectory.exists()) {
    val directoryCreateStatus = new File(mediaFile).mkdirs()

    if (!directoryCreateStatus) {
      logger.error(s"Could not create directory to store files ${mediaFile}")
    }
  }

  import SqlParser._

  def canCreateAssignment(user:User, context:LMSContext):Boolean = db.withConnection(implicit c => {
    SQL(
      s"""
         |SELECT ${LMSContextMembershipProperties.id} FROM ${LMSContextMembershipProperties.table}
         |WHERE
         |  ${LMSContextMembershipProperties.contextId} = {${LMSContextMembershipProperties.contextId}}
         |  AND ${LMSContextMembershipProperties.roleId} = ${userRepo.roles.instructor.id}
         |  AND ${LMSContextMembershipProperties.userId} = {${LMSContextMembershipProperties.userId}}
       """.stripMargin).on(
            LMSContextMembershipProperties.contextId -> context.id,
            LMSContextMembershipProperties.userId -> user.id
      ).executeQuery().as(long(LMSContextMembershipProperties.id) *).nonEmpty
  })

  def canViewAssignment(user:User, assignment:Assignment):Boolean = {
    db.withConnection(implicit c =>
      SQL(s"""
         |SELECT ${LMSContextMembershipProperties.roleId}
         |FROM ${AssignmentProperties.table}, ${LMSContextMembershipProperties.table}
         |WHERE ${AssignmentProperties.id} = {${AssignmentProperties.id}}
         |  AND ${AssignmentProperties.contextId} = ${LMSContextMembershipProperties.contextId}
         |  AND ${LMSContextMembershipProperties.userId} = {${UserProperties.id}}
       """.stripMargin).on(
        AssignmentProperties.id -> assignment.id,
        UserProperties.id -> user.id
      ).executeQuery()
        .as(long(LMSContextMembershipProperties.roleId) *).nonEmpty
    )
  }

  def fullFilename(assignment:Assignment) = s"${mediaFile}document-${assignment.id}-${assignment.document.fileType}"
  def fileURL(assignment:Assignment) = s"/assignments/${assignment.id}/file"

  def getByRemoteId(remoteId:String):Option[Assignment] = {
    val result = db.withConnection(implicit c => {
      val rubric = SQL(
        s"""
           |SELECT ${RubricPromptProperties.id}, ${RubricPromptProperties.description},
           |       ${RubricPromptProperties.min}, ${RubricPromptProperties.max}
           |FROM   ${RubricPromptProperties.table}, ${AssignmentProperties.table}
           |WHERE  ${RubricPromptProperties.assignmentId} = ${AssignmentProperties.id}
           |  AND  ${AssignmentProperties.remoteId} = {${AssignmentProperties.remoteId}}
           |ORDER BY ${RubricPromptProperties.order}
         """.stripMargin
      ).on(
        AssignmentProperties.remoteId -> remoteId
      ).as(
        long(RubricPromptProperties.id) ~ str(RubricPromptProperties.description)
          ~ int(RubricPromptProperties.min) ~ int(RubricPromptProperties.max) *
      ).map({
        case id ~ description ~ min ~ max => RubricPrompt(id, description, min, max)
      })
      val themes = SQL(
        s"""
           |SELECT ${ThemeProperties.id}, ${ThemeProperties.description}, ${ThemeProperties.order}
           |FROM   ${ThemeProperties.table}, ${AssignmentProperties.table}
           |WHERE  ${ThemeProperties.assignmentId} = ${AssignmentProperties.id}
           |  AND  ${AssignmentProperties.remoteId} = {${AssignmentProperties.remoteId}}
           |  AND  ${ThemeProperties.parentId} IS NULL
           |ORDER BY ${ThemeProperties.order}
         """.stripMargin
      ).on(
        AssignmentProperties.remoteId -> remoteId
      ).executeQuery().as(
        long(ThemeProperties.id) ~ str(ThemeProperties.description) ~ int(ThemeProperties.order) *
      ).map({
        case id ~ description ~ order => Theme(id, description, null, List(), null, order)
      })
      SQL(
        s"""
           |SELECT ${AssignmentProperties.id}, ${AssignmentProperties.title},
           |       ${AssignmentProperties.remoteId}, ${AssignmentProperties.description},
           |       ${AssignmentProperties.openDate}, ${AssignmentProperties.closeDate},
           |       ${AssignmentProperties.peerReviewEnabled}, ${AssignmentProperties.rubricType},
           |       ${AssignmentProperties.numberRequired},
           |       ${UserProperties.id}, ${UserProperties.remoteId}, ${UserProperties.email},
           |       ${UserProperties.name}, ${UserProperties.lastLogin},
           |
           |       ${AssignmentDocumentProperties.id}, ${AssignmentDocumentProperties.systemFilename},
           |       ${AssignmentDocumentProperties.originalFilename}, ${AssignmentDocumentProperties.fileType}
           |FROM   ${AssignmentProperties.table}, ${UserProperties.table}, ${AssignmentDocumentProperties.table}
           |WHERE ${AssignmentProperties.remoteId} = {${AssignmentProperties.remoteId}}
           |  AND ${AssignmentProperties.instructorId} = ${UserProperties.id}
           |  AND ${AssignmentDocumentProperties.assignmentId} = ${AssignmentProperties.id}
         """.stripMargin)
        .on(AssignmentProperties.remoteId -> remoteId)
        .executeQuery().as(getAssignmentParser.*).map({
        case (assignmentId ~ title ~ remoteId ~ description ~ openDate ~ closeDate ~
              peerReviewEnabled ~ rubricType ~ numberRequired ~ userId ~ userRemoteId ~ email ~ userName ~ lastLogin ~
              documentId ~ systemFilename ~ originalFilename ~ fileType
          ) => Assignment(
                assignmentId, remoteId,

                originalFilename match {
                  case None => NoDocumentUploaded
                  case Some(filename) => AssignmentDocument(
                    documentId, new File(systemFilename.get), originalFilename.get, fileType.get
                  )
                },

                title, description, openDate.getOrElse(null), closeDate.getOrElse(null),
                User(userId, userRemoteId, userName, email, lastLogin.orNull),
                peerReviewEnabled.getOrElse(false), numberRequired.getOrElse(-1), rubricType.getOrElse("numeric"), rubric, themes
          )
      })
    })

    result.headOption
  }

  def create(user:User, context:LMSContext, title:String, description:String, instructor:User, remoteId:String):Either[common.ELError,Assignment] = if (!canCreateAssignment(user, context)) {
      Left(UserCannotCreateAssignment(user))
    } else {

    db.withTransaction(implicit c => {
      getByRemoteId(remoteId) match {
        case Some(existingAssignment) => {
          logger.warn(s"Assignment with ${remoteId} for user ${instructor.id} already exists")
          Left(AssignmentAlreadyExists(remoteId))
        }
        case None => {

          val id = SQL(
            s"""
               |INSERT INTO ${AssignmentProperties.table} (
               |    ${AssignmentProperties.remoteId}, ${AssignmentProperties.title},
               |    ${AssignmentProperties.description}, ${AssignmentProperties.instructorId},
               |    ${AssignmentProperties.contextId}
               | )
               |VALUES ({${AssignmentProperties.remoteId}}, {${AssignmentProperties.title}},
               |        {${AssignmentProperties.description}},
               |        {${AssignmentProperties.instructorId}},
               |        {${AssignmentProperties.contextId}}
               | )
             """.stripMargin).on(
                AssignmentProperties.remoteId -> remoteId,
                AssignmentProperties.title -> title,
                AssignmentProperties.description -> description,
                AssignmentProperties.instructorId -> instructor.id,
                AssignmentProperties.contextId -> context.id
              ).executeInsert().get

          SQL(
            s"""
               |INSERT INTO ${AssignmentDocumentProperties.table} (
               |    ${AssignmentDocumentProperties.assignmentId},
               |    ${AssignmentDocumentProperties.uploaderId}
               |) VALUES (
               |  {${AssignmentDocumentProperties.assignmentId}},
               |  {${AssignmentDocumentProperties.uploaderId}}
               |)
             """.stripMargin
          ).on(
            AssignmentDocumentProperties.assignmentId -> id,
            AssignmentDocumentProperties.uploaderId -> instructor.id
          ).executeInsert()

          val assignment = Assignment(
            id, remoteId,
            NoDocumentYet,
            title, description,
            new Date(0), new Date(0),
            instructor,
            false, 0,
            "textual",
            List(), List()
          )

          c.commit()

          Right(assignment)
        }
      }
    })
  }

  val getAssignmentParser =
    long(AssignmentProperties.id) ~ str(AssignmentProperties.title) ~
      str(AssignmentProperties.remoteId) ~ str(AssignmentProperties.description) ~
      get[Option[Date]](AssignmentProperties.openDate) ~ get[Option[Date]](AssignmentProperties.closeDate) ~
      get[Option[Boolean]](AssignmentProperties.peerReviewEnabled) ~ get[Option[String]](AssignmentProperties.rubricType) ~
      get[Option[Int]](AssignmentProperties.numberRequired) ~
      long(UserProperties.id) ~ str(UserProperties.remoteId) ~ str(UserProperties.email) ~
      str(UserProperties.name) ~ get[Option[Date]](UserProperties.lastLogin) ~
      long(AssignmentDocumentProperties.id) ~ get[Option[String]](AssignmentDocumentProperties.systemFilename) ~
      get[Option[String]](AssignmentDocumentProperties.originalFilename) ~ get[Option[String]](AssignmentDocumentProperties.fileType)


  def getById(id:Long):Option[Assignment] = getById(id, null)

  def getById(id:Long, user:User):Option[Assignment] = {
    db.withTransaction(implicit c => {
      val rubric = SQL(
        s"""
            |SELECT ${RubricPromptProperties.id}, ${RubricPromptProperties.description},
            |       ${RubricPromptProperties.min}, ${RubricPromptProperties.max}
            |FROM   ${RubricPromptProperties.table}
            |WHERE  ${RubricPromptProperties.assignmentId} = {${RubricPromptProperties.assignmentId}}
            |ORDER BY ${RubricPromptProperties.order}
         """.stripMargin
      ).on(
        RubricPromptProperties.assignmentId -> id
      ).as(
        long(RubricPromptProperties.id) ~ str(RubricPromptProperties.description)
          ~ int(RubricPromptProperties.min) ~ int(RubricPromptProperties.max) *
      ).map({
        case id ~ description ~ min ~ max => RubricPrompt(id, description, min, max)
      })
      val themes = {
        val userClause = if (user != null) {
          s"${ThemeProperties.ownerId} = {${ThemeProperties.ownerId}} OR "
        } else {
          ""
        }
        SQL(
          s"""
           |SELECT ${ThemeProperties.id}, ${ThemeProperties.description}, ${ThemeProperties.order},
           |       ${UserProperties.id}, ${UserProperties.remoteId}, ${UserProperties.name},
           |       ${UserProperties.email}, ${UserProperties.lastLogin}
           |FROM   ${ThemeProperties.table}
           |  INNER JOIN ${UserProperties.table} ON ${UserProperties.id} = ${ThemeProperties.ownerId}
           |WHERE  ${ThemeProperties.assignmentId} = {${ThemeProperties.assignmentId}}
           |  AND  ( $userClause
           |          ${ThemeProperties.ownerId} IN (
           |            SELECT ${LMSContextMembershipProperties.userId}
           |            FROM ${LMSContextMembershipProperties.table}, ${AssignmentProperties.table}
           |            WHERE ${AssignmentProperties.contextId} = ${LMSContextMembershipProperties.contextId}
           |              AND ${LMSContextMembershipProperties.roleId} = ${userRepo.roles.instructor.id}
           |         )
           |       )
           |  AND  ${ThemeProperties.parentId} IS NULL
           |ORDER BY ${ThemeProperties.order}
         """.stripMargin
      ).on(
        ThemeProperties.assignmentId -> id,
       if (user != null) {
         ThemeProperties.ownerId -> user.id
       } else {
         "none" -> "none"
       }
      ).executeQuery().as(
        long(ThemeProperties.id) ~ str(ThemeProperties.description)
          ~ long(
          UserProperties.id) ~ str(UserProperties.remoteId) ~ str(UserProperties.name) ~ str(UserProperties.email)
            ~ get[Option[Date]](
          UserProperties.lastLogin) ~ int(ThemeProperties.order)
          *
      ).map({
        case id ~ description ~ userId ~ userRemoteId ~ userName ~ email ~ lastLogin ~ order => Theme(id, description, null, List(), User(
          userId, userRemoteId, userName, email, lastLogin.orNull
        ), order)
      })
        }
      val assignment = SQL(
        s"""
           |SELECT ${AssignmentProperties.id}, ${AssignmentProperties.title},
           |       ${AssignmentProperties.remoteId}, ${AssignmentProperties.description},
           |       ${AssignmentProperties.openDate}, ${AssignmentProperties.closeDate},
           |       ${AssignmentProperties.peerReviewEnabled}, ${AssignmentProperties.rubricType},
           |       ${AssignmentProperties.numberRequired},
           |       ${UserProperties.id}, ${UserProperties.remoteId}, ${UserProperties.email},
           |       ${UserProperties.name}, ${UserProperties.lastLogin},
           |
           |       ${AssignmentDocumentProperties.id}, ${AssignmentDocumentProperties.systemFilename},
           |       ${AssignmentDocumentProperties.originalFilename}, ${AssignmentDocumentProperties.fileType}
           |FROM   ${AssignmentProperties.table}, ${UserProperties.table}, ${AssignmentDocumentProperties.table}
           |WHERE  ${AssignmentProperties.id} = {${AssignmentProperties.id}}
           |  AND  ${AssignmentProperties.instructorId} = ${UserProperties.id}
           |  AND  ${AssignmentDocumentProperties.assignmentId} = ${AssignmentProperties.id}
         """.stripMargin).on(AssignmentProperties.id -> id).executeQuery().as(
          getAssignmentParser.*
      ).map({
        case (assignmentId ~ title ~ remoteId ~ description ~ openDate ~ closeDate ~
          peerReviewEnabled ~ rubricType ~ numberRequired ~ userId ~ userRemoteId ~ email ~ userName ~ lastLogin) ~
          documentId ~ systemFilename ~ originalFilename ~ fileType => Assignment(
            assignmentId, remoteId,

            originalFilename match {
              case None => NoDocumentUploaded
              case Some(filename) => AssignmentDocument(
                documentId, new File( if (systemFilename.get.charAt(0) == '.') {
                  "." + systemFilename.get
                } else {
                  systemFilename.get
                }
                ), originalFilename.get, fileType.get
              )
            },

            title, description, openDate.getOrElse(new Date(0)), closeDate.getOrElse(new Date(0)),
            User(userId, userRemoteId, userName, email, lastLogin.orNull),
            peerReviewEnabled.getOrElse(false), numberRequired.getOrElse(0), rubricType.getOrElse("numeric"), rubric, themes
          )
      })
      assignment.headOption
    })

  }

  def canUpdateAssignment(user:User, assignment: Assignment):Boolean = assignment.instructor == user

  val noFile = new File("no-file")

  def updateAssignment(user:User, assignment:Assignment):Either[common.ELError,Assignment] = db.withTransaction(implicit c => if (!canUpdateAssignment(user, assignment)) {
    Left(UserCannotUpdateAssignment(user, assignment))
  } else {

    SQL(
      s"""
         |UPDATE ${AssignmentProperties.table}
         |SET
         |    ${AssignmentProperties.title} = {${AssignmentProperties.title}},
         |    ${AssignmentProperties.description} = {${AssignmentProperties.description}},
         |    ${AssignmentProperties.remoteId} = {${AssignmentProperties.remoteId}},
         |    ${AssignmentProperties.updatedAt} = NOW(),
         |
         |    ${AssignmentProperties.openDate} = {${AssignmentProperties.openDate}},
         |    ${AssignmentProperties.closeDate} = {${AssignmentProperties.closeDate}},
         |    ${AssignmentProperties.peerReviewEnabled} = {${AssignmentProperties.peerReviewEnabled}},
         |    ${AssignmentProperties.numberRequired} = {${AssignmentProperties.numberRequired}},
         |    ${AssignmentProperties.rubricType} = {${AssignmentProperties.rubricType}},
         |    ${AssignmentProperties.instructorId} = {${AssignmentProperties.instructorId}}
         |WHERE
         |    ${AssignmentProperties.id} = {${AssignmentProperties.id}}
       """.stripMargin)
      .on(
        AssignmentProperties.title -> assignment.title,
        AssignmentProperties.description -> assignment.description,
        AssignmentProperties.remoteId -> assignment.remoteId,

        AssignmentProperties.openDate -> assignment.openDate,
        AssignmentProperties.closeDate -> assignment.closeDate,
        AssignmentProperties.peerReviewEnabled -> assignment.peerReviewEnabled,
        AssignmentProperties.numberRequired -> assignment.numberRequired,
        AssignmentProperties.rubricType -> assignment.rubricType,
        AssignmentProperties.instructorId -> assignment.instructor.id,
        AssignmentProperties.id -> assignment.id
      ).executeUpdate()

    if (assignment.document != NoDocumentUploaded) {
      assignment.document.file.renameTo(
        new File(fullFilename(assignment))
      )
      SQL(
        s"""
           |UPDATE ${AssignmentDocumentProperties.table}
           |SET
           |    ${AssignmentDocumentProperties.systemFilename} = {${AssignmentDocumentProperties.systemFilename}},
           |    ${AssignmentDocumentProperties.originalFilename} = {${AssignmentDocumentProperties.originalFilename}},
           |    ${AssignmentDocumentProperties.fileType} = {${AssignmentDocumentProperties.fileType}},
           |    ${AssignmentDocumentProperties.uploaderId} = {${AssignmentDocumentProperties.uploaderId}}
           |WHERE
           |  ${AssignmentDocumentProperties.assignmentId} = {${AssignmentDocumentProperties.assignmentId}}
         """.stripMargin
      ).on(
        AssignmentDocumentProperties.systemFilename -> fullFilename(assignment),
        AssignmentDocumentProperties.originalFilename -> assignment.document.originalFilename,
        AssignmentDocumentProperties.fileType -> assignment.document.fileType,
        AssignmentDocumentProperties.uploaderId -> user.id,
        AssignmentDocumentProperties.assignmentId -> assignment.id
      ).executeUpdate()

    }

    val currentRubricIds = SQL(
      s"""
         |SELECT ${RubricPromptProperties.id}
         |FROM ${RubricPromptProperties.table}
         |WHERE ${RubricPromptProperties.assignmentId} = {${RubricPromptProperties.assignmentId}}
       """.stripMargin
    ).on(
      RubricPromptProperties.assignmentId -> assignment.id
    ).executeQuery().as(long(RubricPromptProperties.id) *).map({
      case id => id
    }).toSet

    assignment.reviewRubric.zipWithIndex.foreach({
      case (rubric, index) => {
        val id = if (!rubric.isSaved) {
          SQL(
            s"""
              |INSERT INTO ${RubricPromptProperties.table} (
              |  ${RubricPromptProperties.assignmentId},
              |  ${RubricPromptProperties.description},
              |  ${RubricPromptProperties.min},
              |  ${RubricPromptProperties.max},
              |  ${RubricPromptProperties.order}
              |) VALUES (
              |  {${RubricPromptProperties.assignmentId}},
              |  {${RubricPromptProperties.description}},
              |  {${RubricPromptProperties.min}},
              |  {${RubricPromptProperties.max}},
              |  {${RubricPromptProperties.order}}
              |)
            """.stripMargin).on(
            RubricPromptProperties.assignmentId -> assignment.id,
            RubricPromptProperties.description -> rubric.description,
            RubricPromptProperties.min -> rubric.min,
            RubricPromptProperties.max -> rubric.max,
            RubricPromptProperties.order -> index
          ).executeInsert().get
        } else {
          SQL(
            s"""
               |UPDATE ${RubricPromptProperties.table}
               |SET
               |  ${RubricPromptProperties.assignmentId} = {${RubricPromptProperties.assignmentId}},
               |  ${RubricPromptProperties.description}  = {${RubricPromptProperties.description}},
               |  ${RubricPromptProperties.min}          = {${RubricPromptProperties.min}},
               |  ${RubricPromptProperties.max}          = {${RubricPromptProperties.max}},
               |  ${RubricPromptProperties.order}        = {${RubricPromptProperties.order}},
               |  ${RubricPromptProperties.updatedAt}    = NOW()
               |WHERE
               |  ${RubricPromptProperties.id}           = {${RubricPromptProperties.id}}
             """.stripMargin
          ).on(
            RubricPromptProperties.assignmentId -> assignment.id,
            RubricPromptProperties.description -> rubric.description,
            RubricPromptProperties.min -> rubric.min,
            RubricPromptProperties.max -> rubric.max,
            RubricPromptProperties.order -> index,
            RubricPromptProperties.id -> rubric.id
          ).executeUpdate()
        }
      }
    })

    val rubricsToDelete = currentRubricIds &~ assignment.reviewRubric.map(_.id).toSet
    if (rubricsToDelete.nonEmpty) {
      SQL(s"""
         |DELETE FROM ${RubricPromptProperties.table} WHERE ${RubricPromptProperties.id} IN (${rubricsToDelete.mkString(",")})
       """.stripMargin).
        executeUpdate()
      }

    val currentThemes = SQL(
      s"""
         |SELECT ${ThemeProperties.id} FROM ${ThemeProperties.table}
         |WHERE ${ThemeProperties.assignmentId} = {${ThemeProperties.assignmentId}}
         |  AND ${ThemeProperties.ownerId}      = {${ThemeProperties.ownerId}}
       """.stripMargin).on(
          ThemeProperties.assignmentId -> assignment.id,
          ThemeProperties.ownerId -> assignment.instructor.id
        ).executeQuery().as(
          long(ThemeProperties.id) *
        ).toSet

      val persistentThemeIds = assignment.themes.zipWithIndex.map({
        case (theme, index) => {
          if (!theme.isSaved) {
            SQL(s"""
               |INSERT INTO ${ThemeProperties.table} (
               |  ${ThemeProperties.description}, ${ThemeProperties.assignmentId},
               |  ${ThemeProperties.ownerId}, ${ThemeProperties.order}
               |) VALUES (
               |  {${ThemeProperties.description}}, {${ThemeProperties.assignmentId}},
               |  {${ThemeProperties.ownerId}}, {${ThemeProperties.order}}
               |)
             """.
                stripMargin).on(
              ThemeProperties.description -> theme.description,
              ThemeProperties.assignmentId -> assignment.id,
              ThemeProperties.ownerId -> theme.owner.id,
              ThemeProperties.order -> index
            ).executeInsert().get
          } else {
             SQL(
               s"""
                  |UPDATE ${ThemeProperties.table}
                  |SET
                  |  ${ThemeProperties.description} = {${ThemeProperties.description}},
                  |  ${ThemeProperties.assignmentId} = {${ThemeProperties.assignmentId}},
                  |  ${ThemeProperties.ownerId} = {${ThemeProperties.ownerId}},
                  |  ${ThemeProperties.order} = {${ThemeProperties.order}},
                  |  ${ThemeProperties.updatedAt} = NOW()
                  |WHERE
                  |  ${ThemeProperties.id} = {${ThemeProperties.id}}
                """.stripMargin).on(
               ThemeProperties.description -> theme.description,
               ThemeProperties.assignmentId -> assignment.id,
               ThemeProperties.ownerId -> assignment.instructor.id,
               ThemeProperties.order -> index,
               ThemeProperties.id -> theme.id
             ).executeUpdate()
            theme.id
          }
      }
    }).toSet

    val themeIdsToDelete = currentThemes &~ persistentThemeIds

    if (themeIdsToDelete.nonEmpty) {
      SQL(s"""
         |DELETE FROM ${ThemeProperties.table}
         |WHERE ${ThemeProperties.id} IN (${themeIdsToDelete.mkString(",")})
       """.stripMargin)
        .executeUpdate()
      }

    c.commit()
    Right(getById(assignment.id).get)
  })

}



case class UserCannotCreateAssignment(user:User) extends common.ELError
case class UserCannotUpdateAssignment(user:User, assignment:Assignment) extends common.ELError
case class AssignmentDoesNotExist(assignmentId:Long) extends Throwable with common.ELError
case class AssignmentAlreadyExists(remoteId:String) extends common.ELError
