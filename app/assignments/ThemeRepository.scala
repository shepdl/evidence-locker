package assignments

import java.util.Date
import javax.inject.Inject

import annotations.AnnotationProperties
import anorm._
import com.google.inject.Singleton
import common.{ELError, HandlesMySQLBooleans}
import edu.ucla.cdh.EvidenceLocker.authentication.{User, UserProperties, UserRepository}
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import lti._
import play.api.Logger

/**
  * Created by daveshepard on 7/16/16.
  */
@Singleton
class ThemeRepository @Inject() (
  dbs:DatabaseService, userRepo:UserRepository, override val lmsContextRepo:LMSContextRepo, assignmentRepo:AssignmentRepository
) extends ChecksAccessToAssignment with HandlesMySQLBooleans {

  val db = dbs.db

  val logger = Logger("AssignmentRepository")

  import SqlParser._

  def getChildThemes(parentId:Long, owner:User):List[Theme] = {
    db.withConnection(implicit c => {
      SQL(
        s"""
           |SELECT ${ThemeProperties.id}, ${ThemeProperties.ownerId}, ${ThemeProperties.description}, ${ThemeProperties.order}
           |FROM   ${ThemeProperties.table}
           |WHERE  ${ThemeProperties.parentId} = {${ThemeProperties.parentId}}
           |  AND  ${ThemeProperties.ownerId}  = {${ThemeProperties.ownerId}}
         """.stripMargin).on(
        ThemeProperties.parentId -> parentId,
        ThemeProperties.ownerId  -> owner.id
      ).executeQuery().as(
        long(ThemeProperties.id) ~ long(ThemeProperties.ownerId) ~ str(ThemeProperties.description) ~ int(ThemeProperties.order) *
      ).map({
        case themeId~ownerId~description~order => Theme(themeId, description, null, getChildThemes(themeId, owner), owner, order)
      })
    }).toList
  }

  val noParent = Theme(-1, "No parent", null, List(), null, -1)

  def getThemeTreeForUser(student:User, assignment:Assignment):Either[ELError,List[Theme]] = db.withTransaction(implicit c => {
    userCanAccessAssignment(student, assignment) match {
      case Left(err) => Left(err)
      case Right(status) => status match {
        case false => Left(UserHasNoAccessToAssignment(student, assignment))
        case true  => {
          val themes = SQL(
            s"""
               |SELECT ${ThemeProperties.id}, ${ThemeProperties.description}, ${ThemeProperties.order},
               |       ${UserProperties.id}, ${UserProperties.remoteId}, ${UserProperties.name},
               |       ${UserProperties.email}, ${UserProperties.lastLogin}
               |FROM   ${ThemeProperties.table}, ${UserProperties.table}
               |WHERE  ${ThemeProperties.assignmentId} = {${ThemeProperties.assignmentId}}
               |  AND  ${ThemeProperties.ownerId} = ${UserProperties.id}
               |  AND  ${ThemeProperties.parentId} IS NULL
               |  AND  (${ThemeProperties.ownerId} = {${ThemeProperties.ownerId}} OR ${ThemeProperties.ownerId} = {${AssignmentProperties.instructorId}})
             """.stripMargin).on(
                ThemeProperties.assignmentId -> assignment.id,
                ThemeProperties.ownerId -> student.id,
                AssignmentProperties.instructorId -> assignment.instructor.id
            ).executeQuery().as(
              long(ThemeProperties.id) ~ str(ThemeProperties.description)
               ~ long(UserProperties.id) ~ str(UserProperties.remoteId) ~ str(UserProperties.name)
               ~ str(UserProperties.email) ~ get[Option[Date]](UserProperties.lastLogin) ~ int(ThemeProperties.order) *
            ).map({
            case themeId~description~userId~remoteId~userName~email~lastLogin~order => Theme(
              themeId, description, noParent, getChildThemes(themeId, student), User(
                  userId, remoteId, userName, email, lastLogin.orNull
                ), order
              )
            })
          Right(themes)
        }
      }
    }
  })

  def getById(user:User, id:Long):Option[Theme] = db.withTransaction(implicit c => {
    // TODO: Check if user can view theme (that is, if user is a member of the correct context)
    val theme = SQL(
      s"""
         |SELECT ${ThemeProperties.id}, ${ThemeProperties.description}, ${ThemeProperties.order},
         |       ${UserProperties.id}, ${UserProperties.remoteId}, ${UserProperties.name},
         |       ${UserProperties.email}, ${UserProperties.lastLogin}
         |FROM  ${ThemeProperties.table}, ${UserProperties.table}
         |WHERE ${ThemeProperties.id} = {${ThemeProperties.id}}
         |  AND ${ThemeProperties.ownerId} = ${UserProperties.id}
         |""".stripMargin
    ).on(
      ThemeProperties.id -> id
    ).executeQuery().as(
      long(ThemeProperties.id) ~ str(ThemeProperties.description)
        ~ long(UserProperties.id) ~ str(UserProperties.remoteId) ~ str(UserProperties.name)
        ~ str(UserProperties.email) ~ get[Option[Date]](UserProperties.lastLogin) ~ int(ThemeProperties.order) *
    ).map({
      case themeId ~ description ~ userId ~ remoteId ~ userName ~ email ~ lastLogin ~ order =>
        Theme(themeId, description, getParent(user, id).orNull, getChildThemes(themeId, user), User(
          userId, remoteId, userName, email, lastLogin.orNull
        ), order)
    })
    theme.headOption
  })

  def getParent(user:User, theme:Theme):Option[Theme] = getParent(user, theme.id)

  def getParent(user:User, themeId:Long):Option[Theme] = db.withTransaction(implicit c => {
    SQL(
      s"""
         |SELECT ${ThemeProperties.id}, ${ThemeProperties.description}, ${ThemeProperties.order},
         |       ${UserProperties.id}, ${UserProperties.remoteId}, ${UserProperties.name},
         |       ${UserProperties.email}, ${UserProperties.lastLogin}
         |FROM  ${ThemeProperties.table}, ${UserProperties.table}
         |WHERE ${ThemeProperties.id} IN (
         |    SELECT ${ThemeProperties.parentId}
         |    FROM ${ThemeProperties.table}
         |    WHERE ${ThemeProperties.id} = {${ThemeProperties.id}}
         |  )
         | AND ${ThemeProperties.ownerId} = ${UserProperties.id}
       """.stripMargin
    ).on(
      ThemeProperties.id -> themeId
    ).executeQuery().as(
      long(ThemeProperties.id) ~ str(ThemeProperties.description)
        ~ long(UserProperties.id) ~ str(UserProperties.remoteId) ~ str(UserProperties.name)
        ~ str(UserProperties.email) ~ get[Option[Date]](UserProperties.lastLogin) ~ int(ThemeProperties.order) *
    ).map({
      case themeId ~ description ~ userId ~ remoteId ~ userName ~ email ~ lastLogin ~ order =>
        Theme(themeId, description, null, null, User(
          userId, remoteId, userName, email, lastLogin.orNull
        ), order
      )
    }).headOption
  })

  def getForAnnotation(annotationId:Long):Option[Theme] = db.withConnection(implicit c => {
    SQL(
      s"""
         |SELECT ${ThemeProperties.id}, ${ThemeProperties.description}, ${ThemeProperties.order},
         |       ${UserProperties.id}, ${UserProperties.remoteId}, ${UserProperties.name},
         |       ${UserProperties.email}, ${UserProperties.lastLogin}
         |FROM   ${ThemeProperties.table}, ${UserProperties.table}, ${AnnotationProperties.table}
         |WHERE  ${ThemeProperties.ownerId} = ${UserProperties.id}
         |  AND  ${AnnotationProperties.themeId} = ${ThemeProperties.id}
         |  AND  ${AnnotationProperties.id} = {${AnnotationProperties.id}}
       """.stripMargin
    ).on(
      AnnotationProperties.id -> annotationId.toString
    ).executeQuery().as(
      long(ThemeProperties.id) ~ str(ThemeProperties.description)
        ~ long(UserProperties.id) ~ str(UserProperties.remoteId) ~ str(UserProperties.name)
        ~ str(UserProperties.email) ~ get[Option[Date]](UserProperties.lastLogin) ~ int(ThemeProperties.order) *
    ).map({
      case themeId ~ description ~ userId ~ remoteId ~ userName ~ email ~ lastLogin ~ order =>
        Theme(themeId, description, null, List(), User(
          userId, remoteId, userName, email, lastLogin.orNull
        ), order
        )
    }).headOption
  })

  def userCanCreateThemeAsChildOfTheme(user:User, assignment:Assignment, parentTheme:Theme):Either[ELError,Boolean] = {
    lmsContextRepo.getContextForAssignment(user, assignment) match {
      case None => {
        logger.error(s"Assignment ${assignment.description} (#${assignment.id}) has no context")
        Left(new UnnaturalStateException(s"Assignment ${assignment.description} (#${assignment.id}) has no context"))
      }
      case Some(context) => {
        lmsContextRepo.getRolesInContextForUser(user, context).roles.nonEmpty match {
          case false => Left(UserHasNoAccessToAssignment(user, assignment))
          case true => Right(true)
        }
      }
    }
  }

  def assignmentForTheme(theme:Theme):Either[ELError,Assignment] = db.withTransaction(implicit c => {
    SQL(s"""
         |SELECT ${ThemeProperties.assignmentId}
         |FROM ${ThemeProperties.table}
         |WHERE ${ThemeProperties.id} = {${ThemeProperties.id}}
       """.stripMargin).on(
      ThemeProperties.id -> theme.id
    ).executeQuery().as(long(ThemeProperties.assignmentId) *).map({
      case assignmentId => assignmentId
    }).headOption match {
      case Some(assignmentId) => assignmentRepo.getById(assignmentId) match {
        case None => {
          logger.error(s"Assignment ${assignmentId} (for theme ${theme.id}) not found")
          Left(AssignmentDoesNotExist(assignmentId))
        }
        case Some(assignment) => Right(assignment)
      }
      case None => {
        logger.error(s"Theme ${theme.id} not found")
        Left(new ThemeDoesNotExistException(theme.id))
      }
    }
  })

  def create(user:User, assignment:Assignment, description:String, children:List[Theme], parent:Theme, owner:User, order:Int):Either[ELError, Theme] =
    db.withTransaction(implicit c => {
      userCanCreateThemeAsChildOfTheme(user, assignment, parent) match {
        case Left(ex) => Left(ex)
        case Right(_) => {
          val orderToUse = if (order != -1) {
            order
          } else {
            if (parent != null) {
              val orders = SQL(s"""
                                  |SELECT ${ThemeProperties.order}
                                  |FROM ${ThemeProperties.table}
                                  |WHERE ${ThemeProperties.id} = {parentId}
             """.stripMargin).on(
                "parentId" -> parent.id
              ).executeQuery().as(
                int(
                  ThemeProperties.order) *
              ).map({
                case x => x
              })

              if (orders.nonEmpty) {
                orders.max
              } else {
                -1
              }
            } else {
              -1
            }
          }

          // TODO: verify parent ID and assignment ID
          val fields = s"""
               |(
               |  ${ThemeProperties.description}, ${ThemeProperties.ownerId}, ${ThemeProperties.assignmentId},
               |  ${ThemeProperties.parentId}, ${ThemeProperties.order}
               |)
             """.stripMargin
          val id = if (parent != null) {
            SQL(
              s"""INSERT INTO ${ThemeProperties.table} (
               |  ${ThemeProperties.description}, ${ThemeProperties.ownerId}, ${ThemeProperties.assignmentId},
               |  ${ThemeProperties.parentId}, ${ThemeProperties.order}
               |) VALUES (
               |  {${ThemeProperties.description}}, {${ThemeProperties.ownerId}}, {${ThemeProperties.assignmentId}},
               |  {${ThemeProperties.parentId}}, {${ThemeProperties.order}}
               |)""".stripMargin).on(
                ThemeProperties.description -> description,
                ThemeProperties.ownerId -> owner.id,
                ThemeProperties.assignmentId -> assignment.id,
                ThemeProperties.parentId -> parent.id,
                ThemeProperties.order -> orderToUse
              ).executeInsert().get
          } else {
            SQL(s"""INSERT INTO ${ThemeProperties.table} (
                |  ${ThemeProperties.description}, ${ThemeProperties.ownerId}, ${ThemeProperties.assignmentId},
                |  ${ThemeProperties.order}
                |) VALUES (
                |  {${ThemeProperties.description}}, {${ThemeProperties.ownerId}}, {${ThemeProperties.assignmentId}},
                |  {${ThemeProperties.order}}
                |)""".stripMargin).on(
              ThemeProperties.description -> description,
              ThemeProperties.ownerId -> owner.id,
              ThemeProperties.assignmentId -> assignment.id,
              ThemeProperties.order -> orderToUse
            ).executeInsert().get
          }
          Right(Theme(id, description, parent, children, owner, orderToUse))
        }
      }
    })

  def userCanUpdateTheme(user:User, assignment: Assignment, theme: Theme):Either[ELError,Boolean] = db.withTransaction(implicit c => {
    lmsContextRepo.getContextForAssignment(user, assignment) match {
      case None => {
        logger.error(s"Assignment ${assignment.description} (#${assignment.id} has no context")
        Left(new UnnaturalStateException(s"Assignment ${assignment.description} (#${assignment.id}) has no context"))
      }
      case Some(context) => lmsContextRepo.getRolesInContextForUser(user, context).roles.nonEmpty match {
        case false => Left(UserHasNoAccessToAssignment(user, assignment))
        case true => {
          Right(
            SQL(
              s"""
                 |SELECT (${ThemeProperties.ownerId} = {${ThemeProperties.ownerId}}) AS permission
                 |FROM ${ThemeProperties.table}
                 |WHERE ${ThemeProperties.id} = {${ThemeProperties.id}}
               """.stripMargin).on(
              ThemeProperties.ownerId -> user.id,
              ThemeProperties.id -> theme.id
            ).executeQuery().as(long("permission").single) == 1
          )
        }
      }
    }
  })

  def update(user:User, theme:Theme, assignment:Assignment):Either[ELError, Theme] = db.withTransaction(implicit c => {
    userCanUpdateTheme(user, assignment, theme) match {
      case Left(ex) => Left(ex)
      case Right(canUpdate) => if (canUpdate) {
        SQL(s"""
             |UPDATE ${ThemeProperties.table}
             |SET
             |   ${ThemeProperties.description} = {${ThemeProperties.description}},
             |   ${ThemeProperties.ownerId} = {${ThemeProperties.ownerId}},
             |   ${ThemeProperties.parentId} = {${ThemeProperties.parentId}},
             |   ${ThemeProperties.assignmentId} = {${ThemeProperties.assignmentId}},
             |   ${ThemeProperties.order} = {${ThemeProperties.order}},
             |   ${ThemeProperties.updatedAt} = NOW()
             |WHERE
             |  ${ThemeProperties.id} = {${ThemeProperties.id}}
           """.stripMargin).on(
          ThemeProperties.description -> theme.description,
          ThemeProperties.ownerId -> theme.owner.id,
          ThemeProperties.parentId -> theme.parent.id,
          ThemeProperties.assignmentId -> assignment.id,
          ThemeProperties.order -> theme.order,
          ThemeProperties.id -> theme.id
        ).executeUpdate()
        Right(getById(user, theme.id).get)
      } else {
        c.rollback()
        Left(UserCannotUpdateTheme(user, theme))
      }
    }
  })

  def delete(user:User, assignment: Assignment, theme: Theme):Either[ELError, Theme] = db.withTransaction(implicit c => {
    userCanUpdateTheme(user, assignment, theme) match {
      case Left(ex) => Left(ex)
      case Right(_) => {
        SQL(
          s"""
             |DELETE FROM ${ThemeProperties.table} WHERE ${ThemeProperties.id} = {${ThemeProperties.id}}
           """.stripMargin).on(
          ThemeProperties.id -> theme.id
        ).executeUpdate()
        Right(theme)
      }
    }
  })

}


trait ChecksAccessToAssignment {

  val lmsContextRepo:LMSContextRepo
  val logger:Logger

  def userCanAccessAssignment(user:User, assignment:Assignment):Either[ELError, Boolean] = {
    lmsContextRepo.getContextForAssignment(user, assignment) match {
      case None => {
        logger.error(s"Assignment ${assignment.description} (#${assignment.id}) has no context")
        Left(new UnnaturalStateException(s"Assignment ${assignment.description} (#${assignment.id}) has no context"))
      } case Some(context) => {
        Right(
          lmsContextRepo.getRolesInContextForUser(user, context).roles.nonEmpty
        )
      }
    }
  }

}


case class UserCannotCreateTheme(user:User) extends ELError
case class UserHasNoAccessToAssignment(user:User, assignment: Assignment) extends ELError
case class UserCannotUpdateTheme(user:User, theme: Theme) extends ELError
case class UserCannotDeleteTheme(user:User, theme: Theme) extends ELError

class UnnaturalStateException(message:String) extends Throwable with ELError

class AssignmentNotFoundForTheme(theme:Theme) extends Throwable with ELError


class ParentNotFoundException(parentId:Long) extends Throwable with ELError
class UserNotFoundException(val userId:Long) extends Throwable with ELError {
  def this() = this(-1)
}
class ThemeDoesNotExistException(val themeId:Long) extends Throwable with ELError
class ThemeParentDoesNotExist(val themeId:Long) extends Throwable with ELError
