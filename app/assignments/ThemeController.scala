package assignments

import javax.inject.Inject

import authentication.{AssignmentService, AuthenticationService}
import com.google.inject.Singleton
import common.JsonErrorReporting
import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import play.api.Logger
import play.api.data.Form
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.{JsObject, Json}
import play.api.mvc.{Action, Controller}

/**
  * Created by daveshepard on 7/16/16.
  */
trait ThemeController extends JsonErrorReporting with I18nSupport with RendersThemesAsJson {

  this:Controller =>

  val userRepo:UserRepository
  val assignmentRepo:AssignmentRepository
  val themeRepo:ThemeRepository

  val authenticationService: AuthenticationService
  val assignmentService: AssignmentService

  val messagesApi: MessagesApi

  val logger = Logger("ThemeController")

  import play.api.libs.json._
  import play.api.data.Forms._

  def getThemeTreeForAssignment(assignmentId:Long) = (
    authenticationService.ChecksForUserAction andThen assignmentService.ChecksForAccessToAssignment(assignmentId)
    ) { implicit request => themeRepo.getThemeTreeForUser(request.user, request.assignment) match {
      case Left(error) => Forbidden(jsonError(error.toString))
      case Right(themes) => Ok(Json.arr(themes.map(theme2Json)))
    }
  }

  implicit val themeForm:Form[ThemeData] = Form(
    mapping(
      "description" -> nonEmptyText,
      "parentId" -> optional(number),
      "order" -> optional(number)
    )(ThemeData.apply)(ThemeData.unapply)
  )

  def create(assignmentId:Long) = (
    authenticationService.ChecksForUserAction andThen assignmentService.ChecksForAccessToAssignment(assignmentId)
  )(parse.urlFormEncoded) { implicit request =>
    val user = request.user
    val assignment = request.assignment
    themeForm.bindFromRequest.fold(
      formWithErrors => BadRequest(formWithErrors.errorsAsJson),
      themeData => {
        themeData.parentId match {
          case Some(themeId) => themeRepo.getById(user, themeId) match {
            case None => BadRequest(s"Theme ${themeId} not found")
            case Some(parentTheme) => {
              themeRepo.create(
                user, assignment, themeData.description,
                List(), parentTheme, user, themeData.order.getOrElse(-1)
              ) match {
                case Left(error:UserHasNoAccessToAssignment) => Forbidden(jsonError(error.toString))
                case Left(error) => BadRequest(jsonError(error.toString))
                case Right(theme) => Created(theme2Json(theme))
              }
            }
          }
          case None => {
            themeRepo.create(
              user, assignment, themeData.description,
              List(), null, user, themeData.order.getOrElse(-1)
            ) match {
              case Left(error:UserHasNoAccessToAssignment) => Forbidden(jsonError(error.toString))
              case Left(error) => BadRequest(jsonError(error.toString))
              case Right(theme) => Created(theme2Json(theme))
            }
          }
        }
      }
    )
  }

  def getById(themeId:Long) = authenticationService.ChecksForUserAction { implicit request =>
    val user = request.user
    themeRepo.getById(user, themeId) match {
      case None => {
        NotFound(jsonError(s"Theme ${themeId} not found"))
      }
      case Some(theme) => {
        themeRepo.assignmentForTheme(theme) match {
          case Left(ex) => InternalServerError(jsonError("An unknown error occurred"))
          case Right(assignment) => themeRepo.userCanAccessAssignment(user, assignment)  match {
            case Left(ex) => Forbidden(jsonError("Sorry; you're not allowed to access this assignment"))
            case Right(_) => Ok(theme2Json(theme))
          }
        }
      }
    }
  }

  def update(id:Long) = (authenticationService.ChecksForUserAction) (parse.json) { implicit request =>
    val user = request.user
    val json = request.body
    val parentId = (json \ "parentId").as[Long]
    themeRepo.getById(user, parentId) match {
      case None => NotFound(jsonError(s"Parent $parentId not found"))
      case Some(parentTheme) => {
        val theme = Theme(
          id, (json \ "description").as[String],
          parentTheme,
          List(),
          user,
          0
        )
        themeRepo.assignmentForTheme(theme) match {
          case Left(ex:AssignmentDoesNotExist) => NotFound(jsonError(s"Assignment for theme ${theme.id} not found"))
          case Left(ex:ThemeDoesNotExistException) => NotFound(jsonError(s"Assignment or theme not found"))
          case Right(assignment) => {
            themeRepo.update(user, theme, assignment) match {
              case Left(ex:ThemeDoesNotExistException) => NotFound(jsonError(s"Theme ${theme.id} not found"))
              case Left(ex:UserHasNoAccessToAssignment) => Forbidden(jsonError(s"User ${user.id} is not allowed to access assignment ${assignment.id}"))
              case Left(ex:UserCannotUpdateTheme) => Forbidden(jsonError(s"User ${user.id} is not allowed to update theme ${id}"))
              case Right(theme) => {
                Ok(theme2Json(theme))
              }
            }
          }
        }
      }
    }
  }

  def delete(id:Long) = authenticationService.ChecksForUserAction {implicit request =>
    val user = request.user
    themeRepo.getById(user, id) match {
      case None => {
        logger.warn(s"Theme ${id} not found")
        Forbidden(jsonError(s"Theme ${id} not found"))
      }
      case Some(theme) => {
        themeRepo.assignmentForTheme(theme) match {
          case Left(ex:AssignmentDoesNotExist) => NotFound(jsonError(s"Assignment for theme ${theme.id} not found"))
          case Left(ex:ThemeDoesNotExistException) => NotFound(jsonError(s"Assignment or theme not found"))
          case Left(ex:UserCannotUpdateTheme) => Forbidden(s"User ${user.id} is not allowed to update theme ${id}")
          case Right(assignment) => {
            themeRepo.delete(user, assignment, theme) match {
              case Left(ex) => Forbidden(jsonError(s"Cannot delete theme ${theme.id}"))
              case Right(theme) => Ok(Json.obj("message" -> s"Theme ${id} deleted"))
            }
          }
        }
      }
    }
  }

}

case class ThemeData(description:String, parentId:Option[Int], order:Option[Int])

trait RendersThemesAsJson {

  def theme2Json(theme:Theme):JsObject = Json.obj(
    "id" -> theme.id,
    "description" -> theme.description,
    "children" -> theme.children.map(theme2Json),
    "order" -> theme.order
  )

}


@Singleton
class DefaultThemeController @Inject() (
     override val userRepo:UserRepository, override val assignmentRepo:AssignmentRepository,
     override val themeRepo:ThemeRepository, override val messagesApi:MessagesApi,
     override val authenticationService:AuthenticationService,
     override val assignmentService: AssignmentService
 ) extends ThemeController with Controller
