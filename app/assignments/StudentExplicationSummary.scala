package assignments

import annotations.Explication
import edu.ucla.cdh.EvidenceLocker.authentication.User

/**
  * Created by dave on 11/28/16.
  */
class StudentExplicationSummary(val student:User, val explications:List[Explication])
