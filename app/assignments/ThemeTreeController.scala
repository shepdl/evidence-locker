package assignments

import javax.inject.Inject

import annotations.{Annotation, AnnotationRepository, Explication, ExplicationRepository}
import common.JsonErrorReporting
import edu.ucla.cdh.EvidenceLocker.authentication.{User, UserRepository}
import play.api.Logger
import play.api.mvc.{Action, Controller}
import reviews.{PeerReview, PeerReviewRepository}

/**
  * Created by dave on 9/9/16.
  */
trait ThemeTreeController extends JsonErrorReporting {

  this:Controller =>

  val userRepo:UserRepository
  val themeRepo:ThemeRepository
  val assignmentRepo:AssignmentRepository
  val annotationRepo:AnnotationRepository
  val explicationRepo:ExplicationRepository
  val peerReviewRepo:PeerReviewRepository

  val logger = Logger("ThemeTreeController")

  import play.api.libs.json._

  private def retrieveAdditionalDataForTheme(user:User, theme:Theme):ThemeWithAnnotations = {
    val annotations = annotationRepo.getForThemeFilteredByUser(user, theme, user) match {
      case Right(annotations) => annotations.map(annotation =>  {
        val explication = explicationRepo.getByAnnotationId (user, annotation.id).right.get
        val peerReviews = peerReviewRepo.listCompletedForExplication (user, explication).right.get
        AnnotationWithExplication(annotation, ExplicationWithReviews(explication, peerReviews))
      })
      case Left(_) => List()
    }
    val subThemes = themeRepo.getChildThemes(theme.id, user).map(retrieveAdditionalDataForTheme(user, _))
    ThemeWithAnnotations(theme, subThemes, annotations)
  }

  private def theme2Json(assignment:Assignment, theme:ThemeWithAnnotations):JsObject = Json.obj(
    "id" -> theme.theme.id,
    "description" -> theme.theme.description,
    "children" -> theme.children.map(theme => theme2Json(assignment, theme)),
    "owner" -> Json.obj(
      "id" -> theme.theme.owner.id,
      "ref" -> s"/themes/${theme.theme.owner.id}",
      "name" -> theme.theme.owner.providedName
    ),
    "passages" -> theme.annotations.map(annotation => Json.obj(
      "id" -> annotation.annotation.id,
      "quote" -> annotation.annotation.quote,
      "ranges" -> Json.arr(Json.obj(
        "endOffset" -> annotation.annotation.endOffset,
        "startOffset" -> annotation.annotation.startOffset,
        "end" -> annotation.annotation.end,
        "start" -> annotation.annotation.start
      )),
      "pageNo" -> annotation.annotation.pageNo,
      "explication" -> Json.obj(
        "text" -> annotation.explication.explication.text,
        "submitted" -> annotation.explication.explication.submitted
      ),
      "reviews" -> annotation.explication.reviews.map(review => Json.obj(
        "id" -> review.id,
        "ownerRef" -> "/users/review.reviewer.id",
        "responses" -> review.responses.map(response => Json.obj(
          "description" -> response.prompt.description,
          // TODO: figure out a better way of differentiating these
          "score" -> response.score,
          "min" -> response.prompt.min,
          "max" -> response.prompt.max,
          "text" -> response.text
        ))
      ))
      )
    )
  )

  def getThemeTreeForStudent(assignmentId:Long, userId:Option[Long]) = Action { implicit request =>
    request.session.get("userId") match {
      case None => Forbidden(jsonError("User must be logged in"))
      case Some(loggedInUserId) => {
        userRepo.getById(loggedInUserId.toLong) match {
          case None => NotFound(jsonError("User not found"))
          case Some(user) => {
            assignmentRepo.getById(assignmentId, user) match {
              case None => NotFound(jsonError("Assignment does not exist"))
              case Some(assignment) => {
                // Load all themes, going down through children
                // For each theme, load the annotations
                // For each annotation, load explication
                // For each explication, load the reviews
                val userToFind = userId match {
                  case None => user
                  case Some(userId) => userRepo.getById(userId).get
                }
                val allThemes = assignment.themes.map(theme => retrieveAdditionalDataForTheme(userToFind, theme))
                val asJson = Json.obj(
                  "id" -> assignment.id,
                  "title" -> assignment.title,
                  "description" -> assignment.description,
                  "openDate" -> assignment.openDate.toString,
                  "closeDate" -> assignment.closeDate.toString,
                  "themes" -> allThemes.map(theme => theme2Json(assignment, theme))
                )
                Ok(asJson)
              }
            }
          }
        }
      }
    }
  }
}

class DefaultThemeTreeController @Inject() (
 override val userRepo:UserRepository,
 override val themeRepo:ThemeRepository,
 override val assignmentRepo: AssignmentRepository,
 override val annotationRepo:AnnotationRepository,
 override val explicationRepo:ExplicationRepository,
 override val peerReviewRepo:PeerReviewRepository
) extends ThemeTreeController with Controller

case class ExplicationWithReviews(explication: Explication, reviews:List[PeerReview])
case class AnnotationWithExplication(annotation:Annotation, explication: ExplicationWithReviews)
case class ThemeWithAnnotations(theme:Theme, children:List[ThemeWithAnnotations], annotations:List[AnnotationWithExplication])

