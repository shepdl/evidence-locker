package common

import assignments.{AssignmentController, DefaultAssignmentController}
import authentication.EnrollmentListUpdater
import com.google.inject.AbstractModule
import edu.ucla.cdh.EvidenceLocker.common.{DatabaseService, MySQLDatabaseService}
import play.api.libs.concurrent.AkkaGuiceSupport
import reviews.ReviewEmailer

/**
  * Created by daveshepard on 7/14/16.
  */
class EvidenceLockerModule extends AbstractModule with AkkaGuiceSupport {
  override def configure(): Unit = {
    bind(classOf[DatabaseService]).to(classOf[MySQLDatabaseService])
    bind(classOf[AssignmentController]).to(classOf[DefaultAssignmentController])
    bindActor[EnrollmentListUpdater]("enrollment-list-updater")
    bindActor[ReviewEmailer]("review-emailer")
  }
}
