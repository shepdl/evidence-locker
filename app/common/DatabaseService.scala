package edu.ucla.cdh.EvidenceLocker.common
import java.util.Date
import javax.inject.Inject

import com.google.inject.{ImplementedBy, Singleton}
import play.api.db.{DBApi, Database}
import play.api.inject.ApplicationLifecycle
import play.api.{Configuration, Logger}

import scala.concurrent.Future

/**
  * Created by dave on 6/29/16.
  */

trait DatabaseService {
  val db:Database
}

@Singleton
class MySQLDatabaseService @Inject() (config:Configuration, lifecycle:ApplicationLifecycle, dbApi: DBApi) extends DatabaseService {

  private val logger = Logger("MySQLDatabaseService")

  val db = dbApi.database("default")

  lifecycle.addStopHook(() => {
    Future.successful({
      logger.info("Shutting database down")
      db.shutdown()
      dbApi.shutdown()
    })
  })

}


trait Timestamps {
  val createdAt:Date
  val updatedAt:Date
}


trait TimestampsProperties {
  val createdAt = "created_at"
  val updatedAt = "updated_at"
}
