package common

import anorm._

/**
  * Created by daveshepard on 9/24/16.
  */
trait HandlesMySQLBooleans {

  implicit def columnToBoolean: Column[Boolean] = Column.nonNull1 { (value, meta) =>
    val MetaDataItem(qualified, nullable, clazz) = meta
    value match {
      case bool: Boolean => Right(bool)
      case bit: Int =>      Right(bit == 1)
      case _ =>             Left(TypeDoesNotMatch(s"Cannot convert $value ${value.asInstanceOf[AnyRef].getClass} to Boolean for column $qualified"))
    }
  }

}
