package edu.ucla.cdh.EvidenceLocker.common


trait NodeType {

  val id:Long

  val NO_ID = -1

  def isSaved = id != NO_ID

}
