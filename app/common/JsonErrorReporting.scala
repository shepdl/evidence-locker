package common

import play.api.libs.json.Json

/**
  * Created by daveshepard on 7/15/16.
  */
trait JsonErrorReporting {

  def jsonError(message:String) = Json.obj("error" -> message)

}
