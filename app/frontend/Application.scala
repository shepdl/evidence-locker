package frontend

import java.util.UUID

import play.api.mvc._
import javax.inject.{Inject, Singleton}

import authentication.AuthenticationService

/**
  * The entry point for all the front-end applications.
  *
  */
@Singleton
class Application @Inject()(authService:AuthenticationService) extends Controller {

  def index = Action {
    Ok(views.html.main())
  }

  /**
    * Starts the student annotation, etc. interface.
    *
    * @param assignmentId
    * @return
    */
  def student(assignmentId:Long) = authService.ChecksForUserAction {
    Ok(views.html.student.application())
  }

  /**
    * Starts the instructor interface.
    *
    * @param assignmentId
    * @return A view containing the application HTML, JS, etc. See the views.instructor package.
    */
  def instructor(assignmentId:Long) = authService.ChecksForUserAction {
    Ok(views.html.instructor.application())
  }

  def pdfAsImage = Action {
    Ok(views.html.pdfAsImage())
  }

  def createDemo = Action {
    val resourceId = UUID.randomUUID()
    Redirect(routes.Application.demoStartForm(resourceId))
  }

  def demoStartForm(resourceLinkId:UUID) = Action {
    Ok(views.html.demoStartForm(resourceLinkId.toString))
  }

}
