package lti

import java.io.BufferedReader
import java.security.Principal
import java.util
import java.util.Locale
import javax.servlet.{RequestDispatcher, ServletInputStream}
import javax.servlet.http.{Cookie, HttpServletRequest, HttpSession}

import play.api.mvc.Request

/**
  * Basic class for treating a {@link play.api.mvc.Request} as a {@link javax.servlet.http.HttpServletRequests}
  *
  * Necessary to use IMS Global LTI class. That class expects an {@link javax.servlet.http.HttpServletRequest}, so this
  * class roughly approximates what it should expect.
  *
  * Created by dave on 4/28/17.
  */
class PlayRequestAsServletRequest(request:Request[Map[String,Seq[String]]]) extends HttpServletRequest {

  override def isRequestedSessionIdFromURL: Boolean = ???

  override def getRemoteUser: String = ???

  override def getUserPrincipal: Principal = ???

  override def getHeaderNames: util.Enumeration[_] = new util.Enumeration[String] {
    var counter = 0
    val params = request.headers.keys.toList

    override def hasMoreElements = counter >= params.size

    override def nextElement(): String = {
      counter += 1
      params(counter)
    }
  }

  override def getPathInfo: String = request.path

  override def getAuthType: String = ???

  override def getRequestURL: StringBuffer = new StringBuffer(
    s"${if (request.host.contains("http")) "" else
      s"http${if (request.secure) "s" else "" }://"
    }${request.host}${request.uri}"
  )

  override def getCookies: Array[Cookie] = ???

  override def getQueryString: String = request.rawQueryString

  override def getContextPath: String = ???

  override def getServletPath: String = ???

  override def getRequestURI: String = request.uri

  override def getPathTranslated: String = ???

  override def getIntHeader(name: String): Int = ???

  import scala.collection.JavaConversions._

  override def getHeaders(name: String): util.Enumeration[_] = {
    new util.Enumeration[String]() {
      var counter = 0

      override def hasMoreElements: Boolean = {
        counter > 0
      }

      override def nextElement(): String = {
        counter += 1
        request.headers.get(name).get
      }
    }
  }

  override def getRequestedSessionId: String = ???

  override def isRequestedSessionIdFromUrl: Boolean = ???

  override def isRequestedSessionIdValid: Boolean = ???

  override def getSession(create: Boolean): HttpSession = ???

  override def getSession: HttpSession = ???

  override def getMethod: String = request.method

  override def getDateHeader(name: String): Long = ???

  override def isUserInRole(role: String): Boolean = ???

  override def isRequestedSessionIdFromCookie: Boolean = ???

  override def getHeader(name: String): String = ???

  override def getParameter(name: String): String = request.body(name).head

  override def getRequestDispatcher(path: String): RequestDispatcher = ???

  override def getRealPath(path: String): String = request.path

  override def getLocale: Locale = ???

  override def getRemoteHost: String = ???

  override def getParameterNames: util.Enumeration[_] = ???

  override def isSecure: Boolean = ???

  override def getLocalPort: Int = ???

  override def getAttribute(name: String): AnyRef = ???

  override def removeAttribute(name: String): Unit = ???

  override def getLocalAddr: String = ???

  override def getCharacterEncoding: String = ???

  override def setCharacterEncoding(env: String): Unit = ???

  override def getParameterValues(name: String): Array[String] = request.body(name).toArray

  override def getRemotePort: Int = ???

  override def getServerName: String = ???

  override def getProtocol: String = ???

  override def getLocales: util.Enumeration[_] = ???

  override def getAttributeNames: util.Enumeration[_] = ???

  override def setAttribute(name: String, o: scala.Any): Unit = ???

  override def getRemoteAddr: String = ???

  override def getLocalName: String = ???

  override def getContentLength: Int = ???

  override def getServerPort: Int = ???

  override def getReader: BufferedReader = ???

  override def getContentType: String = ???

  override def getScheme: String = ???

  override def getParameterMap: util.Map[_, _] = request.body.map({
    case (key, value) => (key, value.toArray)
  })

  override def getInputStream: ServletInputStream = ???
}
