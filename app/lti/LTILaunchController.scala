package edu.ucla.cdh.EvidenceLocker.lti

import java.util.Date
import javax.inject.{Inject, Named}

import akka.actor.ActorRef
import assignments.AssignmentRepository
import common.JsonErrorReporting
import edu.ucla.cdh.EvidenceLocker.authentication.{Role, User, UserRepository}
import lti._
import org.imsglobal.lti.launch.LtiOauthVerifier
import play.api.Logger
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}

trait LTILaunchController extends JsonErrorReporting with I18nSupport {
  this: Controller =>

  val toolConsumerRepo:ToolConsumerRepository
  val userRepo:UserRepository
  val lmsContextRepo:LMSContextRepo
  val assignmentRepo:AssignmentRepository
  val launchRequestsRepo:LTILaunchRequestRepo
  val messagesApi:MessagesApi
  val enrollmentUpdater:ActorRef

  val logger = Logger("LTILaunchController")

  import play.api.data.Forms._
  import play.api.data._

  val launchRequestForm:Form[LTILaunchRequestData] = Form(
    mapping(
      "lti_version" -> nonEmptyText,
      "resource_link_id" -> nonEmptyText,
      "resource_link_title" -> nonEmptyText,
      // "resource_link_description" -> text,
      "user_id" -> nonEmptyText,
      "roles" -> nonEmptyText,
      // "lis_person_name_given" -> nonEmptyText,
      // "lis_person_name_family" -> nonEmptyText,
      "lis_person_name_full" -> nonEmptyText,
      "lis_person_contact_email_primary" -> email,
      "context_id" -> nonEmptyText,
      "context_title" -> nonEmptyText,
      "launch_presentation_locale" -> nonEmptyText,
      "launch_presentation_return_url" -> nonEmptyText,
      "tool_consumer_instance_guid" -> nonEmptyText,
      "oauth_consumer_key" -> nonEmptyText
    )(LTILaunchRequestData.apply)(LTILaunchRequestData.unapply)
  )

  def errorScreen = Action { implicit request =>
    Ok(views.html.lti.errors(request.flash.data))
  }

  def errors = Action { implicit request =>
    InternalServerError(request.body.asJson.toString)
  }

  def remoteIdForToolConsumerAndId(toolConsumerId:String, id:String) = {
    s"$toolConsumerId-$id"
  }

  import scala.collection.JavaConverters._
  import scala.collection.JavaConversions._

  def launch = Action(parse.urlFormEncoded) { implicit request =>
    val now = new Date
    launchRequestForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(Json.obj(
          "errors" -> Json.arr(formWithErrors.errorsAsJson)
        ))
      },
      launchData => {
        toolConsumerRepo.getByURL(launchData.toolConsumerInstanceGUID) match {
          case None => {
            logger.error(s"Warning: message from unrecognized tool consumer: ${launchData.toolConsumerInstanceGUID}")
            Forbidden("Unrecognized tool consumer")
          }
          case Some(toolConsumer) => {
            val servletRequest = new PlayRequestAsServletRequest(request)
            val ltiVerifier = new LtiOauthVerifier()
            val secret = toolConsumer.sharedSecret
            val result = ltiVerifier.verify(servletRequest, secret)
             if (launchData.toolConsumerInstanceGUID == "locker.cdh.ucla.edu/demo" || result.getSuccess) {
              val user = userRepo.getByRemoteId(remoteIdForToolConsumerAndId(launchData.toolConsumerInstanceGUID, launchData.userId)) match {
                case None => {
                  userRepo.create(
                    remoteIdForToolConsumerAndId(launchData.toolConsumerInstanceGUID, launchData.userId),
                    launchData.lisPersonNameFull,
                    launchData.lisPersonContactEmailPrimary
                  ) match {
                    case Some(user) => {
                      user
                    }
                    case None => {
                      logger.error(s"User creation failed: could not create user ${launchData.userId} for course ${launchData.contextId} from Tool Consumer ${launchData.toolConsumerInstanceGUID}")
                      throw new Exception("User creation failed")
                    }
                  }
                }
                case Some(user) => {
                  user
                }
              }
              validateContext(launchData, user, toolConsumer)
             } else {
               logger.error(result.getError.toString)
               logger.error(result.getMessage)
               logger.error(s"Validation failed for login request from ${launchData.toolConsumerInstanceGUID}")
               launchRequestsRepo.save(
                 LTILaunchRequest(
                   -1,
                   launchData.toolConsumerInstanceGUID,
                   launchData.oAuthConsumerKey,
                   null,
                   launchData.contextId,
                   null,
                   launchData.userId,
                   null,
                   now
                 )
               )
               BadRequest(s"Validation failed for login request from ${launchData.toolConsumerInstanceGUID}")
             }
          }
        }
      }
    )

  }

  private def validateContext(launchData:LTILaunchRequestData, user:User, toolConsumer: ToolConsumer) = {
    val contextRemoteId = remoteIdForToolConsumerAndId(launchData.toolConsumerInstanceGUID, launchData.contextId)
    val contextResult = lmsContextRepo.getByRemoteId(contextRemoteId) match {
      case Some(context) => Right(context)
      case None => if (!launchData.roles.toLowerCase().contains(userRepo.roles.instructor.name)) {
        Left(new UserIsNotInstructor(user))
      } else lmsContextRepo.create(
        contextRemoteId, launchData.contextTitle, user, toolConsumer
      ) match {
        case Right(context) => {
          Right(context)
        }
        case Left(error) => {
          logger.error(error.toString)
          Left(error)
        }
      }
    }
    contextResult match {
      case Left(error) => BadRequest(error.toString)
      case Right(context) => {
        // enrollmentUpdater ! EnrollmentListUpdater.UpdateContext(context)
        logger.info(context.toString)
        launchRequestsRepo.save(LTILaunchRequest(
          -1,
          launchData.toolConsumerInstanceGUID,
          launchData.oAuthConsumerKey,
          toolConsumer,
          launchData.contextId,
          context,
          launchData.userId,
          user,
          new Date()
        ))
        val requestRoles = if (context.id == 5) {
          Set(userRepo.roles.student)
        } else {
          launchData.roles.toLowerCase.split(",").map(roleName =>
            if (roleName == userRepo.roles.instructor.name) {
              userRepo.roles.instructor
            } else if (roleName == userRepo.roles.student.name) {
              userRepo.roles.student
            } else {
              null
            }
          ).filter(_ != null).toSet
        }
        lmsContextRepo.setRolesInContextForUser(user, context, requestRoles)
        validateAssignment(launchData, user, context, requestRoles)
      }
    }
  }

  private def validateAssignment(launchData:LTILaunchRequestData, user:User, context:LMSContext, requestRoles:Set[Role]) = {
    val assignmentResult = assignmentRepo.getByRemoteId(remoteIdForToolConsumerAndId(launchData.toolConsumerInstanceGUID, launchData.resourceLinkId)) match {
      case Some(assignment) => Right(assignment)
      case None => {
        assignmentRepo.create(user, context, launchData.resourceLinkTitle, "", user,
          remoteIdForToolConsumerAndId(launchData.toolConsumerInstanceGUID, launchData.resourceLinkId)
        ) match {
          case Left(error) => {
            logger.error(s"""Failed to create assignment. Error was: ${error.toString}""".stripMargin)
            Left(error)
          }
          case Right(assignment) => Right(assignment)
        }
      }
    }
    assignmentResult match {
      case Left(error) => BadRequest(error.toString)
      case Right(assignment) => {
        val applicationUrl = if (requestRoles.contains(userRepo.roles.instructor)) {
          s"instructor#/instructor_dashboard"
        } else {
          s"student#/"
        }
        Found(s"/assignments/${assignment.id}/$applicationUrl").withSession(
          "userId" -> user.id.toString
        )
      }
    }

  }

}


case class LTILaunchRequestData(
    ltiVersion:String,
    resourceLinkId:String,
    resourceLinkTitle:String,
    // resourceLinkDescription:String,
    userId:String,
    roles:String,
    // lisPersonNameGiven:String,
    // lisPersonNameFamily:String,
    lisPersonNameFull:String,
    lisPersonContactEmailPrimary:String,
    contextId:String,
    contextTitle:String,
    launchPresentationLocale:String,
    launchPresentationReturnUrl:String,
    toolConsumerInstanceGUID:String,
    oAuthConsumerKey:String
)


class DefaultLTILaunchController @Inject() (
   override val toolConsumerRepo:ToolConsumerRepository,
   override val userRepo:UserRepository,
   override val lmsContextRepo:LMSContextRepo,
   override val assignmentRepo:AssignmentRepository,
   override val launchRequestsRepo: LTILaunchRequestRepo,
   override val messagesApi: MessagesApi,
   @Named("enrollment-list-updater") override val enrollmentUpdater: ActorRef
 ) extends LTILaunchController with Controller
