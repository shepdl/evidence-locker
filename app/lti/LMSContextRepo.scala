package lti

import java.sql.SQLIntegrityConstraintViolationException
import java.util.Date
import javax.inject.Inject

import anorm._
import assignments.{Assignment, AssignmentProperties}
import com.google.inject.Singleton
import common.ELError
import edu.ucla.cdh.EvidenceLocker.authentication._
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import play.api.Logger

/**
  * Created by dave on 8/2/16.
  */
@Singleton
class LMSContextRepo @Inject() (dbs:DatabaseService, userRepo:UserRepository) {

  val db = dbs.db

  val logger = Logger("LMSContextRepo")

  import SqlParser._

  def create(remoteId:String, instanceName:String, instructor:User, toolConsumer:ToolConsumer):Either[ELError,LMSContext] = db.withTransaction(implicit c=> {
    val now = new Date
    try {
      val contextId = SQL(s"""
         |INSERT INTO ${LMSContextProperties.table} (
         |  ${LMSContextProperties.remoteId}, ${LMSContextProperties.instanceName},
         |  ${LMSContextProperties.toolConsumerId}
         |) VALUES (
         |  {${LMSContextProperties.remoteId}}, {${LMSContextProperties.instanceName}},
         |  {${LMSContextProperties.toolConsumerId}}
         |)
       """.stripMargin).on(
        LMSContextProperties.remoteId -> remoteId, LMSContextProperties.instanceName -> instanceName,
        LMSContextProperties.toolConsumerId -> toolConsumer.id
      ).executeInsert().get
      SQL(
        s"""
           |INSERT INTO ${LMSContextMembershipProperties.table} (
           |  ${LMSContextMembershipProperties.contextId},
           |  ${LMSContextMembershipProperties.roleId},
           |  ${LMSContextMembershipProperties.userId}
           |) VALUES (
           |  {${LMSContextMembershipProperties.contextId}},
           |  {${LMSContextMembershipProperties.roleId}},
           |  {${LMSContextMembershipProperties.userId}}
           |)
       """.stripMargin).on(
        LMSContextMembershipProperties.contextId -> contextId,
        LMSContextMembershipProperties.roleId -> userRepo.roles.instructor.id,
        LMSContextMembershipProperties.userId -> instructor.id
      ).executeInsert()

      c.commit()
      Right(
        LMSContext(
          contextId, remoteId, instanceName, List(instructor)
        )
      )
    } catch {
      case ex:SQLIntegrityConstraintViolationException => Left(new InstructorNotFound(instructor.id))
    }

  })

  def getById(id:Long):Option[LMSContext] = db.withConnection(implicit c => {
    val instructors = SQL(
      s"""
         |SELECT ${UserProperties.id}, ${UserProperties.remoteId},
         |       ${UserProperties.name}, ${UserProperties.email},
         |       ${UserProperties.lastLogin}
         |FROM   ${UserProperties.table}, ${LMSContextMembershipProperties.table}
         |WHERE  ${UserProperties.id} = ${LMSContextMembershipProperties.userId}
         |  AND  ${LMSContextMembershipProperties.contextId} = {${LMSContextMembershipProperties.contextId}}
         |  AND  ${LMSContextMembershipProperties.roleId} = {${LMSContextMembershipProperties.roleId}}
       """.stripMargin
    ).on(
      LMSContextMembershipProperties.contextId -> id,
      LMSContextMembershipProperties.roleId -> userRepo.roles.instructor.id
    ).executeQuery().as(
      long(UserProperties.id) ~ str(UserProperties.remoteId)
        ~ str(UserProperties.name) ~ str(UserProperties.email) ~ get[Option[Date]](UserProperties.lastLogin) *
    ).map({
      case id~remoteId~name~email~lastLogin => User(id, remoteId, name, email, lastLogin.orNull)
    })
    val context = SQL(
      s"""
         |SELECT ${LMSContextProperties.id}, ${LMSContextProperties.remoteId},
         |       ${LMSContextProperties.instanceName}
         |FROM   ${LMSContextProperties.table}
         |WHERE  ${LMSContextProperties.id} = {${LMSContextProperties.id}}
       """.stripMargin).on(
      LMSContextProperties.id -> id
    ).executeQuery().as(
      long(LMSContextProperties.id) ~ str(LMSContextProperties.remoteId)
        ~ str(LMSContextProperties.instanceName) *
    ).map({
      case id~remoteId~name => LMSContext(id, remoteId, name, instructors)
    })

    context.headOption
  })

  def getByRemoteId(id:String):Option[LMSContext] = db.withConnection(implicit c => {
    val possibleContexts = SQL(
      s"""
         |SELECT ${LMSContextProperties.id}, ${LMSContextProperties.remoteId},
         |       ${LMSContextProperties.instanceName}
         |FROM   ${LMSContextProperties.table}
         |WHERE  ${LMSContextProperties.remoteId} = {${LMSContextProperties.remoteId}}
       """.stripMargin).on(
      LMSContextProperties.remoteId -> id
    ).executeQuery()
      .as(long(LMSContextProperties.id) ~ str(LMSContextProperties.remoteId) ~ str(LMSContextProperties.instanceName) *).map({
      case id~remoteId~name => LMSContext(id, remoteId, name, List())
    })
    if (possibleContexts.size > 1) {
      throw new LMSContextAccidentallyCreatedException(id)
    }
    possibleContexts.headOption
  })

  def getRolesInContextForUser(user:User, context:LMSContext):LMSContextMembership = db.withConnection(implicit c => {
    val roleSet = SQL(
      s"""
          |SELECT ${LMSContextMembershipProperties.roleId}
          |FROM   ${LMSContextMembershipProperties.table}
          |WHERE ${LMSContextMembershipProperties.userId} = {${LMSContextMembershipProperties.userId}}
          |  AND ${LMSContextMembershipProperties.contextId} = {${LMSContextMembershipProperties.contextId}}
       """.stripMargin
    ).on(
      LMSContextMembershipProperties.contextId -> context.id,
      LMSContextMembershipProperties.userId -> user.id
    ).executeQuery()
      .as(long(LMSContextMembershipProperties.roleId) *).map({
      case roleId => userRepo.roles.byId(roleId)
    }).toSet

    LMSContextMembership(-1, user, context, roleSet)
  })

  def getContextForAssignment(user:User, assignment: Assignment):Option[LMSContext] = db.withConnection(implicit c => {
    val contextIds = SQL(
      s"""
         |SELECT ${LMSContextProperties.id}
         |FROM   ${LMSContextProperties.table}, ${AssignmentProperties.table}
         |WHERE  ${LMSContextProperties.id} = ${AssignmentProperties.contextId}
         |  AND  ${AssignmentProperties.id} = {${AssignmentProperties.id}}
       """.stripMargin).on(
      AssignmentProperties.id -> assignment.id
    ).executeQuery().as(
      long(LMSContextProperties.id) *
    )
    if (contextIds.isEmpty) {
      None
    } else {
      getById(contextIds.head)
    }
  })

  def setRolesInContextForUser(user:User, context:LMSContext, newRoles:Set[Role]) = db.withTransaction(implicit c => {
    val currentRoles = SQL(
      s"""
         |SELECT ${LMSContextMembershipProperties.roleId}
         |FROM   ${LMSContextMembershipProperties.table}
         |WHERE  ${LMSContextMembershipProperties.userId} = {${LMSContextMembershipProperties.userId}}
         |  AND  ${LMSContextMembershipProperties.contextId} = {${LMSContextMembershipProperties.contextId}}
       """.stripMargin).on(
      LMSContextMembershipProperties.userId -> user.id,
      LMSContextMembershipProperties.contextId -> context.id
    ).executeQuery().as(
      long(LMSContextMembershipProperties.roleId) *
    ).map({
      case roleId => roleId
    }).toSet

    val newRoleIds = newRoles.map(_.id)
    val rolesToRemove = currentRoles &~ newRoleIds
    val rolesToAdd = newRoleIds &~ currentRoles

    SQL(
      s"""
         |DELETE FROM ${LMSContextMembershipProperties.table}
         |WHERE ${LMSContextMembershipProperties.contextId} = {${LMSContextMembershipProperties.roleId}}
         |  AND ${LMSContextMembershipProperties.userId} = {${LMSContextMembershipProperties.userId}}
         |  AND ${LMSContextMembershipProperties.roleId} IN ({roleSet})
       """.stripMargin).on(
      LMSContextMembershipProperties.contextId -> context.id,
      LMSContextMembershipProperties.userId -> user.id,
      "roleSet" -> rolesToRemove.mkString(",")
    )

    rolesToAdd.foreach(roleId => {
      SQL(s"""
         |INSERT INTO ${LMSContextMembershipProperties.table} (
         |  ${LMSContextMembershipProperties.contextId},
         |  ${LMSContextMembershipProperties.userId},
         |  ${LMSContextMembershipProperties.roleId}
         |) VALUES (
         |  {${LMSContextMembershipProperties.contextId}},
         |  {${LMSContextMembershipProperties.userId}},
         |  {${LMSContextMembershipProperties.roleId}}
         |)
       """.stripMargin).on(
        LMSContextMembershipProperties.contextId -> context.id,
        LMSContextMembershipProperties.userId -> user.id,
        LMSContextMembershipProperties.roleId -> roleId
      ).executeInsert()
    })
    c.commit()
  })

  def getUsersWithRoleInContext(user:User, lmsContext: LMSContext, role:Role):Either[ELError,List[User]] =
    if (getRolesInContextForUser(user, lmsContext).roles.isEmpty) {
      Left(new UserIsNotInstructor(user))
    } else {
      db.withConnection(implicit c => {
        val users = SQL(
          s"""
             |SELECT ${UserProperties.id}, ${UserProperties.remoteId}, ${UserProperties.email},
             |       ${UserProperties.name}, ${UserProperties.lastLogin}
             |FROM ${UserProperties.table}
             |INNER JOIN ${LMSContextMembershipProperties.table} ON ${LMSContextMembershipProperties.userId} = ${UserProperties.id}
             |WHERE  ${LMSContextMembershipProperties.contextId} = {${LMSContextMembershipProperties.contextId}}
             |  AND  ${LMSContextMembershipProperties.roleId} = {${LMSContextMembershipProperties.roleId}}
         """.stripMargin)
          .on(
            LMSContextMembershipProperties.contextId -> lmsContext.id,
            LMSContextMembershipProperties.roleId -> role.id
          ).executeQuery()
          .as(
            long(UserProperties.id) ~ str(UserProperties.remoteId) ~ str(UserProperties.email)
              ~ str(UserProperties.name) ~ get[Option[Date]](UserProperties.lastLogin) *
          ).map({
          case id ~ remoteId ~ email ~ name ~ lastLogin => User(id, remoteId, name, email, lastLogin.orNull)
        })
        Right(users)
      })
    }

}


class InstructorNotFound(instructorId:Long) extends Throwable with ELError
class UserIsNotInstructor(instructor:User) extends Throwable with ELError

case class LMSContextAccidentallyCreatedException(id: String) extends Throwable with ELError
