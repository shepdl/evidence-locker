package lti

import edu.ucla.cdh.EvidenceLocker.authentication.{Role, User}
import edu.ucla.cdh.EvidenceLocker.common.{NodeType, TimestampsProperties}

/**
  * Created by dave on 8/2/16.
  */
case class LMSContext(id:Long, remoteId:String, instanceName:String, instructors:List[User]) extends NodeType {
  def this(remoteId:String, instanceName:String, instructors:List[User]) = this(
    -1, remoteId, instanceName, instructors
  )
}

case object LMSContextProperties extends TimestampsProperties {
  val table = "lti_contexts"
  val id = s"$table.id"

  val remoteId = s"$table.remote_id"
  val instanceName = s"$table.instance_name"

  val toolConsumerId = s"$table.lti_tool_consumer_id"

}

case class LMSContextMembership(id:Long, user:User, context:LMSContext, roles:Set[Role]) extends NodeType

case object LMSContextMembershipProperties extends TimestampsProperties {
  val table = "user_roles_in_lti_contexts"
  val id = s"$table.id"

  val contextId = s"$table.lti_context_id"
  val roleId = s"$table.role_id"
  val userId = s"$table.user_id"

}