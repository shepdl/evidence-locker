package lti

import javax.inject.Inject

import authentication.AuthenticationService
import play.api.i18n.MessagesApi
import play.api.mvc.Controller

/**
  * Created by dave on 4/11/17.
  */
class DefaultToolConsumerController @Inject()(
  override val toolConsumerRepo: ToolConsumerRepository,
  override val authenticationService: AuthenticationService,
  override val messagesApi: MessagesApi
) extends ToolConsumerController with Controller
