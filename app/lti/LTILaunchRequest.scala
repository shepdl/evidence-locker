package lti

import java.util.Date

import edu.ucla.cdh.EvidenceLocker.authentication.User

/**
  * Created by dave on 2/3/17.
  */
case class LTILaunchRequest(id:Long, reportedToolConsumerGuid:String,
   providedConsumerKey:String, toolConsumer:ToolConsumer,
   providedContextId:String, context:LMSContext,
   providedUserGuid:String, user:User, attemptedAt:Date
)