package lti

import java.util.Date

import edu.ucla.cdh.EvidenceLocker.common.{NodeType, TimestampsProperties}

/**
  * Created by dave on 6/30/16.
  */
case class ToolConsumer (override val id:Long, url:String, sharedSecret:String, apiKey:Option[String], isActive:Boolean, lmsType:String, createdAt:Date) extends NodeType

case object ToolConsumerProperties extends TimestampsProperties {
  val table = "lti_tool_consumers"
  val id = s"$table.id"

  val url = s"$table.url"
  val sharedSecret = s"$table.shared_secret"
  val apiKey = s"$table.api_key"
  val lmsType = s"$table.lms_type"
  val isActive = s"$table.is_active"

  override val createdAt: String = s"$table.created_at"

}
