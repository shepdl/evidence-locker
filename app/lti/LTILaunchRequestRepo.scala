package lti

import javax.inject.Inject

import anorm._
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import play.api.Logger


/**
  * Created by dave on 7/26/17.
  */
class LTILaunchRequestRepo @Inject() (val dbs:DatabaseService) {

  val db = dbs.db

  val logger = Logger("LTILaunchRequestRepository")

  def save(launchRequest:LTILaunchRequest):LTILaunchRequest = db.withTransaction(implicit c => {
    val id = SQL(
      s"""
         |INSERT INTO ${LTILaunchRequestProperties.table} (
         |  ${LTILaunchRequestProperties.reportedToolConsumerGuid},
         |  ${LTILaunchRequestProperties.providedConsumerKey},
         |  ${LTILaunchRequestProperties.toolConsumerId},
         |  ${LTILaunchRequestProperties.providedContextId},
         |  ${LTILaunchRequestProperties.foundContextId},
         |  ${LTILaunchRequestProperties.providedUserGuid},
         |  ${LTILaunchRequestProperties.foundUserId},
         |  ${LTILaunchRequestProperties.attemptedAt}
         |) VALUES (
         |  {${LTILaunchRequestProperties.reportedToolConsumerGuid}},
         |  {${LTILaunchRequestProperties.providedConsumerKey}},
         |  {${LTILaunchRequestProperties.toolConsumerId}},
         |  {${LTILaunchRequestProperties.providedContextId}},
         |  {${LTILaunchRequestProperties.foundContextId}},
         |  {${LTILaunchRequestProperties.providedUserGuid}},
         |  {${LTILaunchRequestProperties.foundUserId}},
         |  {${LTILaunchRequestProperties.attemptedAt}}
         |)
       """.stripMargin).on(
          LTILaunchRequestProperties.reportedToolConsumerGuid -> launchRequest.providedUserGuid,
          LTILaunchRequestProperties.providedConsumerKey -> launchRequest.providedConsumerKey,
          LTILaunchRequestProperties.toolConsumerId -> launchRequest.toolConsumer.id,
          LTILaunchRequestProperties.providedContextId -> launchRequest.providedContextId,
          LTILaunchRequestProperties.foundContextId -> launchRequest.context.id,
          LTILaunchRequestProperties.providedUserGuid -> launchRequest.providedUserGuid,
          LTILaunchRequestProperties.foundUserId -> launchRequest.user.id,
          LTILaunchRequestProperties.attemptedAt -> launchRequest.attemptedAt
      ).executeInsert().get
    c.commit()

    LTILaunchRequest(
      id,
      launchRequest.reportedToolConsumerGuid,
      launchRequest.providedConsumerKey,
      launchRequest.toolConsumer,
      launchRequest.providedContextId,
      launchRequest.context,
      launchRequest.providedUserGuid,
      launchRequest.user,
      launchRequest.attemptedAt
    )
  })

}





object LTILaunchRequestProperties {

  val table = "lti_launch_requests"
  val id = s"$table.id"

  val reportedToolConsumerGuid = s"$table.reported_tool_consumer_guid"
  val providedConsumerKey = s"$table.provided_consumer_key"
  val toolConsumerId = s"$table.found_tool_consumer_id"
  val providedContextId = s"$table.provided_context_id"
  val foundContextId = s"$table.found_context_id"
  val providedUserGuid = s"$table.provided_user_guid"
  val foundUserId = s"$table.found_user_id"

  val attemptedAt = "attempted_at"
}
