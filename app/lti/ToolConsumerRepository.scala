package lti

import java.security.MessageDigest
import java.util.{Base64, Date}
import javax.inject.Inject

import anorm._
import common.HandlesMySQLBooleans
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import play.api.Logger

/**
  * Created by dave on 6/30/16.
  */
class ToolConsumerRepository @Inject() (val dbs:DatabaseService) extends HandlesMySQLBooleans {

  val db = dbs.db

  val logger = Logger("ToolConsumerRepository")

  val HASH_SALT = "ABCDEF".getBytes("UTF-8")

  import SqlParser._

  val base64Encoder = Base64.getUrlEncoder()

  def generateSharedSecret(url:String) = {
    val now = new Date
    val hasher = MessageDigest.getInstance("SHA-256")
    hasher.update(HASH_SALT)
    hasher.update(url.getBytes("UTF-8"))
    hasher.update(now.toString.getBytes("UTF-8"))
    val sharedSecret = new String(base64Encoder.encode(hasher.digest()), "UTF-8")
    sharedSecret
  }

  def create(url:String, apiKey:Option[String], lmsType:String):ToolConsumer = db.withTransaction(implicit c => {

    val now = new Date
    val sharedSecret = generateSharedSecret(url)

    val id = SQL(
      s"""
         |INSERT INTO ${ToolConsumerProperties.table} (
         |  ${ToolConsumerProperties.url}, ${ToolConsumerProperties.sharedSecret},
         |  ${ToolConsumerProperties.apiKey}, ${ToolConsumerProperties.lmsType},
         |  ${ToolConsumerProperties.isActive}
         |) VALUES (
         |  {${ToolConsumerProperties.url}}, {${ToolConsumerProperties.sharedSecret}},
         |  {${ToolConsumerProperties.apiKey}}, {${ToolConsumerProperties.lmsType}},
         |  1
         |)
       """.stripMargin).on(
      ToolConsumerProperties.url -> url,
      ToolConsumerProperties.sharedSecret -> sharedSecret,
      ToolConsumerProperties.apiKey -> apiKey.orNull,
      ToolConsumerProperties.lmsType -> lmsType
    ).executeInsert().get

    ToolConsumer(id, url, sharedSecret, apiKey, true, lmsType, now)
  })

  def create(toolConsumer: ToolConsumer) = db.withTransaction(implicit c => {
    val now = new Date()
    val id = SQL(
      s"""
         |INSERT INTO ${ToolConsumerProperties.table} (
         |  ${ToolConsumerProperties.url}, ${ToolConsumerProperties.sharedSecret},
         |  ${ToolConsumerProperties.apiKey}, ${ToolConsumerProperties.lmsType},
         |  ${ToolConsumerProperties.isActive}
         |) VALUES (
         |  {${ToolConsumerProperties.url}}, {${ToolConsumerProperties.sharedSecret}},
         |  {${ToolConsumerProperties.apiKey}}, {${ToolConsumerProperties.lmsType}},
         |  {${ToolConsumerProperties.isActive}}
         |)
       """.stripMargin).on(
      ToolConsumerProperties.url -> toolConsumer.url,
      ToolConsumerProperties.sharedSecret -> toolConsumer.sharedSecret,
      ToolConsumerProperties.apiKey -> toolConsumer.apiKey.orNull,
      ToolConsumerProperties.lmsType -> toolConsumer.lmsType,
      ToolConsumerProperties.isActive -> toolConsumer.isActive
    ).executeInsert().get
    ToolConsumer(id, toolConsumer.url, toolConsumer.sharedSecret, toolConsumer.apiKey, true, toolConsumer.lmsType, now)
  })

  def update(toolConsumer:ToolConsumer) = db.withTransaction(implicit c=> {
    SQL(
      s"""
         |UPDATE ${ToolConsumerProperties.table}
         |SET
         |  ${ToolConsumerProperties.url} = {${ToolConsumerProperties.url}},
         |  ${ToolConsumerProperties.apiKey} = {${ToolConsumerProperties.apiKey}},
         |  ${ToolConsumerProperties.sharedSecret} = {${ToolConsumerProperties.sharedSecret}},
         |  ${ToolConsumerProperties.lmsType} = {${ToolConsumerProperties.lmsType}},
         |  ${ToolConsumerProperties.isActive} = {${ToolConsumerProperties.isActive}}
         |WHERE ${ToolConsumerProperties.id} = {${ToolConsumerProperties.id}}
       """.stripMargin).on(
        ToolConsumerProperties.url -> toolConsumer.url,
        ToolConsumerProperties.apiKey -> toolConsumer.apiKey,
        ToolConsumerProperties.sharedSecret -> toolConsumer.sharedSecret,
        ToolConsumerProperties.lmsType -> toolConsumer.lmsType,
        ToolConsumerProperties.isActive -> toolConsumer.isActive,
        ToolConsumerProperties.id -> toolConsumer.id
      ).executeUpdate()
    toolConsumer
  })


  val allowableOrderFields = List("created_at", "url")

  val toolConsumerFieldList = s"""
       |  ${ToolConsumerProperties.id}, ${ToolConsumerProperties.url},
       |  ${ToolConsumerProperties.sharedSecret}, ${ToolConsumerProperties.apiKey},
       |  ${ToolConsumerProperties.isActive},
       |  ${ToolConsumerProperties.lmsType}, ${ToolConsumerProperties.createdAt}
     """.stripMargin

  val toolConsumerExtractors = (long(ToolConsumerProperties.id) ~ str(ToolConsumerProperties.url)
    ~ str(ToolConsumerProperties.sharedSecret) ~ get[Option[String]](ToolConsumerProperties.apiKey)
    ~ bool(ToolConsumerProperties.isActive)
    ~ str(ToolConsumerProperties.lmsType) ~ date(ToolConsumerProperties.createdAt) *
  )

  def listBy(field:String, offset:Int, resultsPerPage:Int):List[ToolConsumer] = db.withConnection(implicit c => {
    val orderField = if (allowableOrderFields.contains(field)) field else allowableOrderFields.head

    val results = SQL(
      s"""
         |SELECT $toolConsumerFieldList
         |FROM ${ToolConsumerProperties.table}
         |ORDER BY ${orderField}
         |LIMIT {start}, {resultsPerPage}
       """.stripMargin).on(
      "start" -> offset * resultsPerPage,
      "resultsPerPage" -> resultsPerPage
    ).executeQuery().as(toolConsumerExtractors).map({
      case id~url~sharedSecret~apiKey~isActive~lmsType~createdAt => ToolConsumer(id, url, sharedSecret, apiKey, isActive, lmsType, createdAt)
    })

    results
  })

  def getById(id:Long):Option[ToolConsumer] = db.withConnection(implicit c => {
    val result = SQL(
      s"""
         |SELECT $toolConsumerFieldList
         |FROM ${ToolConsumerProperties.table}
         |WHERE ${ToolConsumerProperties.id} = {${ToolConsumerProperties.id}}
       """.stripMargin).on(
      ToolConsumerProperties.id -> id
    ).executeQuery().as(toolConsumerExtractors).map({
      case id~url~sharedSecret~apiKey~isActive~lmsType~createdAt => ToolConsumer(id, url, sharedSecret, apiKey, isActive, lmsType, createdAt)
    })

    result.headOption
  })

  def getByURL(url:String):Option[ToolConsumer] = db.withConnection(implicit c => {
    val result = SQL(
      s"""
         |SELECT $toolConsumerFieldList
         |FROM ${ToolConsumerProperties.table}
         |WHERE ${ToolConsumerProperties.url} = {${ToolConsumerProperties.url}}
       """.stripMargin).on(
      ToolConsumerProperties.url -> url
    ).executeQuery().as(toolConsumerExtractors).map({
      case id~url~sharedSecret~apiKey~isActive~lmsType~createdAt => ToolConsumer(id, url, sharedSecret, apiKey, isActive, lmsType, createdAt)
    })

    result.headOption
  })

  def generateNewKey(consumer:ToolConsumer):Option[ToolConsumer] = db.withConnection(implicit c => {
    val sharedSecret = generateSharedSecret(consumer.url)
    val numRows = SQL(
      s"""
         |UPDATE ${ToolConsumerProperties.table}
         |SET
         |  ${ToolConsumerProperties.sharedSecret} = {${ToolConsumerProperties.sharedSecret}},
         |  ${ToolConsumerProperties.updatedAt} = NOW()
         |WHERE ${ToolConsumerProperties.id} = {${ToolConsumerProperties.id}}
       """.stripMargin).on(
      ToolConsumerProperties.sharedSecret -> sharedSecret,
      ToolConsumerProperties.id -> consumer.id
    ).executeUpdate()

    if (numRows == 0) {
      None
    } else {
      Some(ToolConsumer(
        consumer.id,
        consumer.url,
        sharedSecret,
        consumer.apiKey,
        consumer.isActive,
        consumer.lmsType,
        consumer.createdAt
      ))
    }
  })

  def getToolConsumerForContext(context:LMSContext):Option[ToolConsumer] = db.withConnection(implicit c => {
    val result = SQL(
      s"""
         |SELECT $toolConsumerFieldList
         |FROM   ${ToolConsumerProperties.table}, ${LMSContextProperties.table}
         |WHERE  ${ToolConsumerProperties.id} = ${LMSContextProperties.toolConsumerId}
       """.stripMargin
    ).executeQuery().as(toolConsumerExtractors).map({
      case id~url~sharedSecret~apiKey~isActive~lmsType~createdAt => ToolConsumer(id, url, sharedSecret, apiKey, isActive, lmsType, createdAt)
    })

    result.headOption
  })

}
