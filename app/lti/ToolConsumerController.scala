package lti

import java.text.SimpleDateFormat
import java.util.{Base64, Date, UUID}

import authentication.AuthenticationService
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.Json
import play.api.mvc.Controller

/**
  * Created by dave on 9/2/16.
  */
trait ToolConsumerController extends I18nSupport {
  this:Controller =>

  val toolConsumerRepo:ToolConsumerRepository
  val authenticationService:AuthenticationService

  val messagesApi: MessagesApi

  def list(page:Int, rpp:Int, sortBy:String) = (
    authenticationService.ChecksForUserAction andThen authenticationService.ChecksThatUserIsAdministrator
  ) {
    val toolConsumers = toolConsumerRepo.listBy(sortBy, page, rpp)
    Ok(Json.obj(
      "consumers" -> toolConsumers.map(toolConsumerToJson)
    ))
  }

  import play.api.data.Forms._

  val createForm = Form(
    mapping(
      "url" -> nonEmptyText,
      "lmsType" -> nonEmptyText
    )(CreateData.apply)(CreateData.unapply)
  )

  val dateFormatter = new SimpleDateFormat("MMMM d, y' at 'H:m:s a")

  case class CreateData(url:String, lmsType:String)

  def toolConsumerToJson(toolConsumer: ToolConsumer) = Json.obj(
    "id" -> toolConsumer.id,
    "url" -> toolConsumer.url,
    "apiKey" -> toolConsumer.apiKey,
    "lmsType" -> toolConsumer.lmsType,
    "isActive" -> toolConsumer.isActive,
    "sharedSecret" -> toolConsumer.sharedSecret,
    "createdAt" -> dateFormatter.format(toolConsumer.createdAt)
  )

  def create = (
    authenticationService.ChecksForUserAction andThen authenticationService.ChecksThatUserIsAdministrator
  ) { implicit request =>
    createForm.bindFromRequest.fold(
      errors => BadRequest(errors.errorsAsJson),
      data => {
        toolConsumerRepo.getByURL(data.url) match {
          case Some(toolConsumer) => BadRequest(views.html.lti.errors(Map(
            "error" -> "A Tool Consumer with that URL has already found"
          )))
          case None => {
            val saved = toolConsumerRepo.create(
              ToolConsumer(
                -1, data.url, UUID.randomUUID().toString, Some(UUID.randomUUID().toString), true, data.lmsType, new Date()
              )
            )
            Ok(toolConsumerToJson(saved))
          }
        }
      }
    )
  }


  def update(id:Long) = (
    authenticationService.ChecksForUserAction andThen authenticationService.ChecksThatUserIsAdministrator
  )(parse.json) { implicit request => {
    toolConsumerRepo.getById(id) match {
      case Some(oldToolConsumer) => {
        val json = request.body
        val toolConsumer = ToolConsumer(
          id,
          (json \ "url").as[String],
          oldToolConsumer.sharedSecret,
          oldToolConsumer.apiKey,
          (json \ "isActive").as[Boolean],
          (json \ "lmsType").as[String],
          oldToolConsumer.createdAt
        )
        toolConsumerRepo.update(toolConsumer)
        Ok(
          toolConsumerToJson(toolConsumer)
        )
      }
      case None => NotFound(Json.obj(
        "error" -> "Not found"
      ))
    }
  }
  }

  def generateNewSharedSecret(id:Long) = (
    authenticationService.ChecksForUserAction andThen authenticationService.ChecksThatUserIsAdministrator
  ) { implicit request =>
    toolConsumerRepo.getById(id) match {
      case None => NotFound(Json.obj(
        "error" -> "Not found"
      ))
      case Some(oldToolConsumer) => {
        val newToolConsumer = toolConsumerRepo.generateNewKey(oldToolConsumer).get
        Ok(toolConsumerToJson(newToolConsumer))
      }
    }
  }

  def delete(id:Long) = (
    authenticationService.ChecksForUserAction andThen authenticationService.ChecksThatUserIsAdministrator
  ) { implicit request =>
    toolConsumerRepo.getById(id) match {
      case None => NotFound(Json.obj(
        "error" -> "Not found"
      ))
      case Some(toolConsumer) => {
        // TODO: Add "deleted" attribute
        Ok("Not deleted. This isn't implemented yet.")
      }
    }
  }

}
