define([], function () {
    'use strict';

    var NavigationController = function ($scope, $location, assignmentRepo) {

        assignmentRepo.getAssignment().then(function (response) {
            var assignment = response.data;
            $scope.assignment = response.data;
            $scope.assignment.completedExplicationPercentage = Math.min(assignment.status.completedExplications / assignment.numberRequired * 100, 100);
            $scope.assignment.completedPeerReviewPercentage = Math.min(assignment.status.completedReviews / assignment.numberRequired * 100, 100);
        });
    };

    NavigationController.$inject = ['$scope', '$location', 'evidenceLocker.api.AssignmentService'];

    return NavigationController;
});
