define(['angular', './NavigationController'], function (ng, NavigationController) {
    'use strict';

    var module = ng.module('evidenceLocker.student_dashboard.routes', ['ngRoute']);

    return module;
});
