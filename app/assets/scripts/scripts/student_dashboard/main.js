define(['angular', './routes', './NavigationController'], function (ng, routes, NavigationController) {
    'use strict';

    var module = ng.module('evidenceLocker.student_dashboard', []);

    module.controller('evidenceLocker.student_dashboard.NavigationController', NavigationController);

    return module;
});
