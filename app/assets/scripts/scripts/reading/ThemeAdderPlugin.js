define(['annotator', 'jquery'], function (Annotator, $) {
    'use strict';

    var annotator = Annotator.Annotator;

    var themeHolder = { 
        currentTheme: null,
        getPageNo: null,
        annotationAdded: null,
        annotationDeleted: null
    };
    annotator.Plugin.ThemeAdderPlugin = function (element, options) {
        var setTheme = options.setTheme;
        options.setTheme = function (theme) {
            setTheme.call(themeHolder, theme);
            options.currentTheme = theme;
        };
        themeHolder.getPageNo = options.getPageNo;
        themeHolder.annotationAdded = options.annotationAdded;
        themeHolder.annotationDeleted = options.annotationDeleted;
        annotator.Plugin.apply(this, arguments);
    };

    var annotationCounter = 0;

    $.extend(annotator.Plugin.ThemeAdderPlugin.prototype, new annotator.Plugin(), {
        events: {
        },
        pluginInit: function () {
            this.annotator
                .subscribe('annotationEditorShown', function (editor, annotation) {
                    // Don't show editor -- just save it once someone clicks the button
                    editor.submit();
                })
                .subscribe("annotationCreated", function (annotation) {
                    var currentAnnotation = annotationCounter;
                    annotationCounter += 1;
                    annotation.text = themeHolder.currentTheme.description;
                    annotation.tapID = currentAnnotation;
                    annotation.pageNo = themeHolder.getPageNo();
                    annotation.theme = themeHolder.currentTheme;
                    var range = annotation.ranges[0];

                    var annotationToSave = {
                        quote: annotation.quote,
                        start: range.start,
                        end: range.end,
                        startOffset: range.startOffset,
                        endOffset: range.endOffset,
                        pageNo: annotation.pageNo
                    };

                    var handleResponse = function (data, error) {
                        if (error !== 'success') {
                            alert(data.error);
                        } else {
                            annotation.id = data.id;
                            themeHolder.currentTheme.passages.push(annotation);
                            themeHolder.annotationAdded(annotation);
                        }
                    };
                    $.post(
                        '/themes/' + themeHolder.currentTheme.id + '/annotations', 
                        annotationToSave, handleResponse, 'json'
                    );
                })
                .subscribe("annotationDeleted", function (annotation) {
                    if (annotation.id) {
                        $.ajax('/annotations/' + annotation.id, {
                            method: 'DELETE',
                            success: function (message) { 
                                annotation.theme.passages = annotation.theme.passages
                                    .filter(function (passage) {
                                    return passage.id !== annotation.id;
                                });
                                themeHolder.annotationDeleted(annotation);
                            }, 
                            dataType: 'json'
                        });
                    }
                })
            ;
        }
    });

});
