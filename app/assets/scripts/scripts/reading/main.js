define(['angular', './ReadingController', './ElPDFDirective'], function (angular, ReadingController, ElPDFDirective) {
    'use strict';

    var module = angular.module('evidenceLocker.reading', ['ngRoute', 'evidenceLocker.api']);

    module.config(['$routeProvider', function ($routeProvider) {
        var reading = {
            templateUrl: '/reading/reading.html',
            controller: ReadingController
        };
        $routeProvider.when('/reading', reading);
        $routeProvider.when('/', reading);
    }]);

    module.controller('evidenceLocker.reading.ReadingController', ReadingController);
    module.directive('elPdf', ElPDFDirective);

    return module;
});
