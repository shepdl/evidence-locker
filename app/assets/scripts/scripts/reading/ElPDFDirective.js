define(['pdfjs', 'jquery', './reading.scss'], function (PDFJS, $) {
    'use strict';

    var PDFDirective = function () {
        return {
            scope: true,
            templateUrl: '/reading/el-pdf.html',
            link: function (scope, element, attrs) {
                var url = attrs['url'],
                    canvas = $('.pdf-canvas', element)[0],
                    textLayerDiv = $('.text-layer', element)
                ;


                scope.nextPage = function () {
                    if (scope.pdf.numPages && scope.currentPage < scope.pdf.numPages) {
                        scope.increasePage();
                    }
                };

                scope.previousPage = function () {
                    if (scope.currentPage > 1) {
                        scope.decreasePage();
                    }
                };

                scope.$watch('currentPage', function (newPage, oldPage) {
                    loadPage(newPage);
                });

                var loadPage = function (pageNo) {
                    if (typeof scope.pdf === 'undefined') {
                        return;
                    }

                    scope.pdf.getPage(pageNo).then(function (page) {
                        var scale = 2.0;
                        var viewport = page.getViewport(scale);
                        var context = canvas.getContext('2d');

                        canvas.height = viewport.height;
                        canvas.width = viewport.width;

                        var renderContext = {
                            canvasContext: context,
                            viewport: viewport
                        };

                        page.render(renderContext);

                        var canvasOffset = $(canvas).offset();

                        textLayerDiv.css({
                            height: viewport.height + 'px',
                            width: viewport.width + 'px',
                            top: canvasOffset.top,
                            left: canvasOffset.left
                        });

                        page.getTextContent().then(function (textContent) {
                            textLayerDiv.empty();
                            PDFJS.renderTextLayer({
                                textContent: textContent,
                                container: textLayerDiv[0],
                                viewport: viewport,
                                textDivs: []
                            });
                        });
                    });
                };


                PDFJS.getDocument(url).then(function (pdf) {
                    scope.pdf = pdf;
                    loadPage(scope.currentPage);
                });
            }
        };
    };

    return PDFDirective;
});
