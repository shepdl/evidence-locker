define(['angular', './ReadingController'], function (angular, ReadingController) {
    'use strict';

    var module = angular.module('evidenceLocker.reading.routes', ['ngRoute']);

    module.config(['$routeProvider', function ($routeProvider) {
        var reading = {
            templateUrl: '/reading/reading.html',
            controller: ReadingController
        };
        $routeProvider.when('/reading', reading);
        $routeProvider.when('/', reading);
    }]);

    return module;
});
