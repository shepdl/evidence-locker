define(['jquery', 'annotator', './ThemeAdderPlugin', './reading.scss'], function ($, Annotator, ThemeAdderPlugin) {
    'use strict';

    var ReadingController = function ($scope, assignmentService, authenticationService, $sce) {

        $scope.messages = [];

        $scope.$watch('messages', function () {
            setTimeout(function () {
                $scope.messages = [];
            }, 3000);
        });

        authenticationService.getUser(function (user) {
            $scope.user = user;
        });

        $scope.assignment = {
            title: '',
            filename: '',
            themes: []
        };

        $scope.activeTheme = null;

        $scope.setTheme = function (theme) {
            $scope.activeTheme = theme;
            options.setTheme(theme);
        };

        var passages = { rows: [] };

        var annotator = null,
            annotatorActivated = false
        ;
        var options = { 
            setTheme: function (theme) {
                if (!annotatorActivated) {
                    annotatorActivated = true;
                }
                this.currentTheme = theme;
            },
            getPageNo: function () {
                return $scope.currentPage;
            },
            annotationAdded : function (annotation) {
                var page = 'page-' + $scope.currentPage;
                if (typeof $scope.passagesByPage[page] === 'undefined') {
                    $scope.passagesByPage[page] = [];
                }
                $scope.passagesByPage[page].push(annotation);
                $scope.$digest();
            },
            annotationDeleted: function (annotation) {
                var page = 'page-' + annotation.pageNo;
                $scope.passagesByPage[page].filter(function (otherAnnotation) {
                    return otherAnnotation.id === annotation.id;
                });
                $scope.$digest();
            }
        };

        $scope.currentPage = 1;

        $scope.increasePage = function () {
            $scope.currentPage += 1;
        };

        $scope.decreasePage = function () {
            $scope.currentPage -= 1;
        };

        $scope.toPage = function (pageNo) {
            $scope.currentPage = parseInt(pageNo);
        };

        $('#text').on('mousedown', function (event) {
            if (!options.currentTheme && !(
                $(event.target).hasClass('annotator-delete') || 
                ($(event.target).prop('tagName') === 'BUTTON')
            )) {
                alert('Please select a theme first');
                event.stopPropagation();
            }
        });

        var setProperties = function (theme) {
            theme.open = true;
            for (var i = 0; i < theme.passages.length; i++) {
                theme.passages[i].theme = theme;
            }
            for (var i = 0; i < theme.children.length; i++) {
                setProperties(theme.children[i]);
            }
        };

        $scope.document = null;
        $scope.passagesByPage = {};

        assignmentService.getAssignment().then(function (response) {
            var collectPassages = function (theme, passages) {
                for (var i = 0; i < theme.passages.length; i++) {
                    var passage = theme.passages[i];
                    passage.text = theme.description;
                    passages.push(passage);
                    if ($scope.document.type === 'pdf') {
                        var pageNo = 'page-' + passage.pageNo; //parseInt(passage.pageNo);
                        if (typeof $scope.passagesByPage[pageNo] === 'undefined') {
                            $scope.passagesByPage[pageNo] = [];
                        }
                        $scope.passagesByPage[pageNo].push(passage);
                    }
                }
                for (var i = 0; i < theme.children.length; i++) {
                    collectPassages(theme.children[i], passages);
                }
            };

            $scope.assignment = response.data;
            $scope.assignment.id = $scope.assignment.ref.split('/').pop();
            $scope.document = response.data.document;
            if ($scope.document.type.indexOf('htm') > -1) {
                assignmentService.loadFile($scope.assignment.filename).then(function (response) {
                    $scope.text = $sce.trustAsHtml(response.data);
                    $scope.$digest();
                }, function (error) {
                    console.log(error);
                });
            }
            assignmentService.getThemeTree().then(function (response) {
                for (var i = 0; i < response.data.themes.length; i++) {
                    setProperties(response.data.themes[i]);
                    collectPassages(response.data.themes[i], passages.rows);
                }
                annotator = $('#text').annotator()
                    .annotator('addPlugin', 'ThemeAdderPlugin', options)
                ;
                $scope.assignment.themes = response.data.themes;
                if ($scope.document.type.indexOf('htm') > -1) {
                    annotator.annotator('loadAnnotations', passages.rows);
                } else if ($scope.document.type === 'pdf') {
                    $scope.$watch('currentPage', function (newPage, oldPage) {
                        var oldAnnotations = $scope.passagesByPage['page-' + oldPage];
                        if (typeof oldAnnotations !== 'undefined') {
                            for (var i = 0; i < oldAnnotations.length; i++) {
                                // annotator.annotator('deleteAnnotation', oldAnnotations[i]);
                            }
                        }
                        var newAnnotations = $scope.passagesByPage['page-' + newPage];
                        if (typeof newAnnotations === 'undefined') {
                            return;
                        }

                        // Copy annotations so that Annotator doesn't delete it
                        var annotationsCopy = newAnnotations.map(function (annotation) {
                            var range = annotation.ranges[0];
                            return {
                                quote: annotation.quote,
                                theme: annotation.theme,
                                pageNo: annotation.pageNo,
                                ranges: [{
                                    start: range.start,
                                    end: range.end,
                                    startOffset: range.startOffset,
                                    endOffset: range.endOffset
                                }],
                                id: annotation.id,
                                text: annotation.text
                            };
                        });
                        // Delay is necessary to give PDF time to render
                        setTimeout(function () {
                            annotator.annotator('loadAnnotations', annotationsCopy);
                        }, 200);
                    });
                } else {
                    alert('Unrecognized format "' + $scope.document.type + 
                            '". You will not be able to annotate this document.'
                    );
                }
                $scope.$digest();
            });
        }, function (error) {
            console.log(error);
        });
    };

    ReadingController.$inject = [
        '$scope', 'evidenceLocker.api.AssignmentService', 
        'evidenceLocker.api.AuthenticationService', '$sce'
    ];

    return ReadingController;
});
