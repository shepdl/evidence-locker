define(['angular', './PeerReviewController', './routes'], function (ng, PeerReviewController) {
    'use strict';

    var module = ng.module('evidenceLocker.peer_review', ['evidenceLocker.peer_review.routes', 'ui.slider']);

    module.controller('evidenceLocker.peer_review.PeerReviewController', PeerReviewController);

    return module;
});
