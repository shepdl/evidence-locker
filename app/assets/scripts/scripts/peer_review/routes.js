define(['angular', './PeerReviewController'], function (ng, PeerReviewController) {
    'use strict';

    var module = ng.module('evidenceLocker.peer_review.routes', ['ngRoute']);

    module.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/peer_review', {
            templateUrl: '/peer_review/peer_review.html',
            controller: PeerReviewController
        });
    }]);

    return module;
});
