define([], function () {
    'use strict';

    var PeerReviewController = function ($scope, assignmentService) {

        $scope.noPeerReviews = false;

        var loadNextResponse = function () {
            assignmentService.getReviewPassage().then(function (response) {
                var data = response.data;
                console.log(data);
                if (data.message) {
                    switch (data.message) {
                        case 'assignment_complete':
                            $scope.complete = true;
                            break;
                        case 'no_peer_reviews':
                            $scope.noPeerReviews = true;
                            break;
                        default:
                            alert('Unrecognized message: ' + data.message);
                    }
                } else {
                    for (var i = 0; i < data.responses.length; i++) {
                        if (data.responses[i].score === -1) {
                            data.responses[i].score = 0;
                        }
                    }
                    $scope.passage = data;

                }
                $scope.$digest();
            }, function (error) {
                console.error(error);
            });
        };

        $scope.save = function (passage) {
            assignmentService.saveReviewPassage(passage).then(function (response) {
                loadNextResponse();
            });
        };

        loadNextResponse();
    };

    PeerReviewController.$inject = ['$scope', 'evidenceLocker.api.AssignmentService'];

    return PeerReviewController;
});
