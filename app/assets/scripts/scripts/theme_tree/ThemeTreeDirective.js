define(['./theme_tree.scss'], function () {
    'use strict';

    var addParents = function (treeNodes) {
        for (var i = 0; i < treeNodes.length; i++) {
            var node = treeNodes[i];
            if (typeof node.passages === 'undefined') {
                node.passages = [];
            }
            for (var j = 0; j < node.children.length; j++) {
                var child = node.children[j];
                child.parent = node;
            }
            addParents(node.children);
        }
    };

    var insertChild = function (theme, index, owner) {
        var target, realIndex;
        if (theme.children.length > 0) {
            target = theme.children;
            realIndex = index || 0;
        } else {
            target = theme.children; //theme.parent.children;
            realIndex = index + 1;
        }
        var newTheme = {
            description: '',
            editing: true,
            open: true,
            parent: theme,
            owner: {
                id: owner.getId(),
                name: owner.getName()
            },
            passages: [],
            children: [],
            focused: true,
            active: false
        };
        target.splice(realIndex, 0, newTheme);
    };

    var findLowestLastChild = function (theme) {
        if (theme.children.length === 0 || !theme.open) {
            return theme;
        } else {
            return findLowestLastChild(theme.children[theme.children.length - 1]);
        }
    };

    var isThemeEmpty = function (theme) {
        return (theme.description === '' && theme.children.length === 0 && 
            (theme.passages == null || theme.passages.length === 0));
    };

    var findNextVisibleElement = function (theme) {
        var indexInSiblingList = theme.parent.children.indexOf(theme);
        if (indexInSiblingList === theme.parent.children.length - 1) {
            return findNextVisibleElement(theme.parent);
        } else {
            return theme.parent.children[indexInSiblingList + 1];
        }
    };

    var moveCursorUp = function (theme) {
        theme.focused = false;
        theme.editing = false;
        var siblingList = theme.parent.children,
            indexInSiblingList = siblingList.indexOf(theme),
            themeToActivate
        ;
        if (indexInSiblingList === 0) {
            themeToActivate = theme.parent;
        } else {
            var previousChild = siblingList[indexInSiblingList - 1];
            if (previousChild.children.length > 0) {
                themeToActivate = findLowestLastChild(previousChild);
            } else {
                themeToActivate = previousChild;
            }
        }
        themeToActivate.focused = true;
        themeToActivate.editing = true;
    };

    var moveCursorDown = function (theme) {
        theme.focused = false;
        theme.editing = false;
        var themeToActivate;

        if (theme.children.length > 0 && theme.open) {
            themeToActivate = theme.children[0];
        } else {
            themeToActivate = findNextVisibleElement(theme);
        }
        themeToActivate.focused = true;
        themeToActivate.editing = true;
    };

    var ThemeTreeDirective = function (themeRepo) {

        var moveThemeUp = function (theme, index) {
            var currentSiblings = theme.parent.children;
            currentSiblings.splice(index, 1);
            currentSiblings.splice(index - 1, 0, theme);
        };

        var moveThemeDown = function (theme, index) {
            var currentSiblings = theme.parent.children;
            currentSiblings.splice(index, 1);
            currentSiblings.splice(index + 1, 0, theme);
        };

        var deleteTheme = function (theme, index) {
            if (theme.children.length > 0) {
                alert('Please delete this theme\'s sub-themes before deleting it.');
                return;
            }
            theme.parent.children.splice(index, 1);
            if (typeof theme.id !== 'undefined') {
                themeRepo.delete(theme);
            }
        };

        var promoteTheme = function (theme, index) {
            if (typeof theme.parent !== 'undefined' && theme.parent !== null) {
                var newParent = theme.parent.parent,
                    currentParent = theme.parent
                ;
                if (typeof newParent === 'undefined') {
                    return;
                }
                currentParent.children.splice(index, 1);
                theme.parent = newParent;
                if (typeof newParent.children === 'undefined') {
                    newParent.children = [];
                }
                newParent.children.push(theme);
                newParent.open = true;

                themeRepo.update({
                    parentId: theme.parent.id,
                    description: theme.description,
                    id: theme.id
                });
            }
        };

        var subordinateTheme = function (theme, index) {
            if (index > 0) {
                var currentParent = theme.parent,
                    newParent = theme.parent.children[index - 1]
                ;

                currentParent.children.splice(index, 1);
                theme.parent = newParent;
                newParent.children.push(theme);
                newParent.open = true;

                themeRepo.update({
                    parentId: theme.parent.id,
                    description: theme.description,
                    id: theme.id
                });
            }
        };

        return {
            scope: false,
            templateUrl: '/theme_tree/theme_tree.html',
            link: function (scope, element, attrs) {

                var noop = function () {};
                var events = {
                    themeClick : scope[attrs.themeClick] || noop
                };

                var keyRaw = attrs.themeKey || attrs.ngModel,
                    keyPieces = keyRaw.split('.'),
                    immediateSave = attrs.immediateSave !== 'disabled',
                    isEditable = attrs['editable'] === 'true',
                    canExplicate = attrs['explications'] === 'true',
                    assignmentId
                ;

                scope.isEditable = isEditable;

                scope.canExplicate = canExplicate;

                scope.$watch('assignment', function (newValue) {
                    assignmentId = newValue.id;
                });

                scope.setOldValue = function (theme) {
                    theme.oldValue = theme.description;
                };

                scope.$watchCollection(keyRaw, function (newThemes) {
                    if (typeof newThemes === 'undefined') {
                        return;
                    }

                    addParents(newThemes);
                    var rootTheme = { id: -1, children: [] };

                    for (var i = 0; i < newThemes.length; i++) {
                        var theme = newThemes[i];
                        theme.parent = rootTheme;
                        rootTheme.children.push(theme);
                    }

                    scope.rootTheme = rootTheme;
                }, true);

                scope.handleClick = function (theme, event) {
                    theme.active = !theme.active;
                    events.themeClick(theme);
                    event.stopPropagation();
                };

                scope.clickPassage = function (passage, event) {
                    event.stopPropagation();
                };

                scope.openOrClose = function (theme, $event) {
                    if (theme.children.length === 0) {
                        return;
                    }
                    if (theme.open === true) {
                        theme.open = false;
                    } else {
                        theme.open = true;
                    }
                    $event.stopPropagation();
                };

                scope.addChild = function (theme, inIndex, event) {
                    var index = inIndex || theme.children.length;
                    insertChild(theme, index, scope.user);
                    event.stopPropagation();
                    if (theme == scope.rootTheme) {
                        scope.assignment.themes.push(theme.children[theme.children.length - 1]);
                    }
                };

                scope.deleteTheme = function (theme, index) {
                    deleteTheme(theme, index);
                };

                scope.promoteTheme = function (theme, index) {
                    promoteTheme(theme, index);
                };

                scope.demoteTheme = function (theme, index) {
                    subordinateTheme(theme, index);
                };

                scope.moveThemeUp = function (theme, index, event) {
                    moveThemeUp(theme, index);
                    event.stopPropagation();
                };
                
                scope.moveThemeDown = function (theme, index, event) {
                    moveThemeDown(theme, index);
                    event.stopPropagation();
                };

                scope.cancelEditing = function (theme, order) {
                    if (theme.oldValue) {
                        theme.description = theme.oldValue;
                        theme.oldValue = '';
                    }
                    theme.editing = false;
                    theme.focused = false;
                };

                scope.completeEditing = function (theme, order) {
                    if (immediateSave) {
                        if (theme.oldValue !== theme.description) {
                            if (typeof theme.id === 'undefined') {
                                if (theme.description !== '') {
                                    themeRepo.create(assignmentId, theme.parent, theme.description, order).then(function (response) {
                                        var savedTheme = response.data;
                                        theme.id = savedTheme.id;
                                    });
                                }
                            } else {
                                themeRepo.update({
                                    parentId: theme.parent.id,
                                    description: theme.description,
                                    id: theme.id
                                });
                            }
                        }
                    }
                    theme.editing = false;
                };

                if (isEditable) {

                    scope.editTheme = function (theme) {
                        if (theme.owner && theme.owner.id !== scope.user.getId()) {
                            alert('Sorry--"' + theme.description + '" was created by ' + theme.owner.name + '.');
                            return;
                        }
                        theme.editing = true;
                        this.focused = true;
                    };

                    scope.keyDown = function (theme, index, event) {
                        var key = event.code || event.key;
                        switch (key) {
                            case 'Enter':
                                // Necessary to prevent "Enter" from submitting a form
                                // if the theme tree is part of a form
                                event.preventDefault();
                                break;
                        }
                    };

                    scope.themeEditorKeyup = function (theme, index, event) {
                        // Sometimes Angular uses its own default events and sometimes it uses jQuery events
                        var key = event.code || event.key;
                        switch (key) {
                            case 'BracketRight':
                            case '[':
                                if (event.ctrlKey) {
                                    subordinateTheme(theme, index);
                                }
                                break;
                            case 'BracketLeft':
                            case ']':
                                if (event.ctrlKey) {
                                    promoteTheme(theme);
                                }
                                break;
                            case 'ArrowUp':
                                if (event.ctrlKey) {  // NOTE: Option also works!
                                    moveThemeUp(theme, index);
                                } else {
                                    moveCursorUp(theme);
                                }
                                break;
                            case 'ArrowDown':
                                if (event.ctrlKey) {  // NOTE: Option also works!
                                    moveThemeDown(theme, index);
                                } else {
                                    moveCursorDown(theme);
                                }
                                break;
                            case 'Escape':
                                theme.editing = false;
                                scope.completeEditing(theme, index);
                                break;
                            case 'Enter':
                                event.stopPropagation();
                                theme.editing = false;
                                scope.completeEditing(theme, index);
                                break;
                        }
                    };
                }


            }
        };
    };

    return ThemeTreeDirective;
});
