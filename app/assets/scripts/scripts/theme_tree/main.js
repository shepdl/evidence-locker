define(['angular', './ThemeTreeController', './ThemeTreeDirective', './FocusableDirective', './routes'], function (angular, ThemeTreeController, ThemeTreeDirective, FocusableDirective) {
    'use strict';

    var module = angular.module('evidenceLocker.theme_tree', ['evidenceLocker.theme_tree.routes']);

    module.controller('evidenceLocker.theme_tree.ThemeTreeController', ThemeTreeController);
    module.directive('themeTree', ['evidenceLocker.api.ThemeService', ThemeTreeDirective]);
    module.directive('focusable', FocusableDirective);

    return module;
});
