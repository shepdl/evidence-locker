define([], function () {
    'use strict';
    var Focusable = function () {
        return {
            scope: true,
            restrict: 'A',
            link: function (scope, element, attrs) {
                var focusAttrName = 'theme.focused';

                scope.$watch(focusAttrName, function (isFocusedNow) {
                    if (isFocusedNow) {
                        element[0].focus();
                    } else {
                        element[0].blur();
                    }
                });
            }
        };
    };

    return Focusable;
});
