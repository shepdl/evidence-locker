define(['angular', './ThemeTreeController'], function (angular, ThemeTreeController) {
    'use strict';

    var module = angular.module('evidenceLocker.theme_tree.routes', ['ngRoute']);

    module.config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/theme_tree', {
            templateUrl: '/theme_tree/index.html',
            controller: ThemeTreeController
        });

    }]);
    return module;

});

