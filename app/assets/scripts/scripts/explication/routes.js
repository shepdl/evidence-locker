define(['angular', './ExplicationController'], function (ng, ExplicationController) {
    'use strict';

    var module = ng.module('evidenceLocker.explication.routes', ['ngRoute']);

    module.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/explication', {
            templateUrl: '/explication/explication.html',
            controller: ExplicationController
        });
    }]);
});
