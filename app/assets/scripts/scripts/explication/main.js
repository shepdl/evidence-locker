define(['angular', './ExplicationController', './routes'], function (ng, ExplicationController, routes) {
    'use strict';

    var module = ng.module('evidenceLocker.explication', ['evidenceLocker.explication.routes']);

    module.controller('evidenceLocker.explication.ExplicationController', ExplicationController);

    return module;
});
