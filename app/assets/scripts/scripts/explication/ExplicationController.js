define(['../API/Authentication'], function (AuthenticationService) {
    'use strict';

    var DashboardController = function ($scope, $http, $location, AuthenticationService, assignmentService) {

        var setProperties = function (theme) {
            theme.open = true;
            for (var i = 0; i < theme.children.length; i++) {
                setProperties(theme.children[i]);
            }
        };

        assignmentService.getAssignment().then(function (response) {
            var assignment = response.data;

            $scope.assignment = {
                id : assignment.ref.split('/').pop(),
                title : assignment.title,
                description: assignment.description,
                themes: assignment.themes,
                numberRequired: assignment.numberRequired,
                status: assignment.status
            };

            assignmentService.getThemeTree().then(function (response) {
                var assignmentData = response.data;

                for (var i = 0; i < response.data.themes.length; i++) {
                    setProperties(response.data.themes[i]);
                }
                $scope.assignment.themes = assignmentData.themes;
                $scope.$digest();
            });

        }, console.error);

        $scope.openOrClose = function (theme, $event) {
            if (theme.children.length === 0) {
                return;
            }
            if (theme.open === true) {
                theme.open = false;
            } else {
                theme.open = true;
            }
            $event.stopPropagation();
        };


        $scope.explicate = function (passage) {
            passage.explication.inProgress = true;
        };

        $scope.saveExplication = function (passage) {
            passage.explication.inProgress = false;

            $http({
                method: 'PUT',
                url: '/annotations/' + passage.id + '/explication',
                data: {
                    text: passage.explication.text,
                    submitted: false
                }
            }).then(function (response) {
                $scope.assignment.status.completedExplications += 1;
                alert("Explication saved");
            });
        };

        $scope.submit = function (passage) {
            passage.explication.inProgress = false;
            passage.explication.submitted = true;
            $http({
                method: 'PUT',
                url: '/annotations/' + passage.id + '/explication',
                data: {
                    text: passage.explication.text,
                    submitted: true
                }
            }).then(function (response) {
                alert("Explication submitted");
            });
        };
    };

    DashboardController.$inject = ['$scope', '$http', '$location', 'evidenceLocker.api.AuthenticationService', 'evidenceLocker.api.AssignmentService'];

    return DashboardController;
});
