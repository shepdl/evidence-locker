define(['angular'], function (ng) {
    'use strict';

    var AuthenticationController = function ($scope, AuthenticationService) {
        $scope.student = {};

        AuthenticationService.setUser(function (user) {
            $scope.student.name = user.getName();
        });
    };

    AuthenticationController.$inject = ['$scope', 'evidenceLocker.api.AuthenticationService'];

    var module = ng.module('evidenceLocker.authentication', ['evidenceLocker.api']);

    module.controller('evidenceLocker.authentication.AuthenticationController', AuthenticationController);

    return module;
});
