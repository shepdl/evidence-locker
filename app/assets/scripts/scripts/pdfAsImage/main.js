var PdfJs = require('pdfjs'),
    $ = require('jquery')
;


var main = function () {
    var canvas = $('.canvas-renderer')[0],
        image = $('.image-renderer')[0],
        textLayer = $('.text-renderer')[0]
    ;

    var url = 'assets/images/neijing.pdf';
    PdfJs.getDocument(url).then(function (pdf) {
        pdf.getPage(1).then(function (page) {
            var scale = 2.0,
                viewport = page.getViewport(scale),
                context = canvas.getContext('2d')
            ;

            canvas.height = viewport.height;
            canvas.width = viewport.width;

            var renderContext = {
                canvasContext: context,
                viewport: viewport
            };

            var canvasOffset = $(canvas).offset();

            $(textLayer).css({
                height: viewport.height + 'px',
                width: viewport.width + 'px',
                position: 'absolute',
                top: canvasOffset.top,
                left: canvasOffset.left,
                // color: 'transparent'
            });

            page.render(renderContext).then(function () {
                $(image).attr('src', canvas.toDataURL());
                $(canvas).hide();
                page.getTextContent().then(function (textContent) {
                    $(textLayer).empty();
                    PDFJS.renderTextLayer({
                        textContent: textContent,
                        container: textLayer,
                        viewport: viewport,
                        textDivs: []
                    });
                    var rawText = textContent.items.map(function (item) {
                        return item.str;
                    }).join(' ');
                    // $(image).attr('longdesc', rawText);
                    $(textLayer).empty();
                    $(textLayer).html('<p>' + rawText + '</p>');
                    // $('#text-content').html('<p>' + rawText + '</p>');
                });
            });


        });
    });
};


$(main);
