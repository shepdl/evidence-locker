define(['./APIBase'], function (APIBase) {
    'use strict';

    var assignmentIdExtractor = /assignments\/([\d]+)\//g;

    var AssignmentService = function ($http, $location) {

        var assignmentId = assignmentIdExtractor.exec(window.location.pathname)[1];

        this.setAssignmentId = function (id) {
            assignmentId = id;
        };

        this.getAssignment = function () {
            return $http({
                method: 'GET',
                url: APIBase.URL('/assignments/' + assignmentId)
            });
        };

        this.getThemeTree = function () {
            return $http({
                method: 'GET',
                url: APIBase.URL('/assignments/' + assignmentId + '/themeTree')
            });
        };

        this.loadFile = function (file) {
            return $http({
                method: 'GET',
                url: APIBase.URL(file)
            });
        };

        this.getReviewPassage = function () {
            return $http({
                method: 'POST',
                url: APIBase.URL('/assignments/' + assignmentId + '/reviews'),
                transformRequest: function (obj) {
                    var str = [];
                    for (var p in obj) {
                        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                    }
                    return str.join('&');
                },
                headers: {
                    'Content-Type' : 'application/x-www-form-urlencoded'
                }
            });
        };

        this.saveReviewPassage = function (passage) {
            return $http({
                method: 'PUT',
                url: APIBase.URL('/reviews/' + passage.id),
                data: passage
            });
        };

        this.getExplicationsByStudent = function (studentId) {
            return $http({
                method: 'GET',
                url: APIBase.URL('/assignments/' + assignmentId + '/explications/' + studentId)
            });
        };

        this.getReviewsByAnnotationId = function (annotationId) {
            return $http({
                method: 'GET',
                url: '/annotations/' + annotationId + '/reviews'
            });
        };


        this.getReviewsByStudent = function (studentId) {
            return $http({
                method: 'GET',
                url: APIBase.URL('/assignments/' + assignmentId + '/peer_reviews/' + studentId)
            });
        };

    };
    
    AssignmentService.$inject = ['$http', '$location'];

    return AssignmentService;
});
