define([], function () {
    'use strict';

    var API = {};

    var BASE = '';

    API.URL = function (url) {
        var separator = '';

        if (url.substr(0, 1) !== '/') {
            separator = '/';
        }
        return BASE + separator + url;
    };

    return API;
});

