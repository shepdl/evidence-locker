define(['./APIBase'], function (APIBase) {
    'use strict';

    var ThemeService = function ($http) {
        
        this.create = function (assignmentId, parentTheme, description, order) {
            var data = { description: description}; 
            if (typeof order !== 'undefined') {
                data.order = order;
            }
            if (parentTheme.id !== -1) {
                data.parentId = parentTheme.id;
            }
            return $http({
                method : 'POST',
                url: APIBase.URL('/assignments/' + assignmentId + '/themes'),
                transformRequest: function (obj) {
                    var str = [];
                    for (var p in obj) {
                        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                    }
                    return str.join('&');
                },
                headers: {
                    'Content-Type' : 'application/x-www-form-urlencoded'
                },
                data: data
            });
        };

        this.get = function (id) {
            return $http.get(APIBase.URL('/themes/' + id));
        };

        this.update = function (theme) {
            return $http({
                method: 'PUT',
                url: APIBase.URL('/themes/' + theme.id), 
                data: {
                    parentId: theme.parentId,
                    description: theme.description,
                    order: theme.order
                }
            });
        };

        this.delete = function (theme) {
            return $http.delete(APIBase.URL('/themes/' + theme.id));
        };

    };

    ThemeService.$inject = ['$http'];

    return ThemeService;
});
