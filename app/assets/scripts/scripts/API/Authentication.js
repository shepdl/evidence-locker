define(['./APIBase'], function (APIBase) {
    'use strict';

    var User = function (inId, inName) {
        var name = inName,
            id = inId
        ;

        this.getName = function () {
            return name;
        };

        this.getId = function () {
            return id;
        };

    };

    var AccessService = function ($http) {

        var currentUser = null;

        var loginUser = function (callback) {
            $http.get(APIBase.URL('/users/whoAmI')).then(function (successResponse) {
                var data = successResponse.data,
                    statusCode = successResponse.status
                ;

                if (statusCode === 200) {
                    currentUser = new User(data.id, data.name);
                    callback(currentUser);
                } else {
                    alert('User not logged in');
                }
            });
        };

        this.setUser = function (callback) {
            if (!currentUser) {
                loginUser(callback);
            } else {
                callback(currentUser);
            }
        };

        this.setPassword = function (user, password) {
            return $http({
                method: 'POST',
                url: '/users/password',
                headers: {
                    'Content-Type' : 'application/x-www-form-urlencoded'
                },
                transformRequest: function (obj) {
                    var str = [];
                    console.log(obj);
                    for (var p in obj) {
                        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                    }
                    return str.join('&');
                },
                data: {
                    password: password
                }
            });
        };

        this.getUser = this.setUser;
    };

    AccessService.$inject = ['$http'];

    return AccessService;

});
