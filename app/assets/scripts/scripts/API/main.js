define(['angular', './Authentication', './ThemeService', './AssignmentService'], function (ng, AuthenticationService, ThemeService, AssignmentService) {
    'use strict';

    var module = ng.module('evidenceLocker.api', []),
        ngInjector = ng.injector(['ng'])
    ;

    module.factory('evidenceLocker.api.AuthenticationService', function () {
        return new AuthenticationService(ngInjector.get('$http'));
    });
    module.factory('evidenceLocker.api.ThemeService', function () {
        return new ThemeService(ngInjector.get('$http'));
    });
    module.factory('evidenceLocker.api.AssignmentService', function () {
        return new AssignmentService(ngInjector.get('$http')); //, ngInjector.get('$location'));
    });

    return module;
});

