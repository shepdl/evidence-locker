define(['angular', 'tool_consumer_edit.scss'], function (ng) {
    'use strict';

    var ToolConsumerEditorController = function ($scope, toolConsumerService) {

        $scope.editing = false;

        $scope.edit = function (status) {
            $scope.editing = status;
        };

        $scope.save = function (toolConsumer) {
            toolConsumerService.save(toolConsumer).then(function (response) {
                var data = response.data;
                alert("Saved");
                $scope.editing(false);
            }, function (error) {
                alert(error);
            });
        };
    };

    ToolConsumerEditorController.$inject = ['$scope', 'evidenceLocker.administration.ToolConsumerService'];

    return ToolConsumerEditorController;
});
