/*jshint unused: vars */
define(['angular', 
    './ToolConsumerListController', './ToolConsumerEditorController',
    './ToolConsumerRepository'
    './API/main', './authentication/main', 'eltpl'
    'main',
]/*deps*/, function (angular, ToolConsumerListController, ToolConsumerEditorController, ToolConsumerRepository)/*invoke*/ {
  'use strict';

  /**
   * @ngdoc overviewG
   * @name evidenceLocker
   * @description
   * # evidenceLocker
   *
   * Main module of the application.
   */
    var ngInjector = angular.injector(['ng']);
    var module = angular
    .module('evidenceLocker.administration', [
        'evidenceLocker.api', 'evidenceLocker.authentication', 'el.tpl',
        'ngRoute'
    ])
    .config(function ($routeProvider) {
      $routeProvider
        .when('tool_consumers', {
            controller: ToolConsumerListController
            templateUrl: '/administration/tool_consumer_list.html'
        })
        .when('tool_consumers/:id', {
            controller: ToolConsumerEditorController
            templateUrl: '/administration/tool_consumer_edit.html'
        })
        .otherwise({
          // redirectTo: '/'
        });
    });

    module.factory('evidenceLocker.administration.ToolConsumerService', function () {
        return new ToolConsumerService(ngInjector.get('$http'));
    });

    module.controller(
        'evidenceLocker.administration.ToolConsumerListController', ToolConsumerListController
    );
    module.controller(
        'evidenceLocker.administration.ToolConsumerEditorController', ToolConsumerEditorController
    );

    
    return module;
});
