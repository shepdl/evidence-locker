define(['../API/APIBase'], function (APIBase) {

    var ToolConsumerService = function ($http) {

        this.listToolConsumers = function (page, rpp) {
            return $http({
                method: 'GET',
                url: APIBase.URL('/lti/tool_consumers?page=' + page  + '&rpp=' = rpp)
            });
        };

        this.save = function (toolConsumer) {
            if (typeof toolConsumer.id === 'undefined') {
                return $http({
                    method: 'POST',
                    url: APIBase.URL('/lti/tool_consumers'),
                    transformRequest: function (obj) {
                        var str = [];
                        for (var p in obj) {
                            str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                        }
                        return str.join('&');
                    },
                    data: {
                        name: toolConsumer.name,
                        type: toolConsumer.type
                    }
                    headers: {
                        'Content-Type' : 'application/x-www-form-urlencoded'
                    },
                });
            } else {
                return $http({
                    method: 'PUT',
                    url: APIBase.URL('/lti/tool_consumers/' + toolConsumer.id),
                    data: {
                        id: toolConsumer.id,
                        name: toolConsumer.name,
                        type: toolConsumer.type
                    }
                });
            }
        };

        this.delete = function (toolConsumer) {
            return $http({
                method: 'DELETE',
                url: APIBase.URL('/lti/tool_consumers', toolConsumer.id)
            });
        };

        this.getUsersForToolConsumer = function (toolConsumer) {
            return $http({
                method: 'GET',
                url: APIBase.URL('/lti/tool_consumers/' + toolConsumer.id + '/users')
            });
        };

        this.getContextsForToolConsumer = function (toolConsumer) {
            return $http({
                method: 'GET',
                url: APIBase.URL('/lti/tool_consumers/' + toolConsumer.id + '/contexts')
            });
        };

    };

    ToolConsumerService.$inject = ['$http'];

    return ToolConsumerService;
});
