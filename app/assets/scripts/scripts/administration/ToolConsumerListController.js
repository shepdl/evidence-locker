define(['angular', './tool_consumer_list.scss'], function (ng) {
    'use strict';

    var ToolConsumerListController = function ($scope, toolConsumerService) {
        
    };

    ToolConsumerListController.$inject = ['$scope', 'evidenceLocker.administration.ToolConsumerService'];

    return ToolConsumerListController;
});
