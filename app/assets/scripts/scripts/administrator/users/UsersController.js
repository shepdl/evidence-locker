/**
 * Created by dave on 4/13/17.
 */
define([], function () {
    'use strict';

    var UsersController = function ($scope) {

        $scope.users = [
            {
                id : 1,
                remoteId: 'ccle.ucla.edu-1',
                name: 'Jean-Luc Picard',
                createdAt: new Date(2000, 1, 1),
                toolConsumerId: 20,
                lastLogin: new Date(2001, 1, 1),
                isAdmin: false
            },
            {
                id : 2,
                remoteId: 'ccle.ucla.edu-2',
                name: 'Molly Obrien',
                createdAt: new Date(2002, 1, 1),
                toolConsumerId: 20,
                lastLogin: new Date(2004, 1, 1),
                isAdmin: true
            }
        ];


        $scope.edit = function (user) {
            user.editing = true;
        };

        $scope.addNew = function () {
            $scope.users.push({
                id: null,
                remoteId: 'local',
                name: '',
                createdAt: new Date(),
                toolConsumerId: null,
                lastLogin: new Date(),
                isAdmin: true,

                editing: true
            })
        };

        $scope.save = function (user) {
            user.editing = false;
        };

    };

    UsersController.$inject = ['$scope'];

    return UsersController;

});
