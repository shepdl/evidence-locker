/**
 * Created by dave on 4/13/17.
 */
define(['angular', './UsersController'], function (ng, UsersController) {

    ng.module('evidenceLocker.administrator.Users', ['ngRoute'])
        .config(['$routeProvider', function ($routeProvider) {
            $routeProvider.when('/users', {
                templateUrl: '/administrator/users/users-list.html',
                controller: UsersController
            })
        }]);

});