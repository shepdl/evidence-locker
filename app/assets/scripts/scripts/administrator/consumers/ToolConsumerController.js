define([], function () {
    'use strict';

    var ToolConsumerListController = function ($scope, tcService) {

        tcService.listToolConsumers().then(function (response) {
            $scope.toolConsumers = response.data.consumers;
            $scope.$digest();
        });

        $scope.edit = function (toolConsumer) {
            toolConsumer.editing = true;
        };

        $scope.save = function (toolConsumer) {
            toolConsumer.editing = false;
            tcService.save(toolConsumer).then(function (response) {
                toolConsumer.id = response.data.id;
                toolConsumer.sharedSecret = response.data.sharedSecret;
            });
        };
        
        $scope.showMessageDialog = function () {
            
        };

        $scope.addNew = function () {
            $scope.toolConsumers.push({
                id: -1,
                name: '',
                sharedSecret: '',
                lmsType: 'unknown',
                createdAt: '2001-01-01T00:00:00',
                editing: true,
                isActive: true
            });
        };

        $scope.generateSharedSecret = function (toolConsumer) {
            tcService.newSharedSecret(toolConsumer).then(function (updatedData) {
                toolConsumer.sharedSecret = updatedData.data.sharedSecret;
                $scope.$digest();
            });
        };

        $scope.setEnabled = function (toolConsumer, status) {
            if (!status) {
                if (confirm('Are you sure you want to disable ' + toolConsumer.name + '?')) {
                    toolConsumer.isActive = false;
                } else {
                    return;
                }
            } else {
                toolConsumer.isActive = true;
            }
            tcService.save(toolConsumer);
        };

    };

    ToolConsumerListController.$inject = ['$scope', 'evidenceLocker.api.ToolConsumerService'];

    return ToolConsumerListController;

});
