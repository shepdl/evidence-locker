/**
 * Created by dave on 4/10/17.
 */

define([], function () {
    var ToolConsumerRepository  = function ($http) {

        this.listToolConsumers = function (inPage, inRpp, inOrderBy) {
            var page = inPage || 0,
                rpp = inRpp || 25,
                orderBy = inOrderBy || 'name'
            ;

            return $http({
                method: 'GET',
                url: '/consumers?page=' + page + '&rpp=' + rpp + '&orderBy=' + orderBy
            });
        }

        this.save = function (toolConsumer) {
            var toSubmit = {
                url: toolConsumer.url,
                name: toolConsumer.name,
                lmsType: toolConsumer.lmsType,
                isActive: toolConsumer.isActive
            },
                url = '/consumers' + ((typeof toolConsumer.id !== 'undefined' && toolConsumer.id !== -1) ? '/' + toolConsumer.id  : ''),
                method = (typeof toolConsumer.id !== 'undefined' && toolConsumer.id !== -1) ? 'PUT' : 'POST'
            ;

            return $http({
                method : method,
                url: url,
                data: toSubmit
            })
        };

        this.newSharedSecret = function (toolConsumer) {
            return $http({
                method: 'POST',
                url: '/consumers/' + toolConsumer.id + '/shared_secret'
            });
        };

    };

    ToolConsumerRepository.$inject = ['$http'];

    return ToolConsumerRepository;
});
