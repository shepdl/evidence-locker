define(['angular', './ToolConsumerController'], function (ng, ToolConsumerController) {

    return ng.module('evidenceLocker.administrator.ToolConsumerList', ['ngRoute'])
        .config(['$routeProvider', function ($routeProvider) {
            $routeProvider.when('/tool_consumers', {
                templateUrl: '/administrator/consumers/tool-consumer-list.html',
                controller: ToolConsumerController
            }).when('/', {
                templateUrl: '/administrator/consumers/tool-consumer-list.html',
                controller: ToolConsumerController
            });
        }]);

})