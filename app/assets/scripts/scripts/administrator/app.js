/*jshint unused: vars */
define(['angular', 'eltpl'
]/*deps*/, function (angular)/*invoke*/ {
    'use strict';

    /**
     * @ngdoc overview
     * @name evidenceLocker
     * @description
     * # evidenceLocker
     *
     * Main module of the application.
     */
    return angular
        .module('evidenceLocker.administrator', [
            'el.tpl'
        ]);
});
