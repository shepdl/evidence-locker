define(['angular', './ProfileController'], function (ng, ProfileController) {
    ng.module('evidenceLocker.administrator.Profile', ['ngRoute'])
        .config(['$routeProvider', function ($routeProvider) {
            $routeProvider.when('/profile', {
                templateUrl: '/administrator/profile/profile.html',
                controller: ProfileController
            })
        }]);
});