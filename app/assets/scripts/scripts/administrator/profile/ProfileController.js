define([], function () {
    var ProfileController = function ($scope, authService) {
        $scope.user = {};
        $scope.password1 = '';
        $scope.password2 = '';

        authService.getUser(function (user) {
            $scope.user = user;
            $scope.$digest();
        });

        $scope.changePassword = function (user) {
            if ($scope.password1 !== $scope.password2) {
                alert('Passwords do not match');
                return;
            } else {
                authService.setPassword(user, $scope.password1).then(function (response) {
                    alert('Password changed');
                    console.info(response);
                })
            }
        }
    };

    ProfileController.$inject = ['$scope', 'evidenceLocker.api.AuthenticationService'];

    return ProfileController;

});