/**
 * Created by dave on 4/10/17.
 */

define(['angular',
    './consumers/ToolConsumerService',
    'angular-route', 'eltpl',
    '../API/main',
    '../authentication/main',
    './consumers/main', './users/main', './profile/main'
], function (angular, ToolConsumerService) {
    'use strict';

    var ngInjector  = angular.injector(['ng']);

    return angular.module('evidenceLocker.administrator', [
        'evidenceLocker.administrator.ToolConsumerList',
        'evidenceLocker.administrator.Users',
        'evidenceLocker.authentication',
        'evidenceLocker.administrator.Profile',
        'el.tpl'
    ])
        .factory('evidenceLocker.api.ToolConsumerService', function () {
            return new ToolConsumerService(ngInjector.get('$http'));
        })
    ;

});
