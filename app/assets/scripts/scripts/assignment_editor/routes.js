define(['angular', './AssignmentEditorController'], function (angular, AssignmentEditorController) {
    'use strict';

    var module = angular.module('evidenceLocker.assignment_editor.routes', ['ngRoute']);

    module.config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/assignment_editor', {
            templateUrl: '/assignment_editor/assignment_editor.html',
            controller: AssignmentEditorController
        }).when('/assignment_editor/:id', {
            templateUrl: '/assignment_editor/assignment_editor.html',
            controller: AssignmentEditorController
        });

    }]);
    return module;

});


