define(['angular', './AssignmentEditorController', './routes', 'ng-file-upload'], function (angular, AssignmentEditorController) {
    'use strict';

    var module = angular.module('evidenceLocker.assignment_editor', ['evidenceLocker.assignment_editor.routes', 'ngFileUpload', 'el.tpl']);

    module.controller('evidenceLocker.assignment_editor.AssignmentEditorController', AssignmentEditorController);

    return module;
});
