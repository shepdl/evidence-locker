define([], function () {
    'use strict';

    var AssignmentURL = '/assignments/';

    var AssignmentEditorController = function ($scope, $location, $http, $routeParams, $filter, Upload, assignmentService, authenticationService) {

        var assignmentId = parseInt($routeParams.id);

        $scope.assignment = {
            title: '',
            description: '',
            themes: [],
            file: null,
            peerReview: true,
            grading: 'numeric',
            criteria: []
        };
       

        assignmentService.getAssignment().then(function (response) {
            var assignment = response.data;
            assignment.grading = assignment.rubricType;
            assignment.criteria = assignment.reviewRubric;
            for (var i = 0; i < assignment.criteria.length; i++) {
                assignment.criteria[i].status = 'OK';
            }

            for (var i = 0; i < assignment.themes.length; i++) {
                assignment.themes[i].owner = {
                    id: parseInt(assignment.themes[i].ownerRef.split('/').pop())
                };
            }

            console.log(assignment);

            $scope.assignment = assignment;

            $scope.$digest();
        });

        authenticationService.getUser(function (user) {
            $scope.user = user;
        });


        $scope.handleKeyDown = function (event) {
            if (event.code === 'Enter') {
                event.preventDefault();
                event.stopPropagation();
            }
        };

        var addCriterion = function (index) {
            if (typeof index === 'undefined') {
                index = $scope.assignment.criteria.length;
            }
            $scope.assignment.criteria.splice(index + 1, 0, { description : '', min: 0, max: 5});
        };

        var deleteCriterion = function (criterion, index) {
            $scope.assignment.criteria.splice(index, 1);
        };

        $scope.addCriterion = addCriterion;
        $scope.deleteCriterion = deleteCriterion;

        $scope.handleKeyUp = function (criterion, index, event) {
            switch (event.code) {
                case 'Backspace': 
                    if (criterion.title === '' && criterion.min === '' && criterion.max === '') {
                        deleteCriterion(criterion, index);
                    }
                    break;
                case 'Enter':
                    addCriterion(index);
                    break;
                default:
                    // TODO: Make sure there are no duplicate criteria
                    break;
            }
        };

        var fileHasChanged = false;

        $scope.save = function (assignment) {
            var handleResponse = function (response) {
                $location.path('/instructor_dashboard');
            };
            var assignmentToSubmit = {
                title: assignment.title,
                description: assignment.description,
                openDate: assignment.openDate,
                closeDate: assignment.closeDate,
                themes: function () {
                    var themes,
                        lastTheme = assignment.themes[assignment.themes.length - 1];
                    if (lastTheme.description === '' && typeof lastTheme.id === 'undefined') {
                        themes = assignment.themes.slice(0, assignment.themes.length - 1);
                    } else {
                        themes = assignment.themes;
                    }
                    return themes.map(function (theme) {
                        return {
                            id: theme.id === undefined ? -1 : theme.id,
                            description: theme.description,
                        };
                    });
                }(),
                peerReviewEnabled: assignment.peerReviewEnabled,
                rubricType: assignment.grading,
                numberRequired: parseInt(assignment.numberRequired),
                reviewRubric: function () {
                    var criteria, 
                        lastCriterion = assignment.criteria[assignment.criteria.length - 1];
                    if (lastCriterion.title === '' && typeof lastCriterion.id === 'undefined') {
                        criteria = assignment.criteria.slice(0, assignment.criteria.length - 1);
                    } else {
                        criteria = assignment.criteria;
                    }
                    return criteria.map(function (criterion) {
                        var newCriterion = { 
                            id: typeof criterion.id === 'undefined' ? -1 : criterion.id,
                            description: criterion.title
                        };
                        if (assignment.grading === 'numeric' || assignment.grading === 'both') {
                            newCriterion.min = parseInt(criterion.min);
                            newCriterion.max = parseInt(criterion.max);
                        }
                        return newCriterion;
                    }).filter(function (criterion) {
                        return criterion.description.length > 0;
                    });
                }()
            };
            var data = {
                assignment: $filter('json')(assignmentToSubmit),
            };
            if (assignment.file) {
                data.assignmentFile = assignment.file;
            }
            Upload.upload({
                url: AssignmentURL + assignmentId,
                data: data
            }).then(handleResponse, function (error) {
                // TODO: show error
                console.log(error);
            }, function (event) {
                var progressPercentage = parseInt(100.0 * event.loaded / event.total);
               console.log('progress:' + progressPercentage + '%');
            });
        };

        $scope.setFile = function (assignment, file) {
            assignment.file = file[0];
            assignment.filename = file[0].name;
            fileHasChanged = true;
        };

    };

    AssignmentEditorController.$inject = [
        '$scope', '$location', '$http', '$routeParams', '$filter', 'Upload', 
        'evidenceLocker.api.AssignmentService',
        'evidenceLocker.api.AuthenticationService'
    ];

    return AssignmentEditorController;
});
