/*jshint unused: vars */
define(['angular', '../API/main', 
       '../reading/main',
       '../authentication/main', '../assignment_editor/main',
       '../instructor_dashboard/main', 'angular-slider', 'eltpl', '../theme_tree/main'
]/*deps*/, function (angular)/*invoke*/ {
  'use strict';

  /**
   * @ngdoc overview
   * @name evidenceLocker
   * @description
   * # evidenceLocker
   *
   * Main module of the application.
   */
  return angular
    .module('evidenceLocker', [ 'evidenceLocker.api', 'evidenceLocker.authentication',
        'evidenceLocker.reading',
        'evidenceLocker.assignment_editor', 'evidenceLocker.instructor_dashboard', 
        'evidenceLocker.theme_tree',
        'el.tpl'
    ])
    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider
        .otherwise({
          // redirectTo: '/'
        });
    }]);
});
