/*jshint unused: vars */
define(['angular',
        './API/main', './authentication/main', './assignment_editor/main', './theme_tree/main', './reading/main', './student_dashboard/main',
       './explication/main', './peer_review/main', 'angular-slider', 'eltpl'
]/*deps*/, function (angular)/*invoke*/ {
  'use strict';

  /**
   * @ngdoc overview
   * @name evidenceLocker
   * @description
   * # evidenceLocker
   *
   * Main module of the application.
   */
  return angular
    .module('evidenceLocker', [
        'evidenceLocker.api', 'evidenceLocker.authentication', 'evidenceLocker.explication',
        'evidenceLocker.assignment_editor', 'evidenceLocker.theme_tree', 'evidenceLocker.reading', 
        'evidenceLocker.student_dashboard', 
        'evidenceLocker.peer_review', 'el.tpl'
    ])
    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider
        .otherwise({
          // redirectTo: '/'
        });
    }]);
});
