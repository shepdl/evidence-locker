define(['angular', './DashboardController', './StudentSummaryController'], function (ng, DashboardController, StudentSummaryController) {
    'use strict';

    var module = ng.module('evidenceLocker.instructor_dashboard', ['ngRoute', 'ui.slider']);

    module.config(['$routeProvider', function ($routeProvider) {
        var instructorDashboard = {
            templateUrl: '/instructor_dashboard/dashboard.html',
            controller: DashboardController
        };
        $routeProvider
            .when('/', instructorDashboard)
            .when('/instructor_dashboard', instructorDashboard)
            .when('/student/:studentId', {
                templateUrl: '/instructor_dashboard/student_summary.html',
                controller: StudentSummaryController
            })
        ;
    }]);

    module.directive('ngInstructorDashboard', function () {
        return {
            templateUrl: '/instructor_dashboard/dashboard.html'
        };
    });

    return module;
});
