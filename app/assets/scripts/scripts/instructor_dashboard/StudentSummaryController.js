define([], function () {
    'use strict';

    var StudentSummaryController = function ($scope, $routeParams, assignmentService) {

        var studentId = parseInt($routeParams.studentId);
        $scope.assignment = {};

        assignmentService.getAssignment().then(function (response) {
            var data = response.data;
            $scope.assignment = {
                id: data.id,
                title: data.title,
                criteriaType: data.rubricType,
                numberRequired: data.numberRequired,
                criteria: data.reviewRubric
            };
            $scope.$digest();
        });

        var reviewLoader = function (explication, annotationId) {
            assignmentService.getReviewsByAnnotationId(annotationId).then(function (response) {
                var data = response.data;
                explication.reviews = data.reviews.map(function (review) {
                    return {
                        reviewer: {
                            name: review.reviewer.name
                        },
                        responses: review.responses
                    };
                }, function (error) {
                    console.error(error);
                });
                $scope.$digest();
            });
        };

        assignmentService.getExplicationsByStudent(studentId).then(function (response) {
            var data = response.data;
            $scope.student = data.student;
            var explications = data.explications.map(function (explication) {
                return {
                    id: explication.id,
                    text: explication.text,
                    theme: explication.theme,
                    annotation: {
                        id : explication.annotation.id,
                        quoteParas: explication.annotation.quote.split('\n')
                    },
                    reviews: []
                };
            });
            explications.map(function (explication) {
                reviewLoader(explication, explication.annotation.id);
            });
            $scope.explications = explications;
            $scope.$digest();
        });

        assignmentService.getReviewsByStudent(studentId).then(function (response) {
            var data = response.data;
            var reviews = data.peerReviews.map(function (review) {
                return {
                    explication: {
                        annotation: review.explication.annotation,
                        text: review.explication.text
                    },
                    theme: review.theme,
                    review: review
                };
            });
            $scope.reviews = reviews;
            $scope.$digest();
        });

    };

    StudentSummaryController.$inject = ['$scope', '$routeParams', 'evidenceLocker.api.AssignmentService'];
    return StudentSummaryController;

});
