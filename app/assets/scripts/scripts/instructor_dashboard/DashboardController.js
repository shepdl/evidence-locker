define(['./dashboard.scss'], function () {
    'use strict';

    var DashboardController = function ($scope, $location, assignmentService) {

        $scope.assignment = {};
        assignmentService.getAssignment().then( function (response) {
            var assignment = response.data;
            $scope.assignment = {
               id: assignment.ref.split('/').pop(),
               title: assignment.title,
               description: assignment.description,
               filename: assignment.filename,
               document: assignment.document,
               openDate: assignment.openDate,
               closeDate: assignment.closeDate,
               peerReviewEnabled: assignment.peerReviewEnabled,
               numberRequired: assignment.numberRequired,
               rubricType: assignment.rubricType,
               criteria: assignment.reviewRubric,
               themes: assignment.themes,
               status: assignment.status,
               totalStudents: assignment.totalStudents,
               explicationsCompleted: assignment.status.explications ? (assignment.status.explications.filter(function (status) {
                   return status.explicationCount === assignment.numberRequired
               }).length) : 0,
               peerReviewsCompleted: assignment.status.peerReviews ? (assignment.status.peerReviews.filter(function (status) {
                   return status.peerReviewsCompleted === assignment.numberRequired
               }).length) : 0
            };

            $scope.$digest();
        });
                                            
        $scope.editAssignment = function (assignment) {
            $location.path('assignment_editor/' + assignment.id);
        };
    };

    DashboardController.$inject = ['$scope', '$location', 'evidenceLocker.api.AssignmentService'];

    return DashboardController;
});
