USE `evidence_locker` ;


DROP TABLE IF EXISTS `evidence_locker`.`documents`;

CREATE TABLE IF NOT EXISTS `evidence_locker`.`documents` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `assignment_id` BIGINT NOT NULL,
  `system_filename` TEXT,
  `original_filename` TEXT,
  `file_type` VARCHAR(10),
  `uploader_id` BIGINT NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`),

  CONSTRAINT `document_uploader_id`
    FOREIGN KEY (`uploader_id`)
    REFERENCES `evidence_locker`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `document_assignment_id`
    FOREIGN KEY (`assignment_id`)
    REFERENCES `evidence_locker`.`assignments` (`id`)
    ON DELETE CASCADE
    on UPDATE NO ACTION

) ENGINE=InnoDB;

INSERT INTO `evidence_locker`.`documents` (
    assignment_id, system_filename, original_filename, file_type, uploader_id,
    created_at, updated_at
) SELECT 
    id, filename, filename, 'HTML', instructor_id, created_at, updated_at
FROM `evidence_locker`.`assignments` 
;


ALTER TABLE `evidence_locker`.`assignments` (
  DROP COLUMN `filename`
);

