package install

import javax.inject.Inject

import anorm._
import com.google.inject.Singleton
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import play.api.Logger

/**
  * Created by dave on 8/26/16.
  */
@Singleton
class InstallDataRepository @Inject() (val dbs:DatabaseService) {

  val db = dbs.db

  val logger = Logger("Installation")

  import SqlParser._

  def isInstallationComplete = db.withConnection(implicit c => {
    val rolesInserted = SQL(
      s"""
         |SELECT COUNT(*) FROM roles
       """.stripMargin
    ).executeQuery().as(long("COUNT(*)").single)

    rolesInserted > 0
  })

}
