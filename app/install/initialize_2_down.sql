USE `evidence_locker` ;

UPDATE `evidence_locker`.`assignments`, `evidence_locker`.`documents`
  SET `assignments`.`filename` = `documents`.`system_filename` 
    WHERE `assignments`.`id` = `documents`.`assignment_id`;

DROP TABLE `evidence_locker`.`documents`;

