package install

import javax.inject.Inject

import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import play.api.Logger
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Action, Controller}

/**
  * Created by dave on 8/26/16.
  */
trait InstallController extends I18nSupport {
  this:Controller =>

  val logger = Logger("Installation")
  val messagesApi:MessagesApi

  val userRepo:UserRepository
  val installationDataRepo:InstallDataRepository

  import play.api.data.Forms._
  import play.api.data._

  def launch = Action {
    if (installationDataRepo.isInstallationComplete) {
      Ok(views.html.installation.installationAlreadyComplete())
    } else {
      Ok(views.html.installation.launch())
    }
  }


  val superUserCreationForm:Form[SuperUserData] = Form(
    mapping(
      "username" -> nonEmptyText,
      "email" -> email,
      "password" -> nonEmptyText(minLength = 8)
    )(SuperUserData.apply)(SuperUserData.unapply)
  )

  def superUserForm = Action {
    Ok(views.html.installation.createSuperUser(superUserCreationForm))
  }

  def createSuperUser = Action(parse.urlFormEncoded) { implicit request =>
    superUserCreationForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.installation.createSuperUser(formWithErrors)),
      superUserData => {
        userRepo.createAdminUser(
          superUserData.username,
          superUserData.email,
          superUserData.password
        )
        Redirect("/install/complete")
      }
    )
  }

  def installationComplete = Action {
    Ok(views.html.installation.complete())
  }

}

case class SuperUserData(username:String, email:String, password:String)


class DefaultController @Inject() (
    override val userRepo:UserRepository,
    override val messagesApi:MessagesApi,
    override val installationDataRepo:InstallDataRepository
  ) extends InstallController with Controller
