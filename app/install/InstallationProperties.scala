package install

/**
  * Created by dave on 8/26/16.
  */
case object InstallationProperties {
  val table = "installation"

  val isComplete = "is_complete"

}
