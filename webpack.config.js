var imports = require('imports-loader'),
    exports = require('exports-loader')
;

require('sass-loader');
require('css-loader');
require('style-loader');

var defaultModule = function (name, extraCommands) {
    return name + '/' + name;
    return bower(name + '/' + name, extraCommands);
};

module.exports = {
    context: __dirname + "/app/assets/scripts/",
    entry: {
        student: './scripts/main.js',
        instructor: './scripts/instructor/main.js',
        pdfAsImage: './scripts/pdfAsImage/main.js',
        administrator: './scripts/administrator/main.js'
    },
    output: {
        filename: '[name].entry.js',
        publicPath: '/assets/javascripts/'
    },
    module: {
        loaders: [
            { test: /angular\/angular/, loader: 'imports?jquery!exports?angular'},
            { test: /angular-/, loader: 'imports?angular' },
            { test: /eltpl/, loader: 'imports?angular=angular'},
            { test: /ng-/, loader: 'imports?angular'},
            { test : /\/jquery\/jquery/, loader: 'exports?jQuery'},
            { test: /annotator\/annotator\.min/, loader: 'imports?jQuery=jquery'},
            { test: /\.scss$/, loaders: ['style', 'css', 'sass'] }
        ],
        rules: [
        { 
            test: /\.scss$/, use: [
                { loader: 'style-loader' },
                { loader: 'css-loader' },
                { loader: 'sass-loader', options: {
                    outFile: 'public/stylesheets/test.css'
              }}
            ]
        }
        ]
    },
    resolve : {
        modulesDirectories: [
            'app/assets/scripts/scripts',
            'bower_components',
            'node_modules'
        ],
        alias : {
            angular: 'angular/angular',
            'angular-animate' : defaultModule('angular-animate'),
            'angular-aria' : defaultModule('angular-aria'),
            'angular-cookies' : defaultModule('angular-cookies'),
            'angular-messages' : defaultModule('angular-messages'),
            'angular-mocks' : defaultModule('angular-mocks'),
            'angular-resource' : defaultModule('angular-resource'),
            'angular-route' : defaultModule('angular-route'),
            'angular-sanitize' : defaultModule('angular-sanitize'),
            'angular-touch' : defaultModule('angular-touch'),
            bootstrap : 'bootstrap/dist/js/bootstrap',
            'ng-file-upload' : defaultModule('ng-file-upload'),
            'annotator' : 'annotator/annotator.min',
            jquery : 'jquery/dist/jquery',
            'angular-slider' : 'angular-slider/dist/slider',
            'pdfjs' : 'pdfjs-dist/build/pdf.combined',
            'eltpl' : 'views/eltpl'
        }
    }
};
