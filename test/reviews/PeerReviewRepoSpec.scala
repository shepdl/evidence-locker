package reviews

import java.util.Date

import annotations.{Annotation, Explication, HasAnnotationRepo, HasExplicationRepo}
import assignments.{HasAssignmentRepo, HasThemeRepo}
import authentication.{HasToolConsumerRepo, HasUserRepo}
import common.HasDatabase
import edu.ucla.cdh.EvidenceLocker.authentication.User
import instructor.InstructorControllerTestCase
import lti.{HasBasicContext, HasLMSContextRepo}
import org.junit.runner._
import org.specs2.mutable._
import org.specs2.runner._
import play.api.test._


@RunWith(classOf[JUnitRunner])
class PeerReviewRepoSpec extends PlaySpecification {

  "A Peer Review Repository" should {

    "create a lock to peer review a passage and return a new Review object when all fields are present" in new PeerReviewRepoTest with PeerReviewTextuallyGradedAssignment with PeerReviewTestCase {
      val student = reviewingStudents.head

      val result = peerReviewRepo.createLock(student, assignment)
      result must beRight[PeerReview]

      val review = result.right.get

      review.reviewer mustEqual student
      review.explication mustEqual explication
      review.responses.zip(assignment.reviewRubric) foreach {
        case (responsePrompt, reviewRubric) => responsePrompt.prompt mustEqual reviewRubric
      }
    }

    "update a peer review" in new PeerReviewRepoTest with PeerReviewTextuallyGradedAssignment with PeerReviewTestCase {
      val student = reviewingStudents.head

      val review = peerReviewRepo.createLock(student, assignment).right.get

      val now = new Date
      val updatedReview = review.addResponse(
        assignment.reviewRubric.zip(review.responses).map({
          case (rr, review) => PromptResponse(
            review.id, "Nice job", rr, -1
          )
        })
      )

      val updateResult = peerReviewRepo.update(student, updatedReview)
      updateResult must beRight[PeerReview]
    }

    "return that a user is done with the assignment when the user has completed the necessary number of peer reviews" in new PeerReviewRepoTest with PeerReviewTextuallyGradedAssignment with PeerReviewTestCase {
      val student = reviewingStudents.head
      val heroism = assignment.themes.head
      val heroicQualities1 = themeRepo.create(mockStudent, assignment, "Heroic Qualities", List(), heroism, mockStudent, 0).right.get

      (1 to assignment.numberRequired).map(x => {
        val annotation = annotationRepo.create(mockStudent, heroicQualities1, Annotation.from(
          "example of heroism", "/p[0]", "/p[1]", 10, 30, "none", mockStudent
        )).right.get
        explicationRepo.save(mockStudent, annotation, Explication.from(
          "The explication", annotation, mockStudent
        ))
      })

      val now = new Date()
      (1 to assignment.numberRequired).foreach(x => {
        val reviewStart = peerReviewRepo.createLock(student, assignment).right.get
        val updatedReview = reviewStart.addResponse(assignment.reviewRubric.zip(reviewStart.responses).map({
          case (rr, response) => {
            PromptResponse(response.id, "This is the review", rr, -1)
          }
        }))
        peerReviewRepo.update(student, updatedReview) must beRight[PeerReview]
      })

      peerReviewRepo.createLock(student, assignment) match {
        case Left(ex:UserIsFinishedWithAssignment) => success
        case _ => failure
      }

    }

    "not update a peer review when the review writer is not the person who started the review" in new PeerReviewRepoTest with PeerReviewTextuallyGradedAssignment with PeerReviewTestCase {
      val student = reviewingStudents.head
      val wrongStudent = reviewingStudents(1)

      val review = peerReviewRepo.createLock(student, assignment).right.get

      val now = new Date
      val updatedReview = review.addResponse(
        assignment.reviewRubric.map(rr => PromptResponse(
          -1, "Nice job", rr, -1
        ))
      )

      val updateResult = peerReviewRepo.update(wrongStudent, updatedReview)
      updateResult match {
        case Left(ex:UserHasNoAccessToReview) => success
        case _ => failure
      }
    }

//    "fail to create a lock when the user is not a member of the class"

//    "expire the lock after the given time is elapsed"
    //
    "not allow a different user to update the same peer review" in new PeerReviewRepoTest with PeerReviewTextuallyGradedAssignment with PeerReviewTestCase {
      val firstStudent = reviewingStudents.head
      val firstStudentReview = peerReviewRepo.createLock(firstStudent, assignment).right.get

      val secondStudent = reviewingStudents(1)
      val secondStudentReview = peerReviewRepo.createLock(secondStudent, assignment).right.get

      val now = new Date
      val updatedFirstStudentReview = firstStudentReview.addResponse(
        assignment.reviewRubric.map(rr => PromptResponse(
          -1, "Good job", rr, -1
        ))
      )

      val updatedSecondStudentReview = secondStudentReview.addResponse(
        assignment.reviewRubric.map(rr => PromptResponse(
          -1, "Nice job", rr, -1
        ))
      )

      peerReviewRepo.update(secondStudent, firstStudentReview) match {
        case Left(ex:UserHasNoAccessToReview) => success
        case _ => failure
      }

      peerReviewRepo.update(firstStudent, secondStudentReview) match {
        case Left(ex:UserHasNoAccessToReview) => success
        case _ => failure
      }
    }

    "not allow more than the allowed number of students to review the same explication" in new PeerReviewRepoTest with PeerReviewTextuallyGradedAssignment with PeerReviewTestCase {
      // Assignment requires each student to do 3 peer reviews
      // Get 3 reviews for every student. Make sure the total number of times each explication is returned is exactly equal to 3
      // Make sure no student gets the same prompt twice

      val heroism = assignment.themes.head
      val explications = (mockStudent :: reviewingStudents).zipWithIndex.map({
        case (student, index) => {
          val passagesToCreate = assignment.numberRequired - (if (student == mockStudent) 1 else 0)
          1 to passagesToCreate map(explicationIndex => {
            val annotation = annotationRepo.create(
              student, themeRepo.create(student, assignment, s"Heroic Qualities $index for ${student.providedName}",
                List(), heroism, student, 0
              ).right.get,
              Annotation.from(s"The selected text for explication $index-$explicationIndex", "/p[0]", "/p[1]", 10, 30, "none", student)
            ).right.get
            explicationRepo.save(
              student, annotation, Explication.from(
                s"Explication #$explicationIndex from ${student.providedName}", annotation, student
              )
            ).right.get
          })
        }
      })

      val now = new Date
      val studentsToExplicationsTheyHaveReviewed = (mockStudent :: reviewingStudents).foldLeft(Map[User,Set[Explication]]()) {
        case (acc, student) => {
          (1 to assignment.numberRequired).foldLeft(acc) {
            case (acc, index) => {
              val review = peerReviewRepo.createLock(student, assignment).right.get
              val updatedReview = review.addResponse(
                assignment.reviewRubric.zip(review.responses).map({
                  case (rr, response) => {
                    PromptResponse(response.id, "Good job!", rr, -1)
                  }
                })
              )
              peerReviewRepo.update(student, updatedReview) match {
                case Right(b:PeerReview) => success
                case x => failure
              }
              acc.updated(student, acc.getOrElse(student, Set()) + review.explication)
            }
          }
        }
      }

      studentsToExplicationsTheyHaveReviewed foreach {
        case (_, explications) => {
          explications must haveSize(assignment.numberRequired)
        }
      }
      val passagesToStudentsThatReviewedThem = studentsToExplicationsTheyHaveReviewed.foldLeft(Map[Explication,Set[User]]()) {
        case (acc, (student, explications)) => explications.foldLeft(acc) {
          case (acc, explication) => acc.updated(explication, acc.getOrElse(explication, Set()) + student)
        }
      }
    }

    "return a message stating that the user should try again later when there are no peer reviews available" in new PeerReviewRepoTest with PeerReviewTextuallyGradedAssignment with PeerReviewTestCase {
      val heroism = assignment.themes.head
      val student = reviewingStudents.head
      1 to assignment.numberRequired map(explicationIndex => {
        val annotation = annotationRepo.create(
          student, themeRepo.create(
            student, assignment, s"Heroic Qualities $explicationIndex for ${student.providedName}",
            List(), heroism, student, 0
          ).right.get,
          Annotation.from(s"The selected text for explication $explicationIndex", "/p[0]", "/p[1]", 10, 30, "none", student)
        ).right.get
        explicationRepo.save(student, annotation, Explication.from(
          s"Explication #${explicationIndex} from ${student.providedName}", annotation, student
        )).right.get
      })

      val now = new Date
      val firstReview = peerReviewRepo.createLock(student, assignment).right.get
      peerReviewRepo.update(student, firstReview.addResponse(
        assignment.reviewRubric.zip(firstReview.responses).map({
          case (rr, criterion) => {
            PromptResponse(
              criterion.id, "Nice", rr, -1
            )
          }
        })
      ))

      peerReviewRepo.createLock(student, assignment) match {
        case Left(ex:UserHasNoPeerReviewsAvailable) => success
        case x => failure
      }
    }

     "only accept textual reviews for a textual assignment" in new PeerReviewRepoTest with PeerReviewTextuallyGradedAssignment with PeerReviewTestCase {
       val student = reviewingStudents.head
       val review = peerReviewRepo.createLock(student, assignment).right.get

       val now = new Date
       val updatedReview = review.addResponse(
         assignment.reviewRubric.zip(review.responses).map({
           case (rr, prompt) => PromptResponse(
             prompt.id, "", rr, 3
           )
         })
       )

       peerReviewRepo.update(student, updatedReview) match {
         case Left(ex:WrongValueType) => ex.expectedType mustEqual ex.TEXTUAL
         case _ => failure
       }
     }

     "only accept numeric reviews for a numerically-graded assignment" in new PeerReviewRepoTest with PeerReviewNumericallyGradedAssignment with PeerReviewTestCase {
       val student = reviewingStudents.head
       val review = peerReviewRepo.createLock(student, assignment).right.get

       val now = new Date
       val updatedReview = review.addResponse(
         assignment.reviewRubric.zip(review.responses).map({
           case (rr, prompt) =>
             PromptResponse(prompt.id, "Some textual commentary that shouldn't be here.", rr, -1)
         })
       )

       peerReviewRepo.update(student, updatedReview) match {
         case Left(ex: WrongValueType) => {
           ex.expectedType mustEqual ex.NUMERIC
         }
         case _ => failure
       }
     }

    "ensure all values are within specified ranges for a numerically-graded assignment" in new PeerReviewRepoTest with PeerReviewNumericallyGradedAssignment with PeerReviewTestCase {
      val student = reviewingStudents.head
      val review = peerReviewRepo.createLock(student, assignment).right.get

      val now = new Date
      val outOfRange = PromptResponse(-1, "", assignment.reviewRubric(0), 300)
      val updatedReview = review.addResponse(
        List(
          outOfRange,
          PromptResponse(-1, "", assignment.reviewRubric(1), 3)
        )
      )

      peerReviewRepo.update(student, updatedReview) match {
        case Left(ex: ValueOutOfRange) => ex.response mustEqual outOfRange
        case _ => failure
      }
    }

    "return a list of all peer reviews for an explication" in new PeerReviewRepoTest with PeerReviewTextuallyGradedAssignment with PeerReviewTestCase {
      val now = new Date
      reviewingStudents.foreach(student => {
        val peerReview = peerReviewRepo.createLock(student, assignment).right.get
        peerReviewRepo.update(student, peerReview.addResponse(
          assignment.reviewRubric.zip(peerReview.responses).map({
            case (rr, prompt) =>
              PromptResponse(prompt.id, "Some textual commentary", rr, -1)
          })
        ))
      })

      val reviews = peerReviewRepo.listCompletedForExplication(mockStudent, explication).right.get
      reviews must haveSize(assignment.numberRequired)
    }

    "return a list of all the peer reviews a student has completed for an assignemnt" in new PeerReviewRepoTest with InstructorControllerTestCase {
      val result = peerReviewRepo.getReviewsCompletedByUserForAssignment(students.head, assignment)
      result must beRight[List[PeerReview]]

      val reviews = result.right.get
      reviews must haveSize(3)
    }

  }

}

class PeerReviewRepoTest extends WithApplication
  with HasDatabase with HasToolConsumerRepo with HasUserRepo with HasAssignmentRepo
  with HasLMSContextRepo with HasBasicContext
  with HasThemeRepo with HasAnnotationRepo with HasExplicationRepo
  with HasPeerReviewRepo {

}
