package reviews

import java.util.Date
import javax.inject.Named

import akka.actor.ActorRef
import annotations._
import assignments.ThemeRepository
import authentication.AuthenticationService
import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import authentication.{AssignmentService, AuthenticationService}
import instructor.InstructorControllerTestCase
import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import org.specs2.runner._
import play.api.i18n.MessagesApi
import play.api.inject.BindingKey
import play.api.libs.json.Json
import play.api.mvc.{Controller, Results}
import play.api.test._


@RunWith(classOf[JUnitRunner])
class PeerReviewControllerSpec extends PlaySpecification with Results with JsonMatchers {

  "A PeerReviewController" should {

    "create a lock to peer review a passage and return a new Review object when all fields are present" in new PeerReviewControllerTest with PeerReviewTextuallyGradedAssignment with PeerReviewTestCase {
      val data = Map(
        "empty" -> Seq("empty")
      )
      val createRequest = FakeRequest(POST, s"/assignments/${assignment.id}/reviews", FakeHeaders(), data)
        .withSession("userId" -> reviewingStudents.head.id.toString)

      val createResponse = controller.createLock(assignment.id)(createRequest)
      status(createResponse) mustEqual OK
      val createJson = contentAsString(createResponse)

      val rubric = assignment.reviewRubric
      createJson must /("responses") /#(0) /("rubricId" -> rubric(0).id.toDouble)
           /("rubricText" -> rubric(0).description)
          /#(1) /("rubricId" -> rubric(1).id.toDouble)
              /("rubricText" -> rubric(1).description)

    }

    "fail to create a lock when the user is not logged in" in new PeerReviewControllerTest with PeerReviewTextuallyGradedAssignment with PeerReviewTestCase {
      val data = Map(
        "empty" -> Seq("empty")
      )
      val createRequest = FakeRequest(POST, s"/assignments/${assignment.id}/reviews", FakeHeaders(), data)

      val createResponse = controller.createLock(assignment.id)(createRequest)
      status(createResponse) mustEqual FORBIDDEN
    }

//    "fail to create a lock when the user is not a member of the class"

//    "expire the lock after the given time is elapsed"
//
//    "not allow a different user to update the same peer review"
    "return a message stating that the user has done enough when they have finished all their peer reviews" in new PeerReviewControllerTest with PeerReviewTextuallyGradedAssignment with PeerReviewTestCase {
      // create 3 explications, one for each student
      val explications = reviewingStudents.zipWithIndex.map({
        case (student, index) => {
          val annotation = annotationRepo.create(
            student, themeRepo.create(student, assignment,
              s"Heroic Qualities $index", List(), assignment.themes.head, student, 0
            ).right.get,
            Annotation.from("The selected text", "/p[0]", "/p[1]", 10, 30, "1", student)
          ).right.get
          explicationRepo.save(
            student, annotation, Explication.from(
              s"An explication from ${student.providedName}", annotation, student
            )
          ).right.get
        }
      })

      val now = new Date
      (1 to assignment.numberRequired).map(index => {
        val review = peerReviewRepo.createLock(mockStudent, assignment).right.get
        peerReviewRepo.update(mockStudent, review.addResponse(
          review.responses.zip(assignment.reviewRubric).map({
            case (reviewPrompt, assignmentPrompt) => PromptResponse(
              reviewPrompt.id, "This is my first response", assignmentPrompt, -1
            )
          })
        ))
      })

      val data = Map("empty" -> Seq("empty"))
      val createRequest = FakeRequest(POST, s"/assignments/${assignment.id}/reviews", FakeHeaders(), data)
        .withSession("userId" -> mockStudent.id.toString)

      val createResponse = controller.createLock(assignment.id)(createRequest)
      status(createResponse) mustEqual OK
      val createJson = contentAsString(createResponse)
      createJson must /("message" -> controller.ASSIGNMENT_COMPLETE)
    }

//    "not allow more than the allowed number of students to review the same explication" in new PeerReviewControllerTest with PeerReviewTextuallyGradedAssignment with PeerReviewTestCase { }

//    "return a message stating that the user should try again later when there are no peer reviews available" in new PeerReviewControllerTest with PeerReviewTextuallyGradedAssignment with PeerReviewTestCase { }

    // "only accept textual reviews for a textual assignment"
    // "only accept numeric reviews for a numerically-graded assignment"
    // "ensure all values are within specified ranges for a numerically-graded assignment"
//
    "update a textual peer review" in new PeerReviewControllerTest with PeerReviewTextuallyGradedAssignment with PeerReviewTestCase {
      val data = Map(
        "empty" -> Seq("empty")
      )
      val reviewingStudent = reviewingStudents.head
      val createRequest = FakeRequest(POST, s"/assignments/${assignment.id}/reviews", FakeHeaders(), data)
        .withSession("userId" -> reviewingStudent.id.toString)

      val createResponse = controller.createLock(assignment.id)(createRequest)
      status(createResponse) mustEqual OK
      val createJson = contentAsJson(createResponse)
      val lockId = (createJson \ "id").as[Long]

      val promptResponse1Id = ((createJson \ "responses")(0) \ "id").as[Long]
      val promptResponse2Id = ((createJson \ "responses")(1) \ "id").as[Long]

      val updateRequest = FakeRequest(PUT, s"/reviews/${lockId}").withBody(Json.parse(
        s"""
           |{
           |  "id" : ${lockId},
           |  "responses" : [
           |    {
           |      "id" : ${promptResponse1Id},
           |      "text" : "Good job!"
           |    },
           |    {
           |      "id" : ${promptResponse2Id},
           |      "text" : "Great job!"
           |    }
           |  ]
           |}
         """.stripMargin))
        .withSession("userId" -> reviewingStudent.id.toString)

      val updateResponse = controller.update(lockId)(updateRequest)
      status(updateResponse) mustEqual OK

      val content = contentAsString(updateResponse)
      content must /("message" -> controller.PEER_REVIEW_SAVED)
    }

    "retrieve an assignment for an instructor when requested by the annotation ID" in new PeerReviewControllerTest with InstructorControllerTestCase {
      val student = studentsWhoHaveCompletedTheAssignment.head
      val explication = explicationRepo.getByUserAndAssignment(instructor, assignment, student).right.get.head
      val annotation = explication.annotation
      val reviews = peerReviewRepo.listCompletedForExplication(instructor, explication).right.get
      val request = FakeRequest(GET, s"/annotations/${annotation.id}/reviews").withSession("userId" -> instructor.id.toString  )
      val response = controller.listByAnnotation(annotation.id)(request)

      status(response) mustEqual OK
      val content = contentAsString(response)

      content must /("reviews").andHave(size(reviews.size))
      content must /("reviews")/#(0)/("id" -> reviews(0).id.toDouble)
      content must /("reviews")/#(0)/("userRef" -> s"/users/${reviews(0).reviewer.id}")
      content must /("reviews")/#(1)/("id" -> reviews(1).id.toDouble)
      content must /("reviews")/#(1)/("userRef" -> s"/users/${reviews(1).reviewer.id}")
    }

    "retrieve an assignment for an instructor or a student when requested by the annotation ID" in new PeerReviewControllerTest with InstructorControllerTestCase {
      val student = studentsWhoHaveCompletedTheAssignment.head
      val explication = explicationRepo.getByUserAndAssignment(student, assignment, student).right.get.head
      val annotation = explication.annotation
      val reviews = peerReviewRepo.listCompletedForExplication(student, explication).right.get
      val request = FakeRequest(GET, s"/annotations/${annotation.id}/reviews").withSession("userId" -> student.id.toString  )
      val response = controller.listByAnnotation(annotation.id)(request)

      status(response) mustEqual OK
      val content = contentAsString(response)

      content must /("reviews").andHave(size(reviews.size))
      content must /("reviews")/#(0)/("id" -> reviews(0).id.toDouble)
      content must /("reviews")/#(0)/("userRef" -> s"/users/${reviews(0).reviewer.id}")
      content must /("reviews")/#(1)/("id" -> reviews(1).id.toDouble)
      content must /("reviews")/#(1)/("userRef" -> s"/users/${reviews(1).reviewer.id}")
    }

    "return all reviews written by a student for that student"  in new PeerReviewControllerTest with InstructorControllerTestCase {
      val student = studentsWhoHaveCompletedTheAssignment.head
      val request = FakeRequest(GET, s"/assignments/${assignment.id}/peer_reviews/${student.id}").withSession("userId" -> student.id.toString  )
      val response = controller.getByUserInAssignment(assignment.id, student.id)(request)

      status(response) mustEqual OK
      val content = contentAsString(response)
      content must /("student")/("id" -> student.id.toDouble)
      content must /("student")/("name" -> student.providedName  )
      content must /("peerReviews").andHave(size(assignment.numberRequired))
    }

    "return all reviews written by a student for the instructor of that course" in new PeerReviewControllerTest with InstructorControllerTestCase {
      val student = studentsWhoHaveCompletedTheAssignment.head
      val request = FakeRequest(GET, s"/assignments/${assignment.id}/peer_reviews/${student.id}").withSession("userId" -> instructor.id.toString  )
      val response = controller.getByUserInAssignment(assignment.id, student.id)(request)

      status(response) mustEqual OK
      val content = contentAsString(response)
      content must /("student")/("id" -> student.id.toDouble)
      content must /("student")/("name" -> student.providedName  )
      content must /("peerReviews").andHave(size(assignment.numberRequired))
    }

  }

}

trait HasPeerReviewRepo {
  val dbService:DatabaseService
  val userRepo:UserRepository
  val explicationRepo:ExplicationRepository

  val peerReviewRepo = new PeerReviewRepository(dbService, userRepo, explicationRepo)
}

class PeerReviewControllerTest extends PeerReviewRepoTest {

  val ur = userRepo
  val ar = assignmentRepo
  val er = explicationRepo
  val anr = annotationRepo
  val prr = peerReviewRepo

  val controller = new PeerReviewController with Controller {
    override val messagesApi = app.injector.instanceOf[MessagesApi]
    override val userRepo = ur
    override val assignmentRepo = ar
    override val explicationRepo = er
    override val annotationRepo = anr
    override val peerReviewRepo = prr

    override val authenticationService = new AuthenticationService(userRepo)
    override val assignmentService = new AssignmentService(lmsContextRepo, userRepo, assignmentRepo)
    override val reviewEmailer: ActorRef = app.injector.instanceOf(BindingKey(classOf[ActorRef]).qualifiedWith("review-emailer"))
    override val themeRepo: ThemeRepository = app.injector.instanceOf[ThemeRepository]
  }
}

