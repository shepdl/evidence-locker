package reviews

import java.io.PrintWriter
import java.util.Date

import annotations.{Annotation, AnnotationRepository, Explication, ExplicationRepository}
import assignments._
import edu.ucla.cdh.EvidenceLocker.authentication.{User, UserRepository}
import lti.{LMSContext, LMSContextRepo}
import play.api.libs.Files.TemporaryFile

/**
  * Created by dave on 8/29/16.
  */
trait PeerReviewTextuallyGradedAssignment {
  val assignmentRepo:AssignmentRepository
  val mockInstructor:User
  val context:LMSContext

  val assignment = {
    val initialTextAssignment = assignmentRepo.create(
      mockInstructor, context, "Textually-Graded Assignment", "Close-read a passage", mockInstructor, "starfleet-123"
    ).right.get

    val file = {
      val tempFile = TemporaryFile("text", ".txt")
      val writer = new PrintWriter(tempFile.file)
      writer.close()
      tempFile
    }

    val document = AssignmentDocument(
      initialTextAssignment.document.id,
      file.file,
      file.file.getName,
      file.file.getName.split(",").reverse.head
    )

    val now = new Date
    val updatedAssignment = Assignment(
      id = initialTextAssignment.id,
      initialTextAssignment.remoteId,
      document = document,
      title = "Textually-Graded Assignment",
      description = initialTextAssignment.description,
      openDate = now,
      closeDate = now,
      instructor = initialTextAssignment.instructor,
      peerReviewEnabled = true,
      numberRequired = 3,
      rubricType = "textual",
      reviewRubric = List(
        RubricPrompt(-1, "Evidence", 0, 5),
        RubricPrompt(-1, "Comprehensiveness", 0, 5)
      ),
      themes = List(
        Theme(-1, "Heroism", null, List(), mockInstructor, 0)
      )
    )

    assignmentRepo.updateAssignment(mockInstructor, updatedAssignment).right.get
  }

}

trait PeerReviewNumericallyGradedAssignment {
  val assignmentRepo:AssignmentRepository
  val mockInstructor:User
  val context:LMSContext

  val assignment = {
    val initialTextAssignment = assignmentRepo.create(
      mockInstructor, context, "Numerically-Graded Assignment", "Close-read a passage", mockInstructor, "starfleet-123"
    ).right.get

    val file = {
      val tempFile = TemporaryFile("text", ".txt")
      val writer = new PrintWriter(tempFile.file)
      writer.close()
      tempFile
    }

    val document = AssignmentDocument(
      initialTextAssignment.document.id,
      file.file,
      file.file.getName,
      file.file.getName.split(",").reverse.head
    )

    val now = new Date
    val updatedAssignment = Assignment(
      id = initialTextAssignment.id,
      initialTextAssignment.remoteId,
      document,
      title = "Numerically-Graded Assignment",
      description = initialTextAssignment.description,
      openDate = now,
      closeDate = now,
      instructor = initialTextAssignment.instructor,
      peerReviewEnabled = true,
      numberRequired = 3,
      rubricType = "numeric",
      reviewRubric = List(
        RubricPrompt(-1, "Evidence", 0, 5),
        RubricPrompt(-1, "Comprehensiveness", 0, 5)
      ),
      themes = List(
        Theme(-1, "Heroism", null, List(), mockInstructor, 0)
      )
    )

    assignmentRepo.updateAssignment(mockInstructor, updatedAssignment).right.get
  }
}

trait PeerReviewTestCase {

  val userRepo:UserRepository
  val themeRepo:ThemeRepository
  val lmsContextRepo:LMSContextRepo
  val annotationRepo:AnnotationRepository
  val explicationRepo:ExplicationRepository
  val assignment:Assignment

  val mockStudent:User
  val context:LMSContext

  val reviewingStudents = List(
    "Molly O'Brien", "Julian Bashir", "Jake Sisko"
  ).zipWithIndex.map({
      case (name, index) => {
        val student = userRepo.create(
          s"student-${index}", name, s"${name.replace(' ', '-').toLowerCase()}"
        ).get
        lmsContextRepo.setRolesInContextForUser(student, context, Set(userRepo.roles.student))
        student
      }
    }
  )

  lmsContextRepo.setRolesInContextForUser(mockStudent, context, Set(userRepo.roles.student))

  val heroicQualities = themeRepo.create(
    mockStudent, assignment, "Heroic Qualities", List(),
    assignment.themes.head, mockStudent, 0
  ).right.get

  val annotation = annotationRepo.create(
    mockStudent, heroicQualities, Annotation.from(
      "Test passage", "/p/1", "/p/2", 10, 30, "1", mockStudent
    )
  ).right.get

  val explication = explicationRepo.save(
    mockStudent, annotation, Explication.from(
      "This is my explication", annotation, mockStudent
    )
  ).right.get

}
