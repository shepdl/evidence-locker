package instructor

import annotations.{Annotation, AnnotationRepository, Explication, ExplicationRepository}
import assignments._
import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import lti.{LMSContext, LMSContextRepo}
import reviews.{PeerReviewNumericallyGradedAssignment, PeerReviewRepository, PromptResponse}

import scala.util.Random

/**
  * Created by dave on 9/30/16.
  */
trait InstructorControllerTestCase extends PeerReviewNumericallyGradedAssignment {
  val assignmentRepo:AssignmentRepository
  val userRepo:UserRepository
  val lmsContextRepo:LMSContextRepo
  val context:LMSContext

  val themeRepo:ThemeRepository
  val annotationRepo:AnnotationRepository
  val explicationRepo:ExplicationRepository
  val peerReviewRepo:PeerReviewRepository

  val instructor = userRepo.create("starfleet-1", "Jean-Luc Picard", "picard@starfleet.edu").get
  lmsContextRepo.setRolesInContextForUser(instructor, context, Set(userRepo.roles.instructor))

  val students = List(
    "Molly O'Brien", "Julian Bashir", "Wesley Crusher", "Alexander Rozhenko", "Jake Sisko",
    "Leonard Akaar", "Jonathan Archer", "Sterling Archer"
  ).zipWithIndex.map({
    case (name, index) => {
      val student = userRepo.create(s"starfleet-${index}", name, s"${name.replace(" ", "-")}@starfleet.edu").get
      lmsContextRepo.setRolesInContextForUser(student, context, Set(userRepo.roles.student))
      student
    }
  })


  /**
    * Of 8 students ...
    *   4 have completed the whole assignment
    *   2 has done all the explications and 2/3 peer reviews
    *   1 has done 2/3 explications
    *   1 has done nothing
    */

  val random = new Random()

  val studentsWhoHaveCompletedTheAssignment = {
    val molly = students(0)

    val heroism = assignment.themes.head

    val mollySubTheme = themeRepo.create(molly, assignment, "Heroic Qualities", List(), heroism, molly, 0).right.get
    val mollyAnnotation1 = annotationRepo.create(molly, mollySubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", molly)).right.get
    val mollyExplication1 = explicationRepo.save(molly, mollyAnnotation1, Explication.from(s"This is my explication 1 by ${molly.providedName}", mollyAnnotation1, molly))
    val mollyAnnotation2 = annotationRepo.create(molly, mollySubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", molly)).right.get
    val mollyExplication2 = explicationRepo.save(molly, mollyAnnotation2, Explication.from(s"This is my explication 2 by ${molly.providedName}", mollyAnnotation2, molly))
    val mollyAnnotation3 = annotationRepo.create(molly, mollySubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", molly)).right.get
    val mollyExplication3 = explicationRepo.save(molly, mollyAnnotation3, Explication.from(s"This is my explication 3 by ${molly.providedName}", mollyAnnotation3, molly))

    val julian = students(1)
    val julianSubTheme = themeRepo.create(julian, assignment, "Heroic Qualities", List(), heroism, julian, 0).right.get
    val julianAnnotation1 = annotationRepo.create(julian, julianSubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", julian)).right.get
    val julianExplication1 = explicationRepo.save(julian, julianAnnotation1, Explication.from(s"This is my explication 1 by ${julian.providedName}", julianAnnotation1, julian))
    val julianAnnotation2 = annotationRepo.create(julian, julianSubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", julian)).right.get
    val julianExplication2 = explicationRepo.save(julian, julianAnnotation2, Explication.from(s"This is my explication 2 by ${julian.providedName}", julianAnnotation2, julian))
    val julianAnnotation3 = annotationRepo.create(julian, julianSubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", julian)).right.get
    val julianExplication3 = explicationRepo.save(julian, julianAnnotation3, Explication.from(s"This is my explication 3 by ${julian.providedName}", julianAnnotation3, julian))

    val wesley = students(2)
    val wesleySubTheme = themeRepo.create(wesley, assignment, "Heroic Qualities", List(), heroism, wesley, 0).right.get
    val wesleyAnnotation1 = annotationRepo.create(wesley, wesleySubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", wesley)).right.get
    val wesleyExplication1 = explicationRepo.save(wesley, wesleyAnnotation1, Explication.from(s"This is my explication 1 by ${wesley.providedName}", wesleyAnnotation1, wesley))
    val wesleyAnnotation2 = annotationRepo.create(wesley, wesleySubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", wesley)).right.get
    val wesleyExplication2 = explicationRepo.save(wesley, wesleyAnnotation2, Explication.from(s"This is my explication 2 by ${wesley.providedName}", wesleyAnnotation2, wesley))
    val wesleyAnnotation3 = annotationRepo.create(wesley, wesleySubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", wesley)).right.get
    val wesleyExplication3 = explicationRepo.save(wesley, wesleyAnnotation3, Explication.from(s"This is my explication 3 by ${wesley.providedName}", wesleyAnnotation3, wesley))

    val alexander = students(3)
    val alexanderSubTheme = themeRepo.create(alexander, assignment, "Heroic Qualities", List(), heroism, alexander, 0).right.get
    val alexanderAnnotation1 = annotationRepo.create(alexander, alexanderSubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", alexander)).right.get
    val alexanderExplication1 = explicationRepo.save(alexander, alexanderAnnotation1, Explication.from(s"This is my explication 1 by ${alexander.providedName}", alexanderAnnotation1, alexander))
    val alexanderAnnotation2 = annotationRepo.create(alexander, alexanderSubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", alexander)).right.get
    val alexanderExplication2 = explicationRepo.save(alexander, alexanderAnnotation2, Explication.from(s"This is my explication 2 by ${alexander.providedName}", alexanderAnnotation2, alexander))
    val alexanderAnnotation3 = annotationRepo.create(alexander, alexanderSubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", alexander)).right.get
    val alexanderExplication3 = explicationRepo.save(alexander, alexanderAnnotation3, Explication.from(s"This is my explication 3 by ${alexander.providedName}", alexanderAnnotation3, alexander))

    val theStudents = List(molly, julian, wesley, alexander)

    theStudents.foreach(student => {
      (1 to assignment.numberRequired).foreach(index => {
        val peerReview = peerReviewRepo.createLock(student, assignment).right.get
        val updatedReview = peerReview.addResponse(assignment.reviewRubric.map(rr => {
          PromptResponse(-1, "", rr, random.nextInt(rr.max - rr.min) + rr.min)
        }))
        peerReviewRepo.update(student, updatedReview).right.get
      })
    })

    theStudents
  }

  val explicationsCompleteButNotPeerReviews = {
    val jake = students(4)
    assert(jake.providedName == "Jake Sisko")

    val heroism = assignment.themes.head

    val jakeSubTheme = themeRepo.create(jake, assignment, "Heroic Qualities", List(), heroism, jake, 0).right.get
    val jakeAnnotation1 = annotationRepo.create(jake, jakeSubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", jake)).right.get
    val jakeExplication1 = explicationRepo.save(jake, jakeAnnotation1, Explication.from(s"This is my explication 1 by ${jake.providedName}", jakeAnnotation1, jake))
    val jakeAnnotation2 = annotationRepo.create(jake, jakeSubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", jake)).right.get
    val jakeExplication2 = explicationRepo.save(jake, jakeAnnotation2, Explication.from(s"This is my explication 2 by ${jake.providedName}", jakeAnnotation2, jake))
    val jakeAnnotation3 = annotationRepo.create(jake, jakeSubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", jake)).right.get
    val jakeExplication3 = explicationRepo.save(jake, jakeAnnotation3, Explication.from(s"This is my explication 3 by ${jake.providedName}", jakeAnnotation3, jake))

    val leonard = students(5)
    val leonardSubTheme = themeRepo.create(leonard, assignment, "Heroic Qualities", List(), heroism, leonard, 0).right.get
    val leonardAnnotation1 = annotationRepo.create(leonard, leonardSubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", leonard)).right.get
    val leonardExplication1 = explicationRepo.save(leonard, leonardAnnotation1, Explication.from(s"This is my explication 1 by ${leonard.providedName}", leonardAnnotation1, leonard))
    val leonardAnnotation2 = annotationRepo.create(leonard, leonardSubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", leonard)).right.get
    val leonardExplication2 = explicationRepo.save(leonard, leonardAnnotation2, Explication.from(s"This is my explication 2 by ${leonard.providedName}", leonardAnnotation2, leonard))
    val leonardAnnotation3 = annotationRepo.create(leonard, leonardSubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", leonard)).right.get
    val leonardExplication3 = explicationRepo.save(leonard, leonardAnnotation3, Explication.from(s"This is my explication 3 by ${leonard.providedName}", leonardAnnotation3, leonard))

    val jonathan = students(6)
    val jonathanSubTheme = themeRepo.create(jonathan, assignment, "Heroic Qualities", List(), heroism, jonathan, 0).right.get
    val jonathanAnnotation1 = annotationRepo.create(jonathan, jonathanSubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", jonathan)).right.get
    val jonathanExplication1 = explicationRepo.save(jonathan, jonathanAnnotation1, Explication.from(s"This is my explication 1 by ${jonathan.providedName}", jonathanAnnotation1, jonathan))
    val jonathanAnnotation2 = annotationRepo.create(jonathan, jonathanSubTheme, Annotation.from("No quote", "/[p]", "/[p1]", 0, 30, "1", jonathan)).right.get
    val jonathanExplication2 = explicationRepo.save(jonathan, jonathanAnnotation2, Explication.from(s"This is my explication 2 by ${jonathan.providedName}", jonathanAnnotation2, jonathan))

    (1 to 2).foreach(index => {
      val peerReview = peerReviewRepo.createLock(jake, assignment).right.get
      val updatedReview = peerReview.addResponse(assignment.reviewRubric.map(rr => {
        PromptResponse(-1, "", rr, random.nextInt(rr.max - rr.min) + rr.min)
      }))
      peerReviewRepo.update(jake, updatedReview).right.get
    })

    (1 to 2).foreach(index => {
      val peerReview = peerReviewRepo.createLock(leonard, assignment).right.get
      val updatedReview = peerReview.addResponse(assignment.reviewRubric.map(rr => {
        PromptResponse(-1, "", rr, random.nextInt(rr.max - rr.min) + rr.min)
      }))
      peerReviewRepo.update(leonard, updatedReview).right.get
    })

    List(jake, leonard)
  }
}
