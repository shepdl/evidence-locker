package authentication

import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import lti.ToolConsumer

/**
  * Created by dave on 6/30/16.
  */
trait HasUserRepo {

  val dbService:DatabaseService
  val toolConsumer:ToolConsumer

  val userRepo = new UserRepository(dbService)

}
