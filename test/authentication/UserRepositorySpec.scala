package authentication

import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import org.junit.runner._
import org.specs2.mutable._
import org.specs2.runner._


@RunWith(classOf[JUnitRunner])
class UserRepositorySpec extends Specification {

  "A UserRepository" should {

    "create a user in response to a call from the LTI Launch Repository" in new UserRepositoryTest {
      val remoteId = "user-1"
      val name = "Josephine Bruin"
      val email = "jbruin@example.ucla.edu"
      val user = userRepo.create(remoteId, name, email).get

      user.remoteId mustEqual remoteId
      user.providedName mustEqual name
      user.email mustEqual email

      val foundUser = userRepo.getById(user.id).get

      foundUser.id mustEqual user.id
      foundUser.remoteId mustEqual user.remoteId
      foundUser.providedName mustEqual user.providedName
      foundUser.email mustEqual user.email

      val foundUserByRemoteId = userRepo.getByRemoteId(remoteId).get

      foundUserByRemoteId.id mustEqual user.id
      foundUserByRemoteId.remoteId mustEqual user.remoteId
      foundUserByRemoteId.providedName mustEqual user.providedName
      foundUserByRemoteId.email mustEqual user.email
    }

    "create a local administrative user" in new UserRepositoryTest {
      val name = "Josephine Bruin, Administrator"
      val email = "jb-admin@example.ucla.edu"
      val password = "abc123"
      val userResult = userRepo.createAdminUser(name, email, password)
      userResult must beSome

      val foundUser = userRepo.getById(userResult.get.id)
      foundUser must beSome
    }

    "log in a local administrative user" in new UserRepositoryTest with LocalAdminLoginTestScenario {
      val result = userRepo.loginWithEmailAndPassword(email, password)
      result must beSome
      val trueResult = result.get
      val comparingUser = userResult.get

      trueResult.id mustEqual comparingUser.id
      trueResult.remoteId mustEqual comparingUser.remoteId
      trueResult.providedName mustEqual comparingUser.providedName
      trueResult.email mustEqual comparingUser.email
    }

    "fail to log in a user who supplies the wrong password" in new UserRepositoryTest with LocalAdminLoginTestScenario {
      val result = userRepo.loginWithEmailAndPassword(email, "not a real password")
      result must beNone
    }

    "fail to log in a user that does not exist" in new UserRepositoryTest with LocalAdminLoginTestScenario {
      val result = userRepo.loginWithEmailAndPassword("Tommy Trojan", "trojans4eva")
      result must beNone
    }

    "return that a user exists if a user with that remote ID does exist" in new UserRepositoryTest {
      val remoteId = "user-1"
      val name = "Josephine Bruin"
      val email = "jbruin@example.ucla.edu"
      val user = userRepo.create(remoteId, name, email).get

      val possibleUser = userRepo.getByRemoteId(remoteId)

      possibleUser must beSome
      val trueUser = possibleUser.get
      trueUser.id mustEqual user.id
      trueUser.remoteId mustEqual user.remoteId
      trueUser.providedName mustEqual user.providedName
      trueUser.email mustEqual user.email
    }

    "return that a user does not exist if a user with that remote ID does not exist" in new UserRepositoryTest {
      val remoteId = "user-1"
      val name = "Josephine Bruin"
      val email = "jbruin@example.ucla.edu"
      val user = userRepo.create(remoteId, name, email).get

      val possibleUser = userRepo.getByRemoteId("user-2")

      possibleUser must beNone
    }

  }

}


trait LocalAdminLoginTestScenario {

  val userRepo:UserRepository

  val name = "Josephine Bruin, Administrator"
  val email = "jb-admin@example.ucla.edu"
  val password = "abc123"
  val userResult = userRepo.createAdminUser(name, email, password)

}




