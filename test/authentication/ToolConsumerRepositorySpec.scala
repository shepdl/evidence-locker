package authentication

import common.HasDatabase
import org.junit.runner._
import org.specs2.mutable._
import org.specs2.runner._
import play.api.test._


@RunWith(classOf[JUnitRunner])
class ToolConsumerRepositorySpec extends Specification {

  "A ToolConsumerRepository" should {

    "create a new tool consumer" in new ToolConsumerRepoTest {
      val url = "https://moodle.fakeu.edu/"
      val newToolConsumer = toolConsumerRepo.create(url, Some("abcdef"), "Moodle")

      newToolConsumer.url mustEqual url

      val foundToolConsumer = toolConsumerRepo.getById(newToolConsumer.id).get
      foundToolConsumer.url mustEqual newToolConsumer.url
      foundToolConsumer.sharedSecret mustEqual newToolConsumer.sharedSecret
      foundToolConsumer.createdAt.getTime must beBetween(
        newToolConsumer.createdAt.getTime - 1000,
        newToolConsumer.createdAt.getTime + 1000
      )

      val foundToolConsumerByURL = toolConsumerRepo.getByURL(url).get
      foundToolConsumerByURL.url mustEqual newToolConsumer.url
      foundToolConsumerByURL.sharedSecret mustEqual newToolConsumer.sharedSecret
      foundToolConsumerByURL.createdAt.getTime must beBetween(
        newToolConsumer.createdAt.getTime - 1000,
        newToolConsumer.createdAt.getTime + 1000
      )

    }

    "not generate the same shared secret multiple tiles" in new ToolConsumerRepoTest {
      val url = "https://moodle.fakeu.edu/"
      val sharedSecret1 = toolConsumerRepo.generateSharedSecret(url)
      Thread.sleep(1000)
      val sharedSecret2 = toolConsumerRepo.generateSharedSecret(url)
      sharedSecret1 mustNotEqual sharedSecret2
    }

    "create a new shared secret for an existing tool consumer that is not the same as the old one" in new ToolConsumerRepoTest  {
      val url = "https://moodle.fakeu.edu/"
      val firstToolConsumer = toolConsumerRepo.create(url, Some("qwerty"), "Canvas")

      Thread.sleep(1000)
      val secondToolConsumer = toolConsumerRepo.generateNewKey(firstToolConsumer).get

      secondToolConsumer.id mustEqual firstToolConsumer.id
      secondToolConsumer.url mustEqual firstToolConsumer.url
      secondToolConsumer.createdAt mustEqual firstToolConsumer.createdAt
      secondToolConsumer.sharedSecret mustNotEqual firstToolConsumer.sharedSecret

    }

  }

}

trait ToolConsumerRepoTest extends WithApplication with HasDatabase with HasToolConsumerRepo


