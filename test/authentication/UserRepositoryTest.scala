package authentication

import common.HasDatabase
import play.api.test.WithApplication


/**
  * Created by dave on 6/30/16.
  */
trait UserRepositoryTest extends WithApplication with HasDatabase with HasUserRepo with HasToolConsumerRepo {
  val toolConsumer = toolConsumerRepo.create("http://lms.example.com/", Some("qwerty"), "Moodle")
}
