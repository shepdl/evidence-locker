package authentication

import assignments.HasAssignmentRepo
import common.HasDatabase
import org.junit.runner.RunWith
import org.specs2.matcher.JsonMatchers
import org.specs2.runner._
import play.api.test.{PlaySpecification, WithApplication}


/**
  * Created by daveshepard on 7/13/16.
  */
@RunWith(classOf[JUnitRunner])
class LTILaunchSpec extends PlaySpecification with JsonMatchers {

  "An LTI Launch Controller" should {

    "create a new activity when an activity with that remote ID does not exist" in new LTILaunchControllerTest {
      // Create HTTP request
      // Check if the user now exists
      // Check if the context now exists
      // Check if the assignment now exists

      1 must_== 1
    }

    "not call the activity create method, and redirect the user's browser to the main app page when the activity exists"

    "show the Tool Consumer Not Recognized error screen when the Tool Consumer is not registered"
    "create a new user record when a user with that remote ID for that tool consumer does not exist"
    "not create a new user record when a user with that remote ID for that tool consumer does not exist"

    "send the user to the Incorrect Role error screen when the Tool Consumer does not report user as having an instructor role and the activity ID is not registered"

    1 mustEqual 1

  }

}

class LTILaunchControllerTest extends WithApplication
  with HasDatabase with HasToolConsumerRepo with HasUserRepo with HasAssignmentRepo {

  val toolConsumer = toolConsumerRepo.create("http://example.com/", None, "Unknown")

  // TODO: create LaunchController
}
