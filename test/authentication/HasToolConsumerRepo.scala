package authentication

import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import lti.ToolConsumerRepository

/**
  * Created by dave on 6/30/16.
  */
trait HasToolConsumerRepo {

  val dbService:DatabaseService

  val toolConsumerRepo = new ToolConsumerRepository(dbService)

}
