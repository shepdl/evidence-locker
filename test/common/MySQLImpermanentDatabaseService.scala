package common

import java.io.{File, FilenameFilter}
import java.sql.SQLException
import javax.inject.Inject

import annotations.{AnnotationProperties, ExplicationProperties}
import com.google.inject.Singleton
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import org.specs2.specification.BeforeAfterEach
import play.api.{Application, Configuration, Logger}
import play.api.db.Database
import play.api.inject.ApplicationLifecycle
import anorm._
import assignments.{AssignmentProperties, RubricPromptProperties, ThemeProperties}
import edu.ucla.cdh.EvidenceLocker.authentication.UserProperties
import lti.{LMSContextMembershipProperties, LMSContextProperties, OAuthCredentialProperties}
import reviews.{PeerReviewProperties, PromptResponseProperties}

import scala.concurrent.Future
import scala.io.Source
import play.api.db.Databases

/**
  * Created by daveshepard on 9/23/16.
  */
@Singleton
class MySQLImpermanentDatabaseService @Inject() (config:Configuration, lifecycle:ApplicationLifecycle, override val db:Database) extends DatabaseService {

  private val logger = Logger("Test Database Service")

//  val db = dbApi.database(config.getString("db.default.dbName").get)

//  new File("conf/evolutions/default/").listFiles().sorted.map(sqlFile => {
//    println(s"Running ${sqlFile.getName}")
//    val initSQL = Source.fromFile(sqlFile).getLines().mkString("\n")
//    db.withTransaction(implicit c => {
//      initSQL.split("--- !Downs").head.split(";").foreach(sql => {
//        if (sql.trim.nonEmpty) {
//          SQL(sql).execute()
//        }
//        val warnings = c.getWarnings
//        if (warnings != null) {
//          println(warnings)
//        }
//      })
//
//      c.commit()
//    })
//  })

//  val initSQL = Source.fromFile(new File("app/install/initialize_database.sql")).getLines().mkString("\n")
  //    .replace("InnoDB", "MEMORY")

//  db.withTransaction(implicit c => {
//    initSQL.split(";").foreach(sql => {
//      if (sql.trim.nonEmpty) {
//        SQL(sql).execute()
//      }
//      val warnings = c.getWarnings
//      if (warnings != null) {
//        println(warnings)
//      }
//    })
//
//    c.commit()
//  })
  logger.info("Created database")

  lifecycle.addStopHook(() => {
    logger.info("Shutting database down")
    Future.successful({
      db.withConnection(implicit c => {
        c.setAutoCommit(false)
        List(
          AnnotationProperties.table, ExplicationProperties.table,
          AssignmentProperties.table, RubricPromptProperties.table, ThemeProperties.table,
          LMSContextProperties.table, LMSContextMembershipProperties.table, OAuthCredentialProperties.table,
          PeerReviewProperties.table, PromptResponseProperties.table
        ).foreach(tableName => {
          SQL(s"""TRUNCATE TABLE ${tableName}""").execute()
        })
        c.commit()
      })
      db.shutdown()
    })
  })

}

trait HasDatabase extends BeforeAfterEach {

  val app:Application
  val dbService = app.injector.instanceOf[MySQLImpermanentDatabaseService]

  def before = {

  }

  def after = {
  }

}
