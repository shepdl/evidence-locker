package annotations

import assignments._
import authentication.{AssignmentService, AuthenticationService, HasToolConsumerRepo, HasUserRepo}
import common.HasDatabase
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import instructor.InstructorControllerTestCase
import lti.{HasBasicContext, HasLMSContextRepo}
import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import org.specs2.runner._
import play.api.i18n.MessagesApi
import play.api.libs.json.Json
import play.api.mvc.{Controller, Results}
import play.api.test._
import reviews.{HasPeerReviewRepo, PeerReviewRepository}


@RunWith(classOf[JUnitRunner])
class ExplicationControllerSpec extends PlaySpecification with Results with JsonMatchers {

  "An ExplicationController" should {

    "create an explication in response to a request" in new ExplicationControllerTest with ExplicationTestCase {
      val payload = Json.parse(s"""
       {
        "text" : "${explication.text.replace('\n', ' ')}"
       }
      """)
      val createExplicationRequest = FakeRequest(
        PUT, s"/annotations/${annotation.id}/explication"
      )
        .withBody(payload)
        .withSession("userId" -> mockStudent.id.toString)

      val createExplicationResponse = controller.create(annotation.id)(createExplicationRequest)
      status(createExplicationResponse) mustEqual CREATED

      val getExplicationRequest = FakeRequest(
        GET, s"/annotations/${annotation.id}/explication"
      ).withSession("userId" -> mockStudent.id.toString)

      val getExplicationResponse = controller.getByAnnotationId(annotation.id)(getExplicationRequest)
      status(getExplicationResponse) mustEqual OK

      val getExplicationJson = contentAsString(getExplicationResponse)
      getExplicationJson must /("text" -> explication.text.replace('\n', ' '))
    }

    "fail to create an explication when a user is not logged in" in new ExplicationControllerTest with ExplicationTestCase {
      val payload = Json.parse(s"""
       {
        "text" : "${explication.text.replace('\n', ' ')}"
       }
      """)
      val createExplicationRequest = FakeRequest(
        PUT, s"/annotations/${annotation.id}/explication"
      )
        .withBody(payload)

      val createExplicationResponse = controller.create(annotation.id)(createExplicationRequest)
      status(createExplicationResponse) mustEqual FORBIDDEN

      val getExplicationRequest = FakeRequest(
        GET, s"/annotations/${annotation.id}/explication"
      ).withSession("userId" -> mockStudent.id.toString)

      val getExplicationResponse = controller.getByAnnotationId(annotation.id)(getExplicationRequest)
      status(getExplicationResponse) mustEqual OK

      val getExplicationJson = contentAsString(getExplicationResponse)
      getExplicationJson must /("text" -> "")
    }

    "fail to create an explication when a user is not the annotation's owner" in new ExplicationControllerTest with ExplicationTestCase {
      val payload = Json.parse(s"""
       {
        "text" : "${explication.text.replace('\n', ' ')}"
       }
      """)
      val createExplicationRequest = FakeRequest(PUT, s"/annotations/${annotation.id}/explication")
        .withBody(payload)
        .withSession("userId" -> mockInstructor.id.toString)

      val createExplicationResponse = controller.create(annotation.id)(createExplicationRequest)
      status(createExplicationResponse) mustEqual FORBIDDEN

      val getExplicationRequest = FakeRequest(
        GET, s"/annotations/${annotation.id}/explication"
      ).withSession("userId" -> mockStudent.id.toString)

      val getExplicationResponse = controller.getByAnnotationId(annotation.id)(getExplicationRequest)
      status(getExplicationResponse) mustEqual OK

      val getExplicationJson = contentAsString(getExplicationResponse)
      getExplicationJson must /("text" -> "")
    }

    "not allow a user to view an explication who is not logged in" in new ExplicationControllerTest with ExplicationTestCase with TestCase2 {
      val payload = Json.parse(s"""
       {
        "text" : "${explication.text.replace('\n', ' ')}"
       }
      """)
      val createExplicationRequest = FakeRequest(
        PUT, s"/annotations/${annotation.id}/explication"
      )
        .withBody(payload)
        .withSession("userId" -> mockStudent.id.toString)

      val createExplicationResponse = controller.create(annotation.id)(createExplicationRequest)
      status(createExplicationResponse) mustEqual CREATED

      val getExplicationRequest = FakeRequest(
        GET, s"/annotations/${annotation.id}/explication"
      )

      val getExplicationResponse = controller.getByAnnotationId(annotation.id)(getExplicationRequest)
      status(getExplicationResponse) mustEqual FORBIDDEN
    }

    "fail to create an explication when a user is not a member of the class" in new ExplicationControllerTest with ExplicationTestCase with TestCase2 {
      val payload = Json.parse(
        s"""
       {
        "text" : "${explication.text.replace('\n', ' ')}"
       }
      """)
      val createExplicationRequest = FakeRequest(PUT, s"/annotations/${annotation.id}/explication")
        .withBody(payload)
        .withSession("userId" -> alternateUser.id.toString)

      val createExplicationResponse = controller.create(annotation.id)(createExplicationRequest)
      status(createExplicationResponse) mustEqual FORBIDDEN

      val getExplicationRequest = FakeRequest(
        GET, s"/annotations/${annotation.id}/explication"
      ).withSession("userId" -> mockStudent.id.toString)

      val getExplicationResponse = controller.getByAnnotationId(annotation.id)(getExplicationRequest)
      status(getExplicationResponse) mustEqual OK

      val getExplicationJson = contentAsString(getExplicationResponse)
      getExplicationJson must /("text" -> "")
    }

    "not allow a user to view an explication if the user is not a member of the context" in new ExplicationControllerTest with ExplicationTestCase with TestCase2 {
      val payload = Json.parse(s"""
       {
        "text" : "${explication.text.replace('\n', ' ')}"
       }
      """)
      val createExplicationRequest = FakeRequest(
        PUT, s"/annotations/${annotation.id}/explication"
      )
        .withBody(payload)
        .withSession("userId" -> mockStudent.id.toString)

      val createExplicationResponse = controller.create(annotation.id)(createExplicationRequest)
      status(createExplicationResponse) mustEqual CREATED

      val getExplicationRequest = FakeRequest(
        GET, s"/annotations/${annotation.id}/explication"
      ).withSession("userId" -> alternateUser.id.toString)

      val getExplicationResponse = controller.getByAnnotationId(annotation.id)(getExplicationRequest)
      status(getExplicationResponse) mustEqual FORBIDDEN
    }

    "update an explication in response to a request" in new ExplicationControllerTest with ExplicationTestCase {
      val firstPayload = Json.parse(s"""
       {
        "text" : "${explication.text.replace('\n', ' ')}"
       }
      """)
      val createExplicationRequest = FakeRequest(
        PUT, s"/annotations/${annotation.id}/explication"
      )
        .withBody(firstPayload)
        .withSession("userId" -> mockStudent.id.toString)

      controller.create(annotation.id)(createExplicationRequest)

      val updatedText = "The text has been updated"
      val secondPayload = Json.parse(
        s"""
           |{
           |  "text" : "$updatedText"
           |}
         """.stripMargin)

      val updateExplicationRequest = FakeRequest(
        PUT, s"/annotations/${annotation.id}/explication"
      )
        .withBody(secondPayload)
        .withSession("userId" -> mockStudent.id.toString)

      val updateExplicationResponse = controller.create(annotation.id)(updateExplicationRequest)
      status(updateExplicationResponse) mustEqual CREATED

      val getExplicationRequest = FakeRequest(
        GET, s"/annotations/${annotation.id}/explication"
      ).withSession("userId" -> mockStudent.id.toString)

      val getExplicationResponse = controller.getByAnnotationId(annotation.id)(getExplicationRequest)
      status(getExplicationResponse) mustEqual OK

      val getExplicationJson = contentAsString(getExplicationResponse)
      getExplicationJson must /("text" -> updatedText)
    }

    "get all explications written by a student in an assignment" in new ExplicationControllerTest with HasPeerReviewRepo with InstructorControllerTestCase {
      val student = studentsWhoHaveCompletedTheAssignment.head
      val request = FakeRequest(GET, s"/assignments/${assignment.id}/explications/${student.id}").withSession("userId" -> instructor.id.toString  )
      val response = controller.getExplicationsByUserInAssignment(assignment.id, student.id)(request)

      status(response) mustEqual OK
      val content = contentAsString(response)
      content must /("student")/("id" -> student.id.toDouble)
      content must /("student")/("name" -> student.providedName)
      content must /("explications").andHave(size(assignment.numberRequired))
    }

  }

}


class ExplicationControllerTest extends WithApplication
  with HasDatabase with HasToolConsumerRepo with HasUserRepo with HasAssignmentRepo
  with HasLMSContextRepo with HasBasicContext
  with HasThemeRepo with HasAnnotationRepo
  with HasExplicationRepo {

  val tr = themeRepo
  val ur = userRepo
  val ar = annotationRepo
  val er = explicationRepo

  val controller = new ExplicationController with Controller {
    override val messagesApi = app.injector.instanceOf[MessagesApi]
    override val userRepo = ur
    override val annotationRepo = ar
    override val explicationRepo = er
    override val authenticationService = new AuthenticationService(userRepo)
    override val assignmentService = new AssignmentService(
      lmsContextRepo, userRepo, assignmentRepo
    )
  }

}
