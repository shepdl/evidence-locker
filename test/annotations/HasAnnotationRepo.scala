package annotations

import assignments.{AssignmentRepository, ThemeRepository}
import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import lti.LMSContextRepo

/**
  * Created by dave on 8/22/16.
  */
trait HasAnnotationRepo {
  val dbService:DatabaseService
  val userRepo:UserRepository
  val lmsContextRepo:LMSContextRepo
  val assignmentRepo:AssignmentRepository
  val themeRepo:ThemeRepository

  val annotationRepo = new AnnotationRepository(
    dbService, userRepo, assignmentRepo, lmsContextRepo, themeRepo
  )
}
