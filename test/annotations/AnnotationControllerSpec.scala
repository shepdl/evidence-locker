package annotations

import assignments._
import authentication.{HasToolConsumerRepo, HasUserRepo}
import common.{ELError, HasDatabase}
import edu.ucla.cdh.EvidenceLocker.authentication.{User, UserRepository}
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import lti.{HasBasicContext, HasLMSContextRepo, LMSContext, LMSContextRepo}
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import play.api.i18n.MessagesApi
import play.api.mvc.{Controller, Results}
import play.api.test._
import play.api.test.Helpers._


@RunWith(classOf[JUnitRunner])
class AnnotationControllerSpec extends PlaySpecification with Results with JsonMatchers {

  "A Theme Controller" should {

    "create a new annotation for a theme that the user has created" in new AnnotationControllerTest with TestCase1 with AnnotationCreationTestCase {
      lmsContextRepo.setRolesInContextForUser(
        mockStudent, context, Set(userRepo.roles.student)
      )
      val theme = themeRepo.create(
        mockStudent, updatedAssignment, "Test theme", List(), updatedAssignment.themes.head, mockStudent, 0
      ).right.get

      val data = Map(
        "quote" -> Seq(quote),
        "start" -> Seq(start),
        "end" -> Seq(end),
        "startOffset" -> Seq(startOffset.toString),
        "endOffset" -> Seq(endOffset.toString)
      )
      val createAnnotationRequest = FakeRequest(POST, s"themes/${theme.id}/annotations", FakeHeaders(), data)
        .withSession("userId" -> mockStudent.id.toString)

      val createAnnotationResponse = controller.create(theme.id)(createAnnotationRequest)
      status(createAnnotationResponse) mustEqual CREATED
      val createAnnotationJson = contentAsString(createAnnotationResponse)

      val annotationId = (contentAsJson(createAnnotationResponse) \ "id").as[Long]

      val getAnnotationRequest = FakeRequest(GET, s"/annotations/${annotationId}").withSession("userId" -> mockStudent.id.toString)
      val getAnnotationResponse = controller.getById(annotationId)(getAnnotationRequest)
      status(getAnnotationResponse) mustEqual OK
      val getAnnotationJson = contentAsString(getAnnotationResponse)
      getAnnotationJson must
        /("id" -> annotationId.toDouble)
        /("text" -> "")
        /("quote" -> quote)
        /("uri" -> s"${controller.domain}/annotations/${annotationId}")
        /("ranges") /#(0)
          /("start" -> start)
          /("end" -> end)
          /("startOffset" -> startOffset.toDouble)
          /("endOffset" -> endOffset.toDouble)
        /("user" -> s"/${controller.domain}/users/${mockStudent.id}")
        /("consumer" -> controller.domain)

      val getThemeRequest = FakeRequest(GET, s"/themes/${theme.id}/annotations").withSession("userId" -> mockStudent.id.toString)
      val getThemeResponse = controller.getForTheme(theme.id)(getThemeRequest)
      status(getThemeResponse) mustEqual OK
      val getThemeJson = contentAsString(getThemeResponse)

      getThemeJson must
        /("id" -> theme.id.toDouble)
        /("annotations") /#(0)
          /("id" -> annotationId.toDouble)
          /("text" -> "")
          /("quote" -> quote)
          /("uri" -> s"${controller.domain}/annotations/${annotationId}")
          /("ranges") /#(0)
          /("start" -> start)
          /("end" -> end)
          /("startOffset" -> startOffset.toDouble)
          /("endOffset" -> endOffset.toDouble)
          /("user" -> s"/${controller.domain}/users/${mockStudent.id}")
          /("consumer" -> controller.domain)

    }

    "fail for a user who is not logged in with 'Forbidden'" in new AnnotationControllerTest with TestCase1 with AnnotationCreationTestCase {
      lmsContextRepo.setRolesInContextForUser(
        mockInstructor, context, Set(userRepo.roles.instructor)
      )
      lmsContextRepo.setRolesInContextForUser(
        mockStudent, context, Set(userRepo.roles.student)
      )
      val theme = themeRepo.create(
        mockInstructor, updatedAssignment, "Test theme", List(), updatedAssignment.themes.head, mockInstructor, 0
      ).right.get

      val data = Map(
        "quote" -> Seq(quote),
        "start" -> Seq(start),
        "end" -> Seq(end),
        "startOffset" -> Seq(startOffset.toString),
        "endOffset" -> Seq(endOffset.toString)
      )

      val createAnnotationRequest = FakeRequest(POST, s"themes/${theme.id}/annotations", FakeHeaders(), data)
      val createAnnotationResponse = controller.create(theme.id)(createAnnotationRequest)
      status(createAnnotationResponse) mustEqual FORBIDDEN

      val getThemeRequest = FakeRequest(GET, s"/themes/${theme.id}/annotations").withSession("userId" -> mockStudent.id.toString)
      val getThemeResponse = controller.getForTheme(theme.id)(getThemeRequest)
      status(getThemeResponse) mustEqual OK
      val getThemeJson = contentAsString(getThemeResponse)

      getThemeJson must
        /("id" -> theme.id.toDouble)
        /("annotations").andHave(size(0))
    }

    "fail to create an annotation for a non-existent theme with 'Not Found'" in new AnnotationControllerTest with TestCase1 with AnnotationCreationTestCase {
      lmsContextRepo.setRolesInContextForUser(
        mockInstructor, context, Set(userRepo.roles.instructor)
      )
      lmsContextRepo.setRolesInContextForUser(
        mockStudent, context, Set(userRepo.roles.student)
      )
      val theme = themeRepo.create(
        mockStudent, updatedAssignment, "Test theme", List(), updatedAssignment.themes.head, mockStudent, 0
      ).right.get
      val data = Map(
        "quote" -> Seq(quote),
        "start" -> Seq(start),
        "end" -> Seq(end),
        "startOffset" -> Seq(startOffset.toString),
        "endOffset" -> Seq(endOffset.toString)
      )

      val nonExistentId = theme.id + 100000
      val createAnnotationRequest = FakeRequest(POST, s"themes/${nonExistentId}/annotations", FakeHeaders(), data)
        .withSession("userId" -> mockStudent.id.toString)
      val createAnnotationResponse = controller.create(nonExistentId)(createAnnotationRequest)
      status(createAnnotationResponse) mustEqual NOT_FOUND
    }

    "fail to create an annotation for a user who is not part of the context with 'Forbidden'" in new AnnotationControllerTest with TestCase1 with AnnotationCreationTestCase {
      lmsContextRepo.setRolesInContextForUser(
        mockInstructor, context, Set(userRepo.roles.instructor)
      )
      val theme = themeRepo.create(
        mockInstructor, updatedAssignment, "Test theme", List(), updatedAssignment.themes.head, mockInstructor, 0
      ).right.get
      val data = Map(
        "quote" -> Seq(quote),
        "start" -> Seq(start),
        "end" -> Seq(end),
        "startOffset" -> Seq(startOffset.toString),
        "endOffset" -> Seq(endOffset.toString)
      )
      val createAnnotationRequest = FakeRequest(POST, s"themes/${theme.id}/annotations", FakeHeaders(), data)
      val createAnnotationResponse = controller.create(theme.id)(createAnnotationRequest)
      status(createAnnotationResponse) mustEqual FORBIDDEN

      val getThemeRequest = FakeRequest(GET, s"/themes/${theme.id}/annotations").withSession("userId" -> mockInstructor.id.toString)
      val getThemeResponse = controller.getForTheme(theme.id)(getThemeRequest)
      status(getThemeResponse) mustEqual OK
      val getThemeJson = contentAsString(getThemeResponse)

      getThemeJson must
        /("id" -> theme.id.toDouble)
        /("description" -> theme.description)
        /("annotations").andHave(size(0))
    }

  "delete an annotation from a theme" in new AnnotationControllerTest with AnnotationDeletionTestCase {
    val deleteAnnotationRequest = FakeRequest(DELETE, s"/annotations/${annotation.id}").withSession("userId" -> mockStudent.id.toString)
    val deleteAnnotationResponse = controller.delete(annotation.id)(deleteAnnotationRequest)

    status(deleteAnnotationResponse) mustEqual OK

    val getAnnotationRequest = FakeRequest(GET, s"/annotations/${annotation.id}").withSession("userId" -> mockStudent.id.toString)
    val getAnnotationResponse = controller.getById(annotation.id)(deleteAnnotationRequest)
    status(getAnnotationResponse) mustEqual NOT_FOUND

    val getThemeAnnotationsRequest = FakeRequest(GET, s"/themes/${theme.id}/annotations").withSession("userId" -> mockStudent.id.toString)
    val getThemeAnnotationsResponse = controller.getForTheme(theme.id)(getThemeAnnotationsRequest)
    val getThemeAnnotationJson = contentAsString(getThemeAnnotationsResponse)

    getThemeAnnotationJson must
      /("annotations").andHave(size(0))
  }

   "fail to delete a non-existent annotation with 'Not Found'" in new AnnotationControllerTest with AnnotationDeletionTestCase {
     val wrongId = annotation.id + 10000
     val deleteAnnotationRequest = FakeRequest(DELETE, s"/annotations/${wrongId}").withSession("userId" -> mockStudent.id.toString)
     val deleteAnnotationResponse = controller.delete(wrongId)(deleteAnnotationRequest)
     status(deleteAnnotationResponse) mustEqual NOT_FOUND
   }

   "fail to delete an annotation when a user is not logged in with 'Forbidden'" in new AnnotationControllerTest with AnnotationDeletionTestCase {
     val deleteAnnotationRequest = FakeRequest(DELETE, s"/annotations/${annotation.id}")
     val deleteAnnotationResponse = controller.delete(annotation.id)(deleteAnnotationRequest)

     status(deleteAnnotationResponse) mustEqual FORBIDDEN

     val getAnnotationRequest = FakeRequest(GET, s"/annotations/${annotation.id}").withSession("userId" -> mockStudent.id.toString)
     val getAnnotationResponse = controller.getById(annotation.id)(getAnnotationRequest)
     status(getAnnotationResponse) mustEqual OK

     val getThemeAnnotationsRequest = FakeRequest(GET, s"/themes/${theme.id}/annotations").withSession("userId" -> mockStudent.id.toString)
     val getThemeAnnotationsResponse = controller.getForTheme(theme.id)(getThemeAnnotationsRequest)
     val getThemeAnnotationJson = contentAsString(getThemeAnnotationsResponse)

     getThemeAnnotationJson must
       /("annotations").andHave(size(1))
   }

   "fail to delete an annotation for a user who is not part of the context with a 'Forbidden'" in new AnnotationControllerTest with AnnotationDeletionTestCase {
     val userNotPartOfContext = userRepo.create(
       "babylon5-123", "Fake user", "user@babylon5.spc"
     ).get
     val deleteAnnotationRequest = FakeRequest(DELETE, s"/annotations/${annotation.id}").withSession("userId" -> userNotPartOfContext.id.toString)
     val deleteAnnotationResponse = controller.delete(annotation.id)(deleteAnnotationRequest)

     status(deleteAnnotationResponse) mustEqual FORBIDDEN

     val getAnnotationRequest = FakeRequest(GET, s"/annotations/${annotation.id}").withSession("userId" -> mockStudent.id.toString)
     val getAnnotationResponse = controller.getById(annotation.id)(getAnnotationRequest)
     status(getAnnotationResponse) mustEqual OK

     val getThemeAnnotationsRequest = FakeRequest(GET, s"/themes/${theme.id}/annotations").withSession("userId" -> mockStudent.id.toString)
     val getThemeAnnotationsResponse = controller.getForTheme(theme.id)(getThemeAnnotationsRequest)
     val getThemeAnnotationJson = contentAsString(getThemeAnnotationsResponse)

     getThemeAnnotationJson must
       /("annotations").andHave(size(1))
   }
  }

}

class AnnotationControllerTest extends WithApplication
  with HasDatabase with HasToolConsumerRepo with HasUserRepo with HasAssignmentRepo
  with HasLMSContextRepo with HasBasicContext
  with HasThemeRepo with HasAnnotationRepo {

  val tr = themeRepo
  val ur = userRepo
  val ar = annotationRepo
  val controller = new AnnotationController with Controller {
    override val themeRepo = tr
    override val messagesApi: MessagesApi = app.injector.instanceOf[MessagesApi]
    override val userRepo = ur
    override val annotationRepo = ar
  }

}

trait AnnotationCreationTestCase {

  val updatedAssignmentResult:Either[ELError,Assignment]
  val lmsContextRepo:LMSContextRepo
  val themeRepo:ThemeRepository
  val mockStudent:User

  val quote = "The test quote"
  val start = "/p[0]"
  val end = "/p[1]"
  val startOffset = 10
  val endOffset = 11
  val updatedAssignment = updatedAssignmentResult.right.get

}

trait AnnotationDeletionTestCase extends TestCase1 with AnnotationCreationTestCase {
  val mockStudent:User
  val context:LMSContext
  val userRepo:UserRepository
  val annotationRepo:AnnotationRepository

  val updatedAssignment:Assignment

  lmsContextRepo.setRolesInContextForUser(
    mockStudent, context, Set(userRepo.roles.student)
  )
  val theme = themeRepo.create(
    mockStudent, updatedAssignment, "Test theme", List(), updatedAssignment.themes.head, mockStudent, 0
  ).right.get

  val annotationInitial = Annotation.from(
    quote, start, end, startOffset, endOffset, "123", mockStudent
  )
  val annotation = annotationRepo.create(mockStudent, theme, annotationInitial).right.get

}
