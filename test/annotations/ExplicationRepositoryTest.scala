package annotations

import assignments.{AssignmentRepository, HasAssignmentRepo, HasThemeRepo, ThemeRepository}
import authentication.{HasToolConsumerRepo, HasUserRepo}
import common.HasDatabase
import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import lti.{HasBasicContext, HasLMSContextRepo, LMSContextRepo}
import play.api.test._


trait HasExplicationRepo {
  val dbService:DatabaseService
  val userRepo:UserRepository
  val lmsContextRepo:LMSContextRepo
  val assignmentRepo:AssignmentRepository
  val themeRepo:ThemeRepository
  val annotationRepo:AnnotationRepository

  val explicationRepo = new ExplicationRepository(
    dbService, userRepo, lmsContextRepo, annotationRepo, assignmentRepo
  )

}

trait ExplicationRepositoryTest extends WithApplication
  with HasDatabase with HasToolConsumerRepo with HasUserRepo with HasAssignmentRepo
  with HasLMSContextRepo with HasBasicContext
  with HasThemeRepo with HasAnnotationRepo with HasExplicationRepo {

  lmsContextRepo.setRolesInContextForUser(mockInstructor, context, Set(userRepo.roles.instructor))
}
