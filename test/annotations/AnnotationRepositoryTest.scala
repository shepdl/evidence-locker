package annotations

import assignments.{HasAssignmentRepo, HasThemeRepo, TestCase1}
import authentication.{HasToolConsumerRepo, HasUserRepo}
import common.HasDatabase
import lti.{HasBasicContext, HasLMSContextRepo}
import play.api.test.WithApplication

/**
  * Created by dave on 8/22/16.
  */
trait AnnotationRepositoryTest extends WithApplication with HasDatabase with HasToolConsumerRepo with HasUserRepo
  with HasAssignmentRepo with HasLMSContextRepo with HasBasicContext with HasThemeRepo with HasAnnotationRepo
  with TestCase1 {

  lmsContextRepo.setRolesInContextForUser(mockInstructor, context, Set(userRepo.roles.instructor))
}
