package annotations

import assignments._
import org.junit.runner._
import org.specs2.mutable._
import org.specs2.runner._


@RunWith(classOf[JUnitRunner])
class AnnotationRepositorySpec extends Specification {

  "An annotation repository" should {

    "create a new annotation for a theme that the user has created without a page number" in new AnnotationRepositoryTest with TestCase1 {
      val quote = "The test quote"
      val start = "/p[0]"
      val end = "/p[1]"
      val startOffset = 10
      val endOffset = 11
      val pageNumber = "none"
      val updatedAssignment = updatedAssignmentResult.right.get
      lmsContextRepo.setRolesInContextForUser(
        mockStudent, context, Set(userRepo.roles.student)
      )
      val theme = themeRepo.create(
        mockStudent, updatedAssignment, "Test theme", List(), updatedAssignment.themes.head, mockStudent, 0
      ).right.get

      val annotation = Annotation.from(quote, start, end, startOffset, endOffset, pageNumber, mockStudent)

      annotationRepo.create(mockStudent, theme, annotation) match {
        case Right(annotation) => {
          annotation.quote mustEqual quote
          annotation.start mustEqual start
          annotation.end mustEqual end
          annotation.startOffset mustEqual startOffset
          annotation.endOffset mustEqual endOffset
          annotation.pageNo mustEqual pageNumber
          annotationRepo.themeForAnnotation(mockStudent, annotation) match {
            case Right(themeFound) => {
              themeFound.id mustEqual theme.id
              themeFound.description mustEqual theme.description
            }
            case _ => failure
          }
        }
        case x => {
          println(x)
          failure
        }
      }
    }

    "create a new annotation for a theme that the user has created with a page number" in new AnnotationRepositoryTest with TestCase1 {
      val quote = "The test quote"
      val start = "/p[0]"
      val end = "/p[1]"
      val startOffset = 10
      val endOffset = 11
      val pageNumber = "1"
      val updatedAssignment = updatedAssignmentResult.right.get
      lmsContextRepo.setRolesInContextForUser(
        mockStudent, context, Set(userRepo.roles.student)
      )
      val theme = themeRepo.create(
        mockStudent, updatedAssignment, "Test theme", List(), updatedAssignment.themes.head, mockStudent, 0
      ).right.get

      val annotation = Annotation.from(quote, start, end, startOffset, endOffset, pageNumber, mockStudent)

      annotationRepo.create(mockStudent, theme, annotation) match {
        case Right(annotation) => {
          annotation.quote mustEqual quote
          annotation.start mustEqual start
          annotation.end mustEqual end
          annotation.startOffset mustEqual startOffset
          annotation.endOffset mustEqual endOffset
          annotation.pageNo mustEqual pageNumber
          annotationRepo.themeForAnnotation(mockStudent, annotation) match {
            case Right(themeFound) => {
              themeFound.id mustEqual theme.id
              themeFound.description mustEqual theme.description
            }
            case _ => failure
          }
        }
        case x => {
          println(x)
          failure
        }
      }
    }

    "fail to create an annotation for a non-existent theme" in new AnnotationRepositoryTest with TestCase1 {
      val quote = "The test quote"
      val start = "/p[0]"
      val end = "/p[1]"
      val startOffset = 10
      val endOffset = 11
      val pageNumber = "none"
      val theme = Theme(1000, "Nonexistent theme", null, List(), mockStudent, 1)

      val annotation = Annotation.from(quote, start, end, startOffset, endOffset, pageNumber, mockStudent)

      annotationRepo.create(mockStudent, theme, annotation) match {
        case Left(ex:ThemeDoesNotExistException) => success
        case _ => failure
      }
    }

    "fail to create an annotation when the user is not a part of the context" in new AnnotationRepositoryTest with TestCase1 with TestCase2 {
      val quote = "The test quote"
      val start = "/p[0]"
      val end = "/p[1]"
      val startOffset = 10
      val endOffset = 11
      val pageNumber = "none"
      val theme = Theme(1000, "Nonexistent theme", null, List(), mockStudent, 1)

      val annotation = Annotation.from(quote, start, end, startOffset, endOffset, pageNumber, mockStudent)

      annotationRepo.create(alternateUser, theme, annotation) match {
        case Left(ex:UserCannotUpdateTheme) => success
        case _ => failure
      }
    }

//    "get an annotation in the correct format"
//    "fail to get an annotation when the user is not part of the context"
//
//    "get all annotations for an assignment"
//    "fail to get annotations when the user is not a member of the context"
//
//    "delete an annotation"
//    "fail to delete an annotation when the user is not part of the context"

  }

}




