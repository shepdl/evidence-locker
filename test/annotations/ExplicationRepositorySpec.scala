package annotations

import assignments.TestCase1
import edu.ucla.cdh.EvidenceLocker.authentication.{User, UserRepository}
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.test._
import play.api.test.Helpers._


@RunWith(classOf[JUnitRunner])
class ExplicationRepositorySpec extends Specification {

  "An Explication Repository" should {

    "create an explication for an annotation that exists" in new ExplicationRepositoryTest with ExplicationTestCase {
      val explicationCreationResult = explicationRepo.save(mockStudent, annotation, explication)
      explicationCreationResult must beRight[Explication]
      val explicationCreation = explicationCreationResult.right.get

      val explicationRetrievedRes = explicationRepo.getById(mockStudent, explicationCreation.id)
      explicationRetrievedRes must beRight[Explication]
      val explicationRetrieved = explicationRetrievedRes.right.get
      explicationRetrieved.annotation mustEqual annotation
      explicationRetrieved.text mustEqual explicationText

      val explicationFromAnnotationRes = explicationRepo.getByAnnotationId(mockStudent, annotation.id)
      explicationFromAnnotationRes must beRight[Explication]
      val explicationRetrievedFromAnnotation = explicationFromAnnotationRes.right.get
      explicationRetrievedFromAnnotation.annotation mustEqual annotation
      explicationRetrievedFromAnnotation.text mustEqual explicationText
    }

    "fail to create an explication for an annotation that does not exist with an AnnotationDoesNotExist error" in new ExplicationRepositoryTest with ExplicationTestCase {
      override val annotation = Annotation.from(
        quote, start, end, startOffset, endOffset, "1", mockStudent
      )

      explicationRepo.save(mockStudent, annotation, explication) match {
        case Left(ex:AnnotationDoesNotExist) => success
        case _ => failure
      }
    }

    "fail to create an explication for an annotation that the user does not own with a UserHasNoAccessToAnnotation error" in new ExplicationRepositoryTest with ExplicationTestCase {
      val otherUser = userRepo.create("babylon5-123", "Other User", "otheruser@babylon5.spc").get
      explicationRepo.save(otherUser, annotation, explication) match {
        case Left(ex:UserHasNoAccessToAnnotation) => success
        case _ => failure
      }
    }

    "update an explication for an annotation that exists" in new ExplicationRepositoryTest with ExplicationUpdateTestCase {
      val newExplication = Explication.from(updatedText, annotation, mockStudent)
      val explicationUpdateResult = explicationRepo.save(mockStudent, annotation, newExplication)
      explicationUpdateResult must beRight[Explication]
      val explicationUpdate = explicationUpdateResult.right.get

      val explicationRetrievedRes = explicationRepo.getById(mockStudent, explicationUpdate.id)
      explicationRetrievedRes must beRight[Explication]
      val explicationRetrieved = explicationRetrievedRes.right.get
      explicationRetrieved.annotation mustEqual annotation
      explicationRetrieved.text mustEqual updatedText
    }

    "fail to create an explication for an annotation that does not exist with an AnnotationDoesNotExist error" in new ExplicationRepositoryTest with ExplicationUpdateTestCase {
      val unsavedAnnotation = Annotation.from(quote, start, end, startOffset, endOffset, "2", mockStudent)
      val newExplication = Explication.from(updatedText, annotation, mockStudent)

      explicationRepo.save(mockStudent, unsavedAnnotation, newExplication) match {
        case Left(ex:AnnotationDoesNotExist) => success
        case ex => failure
      }
    }

    "produce the correct assignment for an explication" in new ExplicationRepositoryTest with ExplicationTestCase {
      failure
    }

    "fail to create an explication for an annotation that the user does not own with a UserHasNoAccessToAnnotation error" in new ExplicationRepositoryTest with ExplicationUpdateTestCase {
      val otherUser = userRepo.create("babylon5-123", "Other User", "otheruser@babylon5.spc").get
      val newExplication = Explication.from(updatedText, annotation, otherUser)

      explicationRepo.save(otherUser, annotation, newExplication) match {
        case Left(ex: UserHasNoAccessToAnnotation) => success
        case _ => failure
      }
    }

  }

}


trait ExplicationTestCase extends TestCase1 with AnnotationCreationTestCase {
  val annotationRepo:AnnotationRepository
  val mockStudent:User
  val userRepo:UserRepository
  val assignment = updatedAssignmentResult.right.get

  lmsContextRepo.setRolesInContextForUser(mockStudent, context, Set(userRepo.roles.student))

  val annotationData = Annotation.from(
    quote, start, end, startOffset, endOffset, "123", mockStudent
  )
  val heroicQualities = themeRepo.create(
    mockStudent, assignment, "Heroic Qualities", List(), assignment.themes.head, mockStudent, 0
  ).right.get

  val annotation = annotationRepo.create(mockStudent, heroicQualities, annotationData).right.get

  val explicationText = """
    |Class aptent taciti sociosqu ad litora torquent per conubia nostra,
    |per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim
    |lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam. In
    |scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem.
    |Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel,
    |suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet.
    |Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in,
    |nibh.
    |""".stripMargin

  val explication = Explication.from(explicationText, annotation, mockStudent)

}

trait ExplicationUpdateTestCase extends ExplicationTestCase {
  val explicationRepo:ExplicationRepository
  val originalExplicationData = Explication.from(
    explicationText, annotation, mockStudent
  )
  val originalExplication = explicationRepo.save(mockStudent, annotation, explication).right.get

  val updatedText =
    """
      |This text has been updated
    """.stripMargin
}
