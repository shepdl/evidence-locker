import edu.ucla.cdh.EvidenceLocker.authentication.User
import org.openqa.selenium.{Cookie, WebDriver}
import org.openqa.selenium.support.ui.{ExpectedCondition, WebDriverWait}
import play.api.libs.Crypto
import play.api.test.TestBrowser

/**
  * Created by dave on 10/12/16.
  */
package object frontend {

  implicit class BrowserExtensions(browser:TestBrowser) {

    def become(user:User) = {
      val signedCookie = Crypto.sign(s"userId=${user.id}")
      val cookie = new Cookie("PLAY_SESSION", s"$signedCookie-userId=${user.id}")
      browser.manage.addCookie(cookie)
    }

    def waitOnOrTimeout(condition:(WebDriver => Boolean), timeout:Int = 5) =
      new WebDriverWait(browser.webDriver, timeout).until(new ExpectedCondition[Boolean] {
        def apply(d: WebDriver) = condition(d)
      })

  }

}
