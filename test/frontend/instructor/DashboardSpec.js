define([
    'angular',
    'angular-mocks',
    'instructor_dashboard/DashboardController',
    'views/eltpl'
], function (ng, ngMock, DashboardController, templates) {
    'use strict';

    var $controller, $rootScope, $compile;
    var instructorModule, assignmentServiceMock, deferred;
    var instructorDashboardHtml;
    var deferred;

    beforeEach(function () {
        ng.mock.module('el.tpl');
        instructorModule = ng.module('dashboard', ['el.tpl']);
        ng.mock.module('dashboard')
    });

    beforeEach(inject(function(_$controller_, _$rootScope_, _$q_, _$compile_, _$templateCache_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        var $q = _$q_;
        deferred = $q.defer();
        assignmentServiceMock = jasmine.createSpyObj('evidenceLocker.api.AssignmentService', ['getAssignment']);
        assignmentServiceMock.getAssignment.and.returnValue(deferred.promise);
        $compile = _$compile_;
    }));

    describe('An Instructor Dashboard Controller', function () {

        it('should load an assignment correctly', function () {
            deferred.resolve({
                data: {
                    ref: '/assignments/1',
                    title: 'Close-reading Romeo and Juliet',
                    description: 'Analyze Romeo and Juliet',
                    filename: '/assignments/1/file',
                    openDate: '2011-01-01',
                    closeDate: '2011-02-01',
                    peerReviewEnabled: true,
                    numberRequired: 4,
                    rubricType: 'numeric',
                    reviewRubric: [
                        {id: 1, description: 'Originality'},
                        {id: 2, description: 'Thoroguhness'}
                    ],
                    themes: [
                        {id : 1, description: 'Heroism'},
                        {id : 2, description: 'Women\'s roles'}
                    ],
                    totalStudents: 8,
                    status: {
                        explications: [
                            {
                                student: { id: 1, name: "Molly O'Brien" },
                                explicationCount: 4
                            },
                            {
                                student: { id: 2, name: "Jake Sisko" },
                                explicationCount: 4
                            },
                            {
                                student: { id: 3, name: "Leonard Ackbar" },
                                explicationCount: 3
                            }
                        ],
                        peerReviews: [
                            {
                                student: { id: 1, name: "Molly O'Brien" },
                                peerReviewsCompleted: 4
                            }
                        ]
                    }
                }
            });
            var $scope = $rootScope.$new(),
                controller = $controller(DashboardController, {
                    $scope: $scope,
                    $http: null,
                    $location: null,
                    'evidenceLocker.api.AssignmentService': assignmentServiceMock
                }
            );

            $scope.$apply();

            expect($scope.assignment).toEqual({
                id: '1',
                title: 'Close-reading Romeo and Juliet',
                description: 'Analyze Romeo and Juliet',
                filename: '/assignments/1/file',
                openDate: '2011-01-01',
                closeDate: '2011-02-01',
                peerReviewEnabled: true,
                numberRequired: 4,
                rubricType: 'numeric',
                criteria: [
                    {id: 1, description: 'Originality'},
                    {id: 2, description: 'Thoroguhness'}
                ],
                themes: [
                    {id : 1, description: 'Heroism'},
                    {id : 2, description: 'Women\'s roles'}
                ],
                status: {
                    explications: [
                        {
                            student: { id: 1, name: "Molly O'Brien" },
                            explicationCount: 4
                        },
                        {
                            student: { id: 2, name: "Jake Sisko" },
                            explicationCount: 4
                        },
                        {
                            student: { id: 3, name: "Leonard Ackbar" },
                            explicationCount: 3
                        }
                    ],
                    peerReviews: [
                        {
                            student: { id: 1, name: "Molly O'Brien" },
                            peerReviewsCompleted: 4
                        }
                    ]
                },
                totalStudents: 8,
                explicationsCompleted: 2,
                peerReviewsCompleted: 1
            });
        });

    });

});