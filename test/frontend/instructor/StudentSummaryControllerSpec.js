define(['angular', 'angular-mocks', 'instructor_dashboard/StudentSummaryController'], function (ng, _, StudentSummaryController) {
    'use strict';

    var $controller, $rootScope, $compile,
        assignmentServiceMock, 
        deferreds
    ;

    beforeEach(inject(function (_$controller_, _$rootScope_, _$q_, _$compile_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        var $q = _$q_;
        deferreds = {
            getAssignment: $q.defer(),
            getExplicationsByStudent: $q.defer(),
            getReviewsByStudent: $q.defer(),
            getReviewsByAnnotationId: [
                $q.deferred(), $q.deferred()
            ]
        };
        assignmentServiceMock = jasmine.createSpyObj('evidenceLocker.api.AssignmentService', [
            'getAssignment', 'getExplicationsByStudent', 'getReviewsByStudent', 'getReviewsByAnnotationId'
        ]);
        assignmentServiceMock.getAssignment.and.returnValue(deferreds.getAssignment);
        // assignmentServiceMock.getExplicationsByStudent.and.returnValue(deferreds.getExplicationsByStudent);
        assignmentServiceMock.getExplicationsByStudent.and.returnValue(deferreds.getExplicationsByStudent);
        assignmentServiceMock.getReviewsByStudent.and.returnValue(deferreds.getReviewsByStudent);
        assignmentServiceMock.getReviewsByAnnotationId.and.callFake(function (arguments) {
            return deferreds.getReviewsByAnnotationId[arguments[0] - 1];
        });
        $compile = _$compile_;
    }));

    var assignment = {
        data: {
            ref: '/assignments/1',
            title: 'Close-reading Romeo and Juliet',
            description: 'Analyze Romeo and Juliet',
            filename: '/assignments/1/file',
            openDate: '2011-01-01',
            closeDate: '2011-02-01',
            peerReviewEnabled: true,
            numberRequired: 4,
            rubricType: 'numeric',
            reviewRubric: [
                {id: 1, description: 'Originality'},
                {id: 2, description: 'Thoroguhness'}
            ],
            themes: [
                {id : 1, description: 'Heroism'},
                {id : 2, description: 'Women\'s roles'}
            ],
            totalStudents: 8,
            status: {
                explications: [
                    {
                        student: { id: 1, name: "Molly O'Brien" },
                        explicationCount: 4
                    },
                    {
                        student: { id: 2, name: "Jake Sisko" },
                        explicationCount: 4
                    },
                    {
                        student: { id: 3, name: "Leonard Ackbar" },
                        explicationCount: 3
                    }
                ],
                peerReviews: [
                    {
                        student: { id: 1, name: "Molly O'Brien" },
                        peerReviewsCompleted: 4
                    }
                ]
            }
        }
    };

    var explication1 = {
        id: 1,
        text: "This is the text of the explication",
        ownerRef: 1,
        annotationRef: "/annotations/1"
    };

    var explication2 = {
        id: 2,
        text: "This is the text of the explication",
        ownerRef: 2,
        annotationRef: "/annotations/2"
    };

    var review1 = {
        id: 1,
        explication: explication1,
        response: [
            {
                rubricId: 1,
                rubricText: "Insight",
                score: 5,
                rubricMin: 0,
                rubricMax: 5
            },
            {
                rubricId: 2,
                rubricText: "Originality",
                score: 2,
                rubricMin: 0,
                rubricMax: 5
            }
        ]
    };
    var review2 = {
        id: 2,
        explication: explication2,
        response: [
            {
                rubricId: 1,
                rubricText: "Insight",
                score: 3,
                rubricMin: 0,
                rubricMax: 5
            },
            {
                rubricId: 2,
                rubricText: "Originality",
                score: 4,
                rubricMin: 0,
                rubricMax: 5
            }
        ]
    };
    var review3 = {
        id: 3,
        explication: explication2,
        response: [
            {
                rubricId: 1,
                rubricText: "Insight",
                score: 2,
                rubricMin: 0,
                rubricMax: 5
            },
            {
                rubricId: 2,
                rubricText: "Originality",
                score: 1,
                rubricMin: 0,
                rubricMax: 5
            }
        ]
    };

    describe('A Student Summary Controller', function () {

        it('should parse incoming data correctly', function () {
            deferreds.getAssignment.resolve(assignment);
            deferreds.getExplicationsByStudent.resolve([explication1, explication2]);
            deferreds.getReviewsByAnnotationId[0].resolve([review1]);
            deferreds.getReviewsByAnnotationId[1].resolve([review2, review3]);
            deferreds.getReviewsByStudent.resolve({
                student: {
                    id: 1,
                    name: "Wesley Crusher"
                },
                peerReviews: [
                ]
            });
            
        });

    });
});
