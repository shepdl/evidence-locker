package frontend.instructor

import annotations.{HasAnnotationRepo, HasExplicationRepo}
import assignments.{HasAssignmentRepo, HasThemeRepo}
import authentication.{HasToolConsumerRepo, HasUserRepo}
import common.HasDatabase
import instructor.InstructorControllerTestCase
import lti.{HasBasicContext, HasLMSContextRepo}
import org.junit.runner._
import org.openqa.selenium.WebDriver
import org.specs2.runner._
import play.api.test._
import reviews.HasPeerReviewRepo


@RunWith(classOf[JUnitRunner])
class PeerReviewSummarySpec extends PlaySpecification {

  "A Peer Review Summary Page " should {

    import frontend.BrowserExtensions

    "display all explications with their peer reviews, and peer reviews written by a student" in new PeerReviewSummaryTest
      with InstructorControllerTestCase {
//      val secret = app.configuration.getString("play.crypto.secret")
      val test = new WithBrowser(webDriver = WebDriverFactory(FIREFOX), app = app) {
        val testStudent = studentsWhoHaveCompletedTheAssignment.head
        val explicationsWrittenByStudent = explicationRepo.getByUserAndAssignment(instructor, assignment, testStudent).right.get
        val peerReviewsWrittenByStudent = peerReviewRepo.getReviewsCompletedByUserForAssignment(testStudent, assignment).right.get
        browser.goTo("/")
        browser.become(instructor)

        browser.goTo(s"/assignments/${assignment.id}/instructor#student/${testStudent.id}")
        browser.waitOnOrTimeout(
          (d:WebDriver) => browser.$(".explication-list").findFirst("h1").getText.contains(testStudent.providedName),
          300
        )
        browser.$(".explication-list").findFirst("h1").getText must contain(testStudent.providedName)
        browser.$(".completion-count").getTexts.get(0) must equalTo(assignment.numberRequired.toString)
        val explicationElements = browser.$(".explication")
        explicationElements.size() mustEqual explicationsWrittenByStudent.size
        (0 until assignment.numberRequired).foreach(index => {
          explicationElements.findFirst(".explication-text").findFirst("p").getText mustEqual explicationsWrittenByStudent(index).text
          // TODO: implement showing the annotation
//          explicationElements.findFirst(".annotation-quote").getText mustEqual explicationsWrittenByStudent(index).annotation.quote
          val reviewsOfThisExplication = peerReviewRepo.listCompletedForExplication(instructor, explicationsWrittenByStudent(index))
            .right.get
          val explicationReviews = explicationElements.get(index).find(".explication-review")
          explicationReviews must haveSize(reviewsOfThisExplication.size)
          (0 until assignment.numberRequired).foreach(reviewIndex => {
            explicationReviews.get(reviewIndex).find(".reviewer-name") mustEqual ("Review by: " + reviewsOfThisExplication(reviewIndex).reviewer.providedName)
            reviewsOfThisExplication(reviewIndex).responses.map(response => {
                explicationReviews.get(reviewIndex).getText mustEqual s"${response.prompt.description} : ${response.score / response.prompt.max}"
              }
            )
          })
        })

        browser.$(".reviews").findFirst("h1").getText must contain(testStudent.providedName)
        val peerReviewElements = browser.$(".review")
        (0 to assignment.numberRequired - 1).foreach(index => {
          val review = peerReviewsWrittenByStudent(index)
          val element = peerReviewElements.get(index)
          element.find("h2").getText must contain(review.reviewer.providedName)
          val responseElements = element.find(".review-score")
          review.responses.zipWithIndex.map({
            case (response, index) => {
              responseElements.get(index).getText mustEqual s"${response.prompt.description} : ${response.score / response.prompt.max}"
            }
          })
        })
        browser.quit()
      }
    }
  }

}

trait PeerReviewSummaryTest extends WithApplication
  with HasDatabase with HasToolConsumerRepo with HasUserRepo with HasAssignmentRepo
  with HasLMSContextRepo with HasBasicContext
  with HasThemeRepo with HasAnnotationRepo with HasExplicationRepo
  with HasPeerReviewRepo


