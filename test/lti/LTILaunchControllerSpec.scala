package lti

import assignments.Assignment
import edu.ucla.cdh.EvidenceLocker.authentication.User
import org.junit.runner._
import org.specs2.mutable._
import org.specs2.runner._
import play.api.test.Helpers._
import play.api.test._


@RunWith(classOf[JUnitRunner])
class LTILaunchControllerSpec extends Specification {

  "An LTILaunchController" should {

//    "present an error message when important fields are missing"

    "create a correct remote user ID" in new LTILaunchControllerTest {
      controller.remoteIdForToolConsumerAndId("1", "1") mustEqual "1-1"
      controller.remoteIdForToolConsumerAndId("ccle.ucla.edu", "1") mustEqual "ccle.ucla.edu-1"
      controller.remoteIdForToolConsumerAndId("12u90qw-qwer0923490234", "124390234") mustEqual "12u90qw-qwer0923490234-124390234"
    }

    "create a new context and instructor when a request arrives from an LMS that has not created activites with that context or instructor, and start a new session, and redirect to the new session" in new LTILaunchControllerTest  {
      val postData = Map(
        "lti_message_type" -> Seq("basic-lti-launch-request"),
        "lti_version" -> Seq("LTI-1p0"),
        "resource_link_id" -> Seq("1"),
        "resource_link_title" -> Seq("Close-reading Hamlet"),
        "resource_link_description" -> Seq("Close-read Hamlet"),
        "user_id" -> Seq("3"),
        "roles" -> Seq("Instructor"),
        "lis_person_name_given" -> Seq("Jean-Luc"),
        "lis_person_name_family" -> Seq("Picard"),
        "lis_person_name_full" -> Seq("Jean-Luc Picard"),
        "lis_person_contact_email_primary" -> Seq("jlp@locker.ucla.edu"),
        "context_id" -> Seq("2"),
        "context_type" -> Seq("CourseSection"),
        "context_title" -> Seq("Shakespeare in its Interstellar Context"),
        "context_label" -> Seq("SIC"),
        "launch_presentation_locale" -> Seq("en"),
        "launch_presentation_return_url" -> Seq("http://example.com/you_redirected_the_demo"),
        "tool_consumer_info_product_family_code" -> Seq("moodle"),
        "tool_consumer_instance_guid" -> Seq("127.0.0.1"),
        "tool_consumer_instance_name" -> Seq("newsite"),
        "tool_consumer_instance_description" -> Seq("New Moodle Site")
      )
      val launchRequest = FakeRequest(POST, "/lti/launch", FakeHeaders(), postData).withFlash("fake" -> "fake")

      val launchResponse = controller.launch().apply(launchRequest)
      status(launchResponse) mustEqual FOUND

      val toolConsumerGUID = postData("tool_consumer_instance_guid").head
      val actualRemoteContextId = s"${toolConsumerGUID}-${postData("context_id").head}"
      val possibleContexts = lmsContextRepo.getByRemoteId(actualRemoteContextId)
      possibleContexts must beSome[LMSContext]
      val context = possibleContexts.get
      context.remoteId mustEqual actualRemoteContextId
      context.instanceName mustEqual postData("context_title").head

      val actualRemoteUserId = s"${toolConsumerGUID}-${postData("user_id").head}"
      val possibleUsers = userRepo.getByRemoteId(actualRemoteUserId)
      possibleUsers must beSome[User]
      val user = possibleUsers.get
      user.remoteId mustEqual actualRemoteUserId
      user.providedName mustEqual postData("lis_person_name_full").head

      val actualRemoteAssignmentId = s"${toolConsumerGUID}-${postData("resource_link_id").head}"
      val possibleRemoteAssignment = assignmentRepo.getByRemoteId(actualRemoteAssignmentId)
      possibleRemoteAssignment must beSome[Assignment]
      val assignment = possibleRemoteAssignment.get
      assignment.remoteId mustEqual actualRemoteAssignmentId
      assignment.title mustEqual postData("resource_link_title").head
      assignment.description mustEqual postData("resource_link_description").head
    }

    "create a new instructor when the context is not new but the instructor is, and start a new session" in new LTILaunchControllerTest {
      val postData = Map(
        "lti_message_type" -> Seq("basic-lti-launch-request"),
        "lti_version" -> Seq("LTI-1p0"),
        "resource_link_id" -> Seq("1"),
        "resource_link_title" -> Seq("Close-reading Hamlet"),
        "resource_link_description" -> Seq("Close-read Hamlet"),
        "user_id" -> Seq("3"),
        "roles" -> Seq("Instructor"),
        "lis_person_name_given" -> Seq("Jean-Luc"),
        "lis_person_name_family" -> Seq("Picard"),
        "lis_person_name_full" -> Seq("Jean-Luc Picard"),
        "lis_person_contact_email_primary" -> Seq("jlp@locker.ucla.edu"),
        "context_id" -> Seq("2"),
        "context_type" -> Seq("CourseSection"),
        "context_title" -> Seq("Shakespeare in its Interstellar Context"),
        "context_label" -> Seq("SIC"),
        "launch_presentation_locale" -> Seq("en"),
        "launch_presentation_return_url" -> Seq("http://example.com/you_redirected_the_demo"),
        "tool_consumer_info_product_family_code" -> Seq("moodle"),
        "tool_consumer_instance_guid" -> Seq("127.0.0.1"),
        "tool_consumer_instance_name" -> Seq("newsite"),
        "tool_consumer_instance_description" -> Seq("New Moodle Site")
      )
      val launchRequest = FakeRequest(POST, "/lti/launch", FakeHeaders(), postData).withFlash("fake" -> "fake")

      val launchResponse = controller.launch().apply(launchRequest)

      val postData2 = Map(
        "lti_message_type" -> Seq("basic-lti-launch-request"),
        "lti_version" -> Seq("LTI-1p0"),
        "resource_link_id" -> Seq("1"),
        "resource_link_title" -> Seq("Close-reading Hamlet"),
        "resource_link_description" -> Seq("Close-read Hamlet"),
        "user_id" -> Seq("4"),
        "roles" -> Seq("Instructor"),
        "lis_person_name_given" -> Seq("William"),
        "lis_person_name_family" -> Seq("Riker"),
        "lis_person_name_full" -> Seq("William Riker"),
        "lis_person_contact_email_primary" -> Seq("william-riker@locker.ucla.edu"),
        "context_id" -> Seq("2"),
        "context_type" -> Seq("CourseSection"),
        "context_title" -> Seq("Shakespeare in its Interstellar Context"),
        "context_label" -> Seq("SIC"),
        "launch_presentation_locale" -> Seq("en"),
        "launch_presentation_return_url" -> Seq("http://example.com/you_redirected_the_demo"),
        "tool_consumer_info_product_family_code" -> Seq("moodle"),
        "tool_consumer_instance_guid" -> Seq("127.0.0.1"),
        "tool_consumer_instance_name" -> Seq("newsite"),
        "tool_consumer_instance_description" -> Seq("New Moodle Site")
      )
      val launchRequest2 = FakeRequest(POST, "/lti/launch", FakeHeaders(), postData2).withFlash("fake" -> "fake")

      val launchResponse2 = controller.launch().apply(launchRequest2)
      status(launchResponse2) mustEqual FOUND

      val toolConsumerGUID = postData("tool_consumer_instance_guid").head
      val actualRemoteContextId = s"${toolConsumerGUID}-${postData("context_id").head}"
      val possibleContexts = lmsContextRepo.getByRemoteId(actualRemoteContextId)
      possibleContexts must beSome[LMSContext]
      val context = possibleContexts.get
      context.remoteId mustEqual actualRemoteContextId
      context.instanceName mustEqual postData("context_title").head

      val actualRemoteUserId = s"${toolConsumerGUID}-${postData("user_id").head}"
      val possibleUsers = userRepo.getByRemoteId(actualRemoteUserId)
      possibleUsers must beSome[User]
      val user = possibleUsers.get
      user.remoteId mustEqual actualRemoteUserId
      user.providedName mustEqual postData("lis_person_name_full").head

      val actualRemoteAssignmentId = s"${toolConsumerGUID}-${postData("resource_link_id").head}"
      val possibleRemoteAssignment = assignmentRepo.getByRemoteId(actualRemoteAssignmentId)
      possibleRemoteAssignment must beSome[Assignment]
      val assignment = possibleRemoteAssignment.get
      assignment.remoteId mustEqual actualRemoteAssignmentId
      assignment.title mustEqual postData("resource_link_title").head
      assignment.description mustEqual postData("resource_link_description").head
    }

    "create a new context when the context is new but the instructor is, and start a new session" in new LTILaunchControllerTest {
      val postData = Map(
        "lti_message_type" -> Seq("basic-lti-launch-request"),
        "lti_version" -> Seq("LTI-1p0"),
        "resource_link_id" -> Seq("1"),
        "resource_link_title" -> Seq("Close-reading Hamlet"),
        "resource_link_description" -> Seq("Close-read Hamlet"),
        "user_id" -> Seq("3"),
        "roles" -> Seq("Instructor"),
        "lis_person_name_given" -> Seq("Jean-Luc"),
        "lis_person_name_family" -> Seq("Picard"),
        "lis_person_name_full" -> Seq("Jean-Luc Picard"),
        "lis_person_contact_email_primary" -> Seq("jlp@locker.ucla.edu"),
        "context_id" -> Seq("2"),
        "context_type" -> Seq("CourseSection"),
        "context_title" -> Seq("Shakespeare in its Interstellar Context"),
        "context_label" -> Seq("SIC"),
        "launch_presentation_locale" -> Seq("en"),
        "launch_presentation_return_url" -> Seq("http://example.com/you_redirected_the_demo"),
        "tool_consumer_info_product_family_code" -> Seq("moodle"),
        "tool_consumer_instance_guid" -> Seq("127.0.0.1"),
        "tool_consumer_instance_name" -> Seq("newsite"),
        "tool_consumer_instance_description" -> Seq("New Moodle Site")
      )
      val launchRequest = FakeRequest(POST, "/lti/launch", FakeHeaders(), postData).withFlash("fake" -> "fake")

      val launchResponse = controller.launch().apply(launchRequest)

      val postData2 = Map(
        "lti_message_type" -> Seq("basic-lti-launch-request"),
        "lti_version" -> Seq("LTI-1p0"),
        "resource_link_id" -> Seq("1"),
        "resource_link_title" -> Seq("Close-reading Dr. Faustus"),
        "resource_link_description" -> Seq("Close-read Dr. Faustus"),
        "user_id" -> Seq("3"),
        "roles" -> Seq("Instructor"),
        "lis_person_name_given" -> Seq("Jean-Luc"),
        "lis_person_name_family" -> Seq("Picard"),
        "lis_person_name_full" -> Seq("Jean-Luc Picard"),
        "lis_person_contact_email_primary" -> Seq("jlp@locker.ucla.edu"),
        "context_id" -> Seq("5"),
        "context_type" -> Seq("CourseSection"),
        "context_title" -> Seq("Marlowe in its Interstellar Context"),
        "context_label" -> Seq("MIC"),
        "launch_presentation_locale" -> Seq("en"),
        "launch_presentation_return_url" -> Seq("http://example.com/you_redirected_the_demo"),
        "tool_consumer_info_product_family_code" -> Seq("moodle"),
        "tool_consumer_instance_guid" -> Seq("127.0.0.1"),
        "tool_consumer_instance_name" -> Seq("newsite"),
        "tool_consumer_instance_description" -> Seq("New Moodle Site")
      )
      val launchRequest2 = FakeRequest(POST, "/lti/launch", FakeHeaders(), postData2).withFlash("fake" -> "fake")

      val launchResponse2 = controller.launch().apply(launchRequest2)
      status(launchResponse2) mustEqual FOUND

      val toolConsumerGUID = postData("tool_consumer_instance_guid").head
      val actualRemoteContextId = s"${toolConsumerGUID}-${postData("context_id").head}"
      val possibleContexts = lmsContextRepo.getByRemoteId(actualRemoteContextId)
      possibleContexts must beSome[LMSContext]
      val context = possibleContexts.get
      context.remoteId mustEqual actualRemoteContextId
      context.instanceName mustEqual postData("context_title").head

      val actualRemoteUserId = s"${toolConsumerGUID}-${postData("user_id").head}"
      val possibleUsers = userRepo.getByRemoteId(actualRemoteUserId)
      possibleUsers must beSome[User]
      val user = possibleUsers.get
      user.remoteId mustEqual actualRemoteUserId
      user.providedName mustEqual postData("lis_person_name_full").head

      val actualRemoteAssignmentId = s"${toolConsumerGUID}-${postData("resource_link_id").head}"
      val possibleRemoteAssignment = assignmentRepo.getByRemoteId(actualRemoteAssignmentId)
      possibleRemoteAssignment must beSome[Assignment]
      val assignment = possibleRemoteAssignment.get
      assignment.remoteId mustEqual actualRemoteAssignmentId
      assignment.title mustEqual postData("resource_link_title").head
      assignment.description mustEqual postData("resource_link_description").head
    }

    "not create a new instructor or context when neither is new, and start a new session" in new LTILaunchControllerTest {
      val postData = Map(
        "lti_message_type" -> Seq("basic-lti-launch-request"),
        "lti_version" -> Seq("LTI-1p0"),
        "resource_link_id" -> Seq("1"),
        "resource_link_title" -> Seq("Close-reading Hamlet"),
        "resource_link_description" -> Seq("Close-read Hamlet"),
        "user_id" -> Seq("3"),
        "roles" -> Seq("Instructor"),
        "lis_person_name_given" -> Seq("Jean-Luc"),
        "lis_person_name_family" -> Seq("Picard"),
        "lis_person_name_full" -> Seq("Jean-Luc Picard"),
        "lis_person_contact_email_primary" -> Seq("jlp@locker.ucla.edu"),
        "context_id" -> Seq("2"),
        "context_type" -> Seq("CourseSection"),
        "context_title" -> Seq("Shakespeare in its Interstellar Context"),
        "context_label" -> Seq("SIC"),
        "launch_presentation_locale" -> Seq("en"),
        "launch_presentation_return_url" -> Seq("http://example.com/you_redirected_the_demo"),
        "tool_consumer_info_product_family_code" -> Seq("moodle"),
        "tool_consumer_instance_guid" -> Seq("127.0.0.1"),
        "tool_consumer_instance_name" -> Seq("newsite"),
        "tool_consumer_instance_description" -> Seq("New Moodle Site")
      )
      val launchRequest = FakeRequest(POST, "/lti/launch", FakeHeaders(), postData).withFlash("fake" -> "fake")

      val launchResponse = controller.launch().apply(launchRequest)

      val postData2 = Map(
        "lti_message_type" -> Seq("basic-lti-launch-request"),
        "lti_version" -> Seq("LTI-1p0"),
        "resource_link_id" -> Seq("1"),
        "resource_link_title" -> Seq("Close-reading Hamlet"),
        "resource_link_description" -> Seq("Close-read Hamlet"),
        "user_id" -> Seq("3"),
        "roles" -> Seq("Instructor"),
        "lis_person_name_given" -> Seq("Jean-Luc"),
        "lis_person_name_family" -> Seq("Picard"),
        "lis_person_name_full" -> Seq("Jean-Luc Picard"),
        "lis_person_contact_email_primary" -> Seq("jlp@locker.ucla.edu"),
        "context_id" -> Seq("2"),
        "context_type" -> Seq("CourseSection"),
        "context_title" -> Seq("Shakespeare in its Interstellar Context"),
        "context_label" -> Seq("SIC"),
        "launch_presentation_locale" -> Seq("en"),
        "launch_presentation_return_url" -> Seq("http://example.com/you_redirected_the_demo"),
        "tool_consumer_info_product_family_code" -> Seq("moodle"),
        "tool_consumer_instance_guid" -> Seq("127.0.0.1"),
        "tool_consumer_instance_name" -> Seq("newsite"),
        "tool_consumer_instance_description" -> Seq("New Moodle Site")
      )
      val launchRequest2 = FakeRequest(POST, "/lti/launch", FakeHeaders(), postData2).withFlash("fake" -> "fake")

      val launchResponse2 = controller.launch().apply(launchRequest2)
      status(launchResponse2) mustEqual FOUND

      val toolConsumerGUID = postData("tool_consumer_instance_guid").head
      val actualRemoteContextId = s"${toolConsumerGUID}-${postData("context_id").head}"
      val possibleContexts = lmsContextRepo.getByRemoteId(actualRemoteContextId)
      possibleContexts must beSome[LMSContext]
      val context = possibleContexts.get
      context.remoteId mustEqual actualRemoteContextId
      context.instanceName mustEqual postData("context_title").head

      val actualRemoteUserId = s"${toolConsumerGUID}-${postData("user_id").head}"
      val possibleUsers = userRepo.getByRemoteId(actualRemoteUserId)
      possibleUsers must beSome[User]
      val user = possibleUsers.get
      user.remoteId mustEqual actualRemoteUserId
      user.providedName mustEqual postData("lis_person_name_full").head

      val actualRemoteAssignmentId = s"${toolConsumerGUID}-${postData("resource_link_id").head}"
      val possibleRemoteAssignment = assignmentRepo.getByRemoteId(actualRemoteAssignmentId)
      possibleRemoteAssignment must beSome[Assignment]
      val assignment = possibleRemoteAssignment.get
      assignment.remoteId mustEqual actualRemoteAssignmentId
      assignment.title mustEqual postData("resource_link_title").head
      assignment.description mustEqual postData("resource_link_description").head
    }

    "create a new assignment when the instructor also has the role of 'student'" in new LTILaunchControllerTest {
      failure
    }

    "launch for a student in the correct context and redirect to the URL of the student application" in new LTILaunchControllerTest {
      val postData = Map(
        "lti_message_type" -> Seq("basic-lti-launch-request"),
        "lti_version" -> Seq("LTI-1p0"),
        "resource_link_id" -> Seq("1"),
        "resource_link_title" -> Seq("Close-reading Hamlet"),
        "resource_link_description" -> Seq("Close-read Hamlet"),
        "user_id" -> Seq("3"),
        "roles" -> Seq("Instructor"),
        "lis_person_name_given" -> Seq("Jean-Luc"),
        "lis_person_name_family" -> Seq("Picard"),
        "lis_person_name_full" -> Seq("Jean-Luc Picard"),
        "lis_person_contact_email_primary" -> Seq("jlp@locker.ucla.edu"),
        "context_id" -> Seq("2"),
        "context_type" -> Seq("CourseSection"),
        "context_title" -> Seq("Shakespeare in its Interstellar Context"),
        "context_label" -> Seq("SIC"),
        "launch_presentation_locale" -> Seq("en"),
        "launch_presentation_return_url" -> Seq("http://example.com/you_redirected_the_demo"),
        "tool_consumer_info_product_family_code" -> Seq("moodle"),
        "tool_consumer_instance_guid" -> Seq("127.0.0.1"),
        "tool_consumer_instance_name" -> Seq("newsite"),
        "tool_consumer_instance_description" -> Seq("New Moodle Site")
      )
      val launchRequest = FakeRequest(POST, "/lti/launch", FakeHeaders(), postData).withFlash("fake" -> "fake")

      val launchResponse = controller.launch().apply(launchRequest)

      val postData2 = Map(
        "lti_message_type" -> Seq("basic-lti-launch-request"),
        "lti_version" -> Seq("LTI-1p0"),
        "resource_link_id" -> Seq("1"),
        "resource_link_title" -> Seq("Close-reading Hamlet"),
        "resource_link_description" -> Seq("Close-read Hamlet"),
        "user_id" -> Seq("6"),
        "roles" -> Seq("Student"),
        "lis_person_name_given" -> Seq("Molly"),
        "lis_person_name_family" -> Seq("O'Brien"),
        "lis_person_name_full" -> Seq("Molly O'Brien"),
        "lis_person_contact_email_primary" -> Seq("mob@locker.ucla.edu"),
        "context_id" -> Seq("2"),
        "context_type" -> Seq("CourseSection"),
        "context_title" -> Seq("Shakespeare in its Interstellar Context"),
        "context_label" -> Seq("SIC"),
        "launch_presentation_locale" -> Seq("en"),
        "launch_presentation_return_url" -> Seq("http://example.com/you_redirected_the_demo"),
        "tool_consumer_info_product_family_code" -> Seq("moodle"),
        "tool_consumer_instance_guid" -> Seq("127.0.0.1"),
        "tool_consumer_instance_name" -> Seq("newsite"),
        "tool_consumer_instance_description" -> Seq("New Moodle Site")
      )
      val launchRequest2 = FakeRequest(POST, "/lti/launch", FakeHeaders(), postData2).withFlash("fake" -> "fake")

      val launchResponse2 = controller.launch().apply(launchRequest2)
      status(launchResponse2) mustEqual FOUND

      val toolConsumerGUID = postData("tool_consumer_instance_guid").head
      val actualRemoteContextId = s"${toolConsumerGUID}-${postData("context_id").head}"
      val possibleContexts = lmsContextRepo.getByRemoteId(actualRemoteContextId)
      possibleContexts must beSome[LMSContext]
      val context = possibleContexts.get
      context.remoteId mustEqual actualRemoteContextId
      context.instanceName mustEqual postData("context_title").head

      val actualRemoteUserId = s"${toolConsumerGUID}-${postData("user_id").head}"
      val possibleUsers = userRepo.getByRemoteId(actualRemoteUserId)
      possibleUsers must beSome[User]
      val user = possibleUsers.get
      user.remoteId mustEqual actualRemoteUserId
      user.providedName mustEqual postData("lis_person_name_full").head

      val actualRemoteAssignmentId = s"${toolConsumerGUID}-${postData("resource_link_id").head}"
      val possibleRemoteAssignment = assignmentRepo.getByRemoteId(actualRemoteAssignmentId)
      possibleRemoteAssignment must beSome[Assignment]
      val assignment = possibleRemoteAssignment.get
      assignment.remoteId mustEqual actualRemoteAssignmentId
      assignment.title mustEqual postData("resource_link_title").head
      assignment.description mustEqual postData("resource_link_description").head
    }

    "fail to create an assignment when a student tries to create a course" in new LTILaunchControllerTest {
      val postData = Map(
        "lti_message_type" -> Seq("basic-lti-launch-request"),
        "lti_version" -> Seq("LTI-1p0"),
        "resource_link_id" -> Seq("1"),
        "resource_link_title" -> Seq("Close-reading Hamlet"),
        "resource_link_description" -> Seq("Close-read Hamlet"),
        "user_id" -> Seq("6"),
        "roles" -> Seq("Student"),
        "lis_person_name_given" -> Seq("Molly"),
        "lis_person_name_family" -> Seq("O'Brien"),
        "lis_person_name_full" -> Seq("Molly O'Brien"),
        "lis_person_contact_email_primary" -> Seq("mob@locker.ucla.edu"),
        "context_id" -> Seq("2"),
        "context_type" -> Seq("CourseSection"),
        "context_title" -> Seq("Shakespeare in its Interstellar Context"),
        "context_label" -> Seq("SIC"),
        "launch_presentation_locale" -> Seq("en"),
        "launch_presentation_return_url" -> Seq("http://example.com/you_redirected_the_demo"),
        "tool_consumer_info_product_family_code" -> Seq("moodle"),
        "tool_consumer_instance_guid" -> Seq("127.0.0.1"),
        "tool_consumer_instance_name" -> Seq("newsite"),
        "tool_consumer_instance_description" -> Seq("New Moodle Site")
      )
      val launchRequest = FakeRequest(POST, "/lti/launch", FakeHeaders(), postData).withFlash("fake" -> "fake")

      val launchResponse2 = controller.launch().apply(launchRequest)
      status(launchResponse2) mustEqual BAD_REQUEST

      val toolConsumerGUID = postData("tool_consumer_instance_guid").head
      val actualRemoteContextId = s"${toolConsumerGUID}-${postData("context_id").head}"
      val possibleContexts = lmsContextRepo.getByRemoteId(actualRemoteContextId)
      possibleContexts must beNone

//      val actualRemoteUserId = s"${toolConsumerGUID}-${postData("user_id").head}"
//      val possibleUsers = userRepo.getByRemoteId(actualRemoteUserId)
//      possibleUsers must beNone

      val actualRemoteAssignmentId = s"${toolConsumerGUID}-${postData("resource_link_id").head}"
      val possibleRemoteAssignment = assignmentRepo.getByRemoteId(actualRemoteAssignmentId)
      possibleRemoteAssignment must beNone
    }

  }

}


