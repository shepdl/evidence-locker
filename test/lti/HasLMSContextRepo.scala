package lti

import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService

/**
  * Created by dave on 8/3/16.
  */
trait HasLMSContextRepo {

  val dbService:DatabaseService
  val userRepo:UserRepository

  val lmsContextRepo = new LMSContextRepo(dbService, userRepo)

}
