package lti

import java.util.Date

import assignments.HasAssignmentRepo
import authentication.{HasToolConsumerRepo, HasUserRepo}
import common.{ELError, HasDatabase}
import edu.ucla.cdh.EvidenceLocker.authentication.User
import org.junit.runner._
import org.specs2.mutable._
import org.specs2.runner._
import play.api.test._


@RunWith(classOf[JUnitRunner])
class LMSContextRepoSpec extends Specification {

  "An LMSContextRepo" should {

    "create a new context when requested" in new LMSContextRepoTest {
      val remoteId = "sf-example-123"
      val description = "Cosmic Archaeology"
      val contextCreateRes = lmsContextRepo.create(
        remoteId,
        description,
        mockInstructor,
        toolConsumer
      )

      contextCreateRes must beRight[LMSContext]
      val contextCreate = contextCreateRes.right.get
      val contextGetRes = lmsContextRepo.getById(contextCreate.id)
      contextGetRes must beSome[LMSContext]
      val context = contextGetRes.get

      context.remoteId mustEqual remoteId
      context.instanceName mustEqual description
      context.instructors must contain(mockInstructor)
    }

    "fail to create a new context when the instructor does not exist" in new LMSContextRepoTest {
      val remoteId = "sf-example-123"
      val description = "Cosmic Archaeology"
      val now = new Date()
      val user = User(
        200, "fake-123", "Not a real instructor", "fake@fake.com", now
      )
      val contextCreateRes = lmsContextRepo.create(
        remoteId,
        description,
        user,
        toolConsumer
      )

      contextCreateRes must beLeft[ELError]
      contextCreateRes match {
        case Left(error:InstructorNotFound) => success
        case _ => failure
      }
    }

    "set the role for a user in a context and verify that the correct roles are returned" in new LMSContextRepoTest {
      val remoteId = "sf-example-123"
      val description = "Cosmic Archaeology"
      val contextCreateRes = lmsContextRepo.create(
        remoteId,
        description,
        mockInstructor,
        toolConsumer
      )

      contextCreateRes must beRight[LMSContext]
      val contextCreate = contextCreateRes.right.get
      val contextGetRes = lmsContextRepo.getById(contextCreate.id)
      contextGetRes must beSome[LMSContext]
      val context = contextGetRes.get

      lmsContextRepo.setRolesInContextForUser(mockInstructor, context, Set(userRepo.roles.instructor))
      val rolesReturned = lmsContextRepo.getRolesInContextForUser(mockInstructor, context)
      rolesReturned.roles must haveSize(1)
      rolesReturned.roles.head must be(userRepo.roles.instructor)
    }

    "update a user's role correctly" in new LMSContextRepoTest {
      val remoteId = "sf-example-123"
      val description = "Cosmic Archaeology"
      val contextCreateRes = lmsContextRepo.create(
        remoteId,
        description,
        mockInstructor,
        toolConsumer
      )

      contextCreateRes must beRight[LMSContext]
      val contextCreate = contextCreateRes.right.get
      val contextGetRes = lmsContextRepo.getById(contextCreate.id)
      contextGetRes must beSome[LMSContext]
      val context = contextGetRes.get

      lmsContextRepo.setRolesInContextForUser(mockInstructor, context, Set(userRepo.roles.instructor))
      lmsContextRepo.setRolesInContextForUser(mockInstructor, context, Set(userRepo.roles.instructor, userRepo.roles.student))
      val rolesReturned = lmsContextRepo.getRolesInContextForUser(mockInstructor, context)
      rolesReturned.roles must haveSize(2)
      rolesReturned.roles must contain(userRepo.roles.instructor)
      rolesReturned.roles must contain(userRepo.roles.student)
    }

    "return that a user does not have any roles in a context when a user is not in a context" in new LMSContextRepoTest {
      val remoteId = "sf-example-123"
      val description = "Cosmic Archaeology"
      val contextCreateRes = lmsContextRepo.create(
        remoteId,
        description,
        mockInstructor,
        toolConsumer
      )

      contextCreateRes must beRight[LMSContext]
      val contextCreate = contextCreateRes.right.get
      val contextGetRes = lmsContextRepo.getById(contextCreate.id)
      contextGetRes must beSome[LMSContext]
      val context = contextGetRes.get

      lmsContextRepo.setRolesInContextForUser(mockInstructor, context, Set(userRepo.roles.instructor))
      val rolesReturned = lmsContextRepo.getRolesInContextForUser(mockStudent, context)
      rolesReturned.roles must beEmpty
    }

    "return that a user is part of a context when that user is part of the context" in new LMSContextRepoTest {
      val remoteId = "sf-example-123"
      val description = "Cosmic Archaeology"
      val contextCreateRes = lmsContextRepo.create(
        remoteId,
        description,
        mockInstructor,
        toolConsumer
      )

      contextCreateRes must beRight[LMSContext]
      val contextCreate = contextCreateRes.right.get
      val contextGetRes = lmsContextRepo.getById(contextCreate.id)
      contextGetRes must beSome[LMSContext]
      val context = contextGetRes.get

      lmsContextRepo.setRolesInContextForUser(mockInstructor, context, Set(userRepo.roles.instructor))
      val roles = lmsContextRepo.getRolesInContextForUser(mockInstructor, context).roles
      roles must not be empty
    }

    "return that a user is not part of a context when that user is not part of the context" in new LMSContextRepoTest {
      val remoteId = "sf-example-123"
      val description = "Cosmic Archaeology"
      val contextCreateRes = lmsContextRepo.create(
        remoteId,
        description,
        mockInstructor,
        toolConsumer
      )

      contextCreateRes must beRight[LMSContext]
      val contextCreate = contextCreateRes.right.get
      val contextGetRes = lmsContextRepo.getById(contextCreate.id)
      contextGetRes must beSome[LMSContext]
      val context = contextGetRes.get

      val roles = lmsContextRepo.getRolesInContextForUser(mockStudent, context).roles
      roles must be empty
    }

  }

}


trait LMSContextRepoTest extends WithApplication with HasDatabase with HasToolConsumerRepo with HasUserRepo with HasAssignmentRepo {
  val toolConsumer = toolConsumerRepo.create("http://lms.starfleet.edu", None, "Unknown")
  val lmsContextRepo = new LMSContextRepo(dbService, userRepo)
}
