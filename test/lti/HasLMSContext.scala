package lti

import ToolConsumer
import edu.ucla.cdh.EvidenceLocker.authentication.User

/**
  * Created by dave on 8/3/16.
  */
trait HasBasicContext {

  val lmsContextRepo:LMSContextRepo
  val toolConsumerRepo:ToolConsumerRepository
  val mockInstructor:User

  val toolConsumer = toolConsumerRepo.create("http://lms.starfleet.edu/", None, "Unknown")

  val context = lmsContextRepo.create("sf-example-123", "Interstellar Archaeology",
    mockInstructor, toolConsumer
  ).right.get

}
