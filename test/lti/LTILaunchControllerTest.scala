package lti

import akka.actor.ActorRef
import assignments.HasAssignmentRepo
import authentication.{HasToolConsumerRepo, HasUserRepo}
import common.HasDatabase
import edu.ucla.cdh.EvidenceLocker.lti.LTILaunchController
import play.api.i18n.MessagesApi
import play.api.inject.BindingKey
import play.api.mvc.Controller
import play.api.test.WithApplication

/**
  * Created by dave on 10/14/16.
  */
trait LTILaunchControllerTest extends WithApplication
  with HasDatabase with HasToolConsumerRepo
    with HasUserRepo with HasLMSContextRepo with HasAssignmentRepo {

  val tcr = toolConsumerRepo
  val ur = userRepo
  val lr = lmsContextRepo
  val ar = assignmentRepo

  val toolConsumer = toolConsumerRepo.create("127.0.0.1", None, "Unknown")

  val controller = new LTILaunchController with Controller {
    val toolConsumerRepo = tcr
    val userRepo = ur
    val lmsContextRepo = lr
    val assignmentRepo = ar
    val messagesApi = app.injector.instanceOf[MessagesApi]
    val enrollmentUpdater = app.injector.instanceOf(BindingKey(classOf[ActorRef]).qualifiedWith("enrollment-list-updater"))
  }
}
