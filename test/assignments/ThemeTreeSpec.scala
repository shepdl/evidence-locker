package assignments

import java.util.Date

import annotations._
import authentication.{HasToolConsumerRepo, HasUserRepo}
import common.HasDatabase
import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import lti.{HasBasicContext, HasLMSContextRepo}
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import play.api.test._
import play.api.test.Helpers._
import reviews._
import play.api.mvc.{Controller, Results}


@RunWith(classOf[JUnitRunner])
class ThemeTreeSpec extends PlaySpecification with Results with JsonMatchers {

  "A call to get the theme tree" should {

    "render the full theme tree with annotations, explications, and reviews for a student" in new ThemeTreeTest {
      val heroism = assignment.themes.head

      val unheroicQualities = themeRepo.create(mockStudent, assignment, "Unheroic Qualities", List(), heroism, mockStudent, 0).right.get
      val hqAnnotation = annotationRepo.create(
        mockStudent, heroicQualities, Annotation.from(
          "Passage 1", "/p[0]", "/p[1]", 10, 30, "none", mockStudent
        )
      ).right.get

      val uhqAnnotation1 = annotationRepo.create(
        mockStudent, unheroicQualities, Annotation.from(
          "Passage 2", "/p[f]", "/p[3]", 20, 40, "2", mockStudent
        )
      ).right.get

      val uhqAnnotation2 = annotationRepo.create(
        mockStudent, unheroicQualities, Annotation.from(
          "Passage 3", "/p[2]", "/p[5]", 30, 50, "23", mockStudent
        )
      ).right.get

      val hqExplication = explicationRepo.save(
        mockStudent, hqAnnotation, Explication.from("My explication", hqAnnotation, mockStudent)
      ).right.get

      val uhq1Explication = explicationRepo.save(
        mockStudent, uhqAnnotation1, Explication.from("Yet another explication", uhqAnnotation1, mockStudent)
      ).right.get


      val now = new Date

      val hqExplicationReview1 = peerReviewRepo.createLock(reviewingStudents(0), assignment).right.get
      peerReviewRepo.update(reviewingStudents(0), hqExplicationReview1.addResponse(
        assignment.reviewRubric.zip(hqExplicationReview1.responses).map({
          case (rr, review) => PromptResponse(
            review.id, "", rr, 2
          )
        })
      ))
      val hqExplicationReview2 = peerReviewRepo.createLock(reviewingStudents(1), assignment).right.get
      peerReviewRepo.update(reviewingStudents(1), hqExplicationReview1.addResponse(
        assignment.reviewRubric.zip(hqExplicationReview2.responses).map({
          case (rr, review) => PromptResponse(
            review.id, "", rr, 2
          )
        })
      ))


      val uhq1ExplicationReview1 = peerReviewRepo.createLock(reviewingStudents(0), assignment).right.get
      val student1uhqExplicationReview1 = uhq1ExplicationReview1.addResponse(
        assignment.reviewRubric.zip(uhq1ExplicationReview1.responses).map({
          case (rr, review) => PromptResponse(
            review.id, "", rr, 3
          )
        })
      )
      peerReviewRepo.update(reviewingStudents(0), student1uhqExplicationReview1)

      val uhq1ExplicationReview1Updated = peerReviewRepo.update(reviewingStudents(0), uhq1ExplicationReview1.addResponse(
        assignment.reviewRubric.zip(uhq1ExplicationReview1.responses).map({
          case (rr, review) => PromptResponse(
            review.id, "", rr, 3
          )
        })
      )).right.get

      val request = FakeRequest(GET, s"/assignments/${assignment.id}/themes/${mockStudent.id}")
        .withSession("userId" -> mockInstructor.id.toString)
      val response = controller.getThemeTreeForStudent(assignment.id, Some(mockStudent.id)).apply(request)

      status(response) mustEqual 200
      val responseJson = contentAsString(response)

//      responseJson must (/("themes")/#(0)).andHave(size(2))
//        (/("children")/#(0)/("passages")).andHave(size(1))

      println(responseJson)
      responseJson must /("themes")/#(0) /("children")/#(0) /("passages")/#(0)
        /("id" -> hqAnnotation.id.toDouble)
        /("quote" -> hqAnnotation.quote)
        /("start" -> hqAnnotation.start)
        /("end" -> hqAnnotation.end)
        /("startOffset" -> hqAnnotation.startOffset.toDouble)
        /("endOffset" -> hqAnnotation.endOffset.toDouble)

        /("explication" -> hqExplication.text)
        /("reviews")
          /#(0)
            /("id" -> hqExplicationReview1.id.toDouble)
            /("ownerRef" -> s"/users/${hqExplicationReview1.reviewer.id}")
            /("responses")
              /#(0)
                /("description" -> hqExplicationReview1.responses(0).prompt.description)
                /("min" -> hqExplicationReview1.responses(0).prompt.min.toDouble)
                /("max" -> hqExplicationReview1.responses(0).prompt.max.toDouble)
                /("score" -> hqExplicationReview1.responses(0).score.toDouble)
              /#(1)
                /("description" -> hqExplicationReview1.responses(1).prompt.description)
                /("min" -> hqExplicationReview1.responses(1).prompt.min.toDouble)
                /("max" -> hqExplicationReview1.responses(1).prompt.max.toDouble)
                /("score" -> hqExplicationReview1.responses(1).score.toDouble)
          /#(1)
            /("id" -> hqExplicationReview2.id.toDouble)
            /("ownerRef" -> s"/users/${hqExplicationReview2.reviewer.id}")
            /("responses")
            /#(0)
              /("description" -> hqExplicationReview2.responses(0).prompt.description)
              /("min" -> hqExplicationReview2.responses(0).prompt.min.toDouble)
              /("max" -> hqExplicationReview2.responses(0).prompt.max.toDouble)
              /("score" -> hqExplicationReview2.responses(0).score.toDouble)
            /#(1)
              /("description" -> hqExplicationReview2.responses(1).prompt.description)
              /("min" -> hqExplicationReview2.responses(1).prompt.min.toDouble)
              /("max" -> hqExplicationReview2.responses(1).prompt.max.toDouble)
              /("score" -> hqExplicationReview2.responses(1).score.toDouble)

      responseJson must /("themes")/#(0) /("children")/#(1)/("passages") /#(0)
          /("id" -> uhqAnnotation1.id.toDouble)
          /("quote" -> uhqAnnotation1.quote)
          /("start" -> uhqAnnotation1.start)
          /("end" -> uhqAnnotation1.end)
          /("startOffset" -> uhqAnnotation1.startOffset.toDouble)
          /("endOffset" -> uhqAnnotation1.endOffset.toDouble)
          /("reviews")
            /#(0)
              /("id" -> uhq1ExplicationReview1.id.toDouble)
              /("ownerRef" -> s"/users/${uhq1ExplicationReview1.reviewer.id}")
              /("responses")
              /#(0)
                /("description" -> uhq1ExplicationReview1.responses(0).prompt.description)
                /("min" -> uhq1ExplicationReview1.responses(0).prompt.min.toDouble)
                /("max" -> uhq1ExplicationReview1.responses(0).prompt.max.toDouble)
                /("score" -> uhq1ExplicationReview1.responses(0).score.toDouble)
              /#(1)
                /("description" -> uhq1ExplicationReview1.responses(1).prompt.description)
                /("min" -> uhq1ExplicationReview1.responses(1).prompt.min.toDouble)
                /("max" -> uhq1ExplicationReview1.responses(1).prompt.max.toDouble)
                /("score" -> uhq1ExplicationReview1.responses(1).score.toDouble)
        /#(1)
          /("id" -> uhqAnnotation2.id.toDouble)
          /("quote" -> uhqAnnotation2.quote)
          /("start" -> uhqAnnotation2.start)
          /("end" -> uhqAnnotation2.end)
          /("startOffset" -> uhqAnnotation2.startOffset.toDouble)
          /("endOffset" -> uhqAnnotation2.endOffset.toDouble)
//      responseJson must /("themes")/#(0)/("children")/#(0)/("annotations")/#(0)/("explication" -> assignment.themes(0).children(0).annotations(0).explication.text)

      val expectedResponse =
        s"""
           |{
           |  "id" : ${assignment.id},
           |  "remoteId" : ${assignment.remoteId},
           |  "title" : "${assignment.title}",
           |  "description" : "${assignment.description}",
           |  "openDate" : "${assignment.openDate}",
           |  "closeDate" : "${assignment.closeDate}",
           |  "peerReviewEnabled" : ${assignment.peerReviewEnabled},
           |  "numberRequired" : ${assignment.numberRequired},
           |  "rubricType" : ${assignment.rubricType},
           |  "themes" : [
           |    {
           |      "description" : "Heroism",
           |      "id" : "${heroism.id}"
           |      "children" : [
           |        {
           |          "id" : ${heroicQualities.id},
           |          "description" : "${heroicQualities.description}",
           |          "passages" : [
           |            {
           |              "id" : ${hqAnnotation.id},
           |              "quote" : "${hqAnnotation.quote}",
           |              "start" : "${hqAnnotation.start}",
           |              "end" : "${hqAnnotation.end}",
           |              "startOffset" : ${hqAnnotation.startOffset},
           |              "endOffset" : ${hqAnnotation.endOffset}
           |
           |              "explication" : ${hqExplication.text},
           |              "reviews" : [
           |                {
           |                  "id" : ${hqExplicationReview1.id},
           |                  "ownerRef" : "/users/${hqExplicationReview1.reviewer.id}"
           |                  "responses" : [
           |                    {
           |                      "description" : "${hqExplicationReview1.responses(0).prompt.description}",
           |                      "min" : ${hqExplicationReview1.responses(0).prompt.min},
           |                      "max" : ${hqExplicationReview1.responses(0).prompt.max},
           |                      "score" : ${hqExplicationReview1.responses(0).score}
           |                    },
           |                    {
           |                      "description" : "${hqExplicationReview1.responses(1).prompt.description}",
           |                      "min" : ${hqExplicationReview1.responses(1).prompt.min},
           |                      "max" : ${hqExplicationReview1.responses(1).prompt.max},
           |                      "score" : ${hqExplicationReview1.responses(1).score}
           |                    }
           |                  ]
           |                },
           |                {
           |                  "id" : ${hqExplicationReview2.id},
           |                  "ownerRef" : "/users/${hqExplicationReview2.reviewer.id}"
           |                  "responses" : [
           |                    {
           |                      "description" : "${hqExplicationReview2.responses(0).prompt.description}",
           |                      "min" : ${hqExplicationReview2.responses(0).prompt.min},
           |                      "max" : ${hqExplicationReview2.responses(0).prompt.max},
           |                      "score" : ${hqExplicationReview2.responses(0).score}
           |                    },
           |                    {
           |                      "description" : "${hqExplicationReview2.responses(1).prompt.description}",
           |                      "min" : ${hqExplicationReview2.responses(1).prompt.min},
           |                      "max" : ${hqExplicationReview2.responses(1).prompt.max},
           |                      "score" : ${hqExplicationReview2.responses(1).score}
           |                    }
           |                  ]
           |                }
           |              ]
           |            }
           |          ]
           |        },
           |        {
           |          "id" : ${unheroicQualities.id},
           |          "description" : "${unheroicQualities.description}",
           |          "passages" : [
           |            {
           |              "id" : ${uhqAnnotation1.id},
           |              "quote" : "${uhqAnnotation1.quote}",
           |              "start" : "${uhqAnnotation1.start}",
           |              "end" : "${uhqAnnotation1.end}",
           |              "startOffset" : ${uhqAnnotation1.startOffset},
           |              "endOffset" : ${uhqAnnotation1.endOffset}
           |
           |              "explication" : ${uhq1Explication.text},
           |              "reviews" : [
           |                {
           |                  "id" : ${uhq1ExplicationReview1.id},
           |                  "ownerRef" : "/users/${uhq1ExplicationReview1.reviewer.id}"
           |                  "responses" : [
           |                    {
           |                      "description" : "${uhq1ExplicationReview1.responses(0).prompt.description}",
           |                      "min" : ${uhq1ExplicationReview1.responses(0).prompt.min},
           |                      "max" : ${uhq1ExplicationReview1.responses(0).prompt.max},
           |                      "score" : ${uhq1ExplicationReview1.responses(0).score}
           |                    }, |                    {
           |                      "description" : "${uhq1ExplicationReview1.responses(1).prompt.description}",
           |                      "min" : ${uhq1ExplicationReview1.responses(1).prompt.min},
           |                      "max" : ${uhq1ExplicationReview1.responses(1).prompt.max},
           |                      "score" : ${uhq1ExplicationReview1.responses(1).score}
           |                    }
           |                  ]
           |                }
           |              ]
           |            },
           |            {
           |              "id" : ${uhqAnnotation2.id},
           |              "quote" : "${uhqAnnotation2.quote}",
           |              "start" : "${uhqAnnotation2.start}",
           |              "end" : "${uhqAnnotation2.end}",
           |              "startOffset" : ${uhqAnnotation2.startOffset},
           |              "endOffset" : ${uhqAnnotation2.endOffset}
           |
           |              "explication" : "",
           |              "reviews" : []
           |            },
           |          ]
           |        },
           |
           |      ]
           |    },
           |  ]
           |}
         """.stripMargin
    }

  }

}


class ThemeTreeTest extends WithApplication
  with HasDatabase with HasToolConsumerRepo with HasUserRepo with HasAssignmentRepo with HasLMSContextRepo with HasBasicContext
  with HasThemeRepo with HasAnnotationRepo with HasExplicationRepo with HasPeerReviewRepo
  with PeerReviewNumericallyGradedAssignment with PeerReviewTestCase {


  val ur = userRepo
  val ar = assignmentRepo
  val pr = peerReviewRepo
  val tr = themeRepo
  val er = explicationRepo
  val anr = annotationRepo

  val controller = new ThemeTreeController with Controller {
    override val peerReviewRepo = pr
    override val themeRepo = tr
    override val userRepo = ur
    override val assignmentRepo = ar
    override val explicationRepo = er
    override val annotationRepo = anr
  }

}
