package assignments

import annotations.{HasAnnotationRepo, HasExplicationRepo}
import authentication.{AssignmentService, AuthenticationService, HasToolConsumerRepo, HasUserRepo}
import common.HasDatabase
import lti.{HasBasicContext, HasLMSContextRepo}
import play.api.mvc.Controller
import play.api.test.WithApplication
import reviews.HasPeerReviewRepo

/**
  * Created by daveshepard on 7/15/16.
  */
class AssignmentControllerTest extends WithApplication
  with HasDatabase with HasToolConsumerRepo with HasUserRepo with HasAssignmentRepo
  with HasAnnotationRepo
  with HasLMSContextRepo with HasBasicContext
  with HasThemeRepo with HasExplicationRepo with HasPeerReviewRepo
{

  val ar = assignmentRepo
  val ur = userRepo
  val er = explicationRepo
  val prr = peerReviewRepo
  val lcr = lmsContextRepo

  val controller = new AssignmentController with Controller {
    override val userRepo = ur
    override val assignmentRepo = ar
    override val explicationRepo = er
    override val peerReviewRepo = prr
    override val lmsContextRepo = lcr
    val authenticationService = new AuthenticationService(userRepo)
    val assignmentService = new AssignmentService(lmsContextRepo, userRepo, assignmentRepo)
  }

}
