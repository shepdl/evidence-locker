package assignments

import java.util.Date

import authentication.{HasToolConsumerRepo, HasUserRepo}
import common.{ELError, HasDatabase}
import edu.ucla.cdh.EvidenceLocker.authentication.{User, UserRepository}
import lti.{HasBasicContext, HasLMSContextRepo, LMSContext, LMSContextRepo}
import org.junit.runner._
import org.specs2.mutable._
import org.specs2.runner._
import play.api.test._


@RunWith(classOf[JUnitRunner])
class ThemeRepositoryCreateSpec extends Specification {

  "A Theme Repository" should {

    "create a child of an existing theme successfully" in new ThemeRepositoryTest {
      val themeDescription = "Heroic Qualities"
      val children = List()
      lmsContextRepo.setRolesInContextForUser(mockStudent, context, Set(userRepo.roles.student))

      val parent = updatedAssignmentResult.right.get.themes.head
      val createThemeResult = themeRepo.create(mockStudent, initialAssignment, themeDescription, children, parent, mockStudent, 0)
      createThemeResult must beRight[Theme]
      val createdTheme = createThemeResult.right.get

      val getThemeResult = themeRepo.getById(mockStudent, createdTheme.id)
      getThemeResult must beSome[Theme]
      val theme = getThemeResult.get
      theme.description mustEqual themeDescription
      theme.children mustEqual children
      theme.owner mustEqual mockStudent

      val parentRes = themeRepo.getParent(mockStudent, theme)
      parentRes must beSome[Theme]

      val retrievedParent = parentRes.get
      retrievedParent.id mustEqual parent.id
    }

    "retrieve a complete theme tree for a user for one assignment" in new ThemeRepositoryTest with UpdateTestCase {

      val parentTheme = updatedAssignment.themes.head
      def createTheme(description:String, parentTheme:Theme) = themeRepo.create(
        mockStudent, updatedAssignment, description, List(), parentTheme, mockStudent, 0
      ).right.get

      val heroicQualities = createdTheme
      val heroicQualitiesSubThemes = List(
        "Takes on a big challenge", "Honesty", "Obeys his religion", "Takes action", "Intelligence"
      ).map(createTheme(_, heroicQualities))
      val unheroicQualities = createTheme("Unheroic Qualities", parentTheme)
      val unheroicQualitiesSubThemes = List(
        "Fails to complete challenges", "Dishonesty", "Only takes action at the very end", "Hesitation"
      ).map(createTheme(_, unheroicQualities))

      createTheme("Thinks carefully about things", heroicQualitiesSubThemes.last)

      val themesRetrievedRes = themeRepo.getThemeTreeForUser(mockStudent, updatedAssignment)

      themesRetrievedRes must beRight[List[Theme]]
      val themesRetrieved = themesRetrievedRes.right.get

      themesRetrieved must haveSize(2)
      themesRetrieved(0).description mustEqual "Heroism"
      themesRetrieved(0).children must haveSize(2)
      themesRetrieved(0).children(0).description mustEqual "Heroic Qualities"
      themesRetrieved(0).children(0).children(0).description mustEqual "Takes on a big challenge"
      themesRetrieved(0).children(0).children(1).description mustEqual "Honesty"
      themesRetrieved(0).children(0).children(2).description mustEqual "Obeys his religion"
      themesRetrieved(0).children(0).children(3).description mustEqual "Takes action"
      themesRetrieved(0).children(0).children(4).description mustEqual "Intelligence"
      themesRetrieved(0).children(0).children(4).children(0).description mustEqual "Thinks carefully about things"

      themesRetrieved(0).children(1).description mustEqual "Unheroic Qualities"
      themesRetrieved(0).children(1).children(0).description mustEqual "Fails to complete challenges"
      themesRetrieved(0).children(1).children(1).description mustEqual "Dishonesty"
      themesRetrieved(0).children(1).children(2).description mustEqual "Only takes action at the very end"
      themesRetrieved(0).children(1).children(3).description mustEqual "Hesitation"

      themesRetrieved(1).description mustEqual "Women's roles"
    }

    "return an assignment for a top-level theme" in new ThemeRepositoryTest with UpdateTestCase {
      val parentTheme = updatedAssignment.themes.head
      val assignmentRes = themeRepo.assignmentForTheme(parentTheme)
      assignmentRes must beRight[Assignment]
      val assignment = assignmentRes.right.get
      assignment mustEqual updatedAssignment
    }

    "return an assignment for a sub-theme" in new ThemeRepositoryTest with UpdateTestCase {
      val parentTheme = updatedAssignment.themes.head
      val subThemeRes = themeRepo.create(mockStudent, updatedAssignment, "Subtheme", List(), parentTheme, mockStudent, 0)
      subThemeRes must beRight[Theme]
      val subTheme = subThemeRes.right.get

      val assignmentRes = themeRepo.assignmentForTheme(subTheme)
      assignmentRes must beRight[Assignment]
      val assignment = assignmentRes.right.get
      assignment mustEqual updatedAssignment
    }

    "retrieve only those themes a teacher has created when the user hasn't created any themes" in new ThemeRepositoryTest with UpdateTestCase {
      val themesRetrievedRes = themeRepo.getThemeTreeForUser(mockStudent, updatedAssignment)
      themesRetrievedRes must beRight[List[Theme]]

      val themesRetrieved = themesRetrievedRes.right.get
      themesRetrieved must haveSize(2)
      themesRetrieved(0).description mustEqual "Heroism"
//      themesRetrieved(0).children must haveSize(0)
      themesRetrieved(1).description mustEqual "Women's roles"
      themesRetrieved(1).children must haveSize(0)
    }

    "retrieve only themes created by that user" in new ThemeRepositoryTest with UpdateTestCase {
      val parentTheme = updatedAssignment.themes.head
      def createTheme(description:String, parentTheme:Theme) = themeRepo.create(
        mockStudent, updatedAssignment, description, List(), parentTheme, mockStudent, 0
      ).right.get
      val heroicQualities = createdTheme
      val heroicQualitiesSubThemes = List(
        "Takes on a big challenge", "Honesty", "Obeys his religion", "Takes action", "Intelligence"
      ).map(createTheme(_, heroicQualities))
      val unheroicQualities = createTheme("Unheroic Qualities", parentTheme)
      val unheroicQualitiesSubThemes = List(
        "Fails to complete challenges", "Dishonesty", "Only takes action at the very end", "Hesitation"
      ).map(createTheme(_, unheroicQualities))

      createTheme("Thinks carefully about things", heroicQualitiesSubThemes.last)


      // The other student
      val wesleyCrusher = userRepo.create("sf-student-1", "Wesley Crusher", "crusher@starfleet.com").get
      lmsContextRepo.setRolesInContextForUser(wesleyCrusher, context, Set(userRepo.roles.student))

      themeRepo.create(
        wesleyCrusher, updatedAssignment, "Kingliness", List(), parentTheme, wesleyCrusher, 0
      )

      val josieBruinsThemesRes = themeRepo.getThemeTreeForUser(mockStudent, updatedAssignment)

      josieBruinsThemesRes must beRight[List[Theme]]
      val josieBruinsThemes = josieBruinsThemesRes.right.get
      josieBruinsThemes must haveSize(2)
      josieBruinsThemes(0).description mustEqual "Heroism"
      josieBruinsThemes(0).children must haveSize(2)
      josieBruinsThemes(0).children(0).description mustEqual "Heroic Qualities"
      josieBruinsThemes(0).children(0).children must haveSize(5)
      josieBruinsThemes(0).children(0).children(0).description mustEqual "Takes on a big challenge"
      josieBruinsThemes(0).children(0).children(1).description mustEqual "Honesty"
      josieBruinsThemes(0).children(0).children(2).description mustEqual "Obeys his religion"
      josieBruinsThemes(0).children(0).children(3).description mustEqual "Takes action"
      josieBruinsThemes(0).children(0).children(4).description mustEqual "Intelligence"
      josieBruinsThemes(0).children(0).children(4).children(0).description mustEqual "Thinks carefully about things"

      josieBruinsThemes(0).children(1).description mustEqual "Unheroic Qualities"
      josieBruinsThemes(0).children(1).children must haveSize(4)
      josieBruinsThemes(0).children(1).children(0).description mustEqual "Fails to complete challenges"
      josieBruinsThemes(0).children(1).children(1).description mustEqual "Dishonesty"
      josieBruinsThemes(0).children(1).children(2).description mustEqual "Only takes action at the very end"
      josieBruinsThemes(0).children(1).children(3).description mustEqual "Hesitation"

      val wesleyCrusherThemeRes = themeRepo.getThemeTreeForUser(wesleyCrusher, updatedAssignment)
      wesleyCrusherThemeRes must beRight[List[Theme]]

      val wesleyCrusherThemes = wesleyCrusherThemeRes.right.get
      wesleyCrusherThemes(0).description mustEqual "Heroism"
      wesleyCrusherThemes(0).children must haveSize(1)
      wesleyCrusherThemes(0).children(0).description mustEqual "Kingliness"
      wesleyCrusherThemes(0).children(0).children must beEmpty
      wesleyCrusherThemes(1).description mustEqual "Women's roles"
      wesleyCrusherThemes(1).children must beEmpty
    }

    "return UserHasNoAccessToAssignment when a user is not a member of the course" in new ThemeRepositoryTest with UpdateTestCase {
      val parentTheme = updatedAssignment.themes.head
      def createTheme(description:String, parentTheme:Theme) = themeRepo.create(
        mockStudent, updatedAssignment, description, List(), parentTheme, mockStudent, 0
      ).right.get
      val heroicQualities = createdTheme
      val heroicQualitiesSubThemes = List(
        "Takes on a big challenge", "Honesty", "Obeys his religion", "Takes action", "Intelligence"
      ).map(createTheme(_, heroicQualities))
      val unheroicQualities = createTheme("Unheroic Qualities", parentTheme)
      val unheroicQualitiesSubThemes = List(
        "Fails to complete challenges", "Dishonesty", "Only takes action at the very end", "Hesitation"
      ).map(createTheme(_, unheroicQualities))

      createTheme("Thinks carefully about things", heroicQualitiesSubThemes.last)

      val josieBruinsThemesRes = themeRepo.getThemeTreeForUser(mockStudent, updatedAssignment)

      josieBruinsThemesRes must beRight[List[Theme]]
      val josieBruinsThemes = josieBruinsThemesRes.right.get
      josieBruinsThemes must haveSize(2)
      josieBruinsThemes(0).description mustEqual "Heroism"
      josieBruinsThemes(0).children(0).children must haveSize(5)
      josieBruinsThemes(0).children(0).description mustEqual "Heroic Qualities"
      josieBruinsThemes(0).children(0).children(0).description mustEqual "Takes on a big challenge"
      josieBruinsThemes(0).children(0).children(1).description mustEqual "Honesty"
      josieBruinsThemes(0).children(0).children(2).description mustEqual "Obeys his religion"
      josieBruinsThemes(0).children(0).children(3).description mustEqual "Takes action"
      josieBruinsThemes(0).children(0).children(4).description mustEqual "Intelligence"
      josieBruinsThemes(0).children(0).children(4).children(0).description mustEqual "Thinks carefully about things"

      josieBruinsThemes(0).children(1).description mustEqual "Unheroic Qualities"
      josieBruinsThemes(0).children(1).children must haveSize(4)
      josieBruinsThemes(0).children(1).children(0).description mustEqual "Fails to complete challenges"
      josieBruinsThemes(0).children(1).children(1).description mustEqual "Dishonesty"
      josieBruinsThemes(0).children(1).children(2).description mustEqual "Only takes action at the very end"
      josieBruinsThemes(0).children(1).children(3).description mustEqual "Hesitation"

      // The other student
      val wesleyCrusher = userRepo.create("sf-student-1", "Wesley Crusher", "crusher@starfleet.com").get

      val wesleyCrusherThemeTreeRes = themeRepo.getThemeTreeForUser(wesleyCrusher, updatedAssignment)
      wesleyCrusherThemeTreeRes match {
        case Left(err:UserHasNoAccessToAssignment) => success
        case _ => failure
      }

    }


    "return UserHasNoAccessToAssignment when the user is not a member of the class" in new ThemeRepositoryTest with TestCase2 {
      val themeDescription = "Heroic Qualities"
      val children = List()
      val parent = updatedAssignmentResult.right.get.themes.head
      val createThemeResult = themeRepo.create(alternateUser, initialAssignment, themeDescription, children, parent, alternateUser, 0)
      createThemeResult match {
        case Left(UserHasNoAccessToAssignment(user, assignment)) => {
          user mustEqual alternateUser
          assignment mustEqual initialAssignment
        }
        case _ => failure
      }
    }

    "Update title and parent of a theme successfully" in new ThemeRepositoryTest with UpdateTestCase {
      val now = new Date
      val newParent = updatedAssignmentResult.right.get.themes(1)
      val newDescription = "Heroic Qualities 2"
      val updatingTheme = Theme(
        theme.id,
        newDescription,
        newParent,
        List(),
        mockStudent,
        0
      )

      themeRepo.update(mockStudent, updatingTheme, updatedAssignment) must beRight[Theme]
      val updatedThemeRes = themeRepo.getById(mockStudent, updatingTheme.id)
      updatedThemeRes must beSome[Theme]
      val updatedTheme = updatedThemeRes.get

      updatedTheme.id mustEqual theme.id
      updatedTheme.description mustEqual newDescription
      updatedTheme.parent.id mustEqual newParent.id
    }

    "Return UserCannotUpdateTheme when the user is not a member of the class" in new ThemeRepositoryTest with UpdateTestCase with TestCase2 {
      val now = new Date
      val newParent = updatedAssignment.themes(1)
      val newDescription = "Heroic Qualities 2"
      val updatingTheme = Theme(
        theme.id,
        newDescription,
        newParent,
        List(),
        alternateUser,
        0
      )

      themeRepo.update(alternateUser, updatingTheme, updatedAssignment) match {
        case Left(_) => success
        case _ => failure
      }
    }

    "Return UserCannotUpdateTheme when the theme was created by someone else" in new ThemeRepositoryTest with UpdateTestCase with TestCase2 {

      val now = new Date
      val newParent = updatedAssignment.themes(1)
      val newDescription = "Heroic Qualities 2"

      val updatingTheme = Theme(
        theme.id,
        newDescription,
        newParent,
        List(),
        alternateUser,
        0
      )

      themeRepo.update(mockInstructor, updatingTheme, updatedAssignment) match {
        case Left(_) => success
        case _ => failure
      }
    }

    "Delete a theme succesfully" in new ThemeRepositoryTest with UpdateTestCase {
      themeRepo.delete(mockStudent, updatedAssignment, theme) match {
        case Right(theme:Theme) => success
        case _ => failure
      }
    }

    "Return UserHasNoAccessToAssignment when the user is not a member of the class" in new ThemeRepositoryTest with UpdateTestCase with TestCase2 {
      themeRepo.delete(alternateUser, updatedAssignment, theme) match {
        case Left(_) => success
        case _ => failure
      }
    }

    "Return UserCannotDeleteTheme when the theme was created by someone else" in new ThemeRepositoryTest with UpdateTestCase with TestCase2 {
      themeRepo.delete(alternateUser, updatedAssignment, theme) match {
        case Left(_) => success
        case _ => failure
      }
    }

  }

}


trait TestCase2 {
  val userRepo:UserRepository
  val alternateUser = userRepo.create("babylon-5-45", "Jeffrey Sinclair", "jsinclair@babylon.sta").get
  val lmsContextRepo:LMSContextRepo
}

trait UpdateTestCase {

  val lmsContextRepo:LMSContextRepo
  val userRepo:UserRepository
  val themeRepo:ThemeRepository

  val updatedAssignmentResult:Either[ELError, Assignment]
  val updatedAssignment = updatedAssignmentResult.right.get
  val context:LMSContext
  val mockStudent:User

  val themeDescription = "Heroic Qualities"
  val children = List()
  lmsContextRepo.setRolesInContextForUser(mockStudent, context, Set(userRepo.roles.student))

  val parent = updatedAssignment.themes.head
  val createThemeResult = themeRepo.create(mockStudent, updatedAssignment, themeDescription, children, parent, mockStudent, 0)
  val createdTheme = createThemeResult.right.get

  val getThemeResult = themeRepo.getById(mockStudent, createdTheme.id)
  val theme = getThemeResult.get

}

trait ThemeRepositoryTest extends WithApplication with HasDatabase with HasToolConsumerRepo with HasUserRepo
  with HasAssignmentRepo with HasLMSContextRepo with HasBasicContext with HasThemeRepo with TestCase1 {

  lmsContextRepo.setRolesInContextForUser(mockInstructor, context, Set(userRepo.roles.instructor))
}


