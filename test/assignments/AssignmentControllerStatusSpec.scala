package assignments

import instructor.InstructorControllerTestCase
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import play.api.mvc.Results
import play.api.test._
import play.api.test.Helpers._


@RunWith(classOf[JUnitRunner])
class AssignmentControllerStatusSpec extends PlaySpecification with Results with JsonMatchers {

  "A set of Assignment Controller Status methods" should {

    "return 'forbidden' when the assignment is not found" in new AssignmentControllerTest with InstructorControllerTestCase {
      val request = FakeRequest(GET, s"/assignments/${assignment.id + 1000}").withSession("userId" -> instructor.id.toString)
      val response = controller.getById(assignment.id + 1000)(request)

      status(response) mustEqual FORBIDDEN
    }

    "only return the number of completed explications and peer reviews when the user is not an instructor" in new AssignmentControllerTest with InstructorControllerTestCase {
      val request = FakeRequest(GET, s"/assignments/${assignment.id}").withSession("userId" -> students(0).id.toString)
      val response = controller.getById(assignment.id)(request)

      status(response) mustEqual OK
      val content = contentAsString(response)

      content must /("status")/("completedExplications" -> assignment.numberRequired.toDouble)
      content must /("status")/("completedReviews" -> assignment.numberRequired.toDouble)

    }

    "return the correct number of students who have completed explications and reviews" in new AssignmentControllerTest with InstructorControllerTestCase {
      val request = FakeRequest(GET, s"/assignments/${assignment.id}").withSession("userId" -> instructor.id.toString)
      val response = controller.getById(assignment.id)(request)

      status(response) mustEqual OK
      val content = contentAsString(response)

      content must (/("status")/("explications")).andHave(size(8))
      content must /("status")/("explications")/#(0)/("student")/("id" -> studentsWhoHaveCompletedTheAssignment(0).id.toDouble)
      content must /("status")/("explications")/#(0)/("explicationCount" -> assignment.numberRequired.toDouble)

      content must (/("status")/("peerReviews")).andHave(size(8))
      content must /("status")/("peerReviews")/#(0)/("student")/("id" -> studentsWhoHaveCompletedTheAssignment(0).id.toDouble)
      content must /("status")/("peerReviews")/#(0)/("peerReviewsCompleted" -> assignment.numberRequired.toDouble)
    }

  }

}