package assignments

import java.io.PrintWriter
import java.util.Date

import edu.ucla.cdh.EvidenceLocker.authentication.User
import lti.LMSContext
import play.api.libs.Files.TemporaryFile

/**
  * Created by daveshepard on 7/15/16.
  */
trait TestCase1 {
  val assignmentRepo:AssignmentRepository
  val mockInstructor:User
  val context:LMSContext

  val title = "Close-reading Hamlet"
  val description = "Read Hamlet closely"
  val remoteId = "starfleet-123"
  val initialAssignment = assignmentRepo.create(
    mockInstructor, context, title, description, mockInstructor, remoteId
  ).right.get

  val tempFileContent = s"""
                           |<sampleXml>
                           |  <content>Hi!</content>
                           |</sampleXml>
         """.stripMargin
  val tempFile = TemporaryFile("test", ".txt")
  val writer = new PrintWriter(tempFile.file)
  writer.write(tempFileContent)
  writer.close()

  val openDate = new Date()
  val closeDate = new Date()

  val document = AssignmentDocument(
    initialAssignment.document.id,
    tempFile.file,
    tempFile.file.getName,
    tempFile.file.getName.split("\\.").reverse.head
  )

  val updateAssignment = Assignment(
    id = initialAssignment.id,
    remoteId = initialAssignment.remoteId,
    document = document,
    title = initialAssignment.title,
    description = initialAssignment.description,
    openDate = openDate,
    closeDate = closeDate,
    instructor = initialAssignment.instructor,
    peerReviewEnabled = true,
    numberRequired = 4,
    rubricType = "numeric",
    reviewRubric = List(
      RubricPrompt(-1, "Insight", 0, 5),
      RubricPrompt(-1, "Originality", 0, 5)
    ),
    themes = List(
      Theme(-1, "Heroism", null, List(), mockInstructor, 0),
      Theme(-1, "Women's roles", null, List(), mockInstructor, 1)
    )
  )
  val updatedAssignmentResult = assignmentRepo.updateAssignment(mockInstructor, updateAssignment)
}
