package assignments

import java.util.Date

import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner._


/**
  * Created by daveshepard on 7/15/16.
  */
@RunWith(classOf[JUnitRunner])
class AssignmentRepositoryPermissionsSpec extends Specification {

  "An Assignment Repository" should {
    "refuse to create an assignment when the user does not have permission to create assignments" in new AssignmentRepoTest {
      val remoteId = "course-2"
      val result = assignmentRepo.create(
        mockStudent,
        context,
        "A course I designed",
        "This is my description",
        mockStudent,
        remoteId
      )

      result must beLeft

      assignmentRepo.getByRemoteId("course-2") must beNone
    }

    "refuse to update when the user does not have the correct permissions with regards to a course" in new AssignmentRepoTest with TestCase1 {
      val now = new Date
      val updatedAssignment = Assignment(
        initialAssignment.id,
        initialAssignment.remoteId,
        document,
        "Ha ha! I updated the title",
        initialAssignment.description,
        new Date(), new Date(),
        initialAssignment.instructor,
        true,
        4,
        "numeric",
        List(
          RubricPrompt(-1, "Originality", 0, 5),
          RubricPrompt(-1, "Insight", 0, 5)
        ),
        List(
          Theme(-1, "Heroism", null, List(), mockInstructor, 1),
          Theme(-1, "Women's Roles", null, List(), mockInstructor, 2)
        )
      )

      assignmentRepo.updateAssignment(mockStudent, updatedAssignment) must beLeft(UserCannotUpdateAssignment(mockStudent, updatedAssignment))
      val retrievedAsssignment = assignmentRepo.getById(initialAssignment.id).get

      retrievedAsssignment.title mustEqual initialAssignment.title
    }
  }

}
