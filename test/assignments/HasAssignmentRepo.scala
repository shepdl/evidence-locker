package assignments

import com.typesafe.config.ConfigFactory
import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import play.api.Configuration

/**
  * Created by daveshepard on 7/15/16.
  */
trait HasAssignmentRepo {
  val dbService:DatabaseService
  val userRepo:UserRepository

  val mockInstructor = userRepo.create("user-2", "Jean-Luc Picard", "picard@example.ucla.edu").get
  val mockStudent = userRepo.create("user-1", "Josephine Bruin", "jbruin@example.ucla.edu").get

  import scala.collection.JavaConversions._

  val assignmentRepo = new AssignmentRepository(Configuration(ConfigFactory.parseMap(Map(
    "evidence_locker.text_upload_dir" -> "/tmp/evidence-locker/"
  ))), dbService, userRepo)
}
