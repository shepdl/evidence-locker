package assignments

import authentication.{HasToolConsumerRepo, HasUserRepo}
import common.HasDatabase
import lti.{HasBasicContext, HasLMSContextRepo}
import play.api.test.WithApplication

/**
  * Created by daveshepard on 7/15/16.
  */
trait AssignmentRepoTest extends WithApplication with HasDatabase with HasToolConsumerRepo
  with HasUserRepo with HasAssignmentRepo with HasLMSContextRepo with HasBasicContext
