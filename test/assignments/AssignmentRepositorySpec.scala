package assignments

import java.io.PrintWriter
import java.util.Date

import org.junit.runner._
import org.specs2.mutable._
import org.specs2.runner._
import play.api.libs.Files.TemporaryFile


@RunWith(classOf[JUnitRunner])
class AssignmentRepositorySpec extends Specification {

  "An AssignmentRepository" should {

    "create an assignment when provided with the basic info from an LTI Launch request" in new AssignmentRepoTest {
      val title = "Close-Reading Hamlet"
      val description = "Read the close-reading of Hamlet"
      val remoteId = "course-123"

      val createdAssignmentResult = assignmentRepo.create(mockInstructor, context, title, description, mockInstructor, remoteId)
      createdAssignmentResult.isRight mustEqual true

      val createdAssignment = createdAssignmentResult.right.get

      val retrievedAssignment = assignmentRepo.getById(createdAssignment.id).get

      retrievedAssignment.id mustEqual createdAssignment.id
      retrievedAssignment.remoteId mustEqual createdAssignment.remoteId
      retrievedAssignment.title mustEqual createdAssignment.title
      retrievedAssignment.description mustEqual createdAssignment.description
      retrievedAssignment.openDate mustEqual createdAssignment.openDate
      retrievedAssignment.closeDate mustEqual createdAssignment.closeDate
      retrievedAssignment.instructor mustEqual createdAssignment.instructor
      retrievedAssignment.peerReviewEnabled mustEqual createdAssignment.peerReviewEnabled
      retrievedAssignment.numberRequired mustEqual createdAssignment.numberRequired
      retrievedAssignment.reviewRubric mustEqual createdAssignment.reviewRubric
      retrievedAssignment.themes mustEqual createdAssignment.themes
    }

    "refuse to create an assignment with a remote ID that already exists from the same source" in new AssignmentRepoTest {
      val title = "Close-Reading Hamlet"
      val description = "Read the close-reading of Hamlet"
      val remoteId = "course-123"

      val createdAssignmentResult = assignmentRepo.create(mockInstructor, context, title, description, mockInstructor, remoteId)
      createdAssignmentResult.isRight mustEqual true

      assignmentRepo.create(mockInstructor, context, title, description, mockInstructor, remoteId) must beLeft(AssignmentAlreadyExists(remoteId))
    }

    "update an assignment with a file, new theme tree, and rubric" in new AssignmentRepoTest {
      val title = "Close-Reading Hamlet"
      val description = "Read the close-reading of Hamlet"
      val remoteId = "course-123"

      val tempFileContent =
        s"""
           |<sampleXml>
           |  <content>Hi!</content>
           |</sampleXml>
         """.stripMargin

      val tempFile = TemporaryFile("test", ".txt")
      val writer = new PrintWriter(tempFile.file)
      writer.write(tempFileContent)
      writer.close()

      val createdAssignmentResult = assignmentRepo.create(mockInstructor, context, title, description, mockInstructor, remoteId)
      createdAssignmentResult.isRight mustEqual true

      val createdAssignment = createdAssignmentResult.right.get

      val now = new Date()

      val document = new AssignmentDocument(
        createdAssignment.document.id,
        tempFile.file,
        tempFile.file.getName,
        "txt"
      )

      val updatedAssignment = Assignment(
        createdAssignment.id,
        createdAssignment.remoteId,
        document,
        createdAssignment.title,
        createdAssignment.description,
        new Date(), new Date(),
        createdAssignment.instructor,
        true,
        4,
        "numeric",
        List(
          RubricPrompt(-1, "Originality", 0, 5),
          RubricPrompt(-1, "Insight", 0, 5)
        ),
        List(
          Theme(-1, "Heroism", null, List(), mockInstructor, 0),
          Theme(-1, "Women's Roles", null, List(), mockInstructor, 1)
        )
      )

      assignmentRepo.updateAssignment(mockInstructor, updatedAssignment)

      val retrievedAssignment = assignmentRepo.getById(createdAssignment.id).get

      println(s"Retrieved open date: ${retrievedAssignment.openDate} and updated open date: ${updatedAssignment.openDate}")
      retrievedAssignment.id mustEqual createdAssignment.id
      retrievedAssignment.remoteId mustEqual updatedAssignment.remoteId
      retrievedAssignment.title mustEqual updatedAssignment.title
      retrievedAssignment.description mustEqual updatedAssignment.description
      retrievedAssignment.openDate.getTime must beBetween(updatedAssignment.openDate.getTime - 500, updatedAssignment.openDate.getTime + 500)
      retrievedAssignment.closeDate.getTime must beBetween(updatedAssignment.closeDate.getTime - 500, updatedAssignment.closeDate.getTime + 500)
      retrievedAssignment.instructor mustEqual updatedAssignment.instructor
      retrievedAssignment.peerReviewEnabled mustEqual updatedAssignment.peerReviewEnabled
      retrievedAssignment.numberRequired mustEqual updatedAssignment.numberRequired
      retrievedAssignment.rubricType mustEqual updatedAssignment.rubricType

      // Themes
      retrievedAssignment.themes(0).description mustEqual updatedAssignment.themes(0).description
      retrievedAssignment.themes(1).description mustEqual updatedAssignment.themes(1).description

      // Rubrics
      retrievedAssignment.reviewRubric(0).description mustEqual updatedAssignment.reviewRubric(0).description
      retrievedAssignment.reviewRubric(0).min mustEqual updatedAssignment.reviewRubric(0).min
      retrievedAssignment.reviewRubric(0).max mustEqual updatedAssignment.reviewRubric(0).max
      retrievedAssignment.reviewRubric(1).description mustEqual updatedAssignment.reviewRubric(1).description
      retrievedAssignment.reviewRubric(1).min mustEqual updatedAssignment.reviewRubric(1).min
      retrievedAssignment.reviewRubric(1).max mustEqual updatedAssignment.reviewRubric(1).max

    }

    "update themes (before students have created themes)" in new AssignmentRepoTest {
      val title = "Close-Reading Hamlet"
      val description = "Read the close-reading of Hamlet"
      val remoteId = "course-123"

      val tempFileContent =
        s"""
           |<sampleXml>
           |  <content>Hi!</content>
           |</sampleXml>
         """.stripMargin

      val tempFile = TemporaryFile("test", ".txt")
      val writer = new PrintWriter(tempFile.file)
      writer.write(tempFileContent)
      writer.close()

      val createdAssignmentResult = assignmentRepo.create(mockInstructor, context, title, description, mockInstructor, remoteId)
      createdAssignmentResult.isRight mustEqual true

      val createdAssignment = createdAssignmentResult.right.get

      val document = new AssignmentDocument(
        createdAssignment.document.id,
        tempFile.file,
        tempFile.file.getName,
        "txt"
      )
      val now = new Date()
      val updatedAssignment = Assignment(
        createdAssignment.id,
        createdAssignment.remoteId,
        document,
        createdAssignment.title,
        createdAssignment.description,
        new Date(), new Date(),
        createdAssignment.instructor,
        true,
        4,
        "numeric",
        List(
          RubricPrompt(-1, "Originality", 0, 5),
          RubricPrompt(-1, "Insight", 0, 5)
        ),
        List(
          Theme(-1, "Heroism", null, List(), mockInstructor, 0),
          Theme(-1, "Women's Roles", null, List(), mockInstructor, 1)
        )
      )

      assignmentRepo.updateAssignment(mockInstructor, updatedAssignment)

      val retrievedAssignment = assignmentRepo.getById(createdAssignment.id).get

      val withRubricsUpdated = Assignment(
        createdAssignment.id,
        createdAssignment.remoteId,
        document,
        createdAssignment.title,
        createdAssignment.description,
        now, now,
        createdAssignment.instructor,
        true,
        4,
        "numeric",
        List(
          RubricPrompt(-1, "Originality", 0, 5),
          RubricPrompt(-1, "Insight", 0, 5)
        ),
        List(
          Theme(-1, "Paganism vs. Christianity", null, List(), mockInstructor, 0),
          retrievedAssignment.themes(1),
          retrievedAssignment.themes(0),
          Theme(-1, "Honor", null, List(), mockInstructor, 3)
        )
      )
    }

    "update the rubric correctly" in new AssignmentRepoTest {
      val title = "Close-Reading Hamlet"
      val description = "Read the close-reading of Hamlet"
      val remoteId = "course-123"

      val tempFileContent =
        s"""
           |<sampleXml>
           |  <content>Hi!</content>
           |</sampleXml>
         """.stripMargin

      val tempFile = TemporaryFile("test", ".txt")
      val writer = new PrintWriter(tempFile.file)
      writer.write(tempFileContent)
      writer.close()

      val createdAssignmentResult = assignmentRepo.create(mockInstructor, context, title, description, mockInstructor, remoteId)
      createdAssignmentResult.isRight mustEqual true

      val createdAssignment = createdAssignmentResult.right.get

      val now = new Date()
      val updatedAssignment = Assignment(
        createdAssignment.id,
        createdAssignment.remoteId,
        document,
        createdAssignment.title,
        createdAssignment.description,
        new Date(), new Date(),
        createdAssignment.instructor,
        true,
        4,
        "numeric",
        List(
          RubricPrompt(-1, "Originality", 0, 5),
          RubricPrompt(-1, "Insight", 0, 5)
        ),
        List(
          Theme(-1, "Heroism", null, List(), mockInstructor, 0),
          Theme(-1, "Women's Roles", null, List(), mockInstructor, 1)
        )
      )

      assignmentRepo.updateAssignment(mockInstructor, updatedAssignment)

      val retrievedAssignment = assignmentRepo.getById(createdAssignment.id).get
      val document = new AssignmentDocument(
        createdAssignment.document.id,
        tempFile.file,
        tempFile.file.getName,
        "txt"
      )

      val withRubricsUpdated = Assignment(
        createdAssignment.id,
        createdAssignment.remoteId,
        document,
        createdAssignment.title,
        createdAssignment.description,
        now, now,
        createdAssignment.instructor,
        true,
        4,
        "numeric",
        List(
          retrievedAssignment.reviewRubric(1),
          RubricPrompt(-1, "Happiness", 0, 5),
          RubricPrompt(-1, "Integrity", 0, 5)
        ),
        List(
          Theme(-1, "Heroism", null, List(), mockInstructor, 0),
          Theme(-1, "Women's Roles", null, List(), mockInstructor, 1)
        )
      )

      assignmentRepo.updateAssignment(mockInstructor, withRubricsUpdated)

      val retrievedAssignment2 = assignmentRepo.getById(createdAssignment.id).get

      retrievedAssignment2.reviewRubric.length mustEqual 3

      retrievedAssignment2.reviewRubric(0).description mustEqual retrievedAssignment.reviewRubric(1).description
      retrievedAssignment2.reviewRubric(1).description mustEqual "Happiness"
      retrievedAssignment2.reviewRubric(2).description mustEqual "Integrity"
    }

  }

}




