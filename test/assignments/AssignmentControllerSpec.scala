package assignments

import java.io.PrintWriter
import java.text.SimpleDateFormat
import java.util.Date

import org.junit.runner.RunWith
import org.specs2.matcher.JsonMatchers
import org.specs2.runner._
import play.api.libs.Files.TemporaryFile
import play.api.libs.json.Json
import play.api.mvc.MultipartFormData.{BadPart, FilePart}
import play.api.mvc._
import play.api.test._

import scala.concurrent.Future


/**
  * Created by daveshepard on 7/11/16.
  */
@RunWith(classOf[JUnitRunner])
class AssignmentControllerSpec extends PlaySpecification with Results with JsonMatchers {

  "An AssignmentController" should {

    val dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z")

    "update an existing assignment for the first time after it has been created (as if through an LTI Launch request)" in new AssignmentControllerTest {
      val assignment = assignmentRepo.create(
        mockInstructor,
        context,
        "Close-reading Shakespeare",
        "Assignment to close-read Shakespeare",
        mockInstructor,
        "abc-123"
      ).right.get

      val tempFileContent = s"""
           |<sampleXml>
           |  <content>Hi!</content>
           |</sampleXml>
         """.stripMargin
      val tempFile = TemporaryFile("test", ".txt")
      val writer = new PrintWriter(tempFile.file)
      writer.write(tempFileContent)
      writer.close()

      val openDate = dateFormatter.format(new Date)
      val assignmentUpdateJson = Json.parse(
        s"""
           |{
           |  "id" : ${assignment.id},
           |  "remoteId" : "${assignment.remoteId}",
           |  "title" : "${assignment.title}",
           |  "description" : "${assignment.description}",
           |  "openDate" : "${openDate}",
           |  "closeDate" : "${openDate}",
           |  "peerReviewEnabled": true,
           |  "numberRequired" : 5,
           |  "rubricType" : "numeric",
           |  "reviewRubric" : [
           |    { "description" : "Accuracy", "min": 0, "max": 5},
           |    { "description" : "Courage", "min": 0, "max": 5}
           |  ],
           |  "themes" : [
           |    { "description" : "Heroism"},
           |    { "description" : "Women's roles" }
           |  ]
           |}
         """.stripMargin)

      val uploadData = MultipartFormData(
        Map("assignment" -> Seq(assignmentUpdateJson.toString())),
        Seq[FilePart[TemporaryFile]](FilePart("assignmentFile", "theFilename.txt", None, tempFile)),
        Seq[BadPart]()
      )
      val updateRequest = FakeRequest[MultipartFormData[TemporaryFile]](
        POST, s"/activities/${assignment.id}", FakeHeaders(), uploadData)
          .withSession("userId" -> mockInstructor.id.toString)

      val updateResponse:Future[Result] = controller.update(assignment.id).apply(updateRequest)

      status(updateResponse) mustEqual OK

      val retrievedAssignment = assignmentRepo.getById(assignment.id).get
      retrievedAssignment.id mustEqual assignment.id
      retrievedAssignment.remoteId mustEqual assignment.remoteId
      retrievedAssignment.title mustEqual assignment.title
      retrievedAssignment.description mustEqual assignment.description
      dateFormatter.format(retrievedAssignment.openDate) mustEqual openDate
      dateFormatter.format(retrievedAssignment.closeDate) mustEqual openDate
      retrievedAssignment.peerReviewEnabled mustEqual true
      retrievedAssignment.numberRequired mustEqual 5
      retrievedAssignment.rubricType mustEqual "numeric"

      retrievedAssignment.reviewRubric(0).description mustEqual "Accuracy"
      retrievedAssignment.reviewRubric(0).min mustEqual 0
      retrievedAssignment.reviewRubric(0).max mustEqual 5

      retrievedAssignment.reviewRubric(1).description mustEqual "Courage"
      retrievedAssignment.reviewRubric(1).min mustEqual 0
      retrievedAssignment.reviewRubric(1).max mustEqual 5

      retrievedAssignment.themes(0).description mustEqual "Heroism"
      retrievedAssignment.themes(1).description mustEqual "Women's roles"

    }

    "retrieve an assignment with the correct format" in new AssignmentControllerTest with TestCase1 {
      updatedAssignmentResult must beRight
      val updatedAssignment = updatedAssignmentResult.right.get

      val getRequest = FakeRequest(GET, s"/assignments/${initialAssignment.id}")
        .withSession("userId" -> mockInstructor.id.toString)
      val getResponse:Future[Result] = controller.getById(initialAssignment.id).apply(getRequest)

      status(getResponse) mustEqual OK
      val getJson = contentAsString(getResponse)

      getJson must /("ref" -> s"/assignments/${initialAssignment.id}")
        /("remoteId" -> initialAssignment.remoteId)
        /("title" -> initialAssignment.title) /("description" -> initialAssignment.description)
        /("openDate" -> updatedAssignment.openDate.toString) /("closeDate" -> updatedAssignment.closeDate.toString)
        /("instructor") / ("name" -> mockInstructor.providedName)
        /("instructor") / ("id" -> mockInstructor.id.toDouble)
        /("peerReviewEnabled" -> true)
        /("numberRequired" -> 0)
        /("rubricType" -> "numeric")
        /("reviewRubric") /#(0) /("description" -> "Insight")
        /("reviewRubric") /#(0) /("min" -> 0)
        /("reviewRubric") /#(0) /("max" -> 5)
        /("reviewRubric") /#(1) /("description" -> "Originality")
        /("reviewRubric") /#(1) /("min" -> 0)
        /("reviewRubric") /#(1) /("max" -> 5)

        /("themes") /#(0) /("description" -> "Heroism")
        /("themes") /#(1) /("description" -> "Women's roles")

    }

    "update an assignment by adding new rubrics, removing old ones, renaming some of them, and changing values" in new AssignmentControllerTest with TestCase1 {
      updatedAssignmentResult must beRight
      val updatedAssignment = updatedAssignmentResult.right.get

      val openDateText = dateFormatter.format(openDate)
      val closeDateText = dateFormatter.format(closeDate)

      val assignmentUpdateJson = Json.parse(
        s"""
           |{
           |  "id" : ${initialAssignment.id},
           |  "remoteId" : "${initialAssignment.remoteId}",
           |  "title" : "${initialAssignment.title}",
           |  "description" : "${initialAssignment.description}",
           |  "openDate" : "${openDateText}",
           |  "closeDate" : "${closeDateText}",
           |  "peerReviewEnabled": true,
           |  "numberRequired" : 5,
           |  "rubricType" : "numeric",
           |  "reviewRubric" : [
           |    {
           |      "id" : ${updatedAssignment.reviewRubric(1).id},
           |      "description" : "${updatedAssignment.reviewRubric(1).description}",
           |      "min" : 1,
           |      "max" : 6
           |    },
           |    { "description" : "Accuracy", "min": 0, "max": 5},
           |    { "description" : "Courage", "min": 0, "max": 5},
           |    {
           |      "id" : ${updatedAssignment.reviewRubric(0).id},
           |      "description" : "${updatedAssignment.reviewRubric(0).description}",
           |      "min" : 1,
           |      "max" : 6
           |    },
           |    { "description" : "Fairness", "min": 0, "max": 5}
           |  ],
           |  "themes" : [
           |    { "id" : ${updatedAssignment.themes(0).id}, "description" : "${updatedAssignment.themes(0).description}"},
           |    { "id" : ${updatedAssignment.themes(1).id}, "description" : "${updatedAssignment.themes(1).description}"}
           |  ]
           |}
         """.stripMargin
      )

      val uploadData = MultipartFormData(
        Map("assignment" -> Seq(assignmentUpdateJson.toString())),
        Seq[FilePart[TemporaryFile]](FilePart("assignmentFile", "theFilename.txt", None, tempFile)),
        Seq[BadPart]()
      )
      val updateRequest = FakeRequest[MultipartFormData[TemporaryFile]](
        POST, s"/activities/${initialAssignment.id}", FakeHeaders(), uploadData)
        .withSession("userId" -> mockInstructor.id.toString)

      val updateResponse:Future[Result] = controller.update(initialAssignment.id).apply(updateRequest)

      status(updateResponse) mustEqual OK
      val retrievedAssignmentOption = assignmentRepo.getById(initialAssignment.id)

      retrievedAssignmentOption must beSome

      val retrievedAssignment = retrievedAssignmentOption.get
      retrievedAssignment.title mustEqual initialAssignment.title
      retrievedAssignment.description mustEqual initialAssignment.description
      dateFormatter.format(retrievedAssignment.openDate) mustEqual openDateText
      dateFormatter.format(retrievedAssignment.closeDate) mustEqual closeDateText
      retrievedAssignment.peerReviewEnabled mustEqual true
      retrievedAssignment.numberRequired mustEqual 5
      retrievedAssignment.rubricType mustEqual "numeric"

      val reviewRubric = retrievedAssignment.reviewRubric
      reviewRubric must have size 5

      reviewRubric(0).id mustEqual updatedAssignment.reviewRubric(1).id
      reviewRubric(0).description mustEqual updatedAssignment.reviewRubric(1).description
      reviewRubric(0).min mustEqual 1
      reviewRubric(0).max mustEqual 6

      reviewRubric(1).description mustEqual "Accuracy"
      reviewRubric(1).min mustEqual 0
      reviewRubric(1).max mustEqual 5

      reviewRubric(2).description mustEqual "Courage"
      reviewRubric(2).min mustEqual 0
      reviewRubric(2).max mustEqual 5

      reviewRubric(3).id mustEqual updatedAssignment.reviewRubric(0).id
      reviewRubric(3).description mustEqual updatedAssignment.reviewRubric(0).description
      reviewRubric(3).min mustEqual 1
      reviewRubric(3).max mustEqual 6

      reviewRubric(4).description mustEqual "Fairness"
      reviewRubric(4).min mustEqual 0
      reviewRubric(4).max mustEqual 5

      val themes = retrievedAssignment.themes
      themes must have size 2

      themes(0).id mustEqual updatedAssignment.themes(0).id
      themes(0).description mustEqual updatedAssignment.themes(0).description
      themes(1).id mustEqual updatedAssignment.themes(1).id
      themes(1).description mustEqual updatedAssignment.themes(1).description

    }

    "update an assignment by adding new themes, removing old ones, and renaming some of them" in new AssignmentControllerTest with TestCase1 {
      updatedAssignmentResult must beRight
      val updatedAssignment = updatedAssignmentResult.right.get

      val openDateText = dateFormatter.format(openDate)
      val closeDateText = dateFormatter.format(closeDate)

      val assignmentUpdateJson = Json.parse(
        s"""
           |{
           |  "id" : ${initialAssignment.id},
           |  "remoteId" : "${initialAssignment.remoteId}",
           |  "title" : "${initialAssignment.title}",
           |  "description" : "${initialAssignment.description}",
           |  "openDate" : "${openDateText}",
           |  "closeDate" : "${closeDateText}",
           |  "peerReviewEnabled": true,
           |  "numberRequired" : 5,
           |  "rubricType" : "numeric",
           |  "reviewRubric" : [
           |    {
           |      "id" : ${updatedAssignment.reviewRubric(0).id},
           |      "description" : "${updatedAssignment.reviewRubric(0).description}",
           |      "min" : 0,
           |      "max" : 5
           |    },
           |    {
           |      "id" : ${updatedAssignment.reviewRubric(1).id},
           |      "description" : "${updatedAssignment.reviewRubric(1).description}",
           |      "min" : 0,
           |      "max" : 5
           |    }
           |  ],
           |  "themes" : [
           |    { "description" : "A new theme" },
           |    { "id" : ${updatedAssignment.themes(1).id}, "description" : "${updatedAssignment.themes(1).description}"},
           |    { "description" : "Something else"},
           |    { "id" : ${updatedAssignment.themes(0).id}, "description" : "Masculinity"},
           |    { "description" : "And more stuff"}
           |  ]
           |}
         """.stripMargin
      )

      val uploadData = MultipartFormData(
        Map("assignment" -> Seq(assignmentUpdateJson.toString())),
        Seq[FilePart[TemporaryFile]](FilePart("assignmentFile", "theFilename.txt", None, tempFile)),
        Seq[BadPart]()
      )
      val updateRequest = FakeRequest[MultipartFormData[TemporaryFile]](
        POST, s"/activities/${initialAssignment.id}", FakeHeaders(), uploadData)
        .withSession("userId" -> mockInstructor.id.toString)

      val updateResponse:Future[Result] = controller.update(initialAssignment.id).apply(updateRequest)


      status(updateResponse) aka "The server response code" mustEqual OK
      val retrievedAssignmentOption = assignmentRepo.getById(initialAssignment.id)

      retrievedAssignmentOption must beSome

      val retrievedAssignment = retrievedAssignmentOption.get
      retrievedAssignment.title mustEqual initialAssignment.title
      retrievedAssignment.description mustEqual initialAssignment.description
      dateFormatter.format(retrievedAssignment.openDate) mustEqual openDateText
      dateFormatter.format(retrievedAssignment.closeDate) mustEqual closeDateText
      retrievedAssignment.peerReviewEnabled mustEqual true
      retrievedAssignment.numberRequired mustEqual 5
      retrievedAssignment.rubricType mustEqual "numeric"

      val reviewRubric = retrievedAssignment.reviewRubric
      reviewRubric must have size 2

      reviewRubric(0).id mustEqual updatedAssignment.reviewRubric(0).id
      reviewRubric(0).description mustEqual updatedAssignment.reviewRubric(0).description
      reviewRubric(1).id mustEqual updatedAssignment.reviewRubric(1).id
      reviewRubric(1).description mustEqual updatedAssignment.reviewRubric(1).description


      val themes = retrievedAssignment.themes

      themes(0).description mustEqual "A new theme"

      themes(1).id mustEqual updatedAssignment.themes(1).id
      themes(1).description mustEqual updatedAssignment.themes(1).description

      themes(2).description mustEqual "Something else"

      themes(3).id mustEqual updatedAssignment.themes(0).id
      themes(3).description mustEqual "Masculinity"

      themes(4).description mustEqual "And more stuff"

    }

    "refuse to update when someone without the correct permissions tries to update a course" in new AssignmentControllerTest with TestCase1 {
      updatedAssignmentResult must beRight
      val updatedAssignment = updatedAssignmentResult.right.get

      val openDateText = dateFormatter.format(openDate)
      val closeDateText = dateFormatter.format(closeDate)

      val assignmentUpdateJson = Json.parse(
        s"""
           |{
           |  "id" : ${initialAssignment.id},
           |  "remoteId" : "${initialAssignment.remoteId}",
           |  "title" : "Ha ha! I updated the title!",
           |  "description" : "${initialAssignment.description}",
           |  "openDate" : "${openDateText}",
           |  "closeDate" : "${closeDateText}",
           |  "peerReviewEnabled": true,
           |  "numberRequired" : 5,
           |  "rubricType" : "numeric",
           |  "reviewRubric" : [
           |    {
           |      "id" : ${updatedAssignment.reviewRubric(0).id},
           |      "description" : "${updatedAssignment.reviewRubric(0).description}",
           |      "min" : 0,
           |      "max" : 5
           |    },
           |    {
           |      "id" : ${updatedAssignment.reviewRubric(1).id},
           |      "description" : "${updatedAssignment.reviewRubric(1).description}",
           |      "min" : 0,
           |      "max" : 5
           |    }
           |  ],
           |  "themes" : [
           |    { "description" : "A new theme" },
           |    { "id" : ${updatedAssignment.themes(1).id}, "description" : "${updatedAssignment.themes(1).description}"},
           |    { "description" : "Something else"},
           |    { "id" : ${updatedAssignment.themes(0).id}, "description" : "Masculinity"},
           |    { "description" : "And more stuff"}
           |  ]
           |}
         """.stripMargin
      )

      val uploadData = MultipartFormData(
        Map("assignment" -> Seq(assignmentUpdateJson.toString())),
        Seq[FilePart[TemporaryFile]](FilePart("assignmentFile", "theFilename.txt", None, tempFile)),
        Seq[BadPart]()
      )
      val updateRequest = FakeRequest[MultipartFormData[TemporaryFile]](
        POST, s"/activities/${initialAssignment.id}", FakeHeaders(), uploadData)
        .withSession("userId" -> mockStudent.id.toString)

      val updateResponse:Future[Result] = controller.update(initialAssignment.id).apply(updateRequest)


      status(updateResponse) aka "The server response code" mustEqual FORBIDDEN
      val retrievedAssignmentOption = assignmentRepo.getById(initialAssignment.id)

      retrievedAssignmentOption must beSome
      val retrievedAssignment = retrievedAssignmentOption.get
      retrievedAssignment.title mustEqual initialAssignment.title
    }

    "return the file when the correct URL is called" in new AssignmentControllerTest with TestCase1 {
      updatedAssignmentResult must beRight
      println(document.file)
      println(document.file.getName)
      println(assignmentRepo.fileURL(updateAssignment))

      val getRequest = FakeRequest(GET, s"/assignments/${initialAssignment.id}/file")
        .withSession("userId" -> mockInstructor.id.toString)
      val getResponse:Future[Result] = controller.getFile(initialAssignment.id).apply(getRequest)

      status(getResponse) mustEqual OK
      getResponse.value.get.get.body.contentLength.get mustEqual tempFileContent.length
      // NOTE: this is commented out because of some issues with the way the controller returns the file
//      val fileContent = contentAsString(getResponse)
//      fileContent mustEqual tempFileContent
    }

//    "report that a user can view an assignment when the user is a member of the context"
//    "report that a user cannot view an assignment when the user is not a member of the context"

  }

}



