package assignments

import annotations.{HasAnnotationRepo, HasExplicationRepo}
import authentication.{AssignmentService, AuthenticationService, HasToolConsumerRepo, HasUserRepo}
import common.HasDatabase
import edu.ucla.cdh.EvidenceLocker.authentication.UserRepository
import edu.ucla.cdh.EvidenceLocker.common.DatabaseService
import lti.{HasBasicContext, HasLMSContextRepo, LMSContextRepo}
import org.junit.runner.RunWith
import org.specs2.matcher.JsonMatchers
import org.specs2.runner._
import play.api.i18n.MessagesApi
import play.api.libs.json.Json
import play.api.mvc.{Controller, Result, Results}
import play.api.test.{FakeHeaders, FakeRequest, PlaySpecification, WithApplication}
import play.mvc.BodyParser.MultipartFormData
import reviews.HasPeerReviewRepo

import scala.concurrent.Future


/**
  * Created by daveshepard on 7/16/16.
  */
@RunWith(classOf[JUnitRunner])
class ThemeControllerSpec extends PlaySpecification with Results with JsonMatchers {

  "A ThemeController" should {

    "create a theme with a theme as a parent" in new ThemeControllerTest with TestCase1 with UpdateTestCase {
      val testCaseThemeDescription = "Unheroic Qualities"
      val postData = Map(
        "description" -> Seq(testCaseThemeDescription),
        "parentId" -> Seq(createdTheme.id.toString),
        "order" -> Seq("1")
      )
      val createRequest = FakeRequest(POST, s"/activities/${updatedAssignment.id}",
        FakeHeaders(), postData).withSession("userId" -> mockStudent.id.toString)

      val createResponse:Future[Result] = controller.create(updatedAssignment.id).apply(createRequest)

      status(createResponse) mustEqual CREATED

      val createResponseJson = contentAsString(createResponse)
      createResponseJson must /("description" -> testCaseThemeDescription)

      val themeId = (Json.parse(createResponseJson) \ "id").as[Long]

      val getRequest = FakeRequest(GET, s"/themes/$themeId").withSession("userId" -> mockStudent.id.toString)

      val getResponse:Future[Result] = controller.getById(themeId).apply(getRequest)

      status(getResponse) mustEqual OK

      val getResponseJson = contentAsString(getResponse)

      getResponseJson must
        /("id" -> themeId.toDouble)
        /("assignmentRef" -> s"/assignment/${updatedAssignment.id}")
        /("description" -> testCaseThemeDescription)

      val getAssignmentRequest = FakeRequest(GET, s"/activities/${updatedAssignment.id}")
        .withSession("userId" -> mockStudent.id.toString)
      val getThemeTreeResponse:Future[Result] = controller.getThemeTreeForAssignment(updatedAssignment.id)
          .apply(getAssignmentRequest)
      status(getThemeTreeResponse) mustEqual OK
      val getAssignmentJson = contentAsString(getThemeTreeResponse)

      getAssignmentJson must
        */("description" -> oneOf(createdTheme.description, testCaseThemeDescription))
    }

    "allow an instructor to see a student's activities in that context" in new ThemeControllerTest with TestCase1 with UpdateTestCase {
      val testCaseThemeDescription = "Unheroic Qualities"
      val postData = Map(
        "description" -> Seq(testCaseThemeDescription),
        "parentId" -> Seq(createdTheme.id.toString),
        "order" -> Seq("1")
      )
      val createRequest = FakeRequest(POST, s"/activities/${updatedAssignment.id}",
        FakeHeaders(), postData).withSession("userId" -> mockStudent.id.toString)

      val createResponse:Future[Result] = controller.create(updatedAssignment.id).apply(createRequest)

      status(createResponse) mustEqual CREATED

      val createResponseJson = contentAsString(createResponse)
      createResponseJson must /("description" -> testCaseThemeDescription)

      val themeId = (Json.parse(createResponseJson) \ "id").as[Long]

      val getRequest = FakeRequest(GET, s"/themes/$themeId")
        .withSession("userId" -> mockInstructor.id.toString)

      val getResponse:Future[Result] = controller.getById(themeId).apply(getRequest)
      status(getResponse) mustEqual OK

      val getResponseJson = contentAsString(getResponse)
      getResponseJson must
        /("id" -> themeId.toDouble)
        /("assignmentRef" -> s"/assignment/${updatedAssignment.id}")
        /("description" -> testCaseThemeDescription)

      val getAssignmentRequest = FakeRequest(GET, s"/activities/${updatedAssignment.id}")
        .withSession("userId" -> mockInstructor.id.toString)
      val getThemeTreeResponse:Future[Result] = controller.getThemeTreeForAssignment(updatedAssignment.id)
        .apply(getAssignmentRequest)
      status(getThemeTreeResponse) mustEqual OK
    }

    "create a theme with an assignment as a parent" in new ThemeControllerTest with TestCase1 with UpdateTestCase {
      val testCaseThemeDescription = "Unheroic Qualities"
      val postData = Map(
        "description" -> Seq(testCaseThemeDescription),
        "order" -> Seq("1")
      )
      val createRequest = FakeRequest(POST, s"/activities/${updatedAssignment.id}", FakeHeaders(), postData)
        .withSession("userId" -> mockStudent.id.toString)

      val createResponse:Future[Result] = controller.create(updatedAssignment.id).apply(createRequest)

      status(createResponse) mustEqual CREATED

      val createResponseJson = contentAsString(createResponse)
      createResponseJson must
        /("description" -> testCaseThemeDescription)

      val themeId = (Json.parse(createResponseJson) \ "id").as[Long]
      val getRequest = FakeRequest(GET, s"/themes/$themeId")
        .withSession("userId" -> mockStudent.id.toString)
      val getResponse:Future[Result] = controller.getById(themeId).apply(getRequest)

      status(getResponse) mustEqual OK


      val getAssignmentRequest = FakeRequest(GET, s"/activities/${updatedAssignment.id}")
        .withSession("userId" -> mockStudent.id.toString)
      val getThemeTreeResponse:Future[Result] = controller.getThemeTreeForAssignment(updatedAssignment.id)
        .apply(getAssignmentRequest)
      status(getThemeTreeResponse) mustEqual OK
      val getAssignmentJson = contentAsString(getThemeTreeResponse)

      getAssignmentJson must
//        /("themes")/("id" -> oneOf(createdTheme.id.toDouble, themeId.toDouble))
        */("description" -> oneOf(createdTheme.description, testCaseThemeDescription))

    }

    "fail to create a theme when the user is not logged in" in new ThemeControllerTest with TestCase1 with UpdateTestCase {
      val testCaseThemeDescription = "Unheroic Qualities"
      val postData = Map(
        "description" -> Seq(testCaseThemeDescription),
        "order" -> Seq("0")
      )
      val createRequest = FakeRequest(POST, s"/activities/${updatedAssignment.id}", FakeHeaders(), postData)

      val createResponse:Future[Result] = controller.create(updatedAssignment.id).apply(createRequest)

      status(createResponse) mustEqual FORBIDDEN

      val getRequest = FakeRequest(GET, s"/themes/1")

      val getResponse:Future[Result] = controller.getById(1).apply(getRequest)

      status(getResponse) mustEqual FORBIDDEN
    }

    "fail to create a theme when the user is not part of the context" in new ThemeControllerTest with TestCase1 with UpdateTestCase with TestCase2 {
      val testCaseThemeDescription = "Unheroic Qualities"
      val postData = Map(
        "description" -> Seq(testCaseThemeDescription),
        "order" -> Seq("0")
      )

      val alternateInstructor = userRepo.create("babylon-5", "Adama", "adama@babylon5").get
      val createRequest = FakeRequest(POST, s"/assignments/${updatedAssignment.id}/themes", FakeHeaders(), postData)
          .withSession("userId" -> alternateInstructor.id.toString)

      val createResponse:Future[Result] = controller.create(updatedAssignment.id).apply(createRequest)

      status(createResponse) mustEqual FORBIDDEN
    }

    "return a 'Forbidden' status when the user is not part of the context" in new ThemeControllerTest with TestCase1 with UpdateTestCase with TestCase2 {
      val testCaseThemeDescription = "Unheroic Qualities"
      val postData = Map(
        "description" -> Seq(testCaseThemeDescription),
        "order" -> Seq("0")
      )

      val alternateInstructor = userRepo.create("babylon-5", "Adama", "adama@babylon5").get

      val heroism = updatedAssignment.themes.head
      val getRequest = FakeRequest(GET, s"/themes/${heroism.id}")
        .withSession("userId" -> alternateInstructor.id.toString)

      val getResponse:Future[Result] = controller.getById(heroism.id).apply(getRequest)

      status(getResponse) mustEqual FORBIDDEN
    }

    "update a theme title and parent" in new ThemeControllerTest with TestCase1 with UpdateTestCase {
      val heroism = updatedAssignment.themes.head
      val womensRoles = updatedAssignment.themes.last
      val initialTheme = themeRepo.create(mockStudent, updatedAssignment, "Heroic Quaalitieis", List(), heroism, mockStudent, 0)
        .right.get

      val correctedTitle = "Heroic Qualities"
      val updateJson = Json.parse(
        s"""
          |{
          |  "id" : ${initialTheme.id},
          |  "parentId" : ${womensRoles.id},
          |  "ownerId" : ${mockStudent.id},
          |  "description" : "$correctedTitle",
          |  "order" : 0
          |}
        """.stripMargin)

      val updateRequest = FakeRequest(PUT, s"/themes/${initialTheme.id}")
        .withBody(updateJson)
        .withSession("userId" -> mockStudent.id.toString)

      val updateResponse:Future[Result] = controller.update(initialTheme.id).apply(updateRequest)

      status(updateResponse) mustEqual OK

      val getRequest = FakeRequest(GET, s"/themes/${initialTheme.id}")
        .withSession("userId" -> mockStudent.id.toString)

      val getResponse:Future[Result] = controller.getById(initialTheme.id).apply(getRequest)

      val getResponseJson = contentAsString(getResponse)

      getResponseJson must /("id" -> initialTheme.id.toDouble)
//      getResponseJson must  /("parentId" -> womensRoles.id.toDouble)
      getResponseJson must  /("description" -> correctedTitle)

      val getHeroismRequest = FakeRequest(GET, s"/themes/${heroism.id}")
        .withSession("userId" -> mockStudent.id.toString)
      val getHeroismResponse = controller.getById(heroism.id).apply(getHeroismRequest)
      val getHeroismJson = contentAsString(getHeroismResponse)

      getHeroismJson must /("children").andHave(size(1))

      val getWomensRolesRequest = FakeRequest(GET, s"/themes/${womensRoles.id}")
        .withSession("userId" -> mockStudent.id.toString)
      val getWomensRolesResponse = controller.getById(womensRoles.id).apply(getWomensRolesRequest)
      val getWomensRolesJson = contentAsString(getWomensRolesResponse)

      getWomensRolesJson must /("children").andHave(size(1))
      getWomensRolesJson must /("children")/#(0)/("id" -> initialTheme.id.toDouble)
      getWomensRolesJson must /("children")/#(0)/("description" -> correctedTitle)
    }

    "fail to update a parent when a user is not logged in" in new ThemeControllerTest with TestCase1 with UpdateTestCase {
      val heroism = updatedAssignment.themes.head
      val womensRoles = updatedAssignment.themes.last
      val initialTheme = themeRepo.create(mockStudent, updatedAssignment, "Heroic Quaalitieis", List(), heroism, mockStudent, 0)
        .right.get

      val correctedTitle = "Heroic Qualities"
      val updateJson = Json.parse(
        s"""
           |{
           |  "id" : ${initialTheme.id},
           |  "parentId" : ${womensRoles.id},
           |  "ownerId" : ${mockStudent.id},
           |  "description" : "$correctedTitle"
           |}
        """.stripMargin)

      val updateRequest = FakeRequest(PUT, s"/themes/${initialTheme.id}")
        .withBody(updateJson)

      val updateResponse:Future[Result] = controller.update(initialTheme.id).apply(updateRequest)

      status(updateResponse) mustEqual FORBIDDEN

      val getRequest = FakeRequest(GET, s"/themes/${initialTheme.id}")
        .withSession("userId" -> mockStudent.id.toString)

      val getResponse:Future[Result] = controller.getById(initialTheme.id).apply(getRequest)

      val getResponseJson = contentAsString(getResponse)

      getResponseJson must /("id" -> initialTheme.id.toDouble)
      getResponseJson must /("description" -> "Heroic Quaalitieis")

      val getHeroismRequest = FakeRequest(GET, s"/themes/${heroism.id}")
        .withSession("userId" -> mockStudent.id.toString)
      val getHeroismResponse = controller.getById(heroism.id).apply(getHeroismRequest)
      val getHeroismJson = contentAsString(getHeroismResponse)

      getHeroismJson must /("children").andHave(size(2))

      val getWomensRolesRequest = FakeRequest(GET, s"/themes/${womensRoles.id}")
        .withSession("userId" -> mockStudent.id.toString)
      val getWomensRolesResponse = controller.getById(womensRoles.id).apply(getWomensRolesRequest)
      val getWomensRolesJson = contentAsString(getWomensRolesResponse)

      getWomensRolesJson must /("children").andHave(size(0))
    }

    "fail to update a theme when the user is not the original owner" in new ThemeControllerTest with TestCase1 with UpdateTestCase {
      val heroism = updatedAssignment.themes.head
      val womensRoles = updatedAssignment.themes.last
      val initialTheme = themeRepo.create(mockStudent, updatedAssignment, "Heroic Quaalitieis", List(), heroism, mockStudent, 0)
        .right.get

      val correctedTitle = "Heroic Qualities"
      val updateJson = Json.parse(
        s"""
           |{
           |  "id" : ${initialTheme.id},
           |  "parentId" : ${womensRoles.id},
           |  "ownerId" : ${mockStudent.id},
           |  "description" : "$correctedTitle"
           |}
        """.stripMargin)

      val updateRequest = FakeRequest(PUT, s"/themes/${initialTheme.id}")
        .withBody(updateJson)
        .withSession("userId" -> mockInstructor.id.toString)

      val updateResponse:Future[Result] = controller.update(initialTheme.id).apply(updateRequest)

      status(updateResponse) mustEqual FORBIDDEN

      val getRequest = FakeRequest(GET, s"/themes/${initialTheme.id}")
        .withSession("userId" -> mockStudent.id.toString)

      val getResponse:Future[Result] = controller.getById(initialTheme.id).apply(getRequest)

      val getResponseJson = contentAsString(getResponse)

      getResponseJson must
        /("id" -> initialTheme.id.toDouble)
      /("parentId" -> heroism.id.toDouble)
      /("description" -> "Heroic Quaalitieis")

      val getHeroismRequest = FakeRequest(GET, s"/themes/${heroism.id}")
        .withSession("userId" -> mockStudent.id.toString)
      val getHeroismResponse = controller.getById(heroism.id).apply(getHeroismRequest)
      val getHeroismJson = contentAsString(getHeroismResponse)

      getHeroismJson must
        /("children").andHave(size(2))

      val getWomensRolesRequest = FakeRequest(GET, s"/themes/${womensRoles.id}")
        .withSession("userId" -> mockStudent.id.toString)
      val getWomensRolesResponse = controller.getById(womensRoles.id).apply(getWomensRolesRequest)
      val getWomensRolesJson = contentAsString(getWomensRolesResponse)

      getWomensRolesJson must
        /("children").andHave(size(0))
    }

    "delete a theme from a parent and confirm that the parents are still there" in new ThemeControllerTest with TestCase1 with UpdateTestCase {
      val heroism = updatedAssignment.themes.head
      val initialTheme = themeRepo.create(mockStudent, updatedAssignment, "Heroic Quaalitieis", List(), heroism, mockStudent, 0)
        .right.get

      val deleteRequest = FakeRequest(DELETE, s"/themes/${initialTheme.id}")
          .withSession("userId" -> mockStudent.id.toString)
      val deleteResponse = controller.delete(initialTheme.id).apply(deleteRequest)

      status(deleteResponse) mustEqual OK

      val getRequest = FakeRequest(GET, s"/themes/${initialTheme.id}")
        .withSession("userId" -> mockStudent.id.toString)
      val getResponse = controller.getById(initialTheme.id).apply(getRequest)

      status(getResponse) mustEqual NOT_FOUND


      val getParentRequest = FakeRequest(GET, s"/themes/${heroism.id}")
        .withSession("userId" -> mockStudent.id.toString)
      val getParentResponse = controller.getById(heroism.id).apply(getParentRequest)

      status(getParentResponse) mustEqual OK
    }

    "fail to delete a theme when a user is not logged in" in new ThemeControllerTest with TestCase1 with UpdateTestCase {
      val heroism = updatedAssignment.themes.head
      val initialTheme = themeRepo.create(mockStudent, updatedAssignment, "Heroic Quaalitieis", List(), heroism, mockStudent, 0)
        .right.get

      val deleteRequest = FakeRequest(DELETE, s"/themes/${initialTheme.id}")
      val deleteResponse = controller.delete(initialTheme.id).apply(deleteRequest)

      status(deleteResponse) mustEqual FORBIDDEN

      val getRequest = FakeRequest(GET, s"/themes/${initialTheme.id}")
        .withSession("userId" -> mockStudent.id.toString)
      val getResponse = controller.getById(initialTheme.id).apply(getRequest)

      status(getResponse) mustEqual OK
    }

    "fail to delete a theme when the user is not allowed to update the theme" in new ThemeControllerTest with TestCase1 with UpdateTestCase {
      val heroism = updatedAssignment.themes.head

      val initialTheme = themeRepo.create(mockStudent, updatedAssignment, "Heroic Quaalitieis for Molly O'Brien to attempt correction", List(), heroism, mockStudent, 0)
        .right.get

      val alternateStudent = userRepo.create(
        "sf-example-4", "Molly O'Brien", "mobrien@starfleet.edu"
      ).get
      lmsContextRepo.setRolesInContextForUser(alternateStudent, context, Set(userRepo.roles.student))

      val deleteRequest = FakeRequest(DELETE, s"/themes/${initialTheme.id}")
        .withSession("userId" -> alternateStudent.id.toString)
      val deleteResponse = controller.delete(initialTheme.id).apply(deleteRequest)
      status(deleteResponse) mustEqual FORBIDDEN

      val getRequest = FakeRequest(GET, s"/themes/${initialTheme.id}")
        .withSession("userId" -> mockStudent.id.toString)
      val getResponse = controller.getById(initialTheme.id).apply(getRequest)

      status(getResponse) mustEqual OK
    }

//    "fail to delete a theme when a user is not a part of a context"
//    "delete a theme and remove all of its children"

  }
}

trait HasThemeRepo {
  val dbService:DatabaseService
  val userRepo:UserRepository
  val lmsContextRepo:LMSContextRepo
  val assignmentRepo:AssignmentRepository

  val themeRepo = new ThemeRepository(dbService, userRepo, lmsContextRepo, assignmentRepo)
}

class ThemeControllerTest extends WithApplication
  with HasDatabase with HasToolConsumerRepo with HasUserRepo with HasAssignmentRepo
  with HasLMSContextRepo with HasBasicContext
  with HasAnnotationRepo
  with HasThemeRepo with HasExplicationRepo with HasPeerReviewRepo {

  val ar = assignmentRepo
  val ur = userRepo
  val tr = themeRepo
  val er = explicationRepo
  val prr = peerReviewRepo
  val lcr = lmsContextRepo

  val controller = new ThemeController with Controller {
    override val userRepo = ur
    override val assignmentRepo = ar
    override val themeRepo = tr

    override val messagesApi = app.injector.instanceOf[MessagesApi]
    override val authenticationService = new AuthenticationService(userRepo)
    override val assignmentService = new AssignmentService(lmsContextRepo, userRepo, assignmentRepo)
  }

  val assignmentController = new AssignmentController with Controller {
    override val userRepo: UserRepository = ur
    override val assignmentRepo: AssignmentRepository = ar
    override val explicationRepo = er
    override val peerReviewRepo = prr
    override val lmsContextRepo = lcr
    override val authenticationService = new AuthenticationService(userRepo)
    override val assignmentService = new AssignmentService(lmsContextRepo, userRepo, assignmentRepo)
  }

}
